#pragma once

#include "SharedType.h"
#include <conio.h>
#include <stdio.h>
#include <dos.h>
#include <ctype.h>
#include <windows.h>

#ifndef TLQ_HANDLER_CLASS_H
#define TLQ_HANDLER_CLASS_H

class CClientSession;
class CGameServer;
class CLkPacket;

class TLQHandler
{
	///-------------Constructor & Destructor-------------///
public:
	TLQHandler();
	~TLQHandler();
	///--------------------------------------------------///
	
private:
	//Members
	LK_TS_T_ID questID;
	LK_TS_T_ID GetTlqID();
	PlayersMain* myPlayer;
public:
	//Functions	
	void SetTLQAccessForPlayer(LK_TS_T_ID QuestID, PlayersMain* cPlayersMain);
	void LoadMobForTLQ(LK_TS_T_ID QuestID, RwUInt32 avatarHandle);
	void SendDirectPlayAckForTLQ();
};

#endif