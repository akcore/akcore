//////////////////////////////////////////////////////////////////////
//
// Mob Manager Functions
//
// This will handle all things related to the MobList map
// and a thread for the mobs as well
// NPCs are included in this section as well.
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"
#include "GameServer.h"
#include <map>

CMobManager* g_pMobManager = NULL;
sVECTOR3 range{ 50.0f, 50.0f, 50.0f };
int MOBS_SPAWNED = 0;
int MAX_MOBS_SPAWNED = 30;
CMobManager::CMobManager()
{
	m_bRun = true;
}

CMobManager::~CMobManager()
{
	Release();
}

void CMobManager::Init()
{
	CreateNpcList();
	CreateMonsterList();
	CreateMobThread();
}

void CMobManager::Release()
{

}


void CMobManager::Run()
{
	while (m_bRun)
	{
		//cout << "Work Starts in Mob Thread" << endl;
		DoWork();
		//cout << "Work Done in Mob Thread" << endl;
		Wait(5000);
		//cout << "Cleaning Up Mobthread" << endl;
	}
}

void CMobManager::CreateMobThread()
{
	CMonster::MonsterData* creaturelist;
	const char* uniqueThread;
	//for (IterType it = m_map_Monster.begin(); it != m_map_Monster.end(); it++)
	//{
		//creaturelist = it->second;
		
		pThread = CLkThreadFactory::CreateThread(this, "CMMThread");
		pThread->Start();
		pThread2 = CLkThreadFactory::CreateThread(this, "CMMThread2");
		pThread2->Start();
		pThread3 = CLkThreadFactory::CreateThread(this, "CMMThread3");
		pThread3->Start();
		pThread4 = CLkThreadFactory::CreateThread(this, "CMMThread4");
		pThread4->Start();
		pThread5 = CLkThreadFactory::CreateThread(this, "CMMThread5");
		pThread5->Start();

	//}
	

}


RwUInt32		CMobManager::CreateUniqueId(void)
{
	if (m_uiMobId++)
	{
		if (m_uiMobId == 0xffffffff)
			m_uiMobId = 0;
	}

	return m_uiMobId;
}


void			CMobManager::DoWork()
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
			PlayersMain* plr = new PlayersMain(NULL, NULL, NULL);
		bool haveAttack = false;

		for (IterType it = m_map_spawned_Monster.begin(); it != m_map_spawned_Monster.end(); it++) //iterate through mob map
		{
			CMonster::MonsterData* mob = it->second;
			if (mob->isSpawned == true)
			{
				if (mob->FightMode == false)  //if not fighting then you better be moving
					if (timeGetTime() - mob->last_mobMove >= MONSTER_MOVE_UPDATE_TICK)
					{
					mob->last_mobMove = timeGetTime();
					mob->MoveToRand();
					}
				//for (CPlayerManager::itterType it = g_pPlayerManager->m_map_Player.begin(); it != g_pPlayerManager->m_map_Player.end(); it++) //We need to do something about this eventually. Its bad.
				//{

				//	plr = g_pPlayerManager->GetPlayer(it->second->GetAvatarHandle());//now iterating though player map to check distances on each mob that is spawned.
				//	
				//	sVECTOR3 myCurPos;
				//	myCurPos.x = mob->curPos.x;
				//	myCurPos.y = mob->curPos.y;
				//	myCurPos.z = mob->curPos.z;
				//	sVECTOR3 SpawnPos;
				//	SpawnPos.x = mob->Spawn_Loc.x;
				//	SpawnPos.y = mob->Spawn_Loc.y;
				//	SpawnPos.z = mob->Spawn_Loc.z;
				//	float distance = Distance(myCurPos, plr->GetPlayerPosition());
				//	float distanceToSpawn = Distance(myCurPos, SpawnPos);

				//	if (mob->IsDead == false && plr->GetPlayerDead() == false)
				//	{
				//		if (distance < mob->Basic_aggro_point && distance > mob->Attack_range && mob->isAggro == false) //If your in aggro range of the mob then you are now targeted
				//		{																								//but you are out of attack range so move closer to attack
				//			mob->isAggro = true;
				//			mob->target = plr->GetAvatarHandle();
				//			mob->MoveToPlayer(plr);
				//			haveAttack = false;

				//		}
				//		else if (mob->isAggro == true && plr->GetAvatarHandle() == mob->target && distance > mob->Attack_range && distance < mob->Basic_aggro_point)
				//		{
				//			mob->MoveToPlayer(plr);			//your aggro but out of attack range. Move to your player
				//		}
				//		else if (distance <= mob->Attack_range && mob->isAggro == true && plr->GetAvatarHandle() == mob->target )
				//		{
				//			if (mob->chainAttackCount >= mob->MaxchainAttackCount)
				//				mob->chainAttackCount = 1;
				//			haveAttack = true;			
				//		//	mob->Attack(plr, app);			//You are aggro and now in attack range. Attack mob.
				//		}
				//		else if (distanceToSpawn > mob->Basic_aggro_point + 10 && mob->isAggro == true && plr->GetAvatarHandle() == mob->target) //You are no longer in aggro or attack range. Go home mob your drunk
				//		{
				//			haveAttack = false;
				//	mob->ResetMob();
				//mob->MoveToSpawn();
				//			cout << "Out of aggro range. Stop following" << endl;
				//		}
				//}
				//	}
			}
		}
		//Sleep(1000);
	}

void CMobManager::CreateNpcList()
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	CSpawnTable* pNPCSpawnTbl = app->g_pTableContainer->GetNpcSpawnTable(1);
	for (CTable::TABLEIT itNPCSpawn = pNPCSpawnTbl->Begin(); itNPCSpawn != pNPCSpawnTbl->End(); ++itNPCSpawn)
	{
		sSPAWN_TBLDAT* pNPCSpwnTblData = (sSPAWN_TBLDAT*)itNPCSpawn->second;
		sNPC_TBLDAT* pNPCTblData = (sNPC_TBLDAT*)app->g_pTableContainer->GetNpcTable()->FindData(pNPCSpwnTblData->mob_Tblidx);

		if (pNPCTblData)
		{
			CMonster::MonsterData * cr = new CMonster::MonsterData;

			cr->Level = pNPCTblData->byLevel;
			cr->CurEP = pNPCTblData->wBasic_EP;
			cr->CurLP = pNPCTblData->wBasic_LP;
			cr->FightMode = false;
			cr->IsDead = false;
			cr->isSpawned = false;
			cr->isAggro = false;
			cr->MonsterID = pNPCTblData->tblidx;
			cr->MonsterSpawnID = pNPCSpwnTblData->tblidx;
			cr->Spawn_Loc = pNPCSpwnTblData->vSpawn_Loc;
			cr->Spawn_Dir = pNPCSpwnTblData->vSpawn_Dir;
			cr->MaxEP = pNPCTblData->wBasic_EP;
			cr->MaxLP = pNPCTblData->wBasic_LP;
			cr->curPos = cr->Spawn_Loc;
			cr->MapID = 1;
			cr->UniqueID = CreateUniqueId();
			m_map_Npc.insert(std::make_pair(cr->UniqueID, cr));
		}
	}
}

void CMobManager::CreateMonsterList()
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	CWorldTable* pWorldTable = app->g_pTableContainer->GetWorldTable();

	CSpawnTable* pMOBSpawnTbl = app->g_pTableContainer->GetMobSpawnTable(1);

	for (CTable::TABLEIT itMOBSpawn = pMOBSpawnTbl->Begin(); itMOBSpawn != pMOBSpawnTbl->End(); ++itMOBSpawn)
	{
		sSPAWN_TBLDAT* pMOBSpwnTblData = (sSPAWN_TBLDAT*)itMOBSpawn->second;
		sMOB_TBLDAT* pMOBTblData = (sMOB_TBLDAT*)app->g_pTableContainer->GetMobTable()->FindData(pMOBSpwnTblData->mob_Tblidx);

		if (pMOBTblData)
		{
			CMonster::MonsterData * cr = new CMonster::MonsterData;

			cr->Level = pMOBTblData->byLevel;
			cr->CurEP = pMOBTblData->wBasic_EP;
			cr->CurLP = pMOBTblData->wBasic_LP;
			cr->FightMode = false;
			cr->IsDead = false;
			cr->isSpawned = false;
			cr->isAggro = false;
			cr->MonsterID = pMOBTblData->tblidx;
			cr->MonsterSpawnID = pMOBSpwnTblData->tblidx;
			cr->Spawn_Loc = pMOBSpwnTblData->vSpawn_Loc;
			cr->Spawn_Dir = pMOBSpwnTblData->vSpawn_Dir;
			cr->MaxEP = pMOBTblData->wBasic_EP;
			cr->MaxLP = pMOBTblData->wBasic_LP;
			cr->Spawn_Quantity = pMOBSpwnTblData->bySpawn_Quantity;
			cr->Move_DelayTime = pMOBSpwnTblData->byMove_DelayTime;
			cr->Run_Speed = pMOBTblData->fRun_Speed;
			cr->Run_Speed_origin = pMOBTblData->fRun_Speed_Origin;
			cr->Walk_Speed = pMOBTblData->fWalk_Speed;
			cr->Walk_Speed_origin = pMOBTblData->fWalk_Speed_Origin;
			cr->Spawn_Cool_Time = pMOBSpwnTblData->wSpawn_Cool_Time * 1000;
			cr->target = 0;
			cr->curPos = cr->Spawn_Loc;
			cr->Basic_aggro_point = pMOBTblData->byScan_Range;
			cr->Attack_range = pMOBTblData->fAttack_Range;
			cr->MaxchainAttackCount = pMOBTblData->byAttack_Animation_Quantity;
			cr->chainAttackCount = 0;
			cr->MapID = 1;
			cr->last_mobMove = timeGetTime();
			cr->last_mobAttack = timeGetTime();
			cr->UniqueID = CreateUniqueId();
			m_map_Monster.insert(std::make_pair(cr->UniqueID, cr));
		}
	}
}

bool CMobManager::RunSpawnCheck(CLkPacket * pPacket, sVECTOR3 curPos, CClientSession * pSession)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
#pragma region monstercheck
	CMonster::MonsterData* creaturelist;
	//if (MOBS_SPAWNED > MAX_MOBS_SPAWNED && pSession->GetSocket());
	//{
		for (IterType it = m_map_Monster.begin(); it != m_map_Monster.end(); it++)
		{
			creaturelist = it->second;
			std::vector<RwUInt32>::iterator handleSearch = std::find(creaturelist->spawnedForHandle.begin(), creaturelist->spawnedForHandle.end(), pSession->GetavatarHandle());

			if (CreatureRangeCheck(curPos, creaturelist->Spawn_Loc) == true)
			{
				if (creaturelist->IsDead == false && creaturelist->isSpawned == false)
				{
					if (handleSearch != creaturelist->spawnedForHandle.end())
					{
						//Your handle was found so dont spawn it.
					}
					else
					{

						CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
						sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();
						creaturelist->isSpawned = true;
						res->wOpCode = GU_OBJECT_CREATE;
						res->sObjectInfo.objType = OBJTYPE_MOB;
						res->handle = creaturelist->UniqueID;
						res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = creaturelist->curPos.x;
						res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = creaturelist->curPos.y;
						res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = creaturelist->curPos.z;
						res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = creaturelist->Spawn_Dir.x;
						res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = creaturelist->Spawn_Dir.y;
						res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = creaturelist->Spawn_Dir.z;
						res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
						res->sObjectInfo.mobState.sCharStateBase.bFightMode = creaturelist->FightMode;
						res->sObjectInfo.mobBrief.tblidx = creaturelist->MonsterID;
						res->sObjectInfo.mobBrief.wCurEP = creaturelist->CurEP;
						res->sObjectInfo.mobBrief.wMaxEP = creaturelist->MaxEP;
						res->sObjectInfo.mobBrief.wCurLP = creaturelist->CurLP;
						res->sObjectInfo.mobBrief.wMaxLP = creaturelist->MaxLP;
						res->sObjectInfo.mobBrief.fLastRunningSpeed = creaturelist->Run_Speed;
						res->sObjectInfo.mobBrief.fLastWalkingSpeed = creaturelist->Walk_Speed;
						creaturelist->spawnedForHandle.push_back(pSession->GetavatarHandle());
						IterType found = m_map_spawned_Monster.find(creaturelist->UniqueID); //Search for the mob before trying to insert duplicate data.
						if (found == m_map_spawned_Monster.end())
							m_map_spawned_Monster.insert(std::make_pair(creaturelist->UniqueID, creaturelist));//This will get rid of tons of overhead on the server. big time. 


						//MOBS_SPAWNED++;
						packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
						g_pApp->Send(pSession->GetHandle(), &packet);
					}
				}
			}

			else if ((creaturelist->isSpawned == true && CreatureRangeCheck(curPos, creaturelist->Spawn_Loc) == false && handleSearch != creaturelist->spawnedForHandle.end()))
			{
				CLkPacket packet(sizeof(sGU_OBJECT_DESTROY));
				sGU_OBJECT_DESTROY * res = (sGU_OBJECT_DESTROY*)packet.GetPacketData();
				res->wOpCode = GU_OBJECT_DESTROY;
				res->handle = creaturelist->UniqueID;
				creaturelist->target = 0;
				creaturelist->spawnedForHandle.erase(handleSearch);
				m_map_spawned_Monster.erase(creaturelist->UniqueID);
				/*	if (handleSearch == creaturelist->spawnedForHandle.end())
					{
					creaturelist->isSpawned = false;
					creaturelist->target = 0;
					}*/
				packet.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
				g_pApp->Send(pSession->GetHandle(), &packet);
			}
			else //if (handleSearch == creaturelist->spawnedForHandle.end())
			{
				creaturelist->isSpawned = false;
				creaturelist->target = 0;
				creaturelist->spawnedForHandle.clear();
				IterType found = m_map_spawned_Monster.find(creaturelist->UniqueID);
				if (found != m_map_spawned_Monster.end())
					m_map_spawned_Monster.erase(found);
			}

		}


#pragma endregion Monstercheckend
#pragma region npccheck

		for (IterType it = m_map_Npc.begin(); it != m_map_Npc.end(); it++)
		{
			creaturelist = it->second;
			std::vector<RwUInt32>::iterator handleSearch = std::find(creaturelist->spawnedForHandle.begin(), creaturelist->spawnedForHandle.end(), pSession->GetavatarHandle());
			sNPC_TBLDAT* pNPCTblData = (sNPC_TBLDAT*)app->g_pTableContainer->GetNpcTable()->FindData(creaturelist->MonsterID);

			if (pNPCTblData && app->mob->CreatureRangeCheck(curPos, creaturelist->Spawn_Loc) == true)
			{
				if (handleSearch != creaturelist->spawnedForHandle.end())
				{
					//Your handle was found so dont spawn it.
				}
				else
				{
					CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
					sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();

					sPacket->wOpCode = GU_OBJECT_CREATE;
					sPacket->sObjectInfo.objType = OBJTYPE_NPC;
					sPacket->handle = creaturelist->UniqueID;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = creaturelist->Spawn_Loc.x;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = creaturelist->Spawn_Loc.y;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = creaturelist->Spawn_Loc.z;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.x = creaturelist->Spawn_Dir.x;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.y = creaturelist->Spawn_Dir.y;
					sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.z = creaturelist->Spawn_Dir.z;
					sPacket->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
					sPacket->sObjectInfo.npcBrief.wCurEP = pNPCTblData->wBasic_EP;
					sPacket->sObjectInfo.npcBrief.wCurLP = pNPCTblData->wBasic_LP;
					sPacket->sObjectInfo.npcBrief.wMaxEP = pNPCTblData->wBasic_EP;
					sPacket->sObjectInfo.npcBrief.wMaxLP = pNPCTblData->wBasic_LP;
					sPacket->sObjectInfo.npcBrief.tblidx = creaturelist->MonsterID;
					creaturelist->isSpawned = true;
					creaturelist->spawnedForHandle.push_back(pSession->GetavatarHandle());
					packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
					g_pApp->Send(pSession->GetHandle(), &packet);

				}
			}
		}
	//}
	return true;

#pragma endregion Npccheckend
}

float CMobManager::Distance(const sVECTOR3 mycurPos, const CLkVector othercurPos)
{
	float first = (mycurPos.x - othercurPos.x) * (mycurPos.x - othercurPos.x);
	float second = (mycurPos.y - othercurPos.y) * (mycurPos.y - othercurPos.y);
	float third = (mycurPos.z - othercurPos.z) * (mycurPos.z - othercurPos.z);
	float distance = sqrt(first + second + third);
	return distance;
}

bool CMobManager::CreatureRangeCheck(sVECTOR3 mycurPos, CLkVector othercurPos)
{
	try
	{
		float distance = LkGetDistance(mycurPos.x, mycurPos.z, othercurPos.x, othercurPos.z);
		if (distance < DISTANCE_TO_SPAWN)
		{
			return true;
		}

		return false;
	}
	catch (exception e)
	{
		cout << "Exception with CreatureRangeCheck : " << e.what() << endl;
	}
	
	
	return false;
}

void CMobManager::SpawnNpcAtLogin(CLkPacket * pPacket, CClientSession * pSession)
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();

	sVECTOR3 curpos = pSession->cPlayersMain->GetPlayerPosition();
	CMonster::MonsterData* creaturelist;
	CSpawnTable* pNPCSpawnTbl;
	for (IterType it = m_map_Npc.begin(); it != m_map_Npc.end(); it++)
	{
		creaturelist = (it->second);
		sNPC_TBLDAT* pNPCTblData = (sNPC_TBLDAT*)app->g_pTableContainer->GetNpcTable()->FindData(creaturelist->MonsterID);
		if (pNPCTblData)
		{
			if (CreatureRangeCheck(curpos, creaturelist->Spawn_Loc) == true || creaturelist->isSpawned == true)
			{

				CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
				sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();
				sPacket->wOpCode = GU_OBJECT_CREATE;
				sPacket->sObjectInfo.objType = OBJTYPE_NPC;
				sPacket->handle = creaturelist->UniqueID;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = creaturelist->Spawn_Loc.x;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = creaturelist->Spawn_Loc.y;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = creaturelist->Spawn_Loc.z;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.x = creaturelist->Spawn_Dir.x;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.y = creaturelist->Spawn_Dir.y;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.z = creaturelist->Spawn_Dir.z;
				sPacket->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				sPacket->sObjectInfo.npcBrief.wCurEP = pNPCTblData->wBasic_EP;
				sPacket->sObjectInfo.npcBrief.wCurLP = pNPCTblData->wBasic_LP;
				sPacket->sObjectInfo.npcBrief.wMaxEP = pNPCTblData->wBasic_EP;
				sPacket->sObjectInfo.npcBrief.wMaxLP = pNPCTblData->wBasic_LP;
				sPacket->sObjectInfo.npcBrief.tblidx = creaturelist->MonsterID;

				packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
				g_pApp->Send(pSession->GetHandle(), &packet);

			}
		}
	}
}
void CMobManager::SpawnMonsterAtLogin(CLkPacket * pPacket, CClientSession * pSession)
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();
	sVECTOR3 curpos = pSession->cPlayersMain->GetPlayerPosition();
	CMonster::MonsterData* creaturelist;

	for (IterType it = m_map_Monster.begin(); it != m_map_Monster.end(); it++)
	{
		creaturelist = (it->second);
		if(creaturelist->IsDead == false)
		{
			if (CreatureRangeCheck(curpos, creaturelist->Spawn_Loc) == true || creaturelist->isSpawned == true)
			{
				CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
				sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();

				res->wOpCode = GU_OBJECT_CREATE;
				res->sObjectInfo.objType = OBJTYPE_MOB;
				res->handle = creaturelist->UniqueID;
				res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = creaturelist->Spawn_Loc.x;
				res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = creaturelist->Spawn_Loc.y;
				res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = creaturelist->Spawn_Loc.z;
				res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = creaturelist->Spawn_Dir.x + rand() % 360;
				res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = creaturelist->Spawn_Dir.y;
				res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = creaturelist->Spawn_Dir.z;
				res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				res->sObjectInfo.mobState.sCharStateBase.bFightMode = creaturelist->FightMode;
				res->sObjectInfo.mobBrief.tblidx = creaturelist->MonsterID;
				res->sObjectInfo.mobBrief.wCurEP = creaturelist->CurEP;
				res->sObjectInfo.mobBrief.wMaxEP = creaturelist->MaxEP;
				res->sObjectInfo.mobBrief.wCurLP = creaturelist->CurLP;
				res->sObjectInfo.mobBrief.wMaxLP = creaturelist->MaxLP;
				res->sObjectInfo.mobBrief.fLastRunningSpeed =  creaturelist->Run_Speed;
				res->sObjectInfo.mobBrief.fLastWalkingSpeed = creaturelist->Walk_Speed;
				//creaturelist->isAggro = false;
				creaturelist->isSpawned = true;
				//pSession->InsertIntoMyMonsterList(creaturelist->MonsterSpawnID, creaturelist->Spawn_Loc, creaturelist->MonsterID);

				packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );
				g_pApp->Send( pSession->GetHandle(), &packet );
			
			}
		}
	}
}

//Searches through mobMap to see if handle exists.
bool CMobManager::FindCreature(RwUInt32 handle)
{
	IterType handleSearch = m_map_Monster.find(handle);
	if (handleSearch != m_map_Monster.end())
		return true;
	else
		return false;
}
//Search the NPC by Handle
TBLIDX CMobManager::FindNpc(RwUInt32 handle)
{
	IterType handleSearch = m_map_Npc.find(handle);
	if (handleSearch != m_map_Npc.end())
		return handleSearch->second->MonsterID;
	else
		return 0;
}
//Simple function to return MonsterData
CMonster::MonsterData*	CMobManager::GetMobByHandle(RwUInt32 Target)
{
	
	IterType handleSearch = m_map_Monster.find(Target);
	if (handleSearch != m_map_Monster.end())
	{
		CMonster::MonsterData * creaturelist = handleSearch->second;
		return creaturelist;
	}
	return NULL;
}

//Function to update the status of mob dying
bool CMobManager::UpdateDeathStatus(RwUInt32 MobID, bool death_status)
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();

	IterType handleSearch = m_map_Monster.find(MobID);
	if (handleSearch != m_map_Monster.end())
	{
		CMonster::MonsterData * creaturelist = handleSearch->second;
		if(death_status == true)
		{
			creaturelist->isSpawned = false;
			creaturelist->isAggro = false;
			creaturelist->target = 0;
			creaturelist->KilledTime = timeGetTime();
			creaturelist->IsDead = death_status;
			creaturelist->CurLP = creaturelist->MaxLP;
			creaturelist->CurEP = creaturelist->MaxEP;
			creaturelist->spawnedForHandle.clear();
		}
		else
		{
			creaturelist->IsDead = death_status;
		}
		return true;
	}
	return true;
}

//Spawns a mob by its spawntblidx
void CMobManager::CreateMobByTblidx(RwUInt32 spawnTblidx, RwUInt32 avatarHandle)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(avatarHandle);
	sVECTOR3 curpos = plr->GetPlayerPosition();
	CSpawnTable* pMOBSpawnTbl = app->g_pTableContainer->GetMobSpawnTable(plr->GetWorldTblx());

		sSPAWN_TBLDAT* pMOBSpwnTblData = (sSPAWN_TBLDAT*)pMOBSpawnTbl->FindData(spawnTblidx);
		sMOB_TBLDAT* pMOBTblData = (sMOB_TBLDAT*)app->g_pTableContainer->GetMobTable()->FindData(pMOBSpwnTblData->mob_Tblidx);

		if (pMOBTblData)
		{
			CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
			sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();
			CMonster::MonsterData * cr = new CMonster::MonsterData;

			cr->Level = pMOBTblData->byLevel;
			cr->CurEP = pMOBTblData->wBasic_EP;
			cr->CurLP = pMOBTblData->wBasic_LP;
			cr->FightMode = false;
			cr->IsDead = false;
			cr->isSpawned = true;
			cr->MonsterID = pMOBTblData->tblidx;
			cr->MonsterSpawnID = pMOBSpwnTblData->tblidx;
			cr->Spawn_Loc = pMOBSpwnTblData->vSpawn_Loc;
			cr->Spawn_Dir = pMOBSpwnTblData->vSpawn_Dir;
			cr->MaxEP = pMOBTblData->wBasic_EP;
			cr->MaxLP = pMOBTblData->wBasic_LP;
			cr->Spawn_Quantity = pMOBSpwnTblData->bySpawn_Quantity;
			cr->Move_DelayTime = pMOBSpwnTblData->byMove_DelayTime;
			cr->Run_Speed = pMOBTblData->fRun_Speed;
			cr->Run_Speed_origin = pMOBTblData->fRun_Speed_Origin;
			cr->Walk_Speed = pMOBTblData->fWalk_Speed;
			cr->Walk_Speed_origin = pMOBTblData->fWalk_Speed_Origin;
			cr->Spawn_Cool_Time = pMOBSpwnTblData->wSpawn_Cool_Time * 1000;
			cr->target = 0;
			cr->curPos = cr->Spawn_Loc;
			cr->Basic_aggro_point = pMOBTblData->byScan_Range;
			cr->Attack_range = pMOBTblData->fAttack_Range;
			cr->MaxchainAttackCount = pMOBTblData->byAttack_Animation_Quantity;
			cr->chainAttackCount = 0;
			cr->UniqueID = CreateUniqueId();
			m_map_Monster.insert(std::make_pair(cr->UniqueID, cr));
			
			res->wOpCode = GU_OBJECT_CREATE;
			res->sObjectInfo.objType = OBJTYPE_MOB;
			res->handle = cr->UniqueID;
			res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = cr->Spawn_Loc.x;
			res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = cr->Spawn_Loc.y;
			res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = cr->Spawn_Loc.z;
			res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = cr->Spawn_Dir.x + rand() % 360;
			res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = cr->Spawn_Dir.y;
			res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = cr->Spawn_Dir.z;
			res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
			res->sObjectInfo.mobState.sCharStateBase.bFightMode = cr->FightMode;
			res->sObjectInfo.mobBrief.tblidx = cr->MonsterID;
			res->sObjectInfo.mobBrief.wCurEP = cr->CurEP;
			res->sObjectInfo.mobBrief.wMaxEP = cr->MaxEP;
			res->sObjectInfo.mobBrief.wCurLP = cr->CurLP;
			res->sObjectInfo.mobBrief.wMaxLP = cr->MaxLP;
			res->sObjectInfo.mobBrief.fLastRunningSpeed = cr->Run_Speed;
			res->sObjectInfo.mobBrief.fLastWalkingSpeed = cr->Walk_Speed;
			
			packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
			g_pApp->Send(plr->GetSession(), &packet);
		}
		plr = NULL;
		delete plr;
}

void CMobManager::CreateMobByMobTblidx(RwUInt32 Tblidx, RwUInt32 avatarHandle)
{
}
//Spawns an NPC by its SpawnTblidx
void CMobManager::CreateNPCByTblidx(RwUInt32 spawnTblidx, RwUInt32 playerHandle)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(playerHandle);
	sVECTOR3 curpos = plr->GetPlayerPosition();
	CSpawnTable* pNPCSpawnTbl = app->g_pTableContainer->GetNpcSpawnTable(plr->GetWorldTblx());

	sSPAWN_TBLDAT* pNPCSpwnTblData = (sSPAWN_TBLDAT*)pNPCSpawnTbl->FindData(spawnTblidx);
	sNPC_TBLDAT* pNPCTblData = (sNPC_TBLDAT*)app->g_pTableContainer->GetNpcTable()->FindData(pNPCSpwnTblData->mob_Tblidx);
	if (pNPCTblData)
	{
		CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
		sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();
		CMonster::MonsterData * cr = new CMonster::MonsterData;

		cr->Level = pNPCTblData->byLevel;
		cr->CurEP = pNPCTblData->wBasic_EP;
		cr->CurLP = pNPCTblData->wBasic_LP;
		cr->FightMode = false;
		cr->IsDead = false;
		cr->isSpawned = true;
		cr->MonsterID = pNPCTblData->tblidx;
		cr->MonsterSpawnID = pNPCSpwnTblData->tblidx;
		cr->Spawn_Loc = pNPCSpwnTblData->vSpawn_Loc;
		cr->Spawn_Dir = pNPCSpwnTblData->vSpawn_Dir;
		cr->MaxEP = pNPCTblData->wBasic_EP;
		cr->MaxLP = pNPCTblData->wBasic_LP;
		cr->curPos = cr->Spawn_Loc;
		cr->UniqueID = CreateUniqueId();
		m_map_Npc.insert(std::make_pair(cr->UniqueID, cr)); 
		
		res->wOpCode = GU_OBJECT_CREATE;
		res->sObjectInfo.objType = OBJTYPE_NPC;
		res->handle = cr->UniqueID;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = cr->Spawn_Loc.x;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = cr->Spawn_Loc.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = cr->Spawn_Loc.z;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = cr->Spawn_Dir.x + rand() % 360;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = cr->Spawn_Dir.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = cr->Spawn_Dir.z;
		res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
		res->sObjectInfo.mobState.sCharStateBase.bFightMode = cr->FightMode;
		res->sObjectInfo.mobBrief.tblidx = cr->MonsterID;
		res->sObjectInfo.mobBrief.wCurEP = cr->CurEP;
		res->sObjectInfo.mobBrief.wMaxEP = cr->MaxEP;
		res->sObjectInfo.mobBrief.wCurLP = cr->CurLP;
		res->sObjectInfo.mobBrief.wMaxLP = cr->MaxLP;
		res->sObjectInfo.mobBrief.fLastRunningSpeed = cr->Run_Speed;
		res->sObjectInfo.mobBrief.fLastWalkingSpeed = cr->Walk_Speed;

		packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
		g_pApp->Send(plr->GetSession(), &packet);
	}
	plr = NULL;
	delete plr;

}

void CMonster::MonsterData::ResetMob()
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();
	this->curPos = this->Spawn_Loc;
	this->FightMode = false;
	this->isAggro = false;
	this->target = 0;
	this->CurLP = this->MaxLP;
	this->CurEP = this->MaxEP;
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_LP_EP));
	sGU_UPDATE_CHAR_LP_EP * res = (sGU_UPDATE_CHAR_LP_EP *)packet.GetPacketData();

	res->handle = this->UniqueID;
	res->wCurEP = this->CurEP;
	res->wCurLP = this->CurLP;
	res->wMaxEP = this->MaxEP;
	res->wMaxLP = this->MaxLP;
	res->wOpCode = GU_UPDATE_CHAR_LP_EP;

	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LP_EP));
	app->UserBroadcast(&packet);
}

void CMonster::MonsterData::MoveToSpawn()
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
	res->wOpCode = GU_UPDATE_CHAR_STATE;
	res->handle = this->UniqueID;
	res->sCharState.sCharStateBase.bFightMode = false;
	res->sCharState.sCharStateBase.byStateID = CHARSTATE_DESTMOVE;
	res->sCharState.sCharStateDetail.sCharStateDestMove.byDestLocCount = 1;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].x = this->Spawn_Loc.x;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].y = this->Spawn_Loc.y;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].z = this->Spawn_Loc.z;
	res->sCharState.sCharStateDetail.sCharStateDestMove.bHaveSecondDestLoc = false;
	packet.SetPacketLen( sizeof(sGU_UPDATE_CHAR_STATE) );
	app->UserBroadcast(&packet);
}

void CMonster::MonsterData::MoveToPlayer(PlayersMain *plr)
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
	
	res->wOpCode = GU_UPDATE_CHAR_STATE;
	res->handle = this->UniqueID;
	res->sCharState.sCharStateBase.bFightMode = true;
	res->sCharState.sCharStateBase.byStateID = CHARSTATE_DESTMOVE;
	res->sCharState.sCharStateDetail.sCharStateDestMove.byDestLocCount = 1;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].x = plr->GetPlayerPosition().x - 1;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].y = plr->GetPlayerPosition().y;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].z = plr->GetPlayerPosition().z - 1;
	res->sCharState.sCharStateDetail.sCharStateDestMove.bHaveSecondDestLoc = false;
	packet.SetPacketLen( sizeof(sGU_UPDATE_CHAR_STATE) );
	app->UserBroadcast(&packet);
	this->curPos.x = plr->GetPlayerPosition().x - 1;
	this->curPos.y = plr->GetPlayerPosition().y;
	this->curPos.z = plr->GetPlayerPosition().z - 1;
}

void CMonster::MonsterData::MoveToRand()
{
	CGameServer * app = (CGameServer*) LkSfxGetApp();
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
       
	res->wOpCode = GU_UPDATE_CHAR_STATE;
	res->handle = this->UniqueID;
	res->sCharState.sCharStateBase.bFightMode = true;
	res->sCharState.sCharStateBase.byStateID = CHARSTATE_DESTMOVE;
	res->sCharState.sCharStateDetail.sCharStateDestMove.byDestLocCount = 1;
	if (rand() % 10 >= 5)
	{
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].x = this->curPos.x + rand() % 3;
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].y = this->curPos.y;
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].z = this->curPos.z + rand() % 3;
	}
	else
	{
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].x = this->curPos.x - rand() % 3;
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].y = this->curPos.y;
		res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[0].z = this->curPos.z - rand() % 3;
	}
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[1].x = this->Spawn_Loc.x;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[1].y = this->Spawn_Loc.y;
	res->sCharState.sCharStateDetail.sCharStateDestMove.avDestLoc[1].z = this->Spawn_Loc.z;
	res->sCharState.sCharStateDetail.sCharStateDestMove.bHaveSecondDestLoc = true;
	packet.SetPacketLen( sizeof(sGU_UPDATE_CHAR_STATE) );
	app->UserBroadcast(&packet);
}

void	CMonster::MonsterData::Attack(PlayersMain *plr, CGameServer *app)
{
	this->FightMode = true;
	CLkPacket packet(sizeof(sGU_CHAR_ACTION_ATTACK));
	sGU_CHAR_ACTION_ATTACK * res = (sGU_CHAR_ACTION_ATTACK *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_ACTION_ATTACK;
	res->hSubject = this->UniqueID;
	res->hTarget = plr->GetAvatarHandle();
	res->dwLpEpEventId = 255;
	res->byBlockedAction = 255;
	float formula = 0;
	if (this->Level <= 5)
		formula = rand() % 25 + 5;
	else
		formula = (this->Str * this->Level) * .08;
	res->wAttackResultValue = formula;
	res->fReflectedDamage = 0;
	res->vShift = plr->GetPlayerPosition();
	this->chainAttackCount += 1;
	res->byAttackSequence = this->chainAttackCount;
	res->bChainAttack = true;
	res->byAttackResult = BATTLE_ATTACK_RESULT_HIT;
	packet.SetPacketLen( sizeof(sGU_CHAR_ACTION_ATTACK) );
	app->UserBroadcast(&packet);
	plr->SetPlayerFight(true);
	plr->SetPlayerDamage(res->wAttackResultValue);
}
