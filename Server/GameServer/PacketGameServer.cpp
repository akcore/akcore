#include "stdafx.h"
#include "LkTokenizer.h"

#include "PacketGameServer.h"

#include "LkPacketTU.h"
#include "LkPacketUT.h"
#include "GameServer.h"
//#include "LkSob.h" //Teste Code

typedef std::list<SBattleData*> ListAttackBegin;
typedef ListAttackBegin::iterator BATTLEIT;
ListAttackBegin				m_listAttackBegin;
SSkillData *pSkillData;

//--------------------------------------------------------------------------------------//
//		Log into Game Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendGameEnterReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- LOAD CHAT SERVER --- \n");
	sUG_GAME_ENTER_REQ * req = (sUG_GAME_ENTER_REQ *)pPacket->GetPacketData();

	avatarHandle = AcquireSerialId();

	g_pPlayerManager->AddNewPlayer(avatarHandle, this->GetHandle(), req->charId, req->accountId);
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	plr->myCCSession = this;
	CLkPacket packet(sizeof(sGU_GAME_ENTER_RES));

	app->db->prepare("UPDATE characters SET IsOnline = 1,TutorialFlag = ? WHERE CharID = ?");
	app->db->setInt(1, (req->bTutorialMode == true ? 0 : 1));
	app->db->setInt(2, plr->GetCharID());
	app->db->execute();

	sGU_GAME_ENTER_RES * res = (sGU_GAME_ENTER_RES *)packet.GetPacketData();

	res->wOpCode = GU_GAME_ENTER_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s(res->achCommunityServerIP, sizeof(res->achCommunityServerIP), app->GetConfigFileExternalIP());
	res->wCommunityServerPort = 20400;

	packet.SetPacketLen(sizeof(sGU_GAME_ENTER_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Send avatar char info
//--------------------------------------------------------------------------------------//
/*void CClientSession::CheckPlayerStat(CGameServer * app, sPC_TBLDAT *pTblData, int level,RwUInt32 playerHandle,bool bUpdate)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(playerHandle);
	if (bUpdate)
	{
		BYTE byStr = plr->GetPcProfile()->avatarAttribute.byBaseStr + (plr->GetPcTblDat()->fLevel_Up_Str * level);
		BYTE byCon = plr->GetPcProfile()->avatarAttribute.byBaseCon + (plr->GetPcTblDat()->fLevel_Up_Con * level);
		BYTE byFoc = plr->GetPcProfile()->avatarAttribute.byBaseFoc + (plr->GetPcTblDat()->fLevel_Up_Foc * level);
		BYTE byDex = plr->GetPcProfile()->avatarAttribute.byBaseDex + (plr->GetPcTblDat()->fLevel_Up_Dex * level);
		BYTE bySol = plr->GetPcProfile()->avatarAttribute.byBaseSol + (plr->GetPcTblDat()->fLevel_Up_Sol * level);
		BYTE byEng = plr->GetPcProfile()->avatarAttribute.byBaseEng + (plr->GetPcTblDat()->fLevel_Up_Eng * level);
		app->db->prepare("UPDATE characters SET BaseStr = ?, BaseCon = ?, BaseFoc = ?, BaseDex = ?,BaseSol = ?, BaseEng = ? WHERE CharID = ?");
		app->db->setInt(1, byStr);
		app->db->setInt(2, byCon);
		app->db->setInt(3, byFoc);
		app->db->setInt(4, byDex);
		app->db->setInt(5, bySol);
		app->db->setInt(6, byEng);
		app->db->setInt(7, plr->GetCharID());
		app->db->execute();

		app->db->prepare("UPDATE characters SET BaseAttackRate = ?, BaseAttackSpeedRate = ?, BaseEnergyDefence = ?, BaseEnergyOffence = ?,BasePhysicalDefence = ?, BasePhysicalOffence = ? WHERE CharID = ?");
		app->db->setInt(1, plr->GetPcTblDat()->wAttack_Rate);
		app->db->setInt(2, plr->GetPcTblDat()->wAttack_Speed_Rate);
		app->db->setInt(3, plr->GetPcTblDat()->wBasic_Energy_Defence + (plr->GetPcTblDat()->byLevel_Up_Energy_Defence * level));
		app->db->setInt(4, plr->GetPcTblDat()->wBasic_Energy_Offence + (plr->GetPcTblDat()->byLevel_Up_Energy_Offence * level));
		app->db->setInt(5, plr->GetPcTblDat()->wBasic_Physical_Defence + (plr->GetPcTblDat()->byLevel_Up_Physical_Defence * level));
		app->db->setInt(6, plr->GetPcTblDat()->wBasic_Physical_Offence + (plr->GetPcTblDat()->byLevel_Up_Physical_Offence * level));
		app->db->setInt(7, plr->GetCharID());
		app->db->execute();

		WORD basiclife = plr->GetPcProfile()->avatarAttribute.wBaseMaxLP + (plr->GetPcTblDat()->byLevel_Up_LP * level);
		WORD levelcon = byCon + (plr->GetPcTblDat()->fLevel_Up_Con * level);
		WORD LP = basiclife + ((levelcon * level) * 4.7);

		WORD basicenergy = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP + (plr->GetPcTblDat()->byLevel_Up_EP * level);
		WORD leveleng = byEng + (plr->GetPcTblDat()->fLevel_Up_Eng * level);
		WORD EP = basicenergy + ((leveleng * level) * 4.2);

		app->db->prepare("UPDATE characters SET BaseMaxLP = ?, BaseMaxEP = ?, BaseMaxRP = ?, BaseDodgeRate = ?, BaseAttackRate = ?, BaseBlockRate = ?, BasePhysicalCriticalRate = ?, BaseEnergyCriticalRate = ? WHERE CharID = ?");
		app->db->setInt(1, LP);
		app->db->setInt(2, EP);
		app->db->setInt(3, plr->GetPcProfile()->avatarAttribute.wBaseMaxRP + (plr->GetPcTblDat()->byLevel_Up_RP * level));
		app->db->setInt(4, plr->GetPcTblDat()->wDodge_Rate);
		app->db->setInt(5, plr->GetPcTblDat()->wAttack_Rate);
		app->db->setInt(6, plr->GetPcTblDat()->wBlock_Rate);
		app->db->setInt(7, 10);
		app->db->setInt(8, 10);
		app->db->setInt(9, plr->GetCharID());
		app->db->execute();

		if (plr->GetPcProfile()->avatarAttribute.byLastStr < byStr)
		{
			plr->GetPcProfile()->avatarAttribute.byLastStr = byStr;
			app->db->prepare("UPDATE characters SET LastStr = ? WHERE CharID = ?");
			app->db->setInt(1, byStr);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastCon < byCon)
		{
			plr->GetPcProfile()->avatarAttribute.byLastCon = byCon;
			app->db->prepare("UPDATE characters SET LastCon = ? WHERE CharID = ?");
			app->db->setInt(1, byCon);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastFoc < byFoc)
		{
			plr->GetPcProfile()->avatarAttribute.byLastFoc = byFoc;
			app->db->prepare("UPDATE characters SET LastFoc = ? WHERE CharID = ?");
			app->db->setInt(1, byFoc);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastDex < byDex)
		{
			plr->GetPcProfile()->avatarAttribute.byLastDex = byDex;
			app->db->prepare("UPDATE characters SET LastDex = ? WHERE CharID = ?");
			app->db->setInt(1, byDex);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastSol < bySol)
		{
			plr->GetPcProfile()->avatarAttribute.byLastSol = bySol;
			app->db->prepare("UPDATE characters SET LastSol = ? WHERE CharID = ?");
			app->db->setInt(1, bySol);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastEng < byEng)
		{
			plr->GetPcProfile()->avatarAttribute.byLastEng = byEng;
			app->db->prepare("UPDATE characters SET LastEng = ? WHERE CharID = ?");
			app->db->setInt(1, byEng);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		for (int i = 0; i < LK_MAX_EQUIP_ITEM_SLOT; i++)
		{
			if (plr->cPlayerInventory->GetEquippedItems()[i].tblidx!= INVALID_TBLIDX)
				plr->UpdateBaseAttributeWithEquip(plr->cPlayerInventory->GetEquippedItems()[i].tblidx, plr->cPlayerInventory->GetEquippedItems()[i].byRank, plr->cPlayerInventory->GetEquippedItems()[i].byGrade);
		}
		plr->GetPcProfile()->avatarAttribute = plr->cPlayerAttribute->GetAvatarAttribute();
		plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
	}
	else
	{
		BYTE byStr = pTblData->byStr + (pTblData->fLevel_Up_Str * level);
		BYTE byCon = pTblData->byCon + (pTblData->fLevel_Up_Con * level);
		BYTE byFoc = pTblData->byFoc + (pTblData->fLevel_Up_Foc * level);
		BYTE byDex = pTblData->byDex + (pTblData->fLevel_Up_Dex * level);
		BYTE bySol = pTblData->bySol + (pTblData->fLevel_Up_Sol * level);
		BYTE byEng = pTblData->byEng + (pTblData->fLevel_Up_Eng * level);
		app->db->prepare("UPDATE characters SET BaseStr = ?, BaseCon = ?, BaseFoc = ?, BaseDex = ?,BaseSol = ?, BaseEng = ? WHERE CharID = ?");
		app->db->setInt(1, byStr);
		app->db->setInt(2, byCon);
		app->db->setInt(3, byFoc);
		app->db->setInt(4, byDex);
		app->db->setInt(5, bySol);
		app->db->setInt(6, byEng);
		app->db->setInt(7, plr->GetCharID());
		app->db->execute();

		app->db->prepare("UPDATE characters SET BaseAttackRate = ?, BaseAttackSpeedRate = ?, BaseEnergyDefence = ?, BaseEnergyOffence = ?,BasePhysicalDefence = ?, BasePhysicalOffence = ? WHERE CharID = ?");
		app->db->setInt(1, pTblData->wAttack_Rate);
		app->db->setInt(2, pTblData->wAttack_Speed_Rate);
		app->db->setInt(3, pTblData->wBasic_Energy_Defence + (pTblData->byLevel_Up_Energy_Defence * level));
		app->db->setInt(4, pTblData->wBasic_Energy_Offence + (pTblData->byLevel_Up_Energy_Offence * level));
		app->db->setInt(5, pTblData->wBasic_Physical_Defence + (pTblData->byLevel_Up_Physical_Defence * level));
		app->db->setInt(6, pTblData->wBasic_Physical_Offence + (pTblData->byLevel_Up_Physical_Offence * level));
		app->db->setInt(7, plr->GetCharID());
		app->db->execute();

		WORD basiclife = pTblData->wBasic_LP + (pTblData->byLevel_Up_LP * level);
		WORD levelcon = byCon + (pTblData->fLevel_Up_Con * level);
		WORD LP = basiclife + ((levelcon * level) * 4.7);

		WORD basicenergy = pTblData->wBasic_EP + (pTblData->byLevel_Up_EP * level);
		WORD leveleng = byEng + (pTblData->fLevel_Up_Eng * level);
		WORD EP = basicenergy + ((leveleng * level) * 4.2);

		app->db->prepare("UPDATE characters SET BaseMaxLP = ?, BaseMaxEP = ?, BaseMaxRP = ?, BaseDodgeRate = ?, BaseAttackRate = ?, BaseBlockRate = ?, BasePhysicalCriticalRate = ?, BaseEnergyCriticalRate = ? WHERE CharID = ?");
		app->db->setInt(1, LP);
		app->db->setInt(2, EP);
		app->db->setInt(3, pTblData->wBasic_RP + (pTblData->byLevel_Up_RP * level));
		app->db->setInt(4, pTblData->wDodge_Rate);
		app->db->setInt(5, pTblData->wAttack_Rate);
		app->db->setInt(6, pTblData->wBlock_Rate);
		app->db->setInt(7, 10);
		app->db->setInt(8, 10);
		app->db->setInt(9, plr->GetCharID());
		app->db->execute();

		if (plr->GetPcProfile()->avatarAttribute.byLastStr < byStr)
		{
			plr->GetPcProfile()->avatarAttribute.byLastStr = byStr;
			app->db->prepare("UPDATE characters SET LastStr = ? WHERE CharID = ?");
			app->db->setInt(1, byStr);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastCon < byCon)
		{
			plr->GetPcProfile()->avatarAttribute.byLastCon = byCon;
			app->db->prepare("UPDATE characters SET LastCon = ? WHERE CharID = ?");
			app->db->setInt(1, byCon);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastFoc < byFoc)
		{
			plr->GetPcProfile()->avatarAttribute.byLastFoc = byFoc;
			app->db->prepare("UPDATE characters SET LastFoc = ? WHERE CharID = ?");
			app->db->setInt(1, byFoc);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastDex < byDex)
		{
			plr->GetPcProfile()->avatarAttribute.byLastDex = byDex;
			app->db->prepare("UPDATE characters SET LastDex = ? WHERE CharID = ?");
			app->db->setInt(1, byDex);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastSol < bySol)
		{
			plr->GetPcProfile()->avatarAttribute.byLastSol = bySol;
			app->db->prepare("UPDATE characters SET LastSol = ? WHERE CharID = ?");
			app->db->setInt(1, bySol);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		if (plr->GetPcProfile()->avatarAttribute.byLastEng < byEng)
		{
			plr->GetPcProfile()->avatarAttribute.byLastEng = byEng;
			app->db->prepare("UPDATE characters SET LastEng = ? WHERE CharID = ?");
			app->db->setInt(1, byEng);
			app->db->setInt(2, plr->GetCharID());
			app->db->execute();
		}
		plr->SetStats(pTblData);
	}	
	
}*/
void CClientSession::SendAvatarCharInfo(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- LOAD CHARACTER INFO FOR GAMESERVER --- \n");
	CLkPacket packet(sizeof(sGU_AVATAR_CHAR_INFO));
	sGU_AVATAR_CHAR_INFO * res = (sGU_AVATAR_CHAR_INFO *)packet.GetPacketData();

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	app->db->prepare("UPDATE characters SET OnlineID = ? WHERE CharID = ?");
	app->db->setInt(1, plr->GetAvatarHandle());
	app->db->setInt(2, plr->GetCharID());
	app->db->execute();

	res->wOpCode = GU_AVATAR_CHAR_INFO;
	res->handle = this->GetavatarHandle();

	plr->CreatePlayerProfile();
	//this->CheckPlayerStat(app, plr->GetPcTblDat(), plr->GetPcProfile()->byLevel, plr->GetAvatarHandle());

	memcpy(&res->sPcProfile, plr->GetPcProfile(), sizeof(sPC_PROFILE));
	memcpy(&res->sCharState, plr->GetCharState(), sizeof(sCHARSTATE));

	res->wCharStateSize = sizeof(sCHARSTATE_BASE);
	packet.SetPacketLen(sizeof(sGU_AVATAR_CHAR_INFO));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Send Avatar Iteminfo
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarItemInfo(CLkPacket * pPacket, CGameServer * app)
{
	printf("Send item info\n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_AVATAR_ITEM_INFO));
	sGU_AVATAR_ITEM_INFO * res = (sGU_AVATAR_ITEM_INFO *)packet.GetPacketData();

	res->wOpCode = GU_AVATAR_ITEM_INFO;
	res->byBeginCount = 0;
	res->byItemCount = plr->cPlayerInventory->GetTotalItemsCount();

	for (int i = 0; i < res->byItemCount; i++)
	{
		res->aItemProfile[i].handle = plr->cPlayerInventory->GetInventory()[i].handle;
		res->aItemProfile[i].tblidx = plr->cPlayerInventory->GetInventory()[i].tblidx;
		res->aItemProfile[i].byPlace = plr->cPlayerInventory->GetInventory()[i].byPlace;
		res->aItemProfile[i].byPos = plr->cPlayerInventory->GetInventory()[i].byPos;
		res->aItemProfile[i].byStackcount = plr->cPlayerInventory->GetInventory()[i].byStackcount;
		res->aItemProfile[i].byRank = plr->cPlayerInventory->GetInventory()[i].byRank;
		res->aItemProfile[i].byGrade = plr->cPlayerInventory->GetInventory()[i].byGrade;
		res->aItemProfile[i].byCurDur = plr->cPlayerInventory->GetInventory()[i].byCurDur;
		res->aItemProfile[i].byBattleAttribute = plr->cPlayerInventory->GetInventory()[i].byBattleAttribute;
		res->aItemProfile[i].aOptionTblidx[0] = plr->cPlayerInventory->GetInventory()[i].aOptionTblidx[0];
		res->aItemProfile[i].aOptionTblidx[1] = plr->cPlayerInventory->GetInventory()[i].aOptionTblidx[1];
	}
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->byItemCount * sizeof(sITEM_PROFILE)));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
void CClientSession::SendSlotInfo(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_QUICK_SLOT_INFO));
	sGU_QUICK_SLOT_INFO * res = (sGU_QUICK_SLOT_INFO *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	res->wOpCode = GU_QUICK_SLOT_INFO;
	res->byQuickSlotCount = plr->cPlayerSkills->GetQuickSlotCount();
	for (int i = 0; i < res->byQuickSlotCount; i++)
	{
		res->asQuickSlotData[i].bySlot = plr->cPlayerSkills->GetQuickSlot()[i].bySlot;
		res->asQuickSlotData[i].byType = plr->cPlayerSkills->GetQuickSlot()[i].byType;
		res->asQuickSlotData[i].hItem = plr->cPlayerSkills->GetQuickSlot()[i].hItem;
		res->asQuickSlotData[i].tblidx = plr->cPlayerSkills->GetQuickSlot()[i].tblidx;
	}
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->byQuickSlotCount * (sizeof(sQUICK_SLOT_DATA))));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Send Avatar Skillinfo
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarSkillInfo(CLkPacket * pPacket, CGameServer * app)
{
	printf("Send skill info\n");

	CLkPacket packet(sizeof(sGU_AVATAR_SKILL_INFO));
	sGU_AVATAR_SKILL_INFO * res = (sGU_AVATAR_SKILL_INFO *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	res->wOpCode = GU_AVATAR_SKILL_INFO;
	res->bySkillCount = plr->cPlayerSkills->GetSkillCount();
	for (int i = 0; i < res->bySkillCount; i++)
	{
		res->aSkillInfo[i].bIsRpBonusAuto = plr->cPlayerSkills->GetSkills()[i].bIsRpBonusAuto;
		res->aSkillInfo[i].byRpBonusType = plr->cPlayerSkills->GetSkills()[i].byRpBonusType;
		res->aSkillInfo[i].bySlotId = plr->cPlayerSkills->GetSkills()[i].bySlotId;
		res->aSkillInfo[i].dwTimeRemaining = plr->cPlayerSkills->GetSkills()[i].dwTimeRemaining;
		res->aSkillInfo[i].nExp = plr->cPlayerSkills->GetSkills()[i].nExp;
		res->aSkillInfo[i].tblidx = plr->cPlayerSkills->GetSkills()[i].tblidx;
	}
	//Changed the struct
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->bySkillCount * sizeof(sSKILL_INFO)));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		SendAvatarHTBInfo Luiz45
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarHTBInfo(CLkPacket * pPacket, CGameServer* app)
{
	CLkPacket packet(sizeof(sGU_AVATAR_HTB_INFO));
	sGU_AVATAR_HTB_INFO * res = (sGU_AVATAR_HTB_INFO *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	res->wOpCode = GU_AVATAR_HTB_INFO;
	res->byHTBSkillCount = plr->cPlayerSkills->GetHTBSkillCount();
	for (int i = 0; i < res->byHTBSkillCount; i++)
	{
		res->aHTBSkillnfo[i].bySlotId = plr->cPlayerSkills->GetHTBSkills()[i].bySlotId;
		res->aHTBSkillnfo[i].dwTimeRemaining = plr->cPlayerSkills->GetHTBSkills()[i].dwTimeRemaining;
		res->aHTBSkillnfo[i].skillId = plr->cPlayerSkills->GetHTBSkills()[i].skillId;
	}
	packet.SetPacketLen(sizeof(sGU_AVATAR_HTB_INFO));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		SendCharRevivalRequest
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharRevivalReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	plr->SendThreadRevivalStatus();
	sUG_CHAR_REVIVAL_REQ * req = (sUG_CHAR_REVIVAL_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_CHAR_REVIVAL_RES));
	sGU_CHAR_REVIVAL_RES * res = (sGU_CHAR_REVIVAL_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_AVATAR_WORLD_INFO));
	sGU_AVATAR_WORLD_INFO * res2 = (sGU_AVATAR_WORLD_INFO *)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* pStand = (sGU_UPDATE_CHAR_STATE*)packet3.GetPacketData();

	CLkPacket packet4(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY* pCorpse = (sGU_OBJECT_DESTROY*)packet4.GetPacketData();

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	app->db->fetch();

	res2->wOpCode = GU_AVATAR_WORLD_INFO;
	res2->worldInfo.tblidx = app->db->getInt("WorldTable");
	res2->worldInfo.worldID = app->db->getInt("WorldID");
	res2->worldInfo.hTriggerObjectOffset = 100000;
	res2->worldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
	res2->vCurLoc.x = app->db->getInt("CurLocX");
	res2->vCurLoc.y = app->db->getInt("CurLocY");
	res2->vCurLoc.z = app->db->getInt("CurLocZ");
	res2->vCurDir.x = app->db->getInt("CurDirX");
	res2->vCurDir.y = app->db->getInt("CurDirY");
	res2->vCurDir.z = app->db->getInt("CurDirZ");

	plr->SetWorldID(app->db->getInt("WorldID"));
	plr->SetWorldTblidx(app->db->getInt("WorldTable"));

	pStand->handle = this->GetavatarHandle();
	pStand->wOpCode = GU_UPDATE_CHAR_STATE;
	pStand->sCharState.sCharStateBase.byStateID = CHARSTATE_FLAG_STUNNED;
	pStand->sCharState.sCharStateBase.vCurLoc.x = res2->vCurLoc.x;
	pStand->sCharState.sCharStateBase.vCurLoc.y = res2->vCurLoc.y;
	pStand->sCharState.sCharStateBase.vCurLoc.z = res2->vCurLoc.z;
	pStand->sCharState.sCharStateBase.vCurDir.x = res2->vCurDir.x;
	pStand->sCharState.sCharStateBase.vCurDir.y = res2->vCurDir.y;
	pStand->sCharState.sCharStateBase.vCurDir.z = res2->vCurDir.z;

	res->wOpCode = GU_CHAR_REVIVAL_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_CHAR_REVIVAL_RES));
	packet2.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
	packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet3);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		SendAvatarBuffInfo Luiz45
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarBuffInfo(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_AVATAR_BUFF_INFO));
	sGU_AVATAR_BUFF_INFO * res = (sGU_AVATAR_BUFF_INFO *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	res->byBuffCount = plr->cPlayerSkills->GetSkillBuffCount();
	for (int i = 0; i < res->byBuffCount; i++)
	{
		res->aBuffInfo[i].bySourceType = plr->cPlayerSkills->GetSkillBuff()[i].bySourceType;
		res->aBuffInfo[i].dwInitialDuration = plr->cPlayerSkills->GetSkillBuff()[i].dwInitialDuration;
		res->aBuffInfo[i].dwTimeRemaining = plr->cPlayerSkills->GetSkillBuff()[i].dwTimeRemaining;
		res->aBuffInfo[i].afEffectValue[0] = plr->cPlayerSkills->GetSkillBuff()[i].afEffectValue[0];
		res->aBuffInfo[i].afEffectValue[1] = plr->cPlayerSkills->GetSkillBuff()[i].afEffectValue[1];
		res->aBuffInfo[i].sourceTblidx = plr->cPlayerSkills->GetSkillBuff()[i].sourceTblidx;
	}
	res->wOpCode = GU_AVATAR_BUFF_INFO;

	packet.SetPacketLen(sizeof(sGU_AVATAR_BUFF_INFO));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		SendAvatarQuestList Luiz45
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarQuestList(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_AVATAR_QUEST_PROGRESS_INFO));
	sGU_AVATAR_QUEST_PROGRESS_INFO * res = (sGU_AVATAR_QUEST_PROGRESS_INFO *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_AVATAR_QUEST_COMPLETE_INFO));
	sGU_AVATAR_QUEST_COMPLETE_INFO * res2 = (sGU_AVATAR_QUEST_COMPLETE_INFO *)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_AVATAR_QUEST_INVENTORY_INFO));
	sGU_AVATAR_QUEST_INVENTORY_INFO * res3 = (sGU_AVATAR_QUEST_INVENTORY_INFO *)packet3.GetPacketData();
	int iQuestCounter = 0;

	app->db->prepare("SELECT * FROM charquestlist WHERE charId = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	int iQuestList = 0;

	while (app->db->fetch())
	{
		int questID = app->db->getInt("questID");
		int currentStep = app->db->getInt("currentStep");
		int nextStep = app->db->getInt("nextStep");
		if (nextStep == 255)
		{
			unsigned char& c = res2->completeInfo.abyQCInfo[questID / eCOMPLETE_QUEST_QUEST_PER_BYTE];
			int nShift = (questID % eCOMPLETE_QUEST_QUEST_PER_BYTE) * eCOMPLETE_QUEST_STATE_MAX;

			c |= (eCOMPLETE_QUEST_STATE_CLEAR << nShift);
			iQuestCounter++;
		}
		else
		{
			res->progressInfo[iQuestList].tId = questID;
			res->progressInfo[iQuestList].byVer = 0;
			res->progressInfo[iQuestList].uData.sQInfoV0.wQState = 0;//ALWAYS 0
			res->progressInfo[iQuestList].uData.sQInfoV0.sMainTSP.tcCurId = currentStep;//Quest Pointer current
			res->progressInfo[iQuestList].uData.sQInfoV0.sMainTSP.tcPreId = nextStep;//Quest Pointer Next
			res->progressInfo[iQuestList].uData.sQInfoV0.sSSM.auiSSM[iQuestList] = questID;
			//Time Quest?
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[0].tcId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[0].taId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[1].tcId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[1].taId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[2].tcId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[2].taId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[3].tcId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sETSlot.asExceptTimer[3].taId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sSToCEvtData.tcId = 0xff;
			res->progressInfo[iQuestList].uData.sQInfoV0.sSToCEvtData.taId = 0xff;
			/*if (app->db->getInt("dwEventData") == 391)
			{
			res->progressInfo[iQuestList].uData.sQInfoV0.sSToCEvtData.tcId = currentStep;
			res->progressInfo[iQuestList].uData.sQInfoV0.sSToCEvtData.taId = app->db->getInt("dwEventData");
			}*/
			///
			res->progressInfo[iQuestList].uData.sQInfoV0.tcQuestInfo = currentStep;
			res->progressInfo[iQuestList].uData.sQInfoV0.taQuestInfo = nextStep;
			res->progressInfo[iQuestList].uData.sQInfoV0.tgExcCGroup = 0;
			iQuestList++;
		}
	}
	res2->wOpCode = GU_AVATAR_QUEST_COMPLETE_INFO;

	res->byProgressCount = iQuestList;
	res->wOpCode = GU_AVATAR_QUEST_PROGRESS_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_QUEST_PROGRESS_INFO));
	packet2.SetPacketLen(sizeof(sGU_AVATAR_QUEST_COMPLETE_INFO));
	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet);

	plr = NULL;
	delete plr;

}

//--------------------------------------------------------------------------------------//
//		SendAvatarInfoEnd
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarInfoEnd(CLkPacket * pPacket)
{
	printf("--- SendAvatarInfoEnd --- \n");
	CLkPacket packet(sizeof(sGU_AVATAR_INFO_END));
	sGU_AVATAR_INFO_END * res = (sGU_AVATAR_INFO_END *)packet.GetPacketData();

	res->wOpCode = GU_AVATAR_INFO_END;
	packet.SetPacketLen(sizeof(sGU_AVATAR_INFO_END));
	g_pApp->Send(this->GetHandle(), &packet);
}

//--------------------------------------------------------------------------------------//
//		Login into World
//--------------------------------------------------------------------------------------//
void CClientSession::SendWorldEnterReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- sGU_AVATAR_WORLD_INFO --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_AVATAR_WORLD_INFO));
	sGU_AVATAR_WORLD_INFO * res = (sGU_AVATAR_WORLD_INFO *)packet.GetPacketData();

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	app->db->fetch();

	res->wOpCode = GU_AVATAR_WORLD_INFO;
	//Uncomment this if lines to see the first tutorial...not working for now this method above is wrong...we need get from database - Luiz45
	/*if (app->db->getInt("TutorialFlag") == 1)
	{*/
		res->worldInfo.tblidx = app->db->getInt("WorldTable");
		res->worldInfo.worldID = app->db->getInt("WorldID");
		res->worldInfo.hTriggerObjectOffset = 100000;
		res->worldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
		res->vCurLoc.x = plr->GetPlayerPosition().x;
		res->vCurLoc.y = plr->GetPlayerPosition().y;
		res->vCurLoc.z = plr->GetPlayerPosition().z;
		res->vCurDir.x = plr->GetPlayerDirection().x;
		res->vCurDir.y = plr->GetPlayerDirection().y;
		res->vCurDir.z = plr->GetPlayerDirection().z;
		plr->SetWorldID(app->db->getInt("WorldID"));
		plr->SetWorldTblidx(app->db->getInt("WorldTable"));
	//}
	//else
	//{
	//	CNewbieTable* pNewBieTable = app->g_pTableContainer->GetNewbieTable();
	//	sNEWBIE_TBLDAT* pNewbieTbldat = reinterpret_cast<sNEWBIE_TBLDAT*>(pNewBieTable->GetNewbieTbldat(app->db->getInt("Race"), app->db->getInt("Class")));
	//	res->worldInfo.tblidx = pNewbieTbldat->tutorialWorld;
	//	res->worldInfo.worldID = pNewbieTbldat->world_Id;
	//	res->worldInfo.hTriggerObjectOffset = 100000;
	//	res->worldInfo.sRuleInfo.byRuleType = GAMERULE_TUTORIAL;
	//	//Hard Coded
	//	res->vCurLoc.x = (-1)*78.90;
	//	res->vCurLoc.y = 46.95;
	//	res->vCurLoc.z = (-1) * 168.35;
	//	res->vCurDir.x = (-1) * 0.95;
	//	res->vCurDir.y = 0;
	//	res->vCurDir.z = 0.30;
	//	plr->SetWorldID(pNewbieTbldat->world_Id);
	//	plr->SetWorldTblidx(pNewbieTbldat->tutorialWorld);
	//	plr->SetPlayerIsInTutorial(true);
	//}

	packet.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
	g_pApp->Send(this->GetHandle(), &packet);

	plr = NULL;
	delete plr;

}
//--------------------------------------------------------------------------------------//
//		Character ready request
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharReadyReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- sGU_OBJECT_CREATE --- \n");
	//SPAN PLAYERS
	CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	app->db->fetch();

	wcscpy_s(plr->GetPcProfile()->awchName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(plr->GetPlayerName()).c_str());

	CPCTable *pPcTable = app->g_pTableContainer->GetPcTable();
	sPC_TBLDAT *pTblData = (sPC_TBLDAT*)pPcTable->GetPcTbldat(app->db->getInt("Race"), app->db->getInt("Class"), app->db->getInt("Gender"));

	res->wOpCode = GU_OBJECT_CREATE;
	res->handle = this->GetavatarHandle();
	res->sObjectInfo.objType = OBJTYPE_PC;
	res->sObjectInfo.pcBrief.tblidx = pTblData->tblidx;
	res->sObjectInfo.pcBrief.bIsAdult = plr->GetPcProfile()->bIsAdult;
	wcscpy_s(res->sObjectInfo.pcBrief.awchName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(plr->GetPlayerName()).c_str());
	wcscpy_s(res->sObjectInfo.pcBrief.wszGuildName, LK_MAX_SIZE_GUILD_NAME_IN_UNICODE, s2ws(plr->GetGuildName()).c_str());
	res->sObjectInfo.pcBrief.charId = plr->GetCharID();
	res->sObjectInfo.pcBrief.sPcShape = plr->GetPcProfile()->sPcShape;
	res->sObjectInfo.pcBrief.wCurLP = plr->GetPcProfile()->wCurLP;
	res->sObjectInfo.pcBrief.wMaxLP = plr->GetPcProfile()->avatarAttribute.wBaseMaxLP;
	res->sObjectInfo.pcBrief.wCurEP = plr->GetPcProfile()->wCurEP;
	res->sObjectInfo.pcBrief.wMaxEP = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP;
	res->sObjectInfo.pcBrief.byLevel = plr->GetPcProfile()->byLevel;
	res->sObjectInfo.pcBrief.fSpeed = plr->GetPcProfile()->avatarAttribute.fLastRunSpeed;
	res->sObjectInfo.pcBrief.wAttackSpeedRate = plr->GetPcProfile()->avatarAttribute.wBaseAttackSpeedRate;
	res->sObjectInfo.pcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;

	for (int i = 0; i < LK_MAX_EQUIP_ITEM_SLOT; i++)
	{
		if ((plr->cPlayerInventory->GetEquippedItems()[i].tblidx != 0) || (plr->cPlayerInventory->GetEquippedItems()[i].tblidx != INVALID_TBLIDX))
		{
			res->sObjectInfo.pcBrief.sItemBrief[i].tblidx  = plr->cPlayerInventory->GetEquippedItems()[i].tblidx;
			res->sObjectInfo.pcBrief.sItemBrief[i].byGrade = plr->cPlayerInventory->GetEquippedItems()[i].byGrade;
			res->sObjectInfo.pcBrief.sItemBrief[i].byRank  = plr->cPlayerInventory->GetEquippedItems()[i].byRank;
			res->sObjectInfo.pcBrief.sItemBrief[i].byBattleAttribute = plr->cPlayerInventory->GetEquippedItems()[i].byBattleAttribute;
			res->sObjectInfo.pcBrief.sItemBrief[i].aOptionTblidx[0] = plr->cPlayerInventory->GetEquippedItems()[i].aOptionTblidx[0];
			res->sObjectInfo.pcBrief.sItemBrief[i].aOptionTblidx[1] = plr->cPlayerInventory->GetEquippedItems()[i].aOptionTblidx[1];			
		}
		else
		{
			res->sObjectInfo.pcBrief.sItemBrief[i].tblidx = INVALID_TBLIDX;
		}
	}

	//Lets update our Character Attributes with equiped scout values ^^
	//Maybe this way is wrong
	for (int i = 0; i < plr->cPlayerInventory->GetTotalItemsCount(); i++)
	{
		//If is a Scout Chip then lets update our character attribute		
		if (plr->cPlayerInventory->GetInventory()[i].byPlace == CONTAINER_TYPE_SCOUT)
		{
			sITEM_TBLDAT* pItemDat = reinterpret_cast<sITEM_TBLDAT*>(app->g_pTableContainer->GetItemTable()->FindData(plr->cPlayerInventory->GetInventory()[i].tblidx));
			plr->cPlayerAttribute->UpdateStatsUsingScouterChips(plr->GetAvatarHandle(), pItemDat->Item_Option_Tblidx);
			//And attach in our EquippedChips Array
			for (int i = 0; i < 4; i++)
			{
				if (plr->GetEquipedChips()[i] == INVALID_TBLIDX)
				{
					plr->GetEquipedChips()[i] = pItemDat->Item_Option_Tblidx;
					break;
				}
			}
		}
		else if (plr->cPlayerInventory->GetInventory()[i].byPlace == CONTAINER_TYPE_EQUIP)
		{
			plr->UpdateBaseAttributeWithEquip(plr->cPlayerInventory->GetInventory()[i].tblidx, plr->cPlayerInventory->GetInventory()[i].byRank, plr->cPlayerInventory->GetInventory()[i].byGrade);
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		}
	}

	memcpy(&this->characterspawnInfo, res, sizeof(sGU_OBJECT_CREATE));
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->UserBroadcastothers(&packet, this);
	app->UserBroadcasFromOthers(GU_OBJECT_CREATE, this);
	app->AddUser(plr->GetPlayerName().c_str(), this);

	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Auth community Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendAuthCommunityServer(CLkPacket * pPacket, CGameServer * app)
{

	CLkPacket packet(sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES));
	sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES * res = (sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES *)packet.GetPacketData();

	res->wOpCode = GU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s((char*)res->abyAuthKey, LK_MAX_SIZE_AUTH_KEY, "ChatCon");
	packet.SetPacketLen(sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}

//--------------------------------------------------------------------------------------//
//		SPAWN NPC
//--------------------------------------------------------------------------------------//
void CClientSession::SendNpcCreate(CLkPacket * pPacket, CGameServer * app)
{
	//	g_pMobManager->SpawnNpcAtLogin(pPacket, this);
}
//--------------------------------------------------------------------------------------//
//		SPAWN MOBS
//--------------------------------------------------------------------------------------//
void CClientSession::SendMonsterCreate(CLkPacket * pPacket, CGameServer * app)
{
	//	g_pMobManager->SpawnMonsterAtLogin(pPacket, this);
}
//--------------------------------------------------------------------------------------//
//		SendEnterWorldComplete
//--------------------------------------------------------------------------------------//
void CClientSession::SendEnterWorldComplete(CLkPacket * pPacket)
{
	printf("--- SendEnterWorldComplete --- \n");

	CLkPacket packet(sizeof(sGU_ENTER_WORLD_COMPLETE));
	sGU_ENTER_WORLD_COMPLETE * res = (sGU_ENTER_WORLD_COMPLETE *)packet.GetPacketData();

	res->wOpCode = GU_ENTER_WORLD_COMPLETE;

	packet.SetPacketLen(sizeof(sGU_ENTER_WORLD_COMPLETE));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

	CLkPacket packet2(sizeof(sGU_AVATAR_RP_DECREASE_START_NFY));
	sGU_AVATAR_RP_DECREASE_START_NFY * res2 = (sGU_AVATAR_RP_DECREASE_START_NFY *)packet2.GetPacketData();
	res2->wOpCode = GU_AVATAR_RP_DECREASE_START_NFY;
	packet2.SetPacketLen(sizeof(sGU_ENTER_WORLD_COMPLETE));
	g_pApp->Send(this->GetHandle(), &packet2);
}

//--------------------------------------------------------------------------------------//
//		Tutorial Hint request
//--------------------------------------------------------------------------------------//
void CClientSession::SendTutorialHintReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TUTORIAL_HINT_UPDATE_REQ * req = (sUG_TUTORIAL_HINT_UPDATE_REQ *)pPacket->GetPacketData();
	//req->dwTutorialHint;
	printf("--- TUTORIAL HINT REQUEST %i --- \n", req->dwTutorialHint);

	CLkPacket packet(sizeof(sGU_TUTORIAL_HINT_UPDATE_RES));
	sGU_TUTORIAL_HINT_UPDATE_RES * res = (sGU_TUTORIAL_HINT_UPDATE_RES *)packet.GetPacketData();

	//app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	//app->db->setInt(1, this->characterID);
	//app->db->execute();
	//app->db->fetch();
	res->wOpCode = GU_TUTORIAL_HINT_UPDATE_RES;
	res->wResultCode = GAME_SUCCESS;
	res->dwTutorialHint = req->dwTutorialHint;

	packet.SetPacketLen(sizeof(sGU_TUTORIAL_HINT_UPDATE_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}

//--------------------------------------------------------------------------------------//
//		Char Ready
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharReady(CLkPacket * pPacket)
{
	printf("--- SEND CHAR READY --- \n");

	CLkPacket packet(sizeof(sUG_CHAR_READY));
	sUG_CHAR_READY * res = (sUG_CHAR_READY *)packet.GetPacketData();

	res->wOpCode = UG_CHAR_READY;
	res->byAvatarType = 0;

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	plr->SetSkillCheck(true);

	packet.SetPacketLen(sizeof(sUG_CHAR_READY));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Char Move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMove(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- SEND CHAR MOVE --- \n");

	sUG_CHAR_MOVE * req = (sUG_CHAR_MOVE*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_CHAR_MOVE));
	sGU_CHAR_MOVE * res = (sGU_CHAR_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_MOVE;
	res->handle = this->GetavatarHandle();
	res->dwTimeStamp = req->dwTimeStamp;
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = 0;
	res->vCurDir.z = req->vCurDir.z;
	res->byMoveDirection = req->byMoveDirection;
	res->byMoveFlag = LK_MOVE_KEYBOARD_FIRST;

	plr->SetPlayerLastDirection(plr->GetPlayerDirection());
	plr->SetPlayerLastPosition(plr->GetPlayerPosition());
	plr->SetPlayerPosition(res->vCurLoc);
	plr->SetPlayerDirection(res->vCurDir);

	packet.SetPacketLen(sizeof(sGU_CHAR_MOVE));

	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Char Destination Move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharDestMove(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER REQUEST DEST MOVE --- \n");

	sUG_CHAR_DEST_MOVE * req = (sUG_CHAR_DEST_MOVE*)pPacket->GetPacketData();

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_CHAR_DEST_MOVE));
	sGU_CHAR_DEST_MOVE * res = (sGU_CHAR_DEST_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_DEST_MOVE;
	res->handle = this->GetavatarHandle();
	res->dwTimeStamp = req->dwTimeStamp;
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->byMoveFlag = LK_MOVE_MOUSE_MOVEMENT;
	res->bHaveSecondDestLoc = false;
	res->byDestLocCount = 1;
	res->avDestLoc[0].x = req->vDestLoc.x;
	res->avDestLoc[0].y = req->vDestLoc.y;
	res->avDestLoc[0].z = req->vDestLoc.z;

	packet.SetPacketLen(sizeof(sGU_CHAR_DEST_MOVE));
	app->UserBroadcastothers(&packet, this);
	if ((plr->GetMob_SpawnTime() - timeGetTime()) >= 30*SECOND)
	g_pMobManager->RunSpawnCheck(pPacket, plr->GetPlayerPosition(), plr->myCCSession);

	plr = NULL;
	delete plr;

}
//--------------------------------------------------------------------------------------//
//		Char Move Sync
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMoveSync(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER MOVE SYNC --- \n");
	sUG_CHAR_MOVE_SYNC * req = (sUG_CHAR_MOVE_SYNC*)pPacket->GetPacketData();

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_CHAR_MOVE_SYNC));
	sGU_CHAR_MOVE_SYNC * res = (sGU_CHAR_MOVE_SYNC *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_MOVE_SYNC;
	res->handle = this->GetavatarHandle();
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = req->vCurDir.y;
	res->vCurDir.z = req->vCurDir.z;

	packet.SetPacketLen(sizeof(sGU_CHAR_MOVE_SYNC));
	app->UserBroadcastothers(&packet, this);

	plr->SetPlayerLastDirection(plr->GetPlayerDirection());
	plr->SetPlayerLastPosition(plr->GetPlayerPosition());
	plr->SetPlayerPosition(res->vCurLoc);
	plr->SetPlayerDirection(res->vCurDir);

	if ( (plr->GetMob_SpawnTime() - timeGetTime()) >= 30 * SECOND)
		g_pMobManager->RunSpawnCheck(pPacket, plr->GetPlayerPosition(), plr->myCCSession);

	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Char Change Heading
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharChangeHeading(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER CHANGE HEADING --- \n");
	sUG_CHAR_CHANGE_HEADING * req = (sUG_CHAR_CHANGE_HEADING*)pPacket->GetPacketData();


	CLkPacket packet(sizeof(sGU_CHAR_CHANGE_HEADING));
	sGU_CHAR_CHANGE_HEADING * res = (sGU_CHAR_CHANGE_HEADING *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_CHANGE_HEADING;
	res->handle = this->GetavatarHandle();
	res->vNewHeading.x = req->vCurrentHeading.x;
	res->vNewHeading.y = req->vCurrentHeading.y;
	res->vNewHeading.z = req->vCurrentHeading.z;
	res->vNewLoc.x = req->vCurrentPosition.x;
	res->vNewLoc.y = req->vCurrentPosition.y;
	res->vNewLoc.z = req->vCurrentPosition.z;

	packet.SetPacketLen(sizeof(sGU_CHAR_CHANGE_HEADING));
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char Jump
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharJump(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER JUMP --- \n");
	sUG_CHAR_JUMP * req = (sUG_CHAR_JUMP*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_CHAR_JUMP));
	sGU_CHAR_JUMP * res = (sGU_CHAR_JUMP *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_JUMP;
	res->handle = this->GetavatarHandle();
	res->vCurrentHeading.x = req->vCurrentHeading.x;
	res->vCurrentHeading.y = req->vCurrentHeading.y;
	res->vCurrentHeading.z = req->vCurrentHeading.z;

	res->vJumpDir.x = 0;
	res->vJumpDir.y = 0;
	res->vJumpDir.z = 0;

	res->byMoveDirection = 1;
	plr->SetPlayerFight(false);

	packet.SetPacketLen(sizeof(sGU_CHAR_JUMP));
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;

}
//--------------------------------------------------------------------------------------//
//		Change Char Direction on floating
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharChangeDirOnFloating(CLkPacket * pPacket, CGameServer * app)
{
	//	printf("--- Change Char Direction on floating --- \n");
	sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING * req = (sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING));
	sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING * res = (sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_CHANGE_DIRECTION_ON_FLOATING;
	res->hSubject = this->GetavatarHandle();
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = req->vCurDir.y;
	res->vCurDir.z = req->vCurDir.z;
	res->byMoveDirection = req->byMoveDirection;

	packet.SetPacketLen(sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING));
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char falling
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharFalling(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- character falling --- \n");
	sUG_CHAR_FALLING * req = (sUG_CHAR_FALLING*)pPacket->GetPacketData();

	req->wOpCode = UG_CHAR_FALLING;
	req->bIsFalling = true;

	req->vCurLoc.x;
	req->vCurLoc.y;
	req->vCurLoc.z;
	req->vCurDir.x;
	req->vCurDir.z;
	req->byMoveDirection;

}

//--------------------------------------------------------------------------------------//
//		GM Command
//--------------------------------------------------------------------------------------//
void CClientSession::RecvServerCommand(CLkPacket * pPacket, CGameServer * app)
{
	sUG_SERVER_COMMAND * pServerCmd = (sUG_SERVER_COMMAND*)pPacket->GetPacketData();

	char chBuffer[1024];
	::WideCharToMultiByte(GetACP(), 0, pServerCmd->awchCommand, -1, chBuffer, 1024, NULL, NULL);

	CLkTokenizer lexer(chBuffer);

	if (!lexer.IsSuccess())
		return;

	enum ECmdParseState
	{
		SERVER_CMD_NONE,
		SERVER_CMD_KEY,
		SERVER_CMD_END,
	};

	ECmdParseState eState = SERVER_CMD_KEY;
	int iOldLine = 0;
	int iLine;

	while (1)
	{
		std::string strToken = lexer.PeekNextToken(NULL, &iLine);
		if (strToken == "")
			break;

		switch (eState)
		{
		case SERVER_CMD_KEY:
			if (strToken == "@setspeed")
			{
				printf("received char speed command");
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				float fSpeed = (float)atof(strToken.c_str());
				CClientSession::SendUpdateCharSpeed(fSpeed, app);
				return;
			}
			else if (strToken == "@addmob")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiMobId = (unsigned int)atoi(strToken.c_str());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				float fDist = (float)atof(strToken.c_str());
				lexer.PopToPeek();
				printf("Executing Mob Func\n");
				this->CreateMonsterById(uiMobId, (unsigned int)fDist);
				printf("Executed\n");
				return;
			}
			else if (strToken == "@addmobg")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int iNum = (unsigned int)atoi(strToken.c_str());
				//SendMonsterGroupCreate(iNum);
				return;
			}
			else if (strToken == "@createitem")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
				this->CreateItemById(uiTblId);
				return;
			}
			else if (strToken == "@learnskill")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
				cout << "Add Skill tblidx " << uiTblId << endl;
				CClientSession::AddSkillById(uiTblId);
				return;
			}
			else if (strToken == "@learnhtb")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
				//	SendCharLearnHTBRes(uiTblId);
				return;
			}
			else if (strToken == "@recover")
			{
				PlayersMain* plr = g_pPlayerManager->GetPlayer(this->avatarHandle);
				plr->GetPcProfile()->wCurLP = plr->GetPcProfile()->avatarAttribute.wLastMaxLP;
				plr->GetPcProfile()->wCurEP = plr->GetPcProfile()->avatarAttribute.wLastMaxEP;
				return;
			}
			else if (strToken == "@levelup")
			{
				PlayersMain* plr = g_pPlayerManager->GetPlayer(this->avatarHandle);
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiLevel = (unsigned int)atof(strToken.c_str());
				//this->SendPlayerLevelUpCheck(app)
				//plr->GetPcProfile()->byLevel = plr->GetPcProfile()->avatarAttribute.wLastMaxEP;
				return;
			}
			else if (strToken == "@directplay")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblidx = (unsigned int)atoi(strToken.c_str());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				bool sync = (bool)atoi(strToken.c_str());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				int playType = (int)atoi(strToken.c_str());
				lexer.PopToPeek();
				printf("Test Direct play\n");
				cout << uiTblidx << " " << sync << " " << playType << endl;
				this->SendTestDirectPlay(uiTblidx, sync, playType);
				printf("Executed\n");
				return;
			}
			else if (strToken == "@announce")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				int announceType = (int)atoi(strToken.c_str());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				wstring wszMsg;
				wszMsg.assign(strToken.begin(), strToken.end());
				lexer.PopToPeek();
				printf("Send Announcement\n");
				wcout << wszMsg << endl;
				this->SendServerAnnouncement(announceType, wszMsg);
				printf("Executed\n");
				return;
			}
			else if (strToken == "@setstats")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				wstring wszAttribute;
				wszAttribute.assign(strToken.begin(), strToken.end());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				int iAmount = (int)atoi(strToken.c_str());
				lexer.PopToPeek();
				printf("Send setstats\n");
				this->UpdateStats(wszAttribute, iAmount);
				printf("Executed\n");
				return;
			}
			else if (strToken == "@teleport")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				int iAmount = (int)atoi(strToken.c_str());
				lexer.PopToPeek();
				CPortalTable* myPortalTbl = app->g_pTableContainer->GetPortalTable();
				sPORTAL_TBLDAT* pPortalTblData = reinterpret_cast<sPORTAL_TBLDAT*>(myPortalTbl->FindData(iAmount));
				if (pPortalTblData)
				{
					CLkPacket packetTeleport(sizeof(sGU_AVATAR_WORLD_INFO));
					sGU_AVATAR_WORLD_INFO* teleport = (sGU_AVATAR_WORLD_INFO*)packetTeleport.GetPacketData();

					teleport->worldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
					teleport->worldInfo.hTriggerObjectOffset = 100000;
					teleport->worldInfo.tblidx = 1;
					teleport->worldInfo.worldID = 1;
					teleport->vCurDir.x = pPortalTblData->vDir.x;
					teleport->vCurDir.y = pPortalTblData->vDir.y;
					teleport->vCurDir.z = pPortalTblData->vDir.z;
					teleport->vCurLoc.x = pPortalTblData->vLoc.x;
					teleport->vCurLoc.y = pPortalTblData->vLoc.y;
					teleport->vCurLoc.z = pPortalTblData->vLoc.z;
					teleport->wOpCode = GU_AVATAR_WORLD_INFO;

					packetTeleport.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
					g_pApp->Send(this->GetHandle(), &packetTeleport);
					return;
				}
			}
			else if (strToken == "@teleportto")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				std::string charName = strToken;
				lexer.PopToPeek();

				app->db->prepare("SELECT * from characters where CharName = ?");
				app->db->setString(1, charName);
				app->db->execute();

				PlayersMain* plrTgt = g_pPlayerManager->GetPlayerByID(app->db->getInt("CharID"));
				if (plrTgt->GetAvatarHandle() != INVALID_TBLIDX)
				{
					CLkPacket packetTeleport(sizeof(sGU_AVATAR_WORLD_INFO));
					sGU_AVATAR_WORLD_INFO* teleport = (sGU_AVATAR_WORLD_INFO*)packetTeleport.GetPacketData();

					teleport->worldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
					teleport->worldInfo.hTriggerObjectOffset = 100000;
					teleport->worldInfo.tblidx = 1;
					teleport->worldInfo.worldID = 1;
					teleport->vCurDir = plrTgt->GetPlayerDirection();
					teleport->vCurLoc = plrTgt->GetPlayerPosition();
					teleport->wOpCode = GU_AVATAR_WORLD_INFO;

					packetTeleport.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
					g_pApp->Send(this->GetHandle(), &packetTeleport);
					return;

					this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"Player Not Online!");
					return;
				}
			}
			else if (strToken == "@learnallskills")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
				cout << "Add Skill tblidx " << uiTblId << endl;
				lexer.PopToPeek();
				CClientSession::AddSkillById(uiTblId);
				return;
			}
			else if (strToken == "@setmarking")
			{
				PlayersMain* plr = g_pPlayerManager->GetPlayer(this->avatarHandle);
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int sMarking = (unsigned int)atof(strToken.c_str());

				plr->GetPcProfile()->sMarking.byCode = sMarking;
				return;
			}
			break;

			lexer.PopToPeek();
		}
	}
}
//--------------------------------------------------------------------------------------//
//		Update Char speed 
//--------------------------------------------------------------------------------------//
void CClientSession::SendUpdateCharSpeed(float fSpeed, CGameServer * app)
{
	printf("Update char speed \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_SPEED));
	sGU_UPDATE_CHAR_SPEED * res = (sGU_UPDATE_CHAR_SPEED *)packet.GetPacketData();

	res->wOpCode = GU_UPDATE_CHAR_SPEED;
	res->handle = plr->GetAvatarHandle();
	res->fLastWalkingSpeed = fSpeed;
	res->fLastRunningSpeed = fSpeed;

	plr->GetPcProfile()->avatarAttribute.fLastRunSpeed = fSpeed;
	plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SPEED));
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		@Admin Learn Skill - Kalisto 
//--------------------------------------------------------------------------------------//
void CClientSession::AddSkillById(uint32_t tblidx)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CSkillTable* pSkillTable = app->g_pTableContainer->GetSkillTable();
	int iSkillCount = this->gsf->GetTotalSlotSkill(plr->GetCharID());//you are still using "this" but "this" is null this is why you get the error #Issue 6
	
	sSKILL_TBLDAT* pSkillData = (sSKILL_TBLDAT*)pSkillTable->FindData(tblidx);
	if (pSkillData != NULL)
	{
		if (app->qry->CheckIfSkillAlreadyLearned(pSkillData->tblidx, plr->GetCharID()) == false)
		{
			CLkPacket packet2(sizeof(sGU_SKILL_LEARNED_NFY));
			sGU_SKILL_LEARNED_NFY * res2 = (sGU_SKILL_LEARNED_NFY *)packet2.GetPacketData();
			iSkillCount++;//don't send this directly if you learned SSJ first...because we always are sending 1...nevermind :P
			res2->wOpCode = GU_SKILL_LEARNED_NFY;
			res2->skillId = pSkillData->tblidx;
			res2->bySlot = iSkillCount;

			app->qry->InsertNewSkill(pSkillData->tblidx, plr->GetCharID(), iSkillCount, pSkillData->wKeep_Time, pSkillData->wNext_Skill_Train_Exp);
			packet2.SetPacketLen(sizeof(sGU_SKILL_LEARNED_NFY));
			g_pApp->Send(this->GetHandle(), &packet2);

			CLkPacket packet(sizeof(sGU_SKILL_LEARN_RES));
			sGU_SKILL_LEARN_RES * res = (sGU_SKILL_LEARN_RES *)packet.GetPacketData();

			res->wOpCode = GU_SKILL_LEARN_RES;
			res->wResultCode = 500;

			packet.SetPacketLen(sizeof(sGU_SKILL_LEARN_RES));
			g_pApp->Send(this->GetHandle(), &packet);
			
		}
		else
			this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"Player already knows skill");
	}
	else
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"No skill with that ID exists");
}

//--------------------------------------------------------------------------------------//
//		Test DirectPlay Scripts -- Admin Function
//--------------------------------------------------------------------------------------//

void CClientSession::SendTestDirectPlay(uint32_t tblidx, bool sync, int playType)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packetAck(sizeof(sGU_CHAR_DIRECT_PLAY));
	sGU_CHAR_DIRECT_PLAY* res = (sGU_CHAR_DIRECT_PLAY*)packetAck.GetPacketData();

	CDirectionLinkTable* pLinkTbl = app->g_pTableContainer->GetDirectionLinkTable();
	sDIRECTION_LINK_TBLDAT *pLinkTblData = reinterpret_cast<sDIRECTION_LINK_TBLDAT*>(pLinkTbl->FindData(tblidx));
	if (pLinkTblData != NULL)
	{
		res->bSynchronize = sync;
		res->byPlayMode = playType;
		res->directTblidx = pLinkTblData->tblidx;
		res->hSubject = plr->GetAvatarHandle();
		res->wOpCode = GU_CHAR_DIRECT_PLAY;

		packetAck.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY));
		g_pApp->Send(this->GetHandle(), &packetAck);
	}
	else
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"No Script with that TblIdx exists");

}

//---------------------------------------------
//---Send Notice Admin - Kalisto
//---------------------------------------------
void CClientSession::SendServerAnnouncement(int type, wstring wsMsg)
{
	CLkPacket packet(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sGU_SYSTEM_DISPLAY_TEXT* sNotice = (sGU_SYSTEM_DISPLAY_TEXT*)packet.GetPacketData();
	CGameServer * app = (CGameServer*)LkSfxGetApp();

	WCHAR wcsMsg[BUDOKAI_MAX_NOTICE_LENGTH + 1];
	char ch[256];
	::ZeroMemory(ch, sizeof(ch));
	char DefChar = '\0';
	WideCharToMultiByte(CP_ACP, 0, wsMsg.c_str(), -1, ch, 256, &DefChar, NULL);
	mbstowcs(wcsMsg, ch, 255);

	sNotice->wOpCode = GU_SYSTEM_DISPLAY_TEXT;
	if (type > 3)
		type = 3;

	sNotice->byDisplayType = type;
	wcscpy_s(sNotice->awchMessage, BUDOKAI_MAX_NOTICE_LENGTH + 1, wcsMsg);
	if (type == 3)
	wcscpy_s(sNotice->awGMChar, LK_MAX_SIZE_CHAR_NAME_UNICODE, plr->GetPcProfile()->awchName);
	else
		wcscpy_s(sNotice->awGMChar, LK_MAX_SIZE_CHAR_NAME_UNICODE, L"AKCore Admin"
		);
	sNotice->wMessageLengthInUnicode = (WORD)wcslen(wcsMsg);
	packet.SetPacketLen(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	app->UserBroadcast(&packet);
}
//---------------------------------------------
//---Create Item by Tblidx - Kalisto
//---------------------------------------------
void CClientSession::CreateItemById(uint32_t tblidx)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CItemTable* pMyItemTbl = app->g_pTableContainer->GetItemTable();
	sITEM_TBLDAT* item = reinterpret_cast<sITEM_TBLDAT*>(pMyItemTbl->FindData(tblidx));
	if (item != NULL)
	{
		GsFunctionsClass* gsFunction = new GsFunctionsClass();//I dont believe that leave this way because the other way is wrong
		gsFunction->CreateUpdateItem(plr, 1, tblidx, false, plr->GetSession());
		gsFunction = NULL;
		delete gsFunction;
	}
	else
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"No such itemID");;
}
//---------------------------------------------
//---Create Monster by Tblidx Admin - Kalisto  NOTE this function is only for show. You can't actually attack or scan mobs they just explode.
//---------------------------------------------
void  CClientSession::CreateMonsterById(unsigned int uiMobId, unsigned int distance)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sVECTOR3 curpos = plr->GetPlayerPosition();
	sVECTOR3 curdir = plr->GetPlayerDirection();
	CMobTable* pMyMobTable = app->g_pTableContainer->GetMobTable();
	sMOB_TBLDAT* mob = reinterpret_cast<sMOB_TBLDAT*>(pMyMobTable->FindData(uiMobId));
	if (mob != NULL)
	{
		CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
		sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();

		res->wOpCode = GU_OBJECT_CREATE;
		res->sObjectInfo.objType = OBJTYPE_MOB;
		res->handle = g_pMobManager->CreateUniqueId();//app->mob->AcquireMOBSerialId() this will get your Player Handle,need change "AcquireSerialId" because here is used to generate a Handler for the players! #Issue 6 Luiz45 // Fixed - Kalisto
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = curpos.x +distance;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = curpos.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = curpos.z;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = curpos.x;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = curpos.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = curpos.z;
		res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
		res->sObjectInfo.mobState.sCharStateBase.bFightMode = false;
		res->sObjectInfo.mobBrief.tblidx = uiMobId;
		res->sObjectInfo.mobBrief.wCurEP = mob->wBasic_EP;
		res->sObjectInfo.mobBrief.wMaxEP = mob->wBasic_EP;
		res->sObjectInfo.mobBrief.wCurLP = mob->wBasic_LP;
		res->sObjectInfo.mobBrief.wMaxLP = mob->wBasic_LP;
		res->sObjectInfo.mobBrief.fLastRunningSpeed = mob->fRun_Speed;
		res->sObjectInfo.mobBrief.fLastWalkingSpeed = mob->fWalk_Speed;
		
		CMonster::MonsterData * cr = new CMonster::MonsterData;
		cr->Level = mob->byLevel;
		cr->CurEP = mob->wBasic_EP;
		cr->CurLP = mob->wBasic_LP;
		cr->FightMode = false;
		cr->IsDead = false;
		cr->isSpawned = true;
		cr->MonsterID = mob->tblidx;
		cr->Spawn_Loc = curpos;
		cr->Spawn_Dir = curdir;
		cr->MaxEP = mob->wBasic_EP;
		cr->MaxLP = mob->wBasic_LP;
		cr->Move_DelayTime = 100;
		cr->Run_Speed = mob->fRun_Speed;
		cr->Run_Speed_origin = mob->fRun_Speed_Origin;
		cr->Walk_Speed = mob->fWalk_Speed;
		cr->Walk_Speed_origin = mob->fWalk_Speed_Origin;
		cr->Spawn_Cool_Time = 100000 * 1000;
		cr->target = 0;
		cr->curPos = cr->Spawn_Loc;
		cr->Basic_aggro_point = mob->byScan_Range;
		cr->Attack_range = mob->fAttack_Range;
		cr->MaxchainAttackCount = mob->byAttack_Animation_Quantity;
		cr->chainAttackCount = 1;
		cr->UniqueID = res->handle;
		g_pMobManager->m_map_Monster.insert(std::make_pair(cr->UniqueID, cr));
		
		packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
		g_pApp->Send(plr->GetSession(), &packet);
	}
	else
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"No Mob Exists with that Tblidx");;
}
//---------------------------------------------
//---Update Attribute Admin - Kalisto
//---------------------------------------------
void CClientSession::UpdateStats(wstring wszString, int iAmount)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	switch (this->gsf->atrributeTokenizer(wszString))
	{
		case eSTR:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseStr = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastStr = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eCON:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseCon = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastCon = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eFOC:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseFoc = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastFoc = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eDEX:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseDex = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastDex = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eSOL:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseSol = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastSol = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eENG:
		{
			if (iAmount > 255)
				iAmount = 255;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.byBaseEng = iAmount;
			plr->GetPcProfile()->avatarAttribute.byLastEng = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eLP:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wBaseMaxLP = iAmount;
			plr->GetPcProfile()->avatarAttribute.wLastMaxLP = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case eEP:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wBaseMaxEP = iAmount;
			plr->GetPcProfile()->avatarAttribute.wLastMaxEP = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
			//CClientSession::CheckPlayerStat(app, NULL, plr->GetPcProfile()->byLevel, plr->GetAvatarHandle(), true);
		}
			break;
		case ePHYSOFF:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wBasePhysicalOffence = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		}
			break;
		case eENGOFF:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wLastEnergyOffence = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		}
			break;
		case ePHYSDEF:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wBasePhysicalDefence = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		}
			break;
		case eENGDEF:
		{
			if (iAmount > 20000)
				iAmount = 20000;
			else if (iAmount < 0)
				iAmount = 0;
			plr->GetPcProfile()->avatarAttribute.wLastEnergyDefence = iAmount;
			plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		}
			break;
		default:
		{
			break;
		}

	}
	if (this->gsf->atrributeTokenizer(wszString) == eINVALIDSTAT)
	{
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"Invalid Stat. Stats not updated.");
	}
	else
		this->gsf->ErrorHandler(this, SERVER_TEXT_NOTICE, L"Stats Updated");
}
void CClientSession::SendPlayerLevelUpCheck(CGameServer * app, int exp)
{
	/*CExpTable *expT = app->g_pTableContainer->GetExpTable();
	for ( CTable::TABLEIT itNPCSpawn = expT->Begin(); itNPCSpawn != expT->End(); ++itNPCSpawn )
	{
	sEXP_TBLDAT *expTbl = (sEXP_TBLDAT *)itNPCSpawn->second;
	printf("%d, %d\n",expTbl->dwExp, expTbl->dwNeed_Exp);
	}*/
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_EXP));
	sGU_UPDATE_CHAR_EXP * response = (sGU_UPDATE_CHAR_EXP*)packet.GetPacketData();

	response->dwIncreasedExp = (exp*app->dExpMultiplier) + rand() % plr->GetPcProfile()->byLevel * 2;
	plr->GetPcProfile()->dwCurExp += response->dwIncreasedExp;
	response->dwAcquisitionExp = exp;
	response->dwCurExp = plr->GetPcProfile()->dwCurExp;
	response->dwBonusExp = ((exp*app->dExpMultiplier) - response->dwIncreasedExp);
	response->handle = plr->GetAvatarHandle();
	response->wOpCode = GU_UPDATE_CHAR_EXP;
	if (plr->GetPcProfile()->dwCurExp >= plr->GetPcProfile()->dwMaxExpInThisLevel)
	{
		CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_SP));
		sGU_UPDATE_CHAR_SP * res2 = (sGU_UPDATE_CHAR_SP *)packet2.GetPacketData();
		plr->GetPcProfile()->dwCurExp -= plr->GetPcProfile()->dwMaxExpInThisLevel;
		plr->GetPcProfile()->dwMaxExpInThisLevel += (plr->GetPcProfile()->dwMaxExpInThisLevel / 2);
		CLkPacket packet1(sizeof(sGU_UPDATE_CHAR_LEVEL));
		sGU_UPDATE_CHAR_LEVEL * response1 = (sGU_UPDATE_CHAR_LEVEL*)packet1.GetPacketData();
		plr->GetPcProfile()->byLevel++;
		response1->byCurLevel = plr->GetPcProfile()->byLevel;
		response1->byPrevLevel = plr->GetPcProfile()->byLevel - 1;
		response1->dwMaxExpInThisLevel = plr->GetPcProfile()->dwMaxExpInThisLevel;
		response1->handle = plr->GetAvatarHandle();
		response1->wOpCode = GU_UPDATE_CHAR_LEVEL;
		packet1.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LEVEL));
		g_pApp->Send(this->GetHandle(), &packet1);
		plr->SetLevelUP();
		plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
		plr->GetPcProfile()->dwSpPoint += 1;
		app->qry->UpdateSPPoint(plr->GetCharID(), plr->GetPcProfile()->dwSpPoint);
		app->qry->UpdatePlayerLevel(plr->GetPcProfile()->byLevel, plr->GetCharID(), plr->GetPcProfile()->dwCurExp, plr->GetPcProfile()->dwMaxExpInThisLevel);
		response->dwCurExp = plr->GetPcProfile()->dwCurExp;
		plr->SetRPBall();
		//plr->SendRpBallInformation();
		res2->wOpCode = GU_UPDATE_CHAR_SP;
		res2->dwSpPoint = plr->GetPcProfile()->dwSpPoint;
		packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SP));
		g_pApp->Send(this->GetHandle(), &packet2);
	}
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EXP));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Select target
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetSelect(CLkPacket * pPacket)
{
	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	m_uiTargetSerialId = req->hTarget;
	//printf("UG_CHAR_TARGET_SELECT %i \n", m_uiTargetSerialId);
}
//--------------------------------------------------------------------------------------//
//		Select target
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetFacing(CLkPacket * pPacket)
{
	//printf("UG_CHAR_TARGET_FACING \n");

	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	m_uiTargetSerialId = req->hTarget;

}
//--------------------------------------------------------------------------------------//
//		target info
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetInfo(CLkPacket * pPacket)
{
	//printf("UG_CHAR_TARGET_INFO \n");
	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	
}
//--------------------------------------------------------------------------------------//
//		Send game leave request
//--------------------------------------------------------------------------------------//
void CClientSession::SendGameLeaveReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- CHARACTER REQUEST LEAVE GAME --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	app->db->prepare("UPDATE characters SET IsOnline = 0, OnlineID = 0 WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();

	plr->SavePlayerData(app);
	app->RemoveUser(plr->GetPlayerName().c_str());
	CLkPacket packet(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY * sPacket = (sGU_OBJECT_DESTROY *)packet.GetPacketData();

	sPacket->wOpCode = GU_OBJECT_DESTROY;
	sPacket->handle = this->GetavatarHandle();
	g_pPlayerManager->RemovePlayer(this->GetavatarHandle());
	packet.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	app->UserBroadcastothers(&packet, this);

	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Char exit request
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharExitReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- Char exit request --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	app->db->prepare("UPDATE characters SET IsOnline = 0, OnlineID = 0 WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();

	CMonster::MonsterData* creaturelist;

	for (std::map<DWORD, CMonster::MonsterData*>::const_iterator it = g_pMobManager->m_map_Monster.begin(); it != g_pMobManager->m_map_Monster.end(); it++)
	{
		creaturelist = it->second;
		if (LkGetDistance(plr->GetPlayerPosition().x, plr->GetPlayerPosition().z, creaturelist->Spawn_Loc.x, creaturelist->Spawn_Loc.z) >= 20)
		{
			std::vector<RwUInt32>::iterator handleSearch = std::find(creaturelist->spawnedForHandle.begin(), creaturelist->spawnedForHandle.end(), this->GetavatarHandle());
			if (handleSearch != creaturelist->spawnedForHandle.end() && creaturelist->spawnedForHandle.size() != 0)
			{
				creaturelist->spawnedForHandle.erase(handleSearch);
				if (creaturelist->spawnedForHandle.size() == 0)
				{
					creaturelist->isAggro = false;
					creaturelist->isSpawned = false;
					creaturelist->IsDead = false;
				}
			}
		}
	}


	plr->SavePlayerData(app);
	// log out of game
	CLkPacket packet1(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY * sPacket = (sGU_OBJECT_DESTROY *)packet1.GetPacketData();
	

	sPacket->wOpCode = GU_OBJECT_DESTROY;
	sPacket->handle = this->GetavatarHandle();
	packet1.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	app->UserBroadcastothers(&packet1, this);
	g_pPlayerManager->RemovePlayer(this->GetavatarHandle());
	app->RemoveUser(plr->GetPlayerName().c_str());

	// log in to char server
	CLkPacket packet(sizeof(sGU_CHAR_EXIT_RES));
	sGU_CHAR_EXIT_RES * res = (sGU_CHAR_EXIT_RES *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_EXIT_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s((char*)res->achAuthKey, LK_MAX_SIZE_AUTH_KEY, "Dbo");
	res->byServerInfoCount = 1;
	strcpy_s(res->aServerInfo[0].szCharacterServerIP, LK_MAX_LENGTH_OF_IP, app->GetConfigFileExternalIP());
	res->aServerInfo[0].wCharacterServerPortForClient = 20300;
	res->aServerInfo[0].dwLoad = 0;

	packet.SetPacketLen(sizeof(sGU_CHAR_EXIT_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Char sit down
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharSitDown(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- Char sit down request --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_CHAR_SITDOWN));
	sGU_CHAR_SITDOWN * sPacket = (sGU_CHAR_SITDOWN *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* pSit = (sGU_UPDATE_CHAR_STATE*)packet2.GetPacketData();

	pSit->handle = this->GetavatarHandle();
	pSit->wOpCode = GU_UPDATE_CHAR_STATE;
	pSit->sCharState.sCharStateBase.byStateID = CHARSTATE_SITTING;
	pSit->sCharState.sCharStateBase.vCurLoc.x = plr->GetPlayerPosition().x;
	pSit->sCharState.sCharStateBase.vCurLoc.y = plr->GetPlayerPosition().y;
	pSit->sCharState.sCharStateBase.vCurLoc.z = plr->GetPlayerPosition().z;
	pSit->sCharState.sCharStateBase.vCurDir.x = plr->GetPlayerDirection().x;
	pSit->sCharState.sCharStateBase.vCurDir.y = plr->GetPlayerDirection().y;
	pSit->sCharState.sCharStateBase.vCurDir.z = plr->GetPlayerDirection().z;

	sPacket->wOpCode = GU_CHAR_SITDOWN;
	sPacket->handle = this->GetavatarHandle();

	packet.SetPacketLen(sizeof(sGU_CHAR_SITDOWN));
	packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	app->UserBroadcastothers(&packet, this);
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
	plr->SetPlayerSit(true);
	app->UserBroadcastothers(&packet2, this);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		Char stand up
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharStandUp(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- Char stand up request --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_CHAR_STANDUP));
	sGU_CHAR_STANDUP * sPacket = (sGU_CHAR_STANDUP *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* pSitUp = (sGU_UPDATE_CHAR_STATE*)packet2.GetPacketData();

	pSitUp->handle = this->GetavatarHandle();
	pSitUp->wOpCode = GU_UPDATE_CHAR_STATE;
	pSitUp->sCharState.sCharStateBase.byStateID = CHARSTATE_STANDING;
	pSitUp->sCharState.sCharStateBase.vCurLoc.x = plr->GetPlayerPosition().x;
	pSitUp->sCharState.sCharStateBase.vCurLoc.y = plr->GetPlayerPosition().y;
	pSitUp->sCharState.sCharStateBase.vCurLoc.z = plr->GetPlayerPosition().z;
	pSitUp->sCharState.sCharStateBase.vCurDir.x = plr->GetPlayerDirection().x;
	pSitUp->sCharState.sCharStateBase.vCurDir.y = plr->GetPlayerDirection().y;
	pSitUp->sCharState.sCharStateBase.vCurDir.z = plr->GetPlayerDirection().z;

	sPacket->wOpCode = GU_CHAR_STANDUP;
	sPacket->handle = this->GetavatarHandle();
	packet.SetPacketLen(sizeof(sGU_CHAR_STANDUP));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet, this);
	app->UserBroadcastothers(&packet2, this);
	plr->SetPlayerSit(false);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		char start mail
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMailStart(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- char start mail --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_MAIL_START_REQ * req = (sUG_MAIL_START_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_MAIL_START_RES));
	sGU_MAIL_START_RES * res = (sGU_MAIL_START_RES *)packet.GetPacketData();

	app->db->prepare("SELECT MailIsAway FROM characters WHERE CharID=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	app->db->fetch();

	//res->hObject = req->hObject;
	res->wOpCode = GU_MAIL_START_RES;
	res->wResultCode = GAME_SUCCESS;
	res->bIsAway = app->db->getBoolean("MailIsAway");

	app->db->prepare("SELECT * FROM mail WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	if (app->db->rowsCount() > 0)
	{
		while (app->db->fetch())
		{
			CLkPacket packet2(sizeof(sGU_MAIL_LOAD_DATA));
			sGU_MAIL_LOAD_DATA * res2 = (sGU_MAIL_LOAD_DATA *)packet2.GetPacketData();
			res2->wOpCode = GU_MAIL_LOAD_DATA;
			res2->sData.bIsAccept = app->db->getBoolean("bIsAccept");
			res2->sData.bIsLock = app->db->getBoolean("bIsLock");
			res2->sData.bIsRead = app->db->getBoolean("bIsRead");
			//	res2->sData.bySenderType = app->db->getInt("SenderType");
			res2->sData.byMailType = app->db->getInt("byMailType");
			wcscpy_s(res2->wszText, LK_MAX_LENGTH_OF_MAIL_MESSAGE_IN_UNICODE, s2ws(app->db->getString("wszText")).c_str());
			wcscpy_s(res2->sData.wszFromName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("wszFromName")).c_str());
			res2->byTextSize = app->db->getInt("byTextSize");
			res2->sData.dwZenny = app->db->getInt("dwZenny");
			res2->byTextSize = app->db->getInt("byTextSize");
			res2->sData.endTime = app->db->getInt("byDay");
			res2->sData.mailID = app->db->getInt("id");
			packet2.SetPacketLen(sizeof(sGU_MAIL_LOAD_DATA));
			g_pApp->Send(this->GetHandle(), &packet2);
		}
	}

	packet.SetPacketLen(sizeof(sGU_MAIL_START_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		load mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailLoadReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- load mails --- \n");

	sUG_MAIL_LOAD_REQ * req = (sUG_MAIL_LOAD_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_MAIL_LOAD_RES));
	sGU_MAIL_LOAD_RES * res = (sGU_MAIL_LOAD_RES *)packet.GetPacketData();

	res->wOpCode = GU_MAIL_LOAD_RES;
	res->hObject = req->hObject;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_MAIL_LOAD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		reload mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailReloadReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	//printf("--- reload mails --- \n");
	sUG_MAIL_RELOAD_REQ * req = (sUG_MAIL_RELOAD_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_MAIL_RELOAD_RES));
	sGU_MAIL_RELOAD_RES * res = (sGU_MAIL_RELOAD_RES *)packet.GetPacketData();

	// COUNT UNREAD MESSAGES START
	app->db->prepare("SELECT COUNT(*) AS countmsg FROM mail WHERE CharID = ? AND bIsRead=0");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	app->db->fetch();
	RwUInt32 count_unread_msg = app->db->getInt("countmsg");
	// COUNT UNREAD MESSAGES END

	app->db->prepare("SELECT * FROM mail WHERE CharID = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	RwUInt32 i = 0;
	while (app->db->fetch())
	{
		res->aMailID[i] = app->db->getInt("id");
		i++;
	}

	res->byMailCount = app->db->rowsCount(); //count all mails
	res->byManagerCount = count_unread_msg; //amount of unread messages
	res->byNormalCount = 0;
	res->hObject = req->hObject;
	res->wOpCode = GU_MAIL_RELOAD_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_MAIL_RELOAD_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	this->SendCharMailStart(pPacket, app);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		read mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailReadReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- UG_MAIL_READ_REQ --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_MAIL_READ_REQ * req = (sUG_MAIL_READ_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT * FROM mail WHERE id = ?");
	app->db->setInt(1, req->mailID);
	app->db->execute();
	app->db->fetch();

	RwUInt32 itemid = app->db->getInt("item_id");

	CLkPacket packet(sizeof(sGU_MAIL_READ_RES));
	sGU_MAIL_READ_RES * res = (sGU_MAIL_READ_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_MAIL_LOAD_INFO));
	sGU_MAIL_LOAD_INFO * res2 = (sGU_MAIL_LOAD_INFO *)packet2.GetPacketData();


	res->byRemainDay = app->db->getInt("byDay");
	res->endTime = 100;
	res->hObject = req->hObject;
	res->mailID = req->mailID;
	res->wOpCode = GU_MAIL_READ_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->wOpCode = GU_MAIL_LOAD_INFO;
	res2->sData.bIsAccept = app->db->getBoolean("bIsAccept");
	res2->sData.bIsLock = app->db->getBoolean("bIsLock");
	res2->sData.bIsRead = app->db->getBoolean("bIsRead");
	res2->byTextSize = app->db->getInt("byTextSize");
	res2->sData.byExpired = 100;
	res2->sData.bySenderType = eMAIL_SENDER_TYPE_BASIC;
	res2->sData.byMailType = app->db->getInt("byMailType");
	res2->sData.dwZenny = app->db->getInt("dwZenny");
	res2->sData.endTime = app->db->getInt("byDay");
	res2->sData.mailID = app->db->getInt("id");
	wcscpy_s(res2->sData.wszFromName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("wszFromName")).c_str());
	wcscpy_s(res2->wszText, LK_MAX_LENGTH_OF_MAIL_MESSAGE_IN_UNICODE, s2ws(app->db->getString("wszText")).c_str());

	//	res2->sData.tCreateTime.second = app->db->getInt("byDay");
	//	res2->sData.tCreateTime.minute = app->db->getInt("byDay");
	//	res2->sData.tCreateTime.hour = app->db->getInt("byDay");
	//	res2->sData.tCreateTime.day = app->db->getInt("tCreateTime");
	//	res2->sData.tCreateTime.month = app->db->getInt("byDay");
	//	res2->sData.tCreateTime.year = app->db->getInt("byDay");

	if (app->db->getInt("byMailType") > 1){
		app->db->prepare("SELECT id,tblidx,count,rank,durability,grade FROM items WHERE id = ?");
		app->db->setInt(1, itemid);
		app->db->execute();
		app->db->fetch();
		res2->sData.sItemProfile.handle = app->db->getInt("id");
		res2->sData.sItemProfile.byCurDur = app->db->getInt("durability");
		res2->sData.sItemProfile.byStackcount = app->db->getInt("count");
		res2->sData.sItemProfile.byGrade = app->db->getInt("grade");
		res2->sData.sItemProfile.tblidx = app->db->getInt("tblidx");
		res2->sData.sItemProfile.byRank = app->db->getInt("rank");
	}

	packet2.SetPacketLen(sizeof(sGU_MAIL_LOAD_INFO));
	g_pApp->Send(this->GetHandle(), &packet2);

	//SET MAIL READ
	app->qry->SetMailRead(req->mailID);

	packet.SetPacketLen(sizeof(sGU_MAIL_READ_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		send mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailSendReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_MAIL_SEND_REQ * req = (sUG_MAIL_SEND_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_MAIL_SEND_RES));
	sGU_MAIL_SEND_RES * res = (sGU_MAIL_SEND_RES *)packet.GetPacketData();

	app->db->prepare("SELECT CharID,MailIsAway FROM characters WHERE CharName = ?");

	char targetname[LK_MAX_SIZE_CHAR_NAME_UNICODE];
	wcstombs(targetname, req->wszTargetName, LK_MAX_SIZE_CHAR_NAME_UNICODE);

	char *text = new char[req->byTextSize];
	wcstombs(text, req->wszText, req->byTextSize);
	text[req->byTextSize] = '\0';

	app->db->setString(1, targetname);
	app->db->execute();
	app->db->fetch();
	int id = app->db->getInt("CharID");

	wcscpy_s(res->wszTargetName, LK_MAX_SIZE_CHAR_NAME_UNICODE, req->wszTargetName);
	res->wOpCode = GU_MAIL_SEND_RES;
	res->hObject = req->hObject;

	if (app->db->getBoolean("MailIsAway") == 1){
		res->wResultCode = GAME_MAIL_TARGET_AWAY_STATE;
	}
	else {
		res->wResultCode = GAME_SUCCESS;
		app->db->prepare("INSERT INTO mail (CharID, byDay, byMailType, byTextSize, dwZenny, wszText, item_id, item_place, item_pos, wszTargetName, wszFromName, bIsAccept, bIsLock, bIsRead) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		app->db->setInt(1, id);
		app->db->setInt(2, req->byDay);
		app->db->setInt(3, req->byMailType);
		app->db->setInt(4, req->byTextSize);
		app->db->setInt(5, req->dwZenny);
		app->db->setString(6, text);
		app->db->setInt(7, req->sItemData.hItem);
		app->db->setInt(8, req->sItemData.byPlace);
		app->db->setInt(9, req->sItemData.byPos);
		app->db->setString(10, targetname);
		app->db->setString(11, plr->GetPlayerName().c_str());
		app->db->setInt(12, app->db->getBoolean("MailIsAway"));
		app->db->setInt(13, false);
		app->db->setInt(14, false);
		app->db->execute();

		if (req->byMailType == 2 || req->byMailType == 5){
			//SET OWNER ID TO 0
			app->qry->UpdateItemOwnerIdWithUniqueID(0, req->sItemData.hItem);
			//DEL ITEM PACKET
			this->gsf->DeleteItemByUIdPlacePos(pPacket, this, req->sItemData.hItem, req->sItemData.byPlace, req->sItemData.byPos);
		}
		else if (req->byMailType == 3){
			//UPDATE CHAR MONEY
			app->qry->SetMinusMoney(plr->GetCharID(), req->dwZenny);
			plr->GetPcProfile()->dwZenny -= req->dwZenny;
			//UPDATE MONEY PACKET
			this->gsf->UpdateCharMoney(pPacket, this, 16, plr->GetPcProfile()->dwZenny, this->GetavatarHandle());
		}
		else if (req->byMailType == 4){
			//UPDATE MONEY
			app->qry->SetMinusMoney(plr->GetCharID(), req->dwZenny);
			plr->GetPcProfile()->dwZenny -= req->dwZenny;
			//UPDATE MONEY PACKET
			this->gsf->UpdateCharMoney(pPacket, this, 16, plr->GetPcProfile()->dwZenny, this->GetavatarHandle());
			//SET OWNER ID TO 0
			app->qry->UpdateItemOwnerIdWithUniqueID(0, req->sItemData.hItem);
			//DEL ITEM PACKET
			this->gsf->DeleteItemByUIdPlacePos(pPacket, this, req->sItemData.hItem, req->sItemData.byPlace, req->sItemData.byPos);
		}

	}

	packet.SetPacketLen(sizeof(sGU_MAIL_SEND_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		delete mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailDelReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- UG_MAIL_DEL_REQ --- \n");

	sUG_MAIL_DEL_REQ * req = (sUG_MAIL_DEL_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_MAIL_DEL_RES));
	sGU_MAIL_DEL_RES * res = (sGU_MAIL_DEL_RES *)packet.GetPacketData();

	app->qry->DeleteFromMailByID(req->mailID);

	res->hObject = req->hObject;
	res->mailID = req->mailID;
	res->wOpCode = GU_MAIL_DEL_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_MAIL_DEL_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		receive item with mail
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailItemReceiveReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- UG_MAIL_ITEM_RECEIVE_REQ --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_MAIL_ITEM_RECEIVE_REQ * req = (sUG_MAIL_ITEM_RECEIVE_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT * FROM mail WHERE id = ?");
	app->db->setInt(1, req->mailID);
	app->db->execute();
	app->db->fetch();

	CLkPacket packet(sizeof(sGU_MAIL_ITEM_RECEIVE_RES));
	sGU_MAIL_ITEM_RECEIVE_RES * res = (sGU_MAIL_ITEM_RECEIVE_RES *)packet.GetPacketData();

	res->hObject = req->hObject;
	res->mailID = req->mailID;
	res->wOpCode = GU_MAIL_ITEM_RECEIVE_RES;
	res->wResultCode = GAME_SUCCESS;


	if (app->db->getInt("byMailType") == 2){
		//CHANGE ITEM OWNER
		app->qry->ChangeItemOwnerByUIdPlacePos(plr->GetCharID(), app->db->getInt("item_id"), 0, 0); // 0 = place and pos
		//CREATE ITEM PACKET
		CLkPacket packet1(sizeof(sGU_ITEM_CREATE));
		sGU_ITEM_CREATE * res1 = (sGU_ITEM_CREATE *)packet1.GetPacketData();

		res1->wOpCode = GU_ITEM_CREATE;
		packet1.SetPacketLen(sizeof(sGU_ITEM_CREATE));
		g_pApp->Send(this->GetHandle(), &packet1);
	}
	else if (app->db->getInt("byMailType") == 3){
		//UPDATE MONEY
		plr->GetPcProfile()->dwZenny += app->db->getInt("dwZenny");
		app->qry->SetPlusMoney(plr->GetCharID(), app->db->getInt("dwZenny"));
		//UPDATE MONEY PACKET
		this->gsf->UpdateCharMoney(pPacket, this, 17, plr->GetPcProfile()->dwZenny, this->GetavatarHandle());
	}
	else if (app->db->getInt("byMailType") == 4){
		//UPDATE MONEY
		app->qry->SetPlusMoney(plr->GetCharID(), app->db->getInt("dwZenny"));
		plr->GetPcProfile()->dwZenny += app->db->getInt("dwZenny");
		//UPDATE MONEY PACKET
		this->gsf->UpdateCharMoney(pPacket, this, 17, plr->GetPcProfile()->dwZenny, this->GetavatarHandle());
		//CHANGE ITEM OWNER
		app->qry->ChangeItemOwnerByUIdPlacePos(plr->GetCharID(), app->db->getInt("item_id"), 0, 0); // 0 = place and pos
		//CREATE ITEM PACKET
		CLkPacket packet3(sizeof(sGU_ITEM_CREATE));
		sGU_ITEM_CREATE * res3 = (sGU_ITEM_CREATE *)packet3.GetPacketData();

		res3->wOpCode = GU_ITEM_CREATE;
		packet3.SetPacketLen(sizeof(sGU_ITEM_CREATE));
		g_pApp->Send(this->GetHandle(), &packet3);
	}

	app->qry->SetMailAccept(plr->GetCharID(), req->mailID);

	packet.SetPacketLen(sizeof(sGU_MAIL_ITEM_RECEIVE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		delete multiple mails
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailMultiDelReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- UG_MAIL_MULTI_DEL_REQ --- \n");

	sUG_MAIL_MULTI_DEL_REQ * req = (sUG_MAIL_MULTI_DEL_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_MAIL_MULTI_DEL_RES));
	sGU_MAIL_MULTI_DEL_RES * res = (sGU_MAIL_MULTI_DEL_RES *)packet.GetPacketData();

	for (RwInt32 j = 0; j < req->byCount; ++j)
	{
		app->qry->DeleteFromMailByID(req->aMailID[j]);

		res->wOpCode = GU_MAIL_MULTI_DEL_RES;
		res->wResultCode = GAME_SUCCESS;
		res->hObject = req->hObject;
		res->byCount = req->byCount;
		res->aMailID[j] = req->aMailID[j];

		packet.SetPacketLen(sizeof(sGU_MAIL_MULTI_DEL_RES));
		g_pApp->Send(this->GetHandle(), &packet);
	}

}
//--------------------------------------------------------------------------------------//
//		lock mail
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailLockReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- LOCK MAIL --- \n");
	sUG_MAIL_LOCK_REQ * req = (sUG_MAIL_LOCK_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT * FROM mail WHERE id = ?");
	app->db->setInt(1, req->mailID);
	app->db->execute();
	app->db->fetch();

	CLkPacket packet(sizeof(sGU_MAIL_LOCK_RES));
	sGU_MAIL_LOCK_RES * res = (sGU_MAIL_LOCK_RES *)packet.GetPacketData();

	res->wOpCode = GU_MAIL_LOCK_RES;
	res->wResultCode = GAME_SUCCESS;
	res->hObject = req->hObject;
	res->mailID = req->mailID;
	res->bIsLock = req->bIsLock;

	app->qry->UpdateMailLock(req->mailID, res->bIsLock);

	packet.SetPacketLen(sizeof(sGU_MAIL_LOCK_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		RETURN MAIL
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharMailReturnReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- RETURN MAIL --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_MAIL_RETURN_REQ * req = (sUG_MAIL_RETURN_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT item_id FROM mail WHERE id = ?");
	app->db->setInt(1, req->mailID);
	app->db->execute();
	app->db->fetch();

	CLkPacket packet(sizeof(sGU_MAIL_RETURN_RES));
	sGU_MAIL_RETURN_RES * res = (sGU_MAIL_RETURN_RES *)packet.GetPacketData();

	res->wOpCode = GU_MAIL_RETURN_RES;
	res->wResultCode = GAME_SUCCESS;
	res->hObject = req->hObject;
	res->mailID = req->mailID;

	app->db->prepare("CALL ReturnMail (?,?)");
	app->db->setInt(1, req->mailID);
	app->db->setString(2, plr->GetPlayerName().c_str());
	app->db->execute();

	packet.SetPacketLen(sizeof(sGU_MAIL_RETURN_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}


//--------------------------------------------------------------------------------------//
//		char away req
//--------------------------------------------------------------------------------------//
void	CClientSession::SendCharAwayReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- char away req --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_CHAR_AWAY_REQ * req = (sUG_CHAR_AWAY_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_AWAY_RES));
	sGU_CHAR_AWAY_RES * res = (sGU_CHAR_AWAY_RES *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_AWAY_RES;
	res->wResultCode = GAME_SUCCESS;
	res->bIsAway = req->bIsAway;

	app->qry->UpdateCharAwayStatus(plr->GetCharID(), req->bIsAway);

	packet.SetPacketLen(sizeof(sGU_CHAR_AWAY_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		char follow move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharFollowMove(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- SEND CHAR FOLLOW MOVE --- \n");
	sUG_CHAR_FOLLOW_MOVE * req = (sUG_CHAR_FOLLOW_MOVE*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_FOLLOW_MOVE));
	sGU_CHAR_FOLLOW_MOVE * res = (sGU_CHAR_FOLLOW_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_FOLLOW_MOVE;
	res->handle = this->GetavatarHandle();
	res->hTarget = this->GetTargetSerialId();
	res->fDistance = req->fDistance;
	res->byMovementReason = req->byMovementReason;
	res->byMoveFlag = LK_MOVE_FLAG_RUN;

	packet.SetPacketLen(sizeof(sGU_CHAR_FOLLOW_MOVE));
	app->UserBroadcastothers(&packet, this);
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}



//--------------------------------------------------------------------------------------//
//		Create Guild
//--------------------------------------------------------------------------------------//
void CClientSession::SendGuildCreateReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- create guild request --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_GUILD_CREATE_REQ * req = (sUG_GUILD_CREATE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_GUILD_CREATE_RES));
	sGU_GUILD_CREATE_RES * res = (sGU_GUILD_CREATE_RES *)packet.GetPacketData();

	res->wOpCode = GU_GUILD_CREATE_RES;
	printf("guild manager id: %i ", req->hGuildManagerNpc);

	app->db->prepare("CALL GuildCreate (?,?, @wResultCode, @cguildid, @charactername)");
	app->db->setString(1, Lk_WC2MB(req->wszGuildName));
	app->db->setInt(2, plr->GetCharID());
	app->db->execute();
	app->db->execute("SELECT @wResultCode, @cguildid, @charactername");
	app->db->fetch();

	int result = app->db->getInt("@wResultCode");

	printf("create guild result %i \n ", result);
	res->wResultCode = result;

	packet.SetPacketLen(sizeof(sGU_GUILD_CREATE_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

	if (result == 200) {

		// CREATE GUILD
		CLkPacket packet2(sizeof(sTU_GUILD_CREATED_NFY));
		sTU_GUILD_CREATED_NFY * res2 = (sTU_GUILD_CREATED_NFY *)packet2.GetPacketData();
		res2->wOpCode = TU_GUILD_CREATED_NFY;
		memcpy(res2->wszGuildName, req->wszGuildName, sizeof(wchar_t)* LK_MAX_SIZE_GUILD_NAME_IN_UNICODE);
		packet2.SetPacketLen(sizeof(sTU_GUILD_CREATED_NFY));
		rc = g_pApp->Send(this->GetHandle(), &packet2);

		// GUILD INFORMATIONS
		CLkPacket packet3(sizeof(sTU_GUILD_INFO));
		sTU_GUILD_INFO * res3 = (sTU_GUILD_INFO *)packet3.GetPacketData();

		res3->wOpCode = TU_GUILD_INFO;
		res3->guildInfo.dwGuildReputation = 0;
		res3->guildInfo.guildId = app->db->getInt("@cguildid");
		res3->guildInfo.guildMaster = plr->GetCharID();
		memcpy(res3->guildInfo.wszName, req->wszGuildName, sizeof(wchar_t)* LK_MAX_SIZE_GUILD_NAME_IN_UNICODE);
		wcscpy_s(res3->guildInfo.awchName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("@charactername")).c_str());
		packet3.SetPacketLen(sizeof(sTU_GUILD_INFO));
		rc = g_pApp->Send(this->GetHandle(), &packet3);

		// GUILD MEMBER INFORMATIONS
		CLkPacket packet4(sizeof(sTU_GUILD_MEMBER_INFO));
		sTU_GUILD_MEMBER_INFO * res4 = (sTU_GUILD_MEMBER_INFO *)packet4.GetPacketData();

		res4->wOpCode = TU_GUILD_MEMBER_INFO;
		res4->guildMemberInfo.bIsOnline = true;
		res4->guildMemberInfo.charId = plr->GetCharID();
		wcscpy_s(res4->guildMemberInfo.wszMemberName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("@charactername")).c_str());
		packet4.SetPacketLen(sizeof(sTU_GUILD_MEMBER_INFO));
		rc = g_pApp->Send(this->GetHandle(), &packet4);
		app->UserBroadcastothers(&packet4, this);

		plr->SetGuildName(app->db->getString("@charactername"));

	}
	plr = NULL;
	delete plr;

}

//--------------------------------------------------------------------------------------//
//		Create Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendCreatePartyReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- create party request --- \n");

	PlayerPartyClass *t = new PlayerPartyClass;

	t->m_partyList.begin();
	t->CreateParty(this->GetHandle());

	sUG_PARTY_CREATE_REQ * req = (sUG_PARTY_CREATE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PARTY_CREATE_RES));
	sGU_PARTY_CREATE_RES * res = (sGU_PARTY_CREATE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_CREATE_RES;
	res->wResultCode = GAME_SUCCESS;
	memcpy(res->wszPartyName, req->wszPartyName, sizeof(wchar_t)* LK_MAX_SIZE_PARTY_NAME_IN_UNICODE);

	packet.SetPacketLen(sizeof(sGU_PARTY_CREATE_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

}
//--------------------------------------------------------------------------------------//
//		Disband Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendDisbandPartyReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- disband party request --- \n");

	CLkPacket packet(sizeof(sGU_PARTY_DISBAND_RES));
	sGU_PARTY_DISBAND_RES * res = (sGU_PARTY_DISBAND_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_DISBAND_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_PARTY_DISBAND_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);


	CLkPacket packet2(sizeof(sGU_PARTY_DISBANDED_NFY));
	sGU_PARTY_DISBANDED_NFY * sPacket2 = (sGU_PARTY_DISBANDED_NFY *)packet2.GetPacketData();
	sPacket2->wOpCode = GU_PARTY_DISBANDED_NFY;

	packet2.SetPacketLen(sizeof(sGU_PARTY_DISBANDED_NFY));
	rc = g_pApp->Send(this->GetHandle(), &packet2);

}
//--------------------------------------------------------------------------------------//
//		Send party invite request
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyInviteReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- Send party invite request --- \n");
	sUG_PARTY_INVITE_REQ * req = (sUG_PARTY_INVITE_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT * FROM characters WHERE OnlineID = ? AND isOnline = 1");
	app->db->setInt(1, req->hTarget);
	app->db->execute();
	app->db->fetch();
	//this->plr->LastPartyHandle = req->hTarget;
	//Invite player
	CLkPacket packet(sizeof(sGU_PARTY_INVITE_RES));
	sGU_PARTY_INVITE_RES * res = (sGU_PARTY_INVITE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_INVITE_RES;
	res->wResultCode = GAME_SUCCESS;
	wcscpy_s(res->wszTargetName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str());

	packet.SetPacketLen(sizeof(sGU_PARTY_INVITE_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);

	//Send invitation request to player
	CLkPacket packet2(sizeof(sGU_PARTY_INVITE_NFY));
	sGU_PARTY_INVITE_NFY * res2 = (sGU_PARTY_INVITE_NFY *)packet2.GetPacketData();

	res2->wOpCode = GU_PARTY_INVITE_NFY;
	res2->bFromPc = true;
	//wcscpy_s(res2->wszInvitorPcName, LK_MAX_SIZE_CHAR_NAME_UNICODE, plr->GetPcProfile()->awchName);

	packet2.SetPacketLen(sizeof(sGU_PARTY_INVITE_NFY));
	app->UserBroadcastothers(&packet2, this);


	/*sUG_PARTY_RESPONSE_INVITATION * req2 = (sUG_PARTY_RESPONSE_INVITATION*)pPacket->GetPacketData();
	CLkPacket packet3(sizeof(sGU_PARTY_RESPONSE_INVITATION_RES));
	sGU_PARTY_RESPONSE_INVITATION_RES * res3 = (sGU_PARTY_RESPONSE_INVITATION_RES *)packet3.GetPacketData();

	res3->wOpCode = GU_PARTY_RESPONSE_INVITATION_RES;
	res3->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen( sizeof(sGU_PARTY_RESPONSE_INVITATION_RES) );
	rc = g_pApp->Send( this->GetHandle(), &packet3 );

	printf("response: %i ",req2->byResponse);
	if(req2->byResponse == 2) // User accepted party invite
	{
	CLkPacket packet4(sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	sGU_PARTY_MEMBER_JOINED_NFY * res4 = (sGU_PARTY_MEMBER_JOINED_NFY *)packet4.GetPacketData();

	res2->wOpCode = GU_PARTY_MEMBER_JOINED_NFY;
	wcscpy_s(res4->memberInfo.awchMemberName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str());

	res4->memberInfo.byClass = app->db->getInt("Class");
	res4->memberInfo.byLevel = app->db->getInt("Level");
	res4->memberInfo.byRace = app->db->getInt("Race");
	res4->memberInfo.hHandle = app->db->getInt("OnlineID");
	res4->memberInfo.vCurLoc.x = (float)app->db->getDouble("CurLocX");
	res4->memberInfo.vCurLoc.y = (float)app->db->getDouble("CurLocY");
	res4->memberInfo.vCurLoc.z = (float)app->db->getDouble("CurLocZ");
	res4->memberInfo.wCurEP = app->db->getInt("CurEP");
	res4->memberInfo.wCurLP = app->db->getInt("CurLP");
	res4->memberInfo.wMaxEP = app->db->getInt("BaseMaxEp");
	res4->memberInfo.wMaxLP = app->db->getInt("BaseMaxLp");
	res4->memberInfo.worldId = app->db->getInt("WorldID");
	res4->memberInfo.dwZenny = app->db->getInt("Money");
	res4->memberInfo.worldTblidx = app->db->getInt("WorldTable");

	packet4.SetPacketLen( sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	rc = g_pApp->Send( this->GetHandle(), &packet );
	app->UserBroadcastothers(&packet4, this);
	//printf("user invited ");
	}*/
}
//--------------------------------------------------------------------------------------//
//		Party invitation response
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyResponse(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- Party invitation response --- \n");

	sUG_PARTY_RESPONSE_INVITATION * req2 = (sUG_PARTY_RESPONSE_INVITATION*)pPacket->GetPacketData();

	CLkPacket packet3(sizeof(sGU_PARTY_RESPONSE_INVITATION_RES));
	sGU_PARTY_RESPONSE_INVITATION_RES * res3 = (sGU_PARTY_RESPONSE_INVITATION_RES *)packet3.GetPacketData();
	res3->wOpCode = GU_PARTY_RESPONSE_INVITATION_RES;
	res3->wResultCode = GAME_FAIL;

	/*if (req2->byResponse == 0 && this->plr->LastPartyHandle != -1) // User accepted party invite
	{
	CLkPacket packet4(sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	sGU_PARTY_MEMBER_JOINED_NFY * res4 = (sGU_PARTY_MEMBER_JOINED_NFY *)packet4.GetPacketData();
	res4->wOpCode = GU_PARTY_MEMBER_JOINED_NFY;

	app->db->prepare("SELECT * FROM characters WHERE OnlineID = ? AND isOnline = 1");
	app->db->setInt(1, this->plr->LastPartyHandle);
	app->db->execute();
	app->db->fetch();

	wcscpy_s(res4->memberInfo.awchMemberName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str());
	printf("1");
	res4->memberInfo.byClass = app->db->getInt("Class");
	res4->memberInfo.byLevel = app->db->getInt("Level");
	res4->memberInfo.byRace = app->db->getInt("Race");
	res4->memberInfo.hHandle = app->db->getInt("OnlineID");
	res4->memberInfo.vCurLoc.x = (float)app->db->getDouble("CurLocX");
	res4->memberInfo.vCurLoc.y = (float)app->db->getDouble("CurLocY");
	res4->memberInfo.vCurLoc.z = (float)app->db->getDouble("CurLocZ");
	res4->memberInfo.wCurEP = app->db->getInt("CurEP");
	res4->memberInfo.wCurLP = app->db->getInt("CurLP");
	res4->memberInfo.wMaxEP = app->db->getInt("BaseMaxEp");
	res4->memberInfo.wMaxLP = app->db->getInt("BaseMaxLp");
	res4->memberInfo.worldId = app->db->getInt("WorldID");
	res4->memberInfo.dwZenny = app->db->getInt("Money");
	res4->memberInfo.worldTblidx = app->db->getInt("WorldTable");

	packet4.SetPacketLen(sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	g_pApp->Send(this->GetHandle(), &packet4);
	app->UserBroadcastothers(&packet4, this);
	res3->wResultCode = GAME_SUCCESS;
	}
	packet3.SetPacketLen(sizeof(sGU_PARTY_RESPONSE_INVITATION_RES));
	g_pApp->Send(this->GetHandle(), &packet3);
	this->plr->LastPartyHandle = -1;*/
}
//--------------------------------------------------------------------------------------//
//		Leave Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyLeaveReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- leave party request --- \n");

	CLkPacket packet(sizeof(sGU_PARTY_LEAVE_RES));
	sGU_PARTY_LEAVE_RES * res = (sGU_PARTY_LEAVE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_LEAVE_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_PARTY_DISBAND_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);


	CLkPacket packet2(sizeof(sGU_PARTY_MEMBER_LEFT_NFY));
	sGU_PARTY_MEMBER_LEFT_NFY * sPacket2 = (sGU_PARTY_MEMBER_LEFT_NFY *)packet2.GetPacketData();
	sPacket2->wOpCode = GU_PARTY_MEMBER_LEFT_NFY;
	//sPacket2->hMember = GET PARTY-MEMBER ID


	packet2.SetPacketLen(sizeof(sGU_PARTY_MEMBER_LEFT_NFY));
	rc = g_pApp->Send(this->GetHandle(), &packet2);

	app->UserBroadcastothers(&packet2, this);
}

//--------------------------------------------------------------------------------------//
//		Execute trigger object
//--------------------------------------------------------------------------------------//
void CClientSession::SendExcuteTriggerObject(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- SendExcuteTriggerObject --- \n");

	sUG_TS_EXCUTE_TRIGGER_OBJECT * req = (sUG_TS_EXCUTE_TRIGGER_OBJECT*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_TS_EXCUTE_TRIGGER_OBJECT_RES));
	sGU_TS_EXCUTE_TRIGGER_OBJECT_RES * res = (sGU_TS_EXCUTE_TRIGGER_OBJECT_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_TS_EXCUTE_TRIGGER));
	sGU_TS_EXCUTE_TRIGGER* res2 = (sGU_TS_EXCUTE_TRIGGER*)packet2.GetPacketData();
	
	sOBJECT_TBLDAT* pObjectData = g_pObjectManager->GetObjectTbldatByHandle(req->hTarget);
	res2->byTsType = 0;	
	res2->tId = pObjectData->minQuestId;//Don't exist here, why?
	res2->wOpCode = GU_TS_EXCUTE_TRIGGER;

	packet2.SetPacketLen(sizeof(sGU_TS_EXCUTE_TRIGGER));
	app->UserBroadcastothers(&packet2, this);
	g_pApp->Send(this->GetHandle(), &packet2);

	res->wOpCode = GU_TS_EXCUTE_TRIGGER_OBJECT_RES;
	res->wResultCode = RESULT_SUCCESS;
	res->hTriggerObject = req->hTarget;

	printf("SOURCE: %i TARGET: %i EVTGENTYPE: %i ", req->hSource, req->hTarget, req->byEvtGenType);

	packet.SetPacketLen(sizeof(sGU_TS_EXCUTE_TRIGGER_OBJECT_RES));
	app->UserBroadcastothers(&packet, this);
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		Character bind to world
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharBindReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- UG_CHAR_BIND_REQ --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_CHAR_BIND_REQ * req = (sUG_CHAR_BIND_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_BIND_RES));
	sGU_CHAR_BIND_RES * res = (sGU_CHAR_BIND_RES *)packet.GetPacketData();

	app->db->prepare("CALL CharBind (?,?, @currentWorldID)");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bindObjectTblidx);
	app->db->execute();
	app->db->execute("SELECT @currentWorldID");
	app->db->fetch();
	sOBJECT_TBLDAT* objMap = reinterpret_cast<sOBJECT_TBLDAT*>(app->g_pTableContainer->GetObjectTable(plr->GetWorldID())->FindData(req->bindObjectTblidx));
	res->wOpCode = GU_CHAR_BIND_RES;
	res->wResultCode = GAME_SUCCESS;
	res->byBindType = DBO_BIND_TYPE_FIRST;
	res->bindObjectTblidx = req->bindObjectTblidx;

	res->bindWorldId = plr->GetWorldID();

	packet.SetPacketLen(sizeof(sGU_CHAR_BIND_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		PORTAL START REQUEST
//--------------------------------------------------------------------------------------//
void CClientSession::SendPortalStartReq(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- PORTAL START REQUEST --- \n");
	sUG_PORTAL_START_REQ * req = (sUG_PORTAL_START_REQ *)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PORTAL_START_RES));
	sGU_PORTAL_START_RES * res = (sGU_PORTAL_START_RES *)packet.GetPacketData();

	res->wOpCode = GU_PORTAL_START_RES;
	res->wResultCode = GAME_SUCCESS;
	res->hNpcHandle = req->handle;

	CLkPacket packet2(sizeof(sGU_PORTAL_INFO));
	sGU_PORTAL_INFO * res2 = (sGU_PORTAL_INFO *)packet2.GetPacketData();

	CPortalTable* pPortalTbl = app->g_pTableContainer->GetPortalTable();
	int i = 0;
	for (CTable::TABLEIT itPortal = pPortalTbl->Begin(); itPortal != pPortalTbl->End(); ++itPortal)
	{
		sPORTAL_TBLDAT* pPortalTblData = (sPORTAL_TBLDAT*)itPortal->second;
		res2->aPortalID[i] = pPortalTblData->tblidx;
		res2->byCount = i;
		i++;
	}
	res2->wOpCode = GU_PORTAL_INFO;


	packet2.SetPacketLen(sizeof(sGU_PORTAL_INFO));
	int rc = g_pApp->Send(this->GetHandle(), &packet2);
	Sleep(1);
	packet.SetPacketLen(sizeof(sGU_PORTAL_START_RES));
	rc = g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		PORTAL ADD REQUEST
//--------------------------------------------------------------------------------------//
void CClientSession::SendPortalAddReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("PORTAL ADD REQUEST");
	sUG_PORTAL_ADD_REQ* req = (sUG_PORTAL_ADD_REQ *)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PORTAL_ADD_RES));
	sGU_PORTAL_ADD_RES * res = (sGU_PORTAL_ADD_RES *)packet.GetPacketData();
	CWayPointTable* pos;
	res->wOpCode = GU_PORTAL_ADD_RES;
	res->wResultCode = GAME_SUCCESS;
	res->hNpcHandle = req->handle;
	res->PortalID = 255;

	packet.SetPacketLen(sizeof(sGU_PORTAL_ADD_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}
void CClientSession::SendPortalTelReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_PORTAL_REQ* req = (sUG_PORTAL_REQ *)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PORTAL_RES));
	sGU_PORTAL_RES * res = (sGU_PORTAL_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_CHAR_TELEPORT_RES));
	sGU_CHAR_TELEPORT_RES * res2 = (sGU_CHAR_TELEPORT_RES *)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet3.GetPacketData();

	packet.SetPacketLen(sizeof(sGU_PORTAL_RES));
	packet2.SetPacketLen(sizeof(sGU_CHAR_TELEPORT_RES));
	packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	int iMyPortalID = req->byPoint;
	iMyPortalID += 1;
	CPortalTable* myPortalTbl = app->g_pTableContainer->GetPortalTable();
	sPORTAL_TBLDAT* pPortalTblData = reinterpret_cast<sPORTAL_TBLDAT*>(myPortalTbl->FindData(iMyPortalID));
	if (pPortalTblData)
	{
		if (iMyPortalID == pPortalTblData->tblidx)
		{
			//pPortalTblData->adwPointZenny;
			//pPortalTblData->dwPointName;
			//pPortalTblData->szPointNameText;
			//pPortalTblData->vMap;
			res->vDir.x = pPortalTblData->vDir.x;
			res->vDir.y = pPortalTblData->vDir.y;
			res->vDir.z = pPortalTblData->vDir.z;
			res->vLoc.x = pPortalTblData->vLoc.x;
			res->vLoc.y = pPortalTblData->vLoc.y;
			res->vLoc.z = pPortalTblData->vLoc.z;
			res->byPoint = pPortalTblData->tblidx;
			res->worldID = pPortalTblData->worldId;
			res->wOpCode = GU_PORTAL_RES;
			res->wResultCode = GAME_SUCCESS;
			res->hNpcHandle = req->handle;//This need be our npc

			res2->bIsToMoveAnotherServer = false;
			//res2->sWorldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
			//res2->sWorldInfo.hTriggerObjectOffset = 100000;
			//res2->sWorldInfo.tblidx = this->plr->GetWorldTableID();
			//res2->sWorldInfo.worldID = res->worldID;
			res2->vNewDir.x = res->vDir.x;
			res2->vNewDir.y = res->vDir.y;
			res2->vNewDir.z = res->vDir.z;
			res2->vNewLoc.x = res->vLoc.x;
			res2->vNewLoc.y = res->vLoc.y;
			res2->vNewLoc.z = res->vLoc.z;
			res2->wOpCode = GU_CHAR_TELEPORT_RES;
			res2->wResultCode = GAME_SUCCESS;
			//res2->sWorldInfo.sRuleInfo.sTimeQuestRuleInfo;

			plr->SetPlayerLastPosition(plr->GetPlayerPosition());
			plr->SetPlayerLastDirection(plr->GetPlayerDirection());
			plr->SetPlayerDirection(res2->vNewDir);
			plr->SetPlayerPosition(res2->vNewLoc);
			app->db->prepare("UPDATE characters SET CurLocX=? , CurLocY=? , CurLocZ=? , CurDirX=? , CurDirZ=? WHERE CharID = ?");
			app->db->setInt(1, res->vLoc.x);
			app->db->setInt(2, res->vLoc.y);
			app->db->setInt(3, res->vLoc.z);
			app->db->setInt(4, res->vDir.x);
			app->db->setInt(5, res->vDir.z);
			app->db->setInt(6, plr->GetCharID());
			app->db->execute();

			g_pApp->Send(this->GetHandle(), &packet);
			g_pApp->Send(this->GetHandle(), &packet2);
		}
		else
		{
			res->wOpCode = GU_PORTAL_RES;
			res->wResultCode = GAME_PORTAL_NOT_EXIST;
			g_pApp->Send(this->GetHandle(), &packet);
			this->gsf->printError("An error is occured in SendPortalTelReq: GAME_PORTAL_NOT_EXIST");
		}
	}
	else
	{
		res->wOpCode = GU_PORTAL_RES;
		res->wResultCode = GAME_PORTAL_NOT_EXIST;
		g_pApp->Send(this->GetHandle(), &packet);
	}
	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		ATTACK BEGIN
//--------------------------------------------------------------------------------------//
void CClientSession::SendAttackBegin(CLkPacket * pPacket, CGameServer * app)
{

	sUG_CHAR_ATTACK_BEGIN* req = (sUG_CHAR_ATTACK_BEGIN *)pPacket->GetPacketData();

	printf("--- ATTACK BEGIN --- \n");

	//req->byType = 0 is for Attacking player and 1 is the Pet who are attacking the npc
	if (req->byType == 0)
	{
		AddAttackBegin(this->GetavatarHandle(), this->GetTargetSerialId());
		SendCharActionAttack(this->GetavatarHandle(), this->GetTargetSerialId(), pPacket);		
	}
	else if (req->byType == 1)
	{
		this->gsf->printError("An error is occured in SendAttackBegin: req->byType == 1");
	}
	else if (req->byType == 2)
	{
		AddAttackBegin(this->GetTargetSerialId(), this->GetavatarHandle());
		SendMobActionAttack(this->GetTargetSerialId(), this->GetavatarHandle(),pPacket);
	}
}
//--------------------------------------------------------------------------------------//
//		ATTACK END
//---------------------------------------------------------------------------------------//
void CClientSession::SendAttackEnd(CLkPacket * pPacket, CGameServer * app)
{
	//printf("--- ATTACK END --- \n");
	sUG_CHAR_ATTACK_END* req = (sUG_CHAR_ATTACK_END *)pPacket->GetPacketData();

	if (req->byType == 0)
	{
		RemoveAttackBegin(this->GetavatarHandle(), this->GetTargetSerialId());
	}
	else if (req->byType == 1)
	{
		this->gsf->printError("An error is occured in SendAttackEnd: req->byType == 1");
	}
}

void CClientSession::AddAttackBegin(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId)
{
	SBattleData *pBattleData = new SBattleData;
	printf("AddAttackBegin SERIAL %i %i \n", uiSerialId, m_uiTargetSerialId);

	pBattleData->uiSerialId = uiSerialId;
	pBattleData->m_uiTargetSerialId = m_uiTargetSerialId;
	pBattleData->bAttackMode = true;
	pBattleData->dwCurrTime = timeGetTime();

	m_listAttackBegin.push_back(pBattleData);

}

void CClientSession::RemoveAttackBegin(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId)
{
	SBattleData *pBattleData;
	for (BATTLEIT it = m_listAttackBegin.begin(); it != m_listAttackBegin.end(); it++)
	{
		pBattleData = (*it);
		if (pBattleData->uiSerialId == uiSerialId)
		{
			RWS_DELETE(pBattleData);
			m_listAttackBegin.erase(it);
			return;
		}
	}
}

void CClientSession::SendCharActionAttack(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId, CLkPacket * pPacket)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	static int byChainAttack = plr->GetChainAttack();
	int CurHP = 0;
	RwBool bDamageApply = true;
	float formula;
	plr->SetPlayerFight(true);
	CLkPacket packet(sizeof(sGU_CHAR_ACTION_ATTACK));
	sGU_CHAR_ACTION_ATTACK * res = (sGU_CHAR_ACTION_ATTACK *)packet.GetPacketData();
	
	res->wOpCode = GU_CHAR_ACTION_ATTACK;
	res->hSubject = uiSerialId;
	res->hTarget = m_uiTargetSerialId;
	res->dwLpEpEventId = 255;
	res->byBlockedAction = 255;
	CMonster::MonsterData *lol = NULL;
	//If we Find the Target as Mob or if the Attacker is Mob
	if (g_pMobManager->FindCreature(m_uiTargetSerialId))
	{
		lol = g_pMobManager->GetMobByHandle(m_uiTargetSerialId);
		if (lol != NULL)
		{
			CurHP = (RwUInt32)lol->CurLP;
			plr->SetAttackTarget(m_uiTargetSerialId);
			cout << "Current Target Handle is " << plr->GetAttackTarget() << endl;
		}
	}
	if (CurHP <= 0)
		CurHP = 0;
	//For player Attacks we will check if he is in fighting,if the Mob/Target has HP> than 0,and if the Attacker owner is the Player
	if ((plr->GetPlayerFight() == true) && (CurHP > 0)&& 
		(!lol->IsDead)				    && (plr->GetAvatarHandle() == uiSerialId) &&
		(!plr->GetPlayerDead())         && (!plr->GetSkillInUse()))
	{
		if (plr->GetPcProfile()->byLevel <= 5)
			formula = rand() % 50 + 5 + plr->cPlayerAttribute->GetAvatarAttribute().wLastEnergyOffence + plr->cPlayerAttribute->GetAvatarAttribute().wLastPhysicalOffence;
		else
			if(rand() % 2 == 1) //until we can figure this out
				formula = (plr->cPlayerAttribute->GetAvatarAttribute().wLastEnergyOffence + plr->cPlayerAttribute->GetAvatarAttribute().byLastSol);
			else
				formula = (plr->cPlayerAttribute->GetAvatarAttribute().wLastPhysicalOffence + plr->cPlayerAttribute->GetAvatarAttribute().byLastStr);

		res->wAttackResultValue = formula;
		res->fReflectedDamage = 0;
		res->vShift.x = plr->GetPlayerPosition().x;
		res->vShift.y = plr->GetPlayerPosition().y;
		res->vShift.z = plr->GetPlayerPosition().z;

		res->byAttackSequence = byChainAttack;//rand()%6;
		cout << "AttackSequence is " << byChainAttack << endl;
		cout << "Player chain is " << plr->GetChainAttack() << endl;
		res->bChainAttack = true;

		if (res->bChainAttack)
			res->byAttackSequence = byChainAttack % 6; //+ LK_BATTLE_CHAIN_ATTACK_START;
		else
			res->byAttackSequence = rand() % 2;

		if (res->byAttackSequence == 6)
		{
			plr->SetChainAttack(1);
			byChainAttack = 1;
			if (rand() % 2)
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_KNOCKDOWN;
			}
			else
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_SLIDING;
			}
		}
		else
		{
			RwInt32 iRandValue = rand() % 5;
			if (iRandValue <= 2)
			{
				res->byAttackResult = BATTLE_ATTACK_RESULT_HIT;
				bDamageApply = true;
			}
			else if (iRandValue == 5)
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_CRITICAL_HIT;
			}

			else if (iRandValue == 3)
			{
				bDamageApply = false;
				res->byAttackResult = BATTLE_ATTACK_RESULT_DODGE;
			}
			else
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_BLOCK;
				res->byBlockedAction = DBO_GUARD_TYPE_DAMAGE_SKILL;
			}
		}
		res->byAttackSequence = (res->byAttackSequence == 0 ? 1 : res->byAttackSequence);//NEVER LET THE 0 Comes because in Attack they are always 1 if the 0 comes then you got a crash - Luiz45
		packet.SetPacketLen(sizeof(sGU_CHAR_ACTION_ATTACK));
		int rc = g_pApp->Send(this->GetHandle(), &packet);
		app->UserBroadcast(&packet);
		app->UserBroadcastothers(&packet, this);

		plr->SetChainAttack(byChainAttack++);
		plr->SetLastTimeFight(timeGetTime());
		// update LP
		if (bDamageApply == true)
		{
			CurHP -= (res->wAttackResultValue * 2);
			g_pMobManager->GetMobByHandle(m_uiTargetSerialId)->CurLP = CurHP;
			g_pMobManager->GetMobByHandle(m_uiTargetSerialId)->target = uiSerialId;
		}
		if (CurHP <= 0)
		{
			plr->SetPlayerFight(false);
			CClientSession::SendMobLoot(&packet, app, m_uiTargetSerialId);
			CurHP = 0;
			SendCharUpdateFaintingState(pPacket, app, uiSerialId, m_uiTargetSerialId);
			g_pMobManager->GetMobByHandle(m_uiTargetSerialId)->IsDead = true;
			lol->IsDead = true;
			plr->SetChainAttack(1);
			byChainAttack = 1;
			plr->SetAttackTarget(0);
		}
		else
			SendCharUpdateLp(pPacket, app, CurHP, m_uiTargetSerialId);
		//If we are attacking a Mob then we gonna send a request to attack our little and beautiful player
		if ((lol != NULL) && (!lol->IsDead))
		{
			
		}
	}	
	plr = NULL;
	delete plr;
}
void CClientSession::SendMobActionAttack(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId, CLkPacket * pPacket)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	static int byChainAttack = plr->ChainNumber();
	int CurHP = 0;
	RwBool bDamageApply = true;
	float formula;
	plr->SetPlayerFight(true);
	CLkPacket packet(sizeof(sGU_CHAR_ACTION_ATTACK));
	sGU_CHAR_ACTION_ATTACK * res = (sGU_CHAR_ACTION_ATTACK *)packet.GetPacketData();
	
	

	res->wOpCode = GU_CHAR_ACTION_ATTACK;
	res->hSubject = uiSerialId;
	res->hTarget = m_uiTargetSerialId;
	res->dwLpEpEventId = 255;
	res->byBlockedAction = 255;
	CMonster::MonsterData *lol = NULL;
	//If we Find the Target as Mob or if the Attacker is Mob
	if (g_pMobManager->FindCreature(uiSerialId))
	{
		lol = g_pMobManager->GetMobByHandle(uiSerialId);
		CurHP = plr->GetPcProfile()->wCurLP;
	}
	
	if (CurHP <= 0)
		CurHP = 0;
	//For player Attacks we will check if he is in fighting,if the Mob/Target has HP> than 0,and if the Attacker owner is the Player
	if ((CurHP > 0) &&	(!lol->IsDead) && 
		(plr->GetAvatarHandle() != uiSerialId) &&	(!plr->GetPlayerDead()) && 
		(!plr->GetSkillInUse()))
	{
		if (lol->Level <= 5)
			formula = rand() % 50 + 5;
		else
			formula = (lol->Str * lol->Level) * .08;

		res->wAttackResultValue = formula;
		res->fReflectedDamage = 0;
		res->vShift.x = lol->curPos.x;
		res->vShift.y = lol->curPos.y;
		res->vShift.z = lol->curPos.z;

		res->byAttackSequence = byChainAttack;//rand()%6;
		cout << "AttackSequence is " << byChainAttack << endl;
		res->bChainAttack = true;

		res->byAttackSequence = rand() % 2;

		//Change to Use Mob Skill or whatever
		if (res->byAttackSequence == 6)
		{
			//plr->SetChainAttack(0);
			byChainAttack = 0;
			if (rand() % 2)
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_KNOCKDOWN;
			}
			else
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_SLIDING;
			}
		}
		else
		{
			RwInt32 iRandValue = rand() % 5;
			if (iRandValue <= 2)
			{
				res->byAttackResult = BATTLE_ATTACK_RESULT_HIT;
				bDamageApply = true;
			}
			else if (iRandValue == 5)
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_CRITICAL_HIT;
			}

			else if (iRandValue == 3)
			{
				bDamageApply = false;
				res->byAttackResult = BATTLE_ATTACK_RESULT_DODGE;
			}
			else
			{
				bDamageApply = true;
				res->byAttackResult = BATTLE_ATTACK_RESULT_BLOCK;
				res->byBlockedAction = 1;
			}
		}
		res->byAttackSequence = (res->byAttackSequence == 0 ? 1 : res->byAttackSequence);//NEVER LET THE 0 Comes because in Attack they are always 1 if the 0 comes then you got a crash - Luiz45
		packet.SetPacketLen(sizeof(sGU_CHAR_ACTION_ATTACK));
		int rc = g_pApp->Send(this->GetHandle(), &packet);
		app->UserBroadcast(&packet);

		lol->chainAttackCount = byChainAttack;		
		// update LP
		if (bDamageApply == true)
		{
			CurHP -= (res->wAttackResultValue * 2);
			plr->GetPcProfile()->wCurLP = CurHP;
		}
		if (CurHP <= 0)
		{
			plr->SetPlayerFight(false);
			CurHP = 0;
			plr->SendThreadUpdateDeathStatus();
			plr->SetChainAttack(0);
			byChainAttack = 1;

		}
		else
			SendCharUpdateLp(pPacket, app, CurHP, m_uiTargetSerialId);


		//If we are attacking a Mob then we gonna send a request to attack our little and beautiful player
		if ((plr != NULL) && (!plr->GetPlayerDead()))
		{
			CLkPacket packetMob(sizeof(sUG_CHAR_ATTACK_BEGIN));
			sUG_CHAR_ATTACK_BEGIN* req = (sUG_CHAR_ATTACK_BEGIN*)packetMob.GetPacketData();
			req->byAvatarType = 0;
			req->byType = 0;
			req->wOpCode = UG_CHAR_ATTACK_BEGIN;
			packetMob.SetPacketLen(sizeof(sUG_CHAR_ATTACK_BEGIN));
			Sleep(2300);
			app->GetInstance()->Send(plr->GetSession(), &packetMob);			
		}
	}
}
void CClientSession::SendCharUpdateLp(CLkPacket * pPacket, CGameServer * app, RwUInt16 wLp, RwUInt32 m_uiTargetSerialId)
{
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_LP));
	sGU_UPDATE_CHAR_LP * res = (sGU_UPDATE_CHAR_LP *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	res->wOpCode = GU_UPDATE_CHAR_LP;
	res->handle = m_uiTargetSerialId;
	if (g_pMobManager->FindCreature(m_uiTargetSerialId) == true)
	{
		CMonster::MonsterData *lol = g_pMobManager->GetMobByHandle(m_uiTargetSerialId);
		if (lol != NULL)
		{
			lol->FightMode = true;
			lol->CurLP = (WORD)wLp;
			res->wCurLP = lol->CurLP;
			res->wMaxLP = lol->MaxLP;
			res->dwLpEpEventId = 255;
			packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LP));
			app->UserBroadcastothers(&packet, this);
			g_pApp->Send(this->GetHandle(), &packet);
			if (lol->isAggro == false)
			{
				lol->isAggro = true;
				lol->target = plr->GetAvatarHandle();
			}
		}
	}
	else
	{
		this->gsf->printOk("PVP OK");
		PlayersMain *lol2 = g_pPlayerManager->GetPlayer(m_uiTargetSerialId);
		if (lol2)
		{
			res->wCurLP = lol2->GetPcProfile()->wCurLP = wLp;
			res->wMaxLP = lol2->GetPcProfile()->avatarAttribute.wLastMaxLP;
			packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LP));

			app->UserBroadcastothers(&packet, this);
			g_pApp->Send(this->GetHandle(), &packet);
		}
	}

	plr = NULL;
	delete plr;
}
void	CClientSession::SendMobLoot(CLkPacket * pPacket, CGameServer * app, RwUInt32 m_uiTargetSerialId)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res2 = (sGU_OBJECT_CREATE *)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res3 = (sGU_OBJECT_CREATE *)packet3.GetPacketData();

	CLkPacket packet4(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res4 = (sGU_OBJECT_CREATE *)packet4.GetPacketData();

	CLkPacket packet5(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res5 = (sGU_OBJECT_CREATE *)packet5.GetPacketData();

	int mobid = 0;
	CMonster::MonsterData* mob = g_pMobManager->GetMobByHandle(m_uiTargetSerialId);

	//Randomizing numbers
	std::random_device rd;
	std::mt19937_64 mt(rd());
	std::uniform_real_distribution<float> distribution(0, 20.0f);

	if ((mobid = mob->MonsterID) != 0)
	{
		sMOB_TBLDAT* mob = (sMOB_TBLDAT*)app->g_pTableContainer->GetMobTable()->FindData(mobid);
		/*printf("Each rate control %d\n", mob->byDropEachRateControl);
		printf("Drop Eeach Item %d\n", mob->byDropEItemRateControl);
		printf("Drop Legendary Item %d\n", mob->byDropLItemRateControl);
		printf("Drop Normal Item %d\n", mob->byDropNItemRateControl);
		printf("Drop Superior Item %d\n", mob->byDropSItemRateControl);
		printf("Drop Type %d\n", mob->byDropTypeRateControl);
		printf("Drop each tblidx %d\n", mob->dropEachTblidx);
		printf("Drop Quest tblidx %d\n", mob->dropQuestTblidx);
		printf("Drop Type TBLIDX %d\n", mob->dropTypeTblidx);
		printf("Drop Item TBLIDX %d\n", mob->drop_Item_Tblidx);*/
		CBasicDropTable *bDrop = app->g_pTableContainer->GetBasicDropTable();
		CEachDropTable * edrop = app->g_pTableContainer->GetEachDropTable();
		CNormalDropTable * ndrop = app->g_pTableContainer->GetNormalDropTable();
		CSuperiorDropTable * sdrop = app->g_pTableContainer->GetSuperiorDropTable();
		CLegendaryDropTable * ldrop = app->g_pTableContainer->GetLegendaryDropTable();
		CExcellentDropTable * exdrop = app->g_pTableContainer->GetExcellentDropTable();
		CItemTable * iTable = app->g_pTableContainer->GetItemTable();
		CTypeDropTable * tdrop = app->g_pTableContainer->GetTypeDropTable();
		sVECTOR3 playerPos = plr->GetPlayerPosition();
		if ((rand() % 100) >= mob->fDrop_Zenny_Rate)
		{
			res->handle = AcquireItemSerialId();
			res->sObjectInfo.objType = OBJTYPE_DROPMONEY;
			res->sObjectInfo.moneyBrief.dwZenny = mob->dwDrop_Zenny * app->dZennyMultiplier;
			res->sObjectInfo.moneyState.bIsNew = true;
			sVECTOR3 mypos = plr->GetPlayerPosition();
			res->sObjectInfo.moneyState.vCurLoc.x = mypos.x + rand() % 2;
			res->sObjectInfo.moneyState.vCurLoc.y = mypos.y;
			res->sObjectInfo.moneyState.vCurLoc.z = mypos.z + rand() % 2;
			res->wOpCode = GU_OBJECT_CREATE;

			packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
			g_pApp->Send(this->GetHandle(), &packet);
			app->AddNewZennyAmount(res->handle, mob->dwDrop_Zenny);
		}
		//Drop Each Table
		if (mob->dropEachTblidx != INVALID_TBLIDX)
		{
			sEACH_DROP_TBLDAT* eDropDat = (sEACH_DROP_TBLDAT*)(edrop->FindData(mob->dropEachTblidx));
			for (int i = 0; i < LK_MAX_EACH_DROP; i++)
			{
				float random = distribution(mt);
				//if ((random >= eDropDat->afDrop_Rate[i]) && eDropDat->aItem_Tblidx[i] < 0)
				if (eDropDat->aItem_Tblidx[i] != INVALID_TBLIDX && (random <= eDropDat->afDrop_Rate[i]))
				{
					res2->handle = AcquireItemSerialId();
					res2->sObjectInfo.objType = OBJTYPE_DROPITEM;
					res2->sObjectInfo.itemBrief.tblidx = eDropDat->aItem_Tblidx[i];
					res2->sObjectInfo.itemBrief.byGrade = 1;
					res2->sObjectInfo.itemBrief.byRank = 1;
					res2->sObjectInfo.itemState.bIsNew = true;
					res2->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
					res2->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
					res2->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
					res2->wOpCode = GU_OBJECT_CREATE;
					//printf("Item Created %d\n\r", eDropDat->aItem_Tblidx[i]);
					packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
					g_pApp->Send(this->GetHandle(), &packet2);
					app->AddNewItemDrop(res2->handle, eDropDat->aItem_Tblidx[i], 1, 1);
				}
			}
		}
		//Main Drop Tables
		if (mob->drop_Item_Tblidx != INVALID_TBLIDX && mob->drop_Item_Tblidx < 200000)
		{
			sBASIC_DROP_TBLDAT* DropDat = (sBASIC_DROP_TBLDAT*)(bDrop->FindData(mob->drop_Item_Tblidx));
			if (DropDat != NULL)
			{
				for (int i = 0; i < LK_MAX_DROP_TABLE_SELECT; i++)
				{
					float random = distribution(mt);
					//if ((random >= eDropDat->afDrop_Rate[i]) && eDropDat->aItem_Tblidx[i] < 0)
					//Normal Drops
					if (DropDat->aNoramalDropTblidx[i] != INVALID_TBLIDX && (random <= DropDat->afNoramalTblidxRate[i]))
					{
						sNORMAL_DROP_TBLDAT* nDropDat = (sNORMAL_DROP_TBLDAT*)(ndrop->FindData(DropDat->aNoramalDropTblidx[i]));
						cout << "nDropDat tblidx = " << nDropDat->aItem_Tblidx[i] << endl;
						if (nDropDat->aItem_Tblidx[i] != INVALID_TBLIDX && (random <= nDropDat->afDrop_Rate[i])) 
						{							
							sITEM_TBLDAT* NormalItem = (sITEM_TBLDAT*)app->g_pTableContainer->GetItemTable()->FindData(nDropDat->aItem_Tblidx[i]);
							res2->handle = AcquireItemSerialId();
							res2->sObjectInfo.objType = OBJTYPE_DROPITEM;
							res2->sObjectInfo.itemBrief.tblidx = nDropDat->aItem_Tblidx[i];
							res2->sObjectInfo.itemBrief.byGrade = 1;
							res2->sObjectInfo.itemBrief.byRank = NormalItem->byRank;
							res2->sObjectInfo.itemState.bIsNew = true;
							res2->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
							res2->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
							res2->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
							res2->wOpCode = GU_OBJECT_CREATE;
							printf("Item Created %d\n\r", nDropDat->aItem_Tblidx[i]);
							packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
							g_pApp->Send(this->GetHandle(), &packet2);
							app->AddNewItemDrop(res2->handle, nDropDat->aItem_Tblidx[i], 1, NormalItem->byRank);
						}
					}
					//Superior Drops
					if (DropDat->aSuperiorDropTblidx[i] != INVALID_TBLIDX && (random <= DropDat->afSuperiorTblidxRate[i]))
					{
						sSUPERIOR_DROP_TBLDAT* sDropDat = (sSUPERIOR_DROP_TBLDAT*)(sdrop->FindData(DropDat->aSuperiorDropTblidx[i]));
						for (int z = 0; z < LK_MAX_SUPERIOR_DROP; z++)
						{
							if (sDropDat->aItem_Tblidx[z] != INVALID_TBLIDX && sDropDat->aItem_Tblidx[z] < 200000 && (random <= sDropDat->afDrop_Rate[i]))
							{
								cout << "sDropDat tblidx = " << sDropDat->aItem_Tblidx[i] << endl;
								//Randomizing numbers for  see if the player get a Fucking item
								std::random_device rd2;
								std::mt19937_64 mt2(rd2());
								std::uniform_int_distribution<int> distribution2(1, 3);
								sITEM_TBLDAT* pSuperior = (sITEM_TBLDAT*)app->g_pTableContainer->GetItemTable()->FindData(sDropDat->aItem_Tblidx[z]);
								res2->handle = AcquireItemSerialId();
								res2->sObjectInfo.objType = OBJTYPE_DROPITEM;
								res2->sObjectInfo.itemBrief.tblidx = sDropDat->aItem_Tblidx[z];
								res2->sObjectInfo.itemBrief.byGrade = distribution2(mt2);
								res2->sObjectInfo.itemBrief.byRank = ITEM_RANK_SUPERIOR;
								res2->sObjectInfo.itemState.bIsNew = true;
								res2->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
								res2->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->wOpCode = GU_OBJECT_CREATE;
								printf("Item Created %d\n\r", sDropDat->aItem_Tblidx[z]);
								packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
								g_pApp->Send(this->GetHandle(), &packet2);
								app->AddNewItemDrop(res2->handle, sDropDat->aItem_Tblidx[z], res2->sObjectInfo.itemBrief.byGrade, ITEM_RANK_SUPERIOR);
							}
						}

					}
					//Excellent Drops
					if (DropDat->aExcellentDropTblidx[i] != INVALID_TBLIDX && (random <= DropDat->afExcellentTblidxRate[i]))
					{

						sEXCELLENT_DROP_TBLDAT* sDropDat = (sEXCELLENT_DROP_TBLDAT*)(sdrop->FindData(DropDat->aExcellentDropTblidx[i]));
						cout << "eDropDat tblidx = " << sDropDat->aItem_Tblidx[i] << endl;						
						for (int z = 0; z < LK_MAX_EXCELLENT_DROP; z++)
						{
							if (sDropDat->aItem_Tblidx[z] != INVALID_TBLIDX && sDropDat->aItem_Tblidx[z] < 200000 && (random <= sDropDat->afDrop_Rate[i]))
							{
								//Randomizing numbers for  see if the player get a Fucking item
								std::random_device rd3;
								std::mt19937_64 mt3(rd3());
								std::uniform_int_distribution<int> distribution3(1, 10);
								int iGrade = distribution3(mt3);
								sITEM_TBLDAT* pExcelent = (sITEM_TBLDAT*)app->g_pTableContainer->GetItemTable()->FindData(sDropDat->aItem_Tblidx[z]);
								res2->handle = AcquireItemSerialId();
								res2->sObjectInfo.objType = OBJTYPE_DROPITEM;
								res2->sObjectInfo.itemBrief.tblidx = sDropDat->aItem_Tblidx[z];
								if (iGrade > 6)
								{
									res2->sObjectInfo.itemBrief.byGrade = iGrade;
									res2->sObjectInfo.itemBrief.byRank = ITEM_RANK_RARE;
								}
								else
								{
									res2->sObjectInfo.itemBrief.byGrade = iGrade;
									res2->sObjectInfo.itemBrief.byRank = ITEM_RANK_EXCELLENT;
								}
								res2->sObjectInfo.itemState.bIsNew = true;
								res2->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
								res2->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->wOpCode = GU_OBJECT_CREATE;
								printf("Item Created %d\n\r", DropDat->aExcellentDropTblidx[z]);
								packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
								g_pApp->Send(this->GetHandle(), &packet2);
								app->AddNewItemDrop(res2->handle, sDropDat->aItem_Tblidx[z], res2->sObjectInfo.itemBrief.byGrade, res2->sObjectInfo.itemBrief.byRank);
							}

						}
					}
					//Legendary Drops
					if (DropDat->aLegendaryDropTblidx[i] != INVALID_TBLIDX  && (random <= DropDat->afLegendaryTblidxRate[i]))
					{
						sLEGENDARY_DROP_TBLDAT* sDropDat = (sLEGENDARY_DROP_TBLDAT*)(sdrop->FindData(DropDat->aLegendaryDropTblidx[i]));
						cout << "lDropDat tblidx = " << sDropDat->aItem_Tblidx[i] << endl;
						for (int z = 0; z < LK_MAX_LEGENDARY_DROP; z++)
						{
							if (sDropDat->aItem_Tblidx[z] != INVALID_TBLIDX && sDropDat->aItem_Tblidx[z] < 200000 && (random <= sDropDat->afDrop_Rate[i]))
							{
								//Randomizing numbers for  see if the player get a Fucking item
								std::random_device rd4;
								std::mt19937_64 mt4(rd4());
								std::uniform_int_distribution<int> distribution4(10, 15);
								sITEM_TBLDAT* pLegendary = (sITEM_TBLDAT*)app->g_pTableContainer->GetItemTable()->FindData(sDropDat->aItem_Tblidx[z]);
								res2->handle = AcquireItemSerialId();
								res2->sObjectInfo.objType = OBJTYPE_DROPITEM;
								res2->sObjectInfo.itemBrief.tblidx = sDropDat->aItem_Tblidx[z];
								res2->sObjectInfo.itemBrief.byGrade = distribution4(mt4);
								res2->sObjectInfo.itemBrief.byRank = ITEM_RANK_LEGENDARY;
								res2->sObjectInfo.itemState.bIsNew = true;
								res2->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
								res2->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
								res2->wOpCode = GU_OBJECT_CREATE;
								printf("Item Created %d\n\r", sDropDat->aItem_Tblidx[z]);
								packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
								g_pApp->Send(this->GetHandle(), &packet2);
								app->AddNewItemDrop(res2->handle, sDropDat->aItem_Tblidx[z], res2->sObjectInfo.itemBrief.byGrade, res2->sObjectInfo.itemBrief.byRank);
							}
						}

					}
				}
			}
			//Drop Quest Table
			
			//if (mob->dropQuestTblidx != INVALID_TBLIDX)
			//{
			//	printf("DropQuest not = 0\n\r");
			//	sQUEST_DROP_TBLDAT* qDropDat = (sQUEST_DROP_TBLDAT*)mob->dropQuestTblidx;
			//	for (int i = 0; i < 5; i++)
			//	{
			//		float random = distribution(mt);
			//		//cout << "Drop rate " << qDropDat->aDropRate[i] << " random is " << random << endl;

			//		printf("%d\n\r", i);
			//		if ((random >= qDropDat->aDropRate[i]) && qDropDat->aQuestItemTblidx[i] < 0)
			//		{
			//			res3->handle = AcquireSerialId();
			//			res3->sObjectInfo.objType = OBJTYPE_DROPITEM;
			//			res3->sObjectInfo.itemBrief.tblidx = qDropDat->aQuestItemTblidx[i];
			//			res3->sObjectInfo.itemBrief.byGrade = 1;
			//			res3->sObjectInfo.itemBrief.byRank = 1;
			//			res3->sObjectInfo.itemState.bIsNew = true;
			//			res3->sObjectInfo.itemState.vCurLoc.x = playerPos.x + (distribution(mt));
			//			res3->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
			//			res3->sObjectInfo.itemState.vCurLoc.z = playerPos.z + (distribution(mt));
			//			res3->wOpCode = GU_OBJECT_CREATE;
			//			printf("Item Created %d", qDropDat->aQuestItemTblidx[i]);
			//			packet3.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
			//			g_pApp->Send(this->GetHandle(), &packet3);
			//		app->AddNewItemDrop(res3->handle, qDropDat->aQuestItemTblidx[i], 1, 1);
			//		}
			//	}
			//}
			/*if (mob->dropTypeTblidx != NULL)
			{
			sTYPE_DROP_TBLDAT* tDropDat = (sTYPE_DROP_TBLDAT*)tdrop->FindData(mob->dropEachTblidx);
			if (tDropDat->aItem_Tblidx[0] != NULL)
			{
			res3->handle = AcquireSerialId();
			res3->sObjectInfo.objType = OBJTYPE_DROPITEM;
			res3->sObjectInfo.itemBrief.tblidx = tDropDat->aItem_Tblidx[0];
			res3->sObjectInfo.itemBrief.byGrade = 1;
			res3->sObjectInfo.itemBrief.byRank = 1;
			res3->sObjectInfo.itemState.bIsNew = true;
			res3->sObjectInfo.itemState.vCurLoc.x = playerPos.x + ((rand() / (float)RAND_MAX * 2) + 0.1f);
			res3->sObjectInfo.itemState.vCurLoc.y = playerPos.y;
			res3->sObjectInfo.itemState.vCurLoc.z = playerPos.z + ((rand() / (float)RAND_MAX * 2) + 0.1f);
			res3->wOpCode = GU_OBJECT_CREATE;

			packet3.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
			g_pApp->Send(this->GetHandle(), &packet3);
			app->AddNewItemDrop(res3->handle, tDropDat->aItem_Tblidx[0]);
			}*/
			//}
		}

		if (plr->GetPcProfile()->byLevel < 50)
			CClientSession::SendPlayerLevelUpCheck(app, mob->wExp);

		plr = NULL;
		delete plr;
	}
}
void CClientSession::SendCharUpdateFaintingState(CLkPacket * pPacket, CGameServer * app, RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId)
{
	//printf("char die: %i \n", m_uiTargetSerialId);
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	RemoveAttackBegin(uiSerialId, m_uiTargetSerialId);
	plr->SetPlayerFight(false);
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
	if (g_pMobManager->FindCreature(m_uiTargetSerialId))
	{
		res->wOpCode = GU_UPDATE_CHAR_STATE;
		res->handle = m_uiTargetSerialId;
		res->sCharState.sCharStateBase.byStateID = CHARSTATE_FAINTING;
		res->sCharState.sCharStateBase.vCurLoc.x = plr->GetPlayerPosition().x;
		res->sCharState.sCharStateBase.vCurLoc.y = plr->GetPlayerPosition().y;
		res->sCharState.sCharStateBase.vCurLoc.z = plr->GetPlayerPosition().z;
		res->sCharState.sCharStateBase.vCurDir.x = plr->GetPlayerDirection().x;
		res->sCharState.sCharStateBase.vCurDir.y = plr->GetPlayerDirection().y;
		res->sCharState.sCharStateBase.vCurDir.z = plr->GetPlayerDirection().z;

		packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
		app->UserBroadcastothers(&packet, this);
		g_pApp->Send(this->GetHandle(), &packet);
		g_pMobManager->UpdateDeathStatus(m_uiTargetSerialId, true);
	}
	plr = NULL;
	delete plr;
}

void CClientSession::SendCharSkillRes(CLkPacket * pPacket, CGameServer * app)
{
	SSkillData *pSkillData = new SSkillData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_CHAR_SKILL_REQ *pCharSkillReq = (sUG_CHAR_SKILL_REQ*)pPacket->GetPacketData();

	app->db->prepare("SELECT * FROM skills WHERE owner_id=? AND SlotID= ? ");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, pCharSkillReq->bySlotIndex);
	app->db->execute();
	app->db->fetch();

	int skillID = app->db->getInt("skill_id");
	CSkillTable * skillTable = app->g_pTableContainer->GetSkillTable();
	sSKILL_TBLDAT * skillDataOriginal = reinterpret_cast<sSKILL_TBLDAT*>(skillTable->FindData(skillID));
	plr->SetSkillInUse(true);
	
	pSkillData->pCharSkillTarget = pCharSkillReq->hTarget;
	pSkillData->m_uiSkillTblId = skillID;	//GetSkillId[m_Char.iClass][pCharSkillReq->bySlotIndex];
	pSkillData->m_bySkillActiveType = SKILL_TYPE_CASTING;
	pSkillData->m_uiSkillTime = timeGetTime();

	CLkPacket packet(sizeof(sGU_CHAR_SKILL_RES));
	sGU_CHAR_SKILL_RES * sPacket = (sGU_CHAR_SKILL_RES *)packet.GetPacketData();

	sPacket->wOpCode = GU_CHAR_SKILL_RES;
	sPacket->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_CHAR_SKILL_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcast(&packet);
	plr = NULL;
	delete plr;
	switch (skillDataOriginal->bySkill_Active_Type)
	{
	case SKILL_ACTIVE_TYPE_DD:
	{
		printf("SKILL_ACTIVE_TYPE_DD\n");
		SendCharSkillAction(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_BB:
	{
		printf("SKILL_ACTIVE_TYPE_BB\n");
		SendCharSkillCasting(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_CB:
	{
		printf("SKILL_ACTIVE_TYPE_CB\n");
		SendCharSkillAction(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_DB:
	{
		printf("SKILL_ACTIVE_TYPE_DB\n");
		SendCharSkillCasting(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_DC:
	{
		printf("SKILL_ACTIVE_TYPE_DC\n");
		SendCharSkillCasting(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_DH:
	{
		printf("SKILL_ACTIVE_TYPE_DH\n");
		SendCharSkillCasting(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	case SKILL_ACTIVE_TYPE_DOT:
	{
		printf("SKILL_ACTIVE_TYPE_DOT\n");
		SendCharSkillAction(pPacket, app, skillDataOriginal, pCharSkillReq->byRpBonusType);
	}break;
	}

	/*if ((skillDataOriginal->dwKeepTimeInMilliSecs != 0) || (skillDataOriginal->dwTransform_Use_Info_Bit_Flag==1))
	SendCharSkillCasting(pPacket, app, skillID);
	else
	SendCharSkillAction(pPacket, app, skillID);*/
}
//--------------------------------------------------------------------------------------//
//		Char Skill Send
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharSkillAction(CLkPacket * pPacket, CGameServer * app, sSKILL_TBLDAT* SkillNow, int RpSelectedType)
{
	CLkPacket packet(sizeof(sGU_CHAR_ACTION_SKILL));
	sGU_CHAR_ACTION_SKILL * res = (sGU_CHAR_ACTION_SKILL *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CSkillTable *pSkillTbl = app->g_pTableContainer->GetSkillTable();

	res->wOpCode = GU_CHAR_ACTION_SKILL;
	res->handle = this->GetavatarHandle();
	res->wResultCode = GAME_SUCCESS;
	res->hAppointedTarget = this->GetTargetSerialId();
	res->skillId = SkillNow->tblidx;
	res->dwLpEpEventId = SkillNow->tblidx;
	res->bySkillResultCount = 1;

	res->byRpBonusType = RpSelectedType;
	res->aSkillResult[0].hTarget = this->GetTargetSerialId();
	res->aSkillResult[0].byAttackResult = this->gsf->GetBattleResultEffect(RpSelectedType);
	res->aSkillResult[0].effectResult1.fResultValue = SkillNow->fSkill_Effect_Value[0];
	res->aSkillResult[0].effectResult2.fResultValue = SkillNow->fSkill_Effect_Value[1];
	res->aSkillResult[0].byBlockedAction = 255;
	res->aSkillResult[1].hTarget = this->GetTargetSerialId() + 1;
	res->aSkillResult[1].byAttackResult = this->gsf->GetBattleResultEffect(RpSelectedType);
	res->aSkillResult[1].effectResult1.fResultValue = SkillNow->fSkill_Effect_Value[0];
	res->aSkillResult[1].effectResult2.fResultValue = SkillNow->fSkill_Effect_Value[1];
	res->aSkillResult[1].byBlockedAction = 255;

	//Char update EP
	CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_EP));
	sGU_UPDATE_CHAR_EP * pUpdateEp = (sGU_UPDATE_CHAR_EP*)packet2.GetPacketData();
	plr->GetPcProfile()->wCurEP = (plr->GetPcProfile()->wCurEP - SkillNow->wRequire_EP);//Sub by Required EP
	pUpdateEp->handle = this->GetavatarHandle();
	pUpdateEp->wCurEP = plr->GetPcProfile()->wCurEP;
	pUpdateEp->wMaxEP = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP;
	pUpdateEp->wOpCode = GU_UPDATE_CHAR_EP;

	//Condition for LP...skill is Optional
	CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_LP));
	if (SkillNow->wRequire_LP)
	{
		sGU_UPDATE_CHAR_LP * pUpdateLp = (sGU_UPDATE_CHAR_LP*)packet3.GetPacketData();
		plr->GetPcProfile()->wCurLP -= SkillNow->wRequire_LP;//Sub by Required EP
		pUpdateLp->handle = this->GetavatarHandle();
		pUpdateLp->wCurLP = plr->GetPcProfile()->wCurLP;
		pUpdateLp->wMaxLP = plr->GetPcProfile()->avatarAttribute.wBaseMaxLP;
		pUpdateLp->wOpCode = GU_UPDATE_CHAR_LP;

	}

	//Preparing packets
	packet.SetPacketLen(sizeof(sGU_CHAR_ACTION_SKILL));
	packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EP));
	packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LP));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet3);
	app->UserBroadcastothers(&packet2, this);
	app->UserBroadcastothers(&packet3, this);
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
	float newLP = 0;	
	if (g_pMobManager->FindCreature(res->hAppointedTarget) == true) //MOBS
	{
		CMonster::MonsterData *lol = g_pMobManager->GetMobByHandle(res->hAppointedTarget);
		if (lol != NULL)
		{

			lol->FightMode = true;
			newLP = (float)lol->CurLP;
			newLP -= res->aSkillResult[0].effectResult1.DD_DOT_fDamage + 100;
			printf("LP: %f, damage: %f\n", newLP, res->aSkillResult[0].effectResult1.DD_DOT_fDamage + 100);
			if (newLP <= 0 || (newLP > lol->MaxLP))
			{
				lol->IsDead = true;
				CClientSession::SendMobLoot(&packet, app, res->hAppointedTarget);
				this->gsf->printOk("DIE MOTHER FUCKER");
				SendCharUpdateFaintingState(pPacket, app, this->GetavatarHandle(), res->hAppointedTarget);
			}
			else if (newLP > 0 && lol->IsDead == false)
			{
				SendCharUpdateLp(pPacket, app, newLP, res->hAppointedTarget);
			}
		}
	}
}
//-------------------------------------------------------------------//
//----------Fixed Casting Buff/Transform Skills - Luiz45-------------//
//-------------------------------------------------------------------//
void CClientSession::SendCharSkillCasting(CLkPacket * pPacket, CGameServer * app, sSKILL_TBLDAT* SkillNow, int RpSelectedType)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	//Skill Events Prepare
	CLkPacket packet(sizeof(sGU_CHAR_ACTION_SKILL));
	CSkillTable *pSkillTbl = app->g_pTableContainer->GetSkillTable();
	sGU_CHAR_ACTION_SKILL * res = (sGU_CHAR_ACTION_SKILL *)packet.GetPacketData();

	res->skillId = SkillNow->tblidx;
	res->wResultCode = GAME_SUCCESS;
	res->byRpBonusType = RpSelectedType;//Untested
	res->wOpCode = GU_CHAR_ACTION_SKILL;
	res->handle = this->GetavatarHandle();//My Handle
	res->hAppointedTarget = this->GetTargetSerialId();//Get myself

	//Buff Events Prepare
	CLkPacket packet2(sizeof(sGU_BUFF_REGISTERED));
	sGU_BUFF_REGISTERED * pBuffData = (sGU_BUFF_REGISTERED*)packet2.GetPacketData();
	pBuffData->wOpCode = GU_BUFF_REGISTERED;
	pBuffData->tblidx = SkillNow->tblidx;
	pBuffData->hHandle = this->GetavatarHandle();
	pBuffData->dwInitialDuration = SkillNow->dwKeepTimeInMilliSecs;
	pBuffData->dwTimeRemaining = SkillNow->dwKeepTimeInMilliSecs;
	pBuffData->afEffectValue[0] = SkillNow->fSkill_Effect_Value[0];
	pBuffData->afEffectValue[1] = SkillNow->fSkill_Effect_Value[1];
	pBuffData->bySourceType = DBO_OBJECT_SOURCE_SKILL;

	plr->cPlayerSkills->AddBuff(SkillNow);
	
	//Char update EP
	CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_EP));
	sGU_UPDATE_CHAR_EP * pUpdateEp = (sGU_UPDATE_CHAR_EP*)packet3.GetPacketData();
	plr->GetPcProfile()->wCurEP = (plr->GetPcProfile()->wCurEP - SkillNow->wRequire_EP);//Sub by Required EP
	pUpdateEp->handle = this->GetavatarHandle();
	pUpdateEp->wCurEP = plr->GetPcProfile()->wCurEP;
	pUpdateEp->wMaxEP = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP;
	pUpdateEp->wOpCode = GU_UPDATE_CHAR_EP;

	//Condition for LP...skill is Optional
	CLkPacket packet4(sizeof(sGU_UPDATE_CHAR_LP));
	if (SkillNow->wRequire_LP)
	{
		sGU_UPDATE_CHAR_LP * pUpdateLp = (sGU_UPDATE_CHAR_LP*)packet4.GetPacketData();
		plr->GetPcProfile()->wCurLP = (plr->GetPcProfile()->wCurLP - SkillNow->wRequire_LP);//Sub by Required LP
		pUpdateLp->handle = this->GetavatarHandle();
		pUpdateLp->wCurLP = plr->GetPcProfile()->wCurLP;
		pUpdateLp->wMaxLP = plr->GetPcProfile()->avatarAttribute.wBaseMaxLP;
		pUpdateLp->wOpCode = GU_UPDATE_CHAR_LP;

	}

	packet.SetPacketLen(sizeof(sGU_CHAR_ACTION_SKILL));
	packet2.SetPacketLen(sizeof(sGU_BUFF_REGISTERED));
	packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EP));
	packet4.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LP));

	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet3);
	g_pApp->Send(this->GetHandle(), &packet4);
	app->UserBroadcastothers(&packet2, this);
	app->UserBroadcastothers(&packet, this);
	app->UserBroadcastothers(&packet3, this);
	app->UserBroadcastothers(&packet4, this);
	plr = NULL;
	delete plr;
}

void CGameServer::UpdateClient(CLkPacket * pPacket, CClientSession * pSession)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();

	// BASIC ATTACK
	SBattleData *pBattleData;

	for (BATTLEIT it = m_listAttackBegin.begin(); it != m_listAttackBegin.end(); it++)
	{
		pBattleData = (*it);
		if (timeGetTime() - pBattleData->dwCurrTime >= MONSTER_ATTACK_UPDATE_TICK)
		{
		//	app->pSession->SendCharActionAttack(pBattleData->uiSerialId, pBattleData->m_uiTargetSerialId, pPacket);
			pBattleData->dwCurrTime = timeGetTime();
		}
	}
}

//--------------------------------------------------------------------------------------//
//		Char toggle fight
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharToggleFighting(CLkPacket * pPacket, CGameServer * app)
{
	//CHAR_TOGG_FIGHTING
	//app->mob->AddToWorld(pPacket, this);
	sUG_CHAR_TOGG_FIGHTING * req = (sUG_CHAR_TOGG_FIGHTING *)pPacket->GetPacketData();

	SBattleData *pBattleData;
	ListAttackBegin::iterator it;
	if (m_listAttackBegin.empty() == false)
	{
		for (it = m_listAttackBegin.begin(); it != m_listAttackBegin.end(); it++)
		{
			pBattleData = (*it);
			if (pBattleData->uiSerialId == this->GetTargetSerialId())
			{
				if (req->bFightMode)
					pBattleData->bAttackMode = TRUE;
				else
					pBattleData->bAttackMode = FALSE;
			}
		}
	}

	CLkPacket packet2(sizeof(sGU_CHAR_FIGHTMODE));
	sGU_CHAR_FIGHTMODE * res = (sGU_CHAR_FIGHTMODE *)packet2.GetPacketData();
	res->handle = this->GetTargetSerialId();
	res->wOpCode = GU_CHAR_FIGHTMODE;
	if (req->bFightMode)
		res->bFightMode = false;
	else
		res->bFightMode = true;

	packet2.SetPacketLen(sizeof(sGU_CHAR_FIGHTMODE));
	g_pApp->Send(this->GetHandle(), &packet2);
}

//--------------------------------------------------------------------------------------//
//		SKILL SHOP REQUEST
//--------------------------------------------------------------------------------------//
void CClientSession::SendShopSkillReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_SHOP_SKILL_BUY_REQ * req = (sUG_SHOP_SKILL_BUY_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_SHOP_SKILL_BUY_RES));
	sGU_SHOP_SKILL_BUY_RES * res = (sGU_SHOP_SKILL_BUY_RES *)packet.GetPacketData();

	res->hNpchandle = req->hNpchandle;
	res->wOpCode = GU_SHOP_SKILL_BUY_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_SHOP_SKILL_BUY_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}

//--------------------------------------------------------------------------------------//
//		Char learn skill
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharLearnSkillReq(CLkPacket * pPacket, CGameServer * app)
{
	WORD		skill_learn_result = 0;
	// BUY SKILL FROM SHOP
	sUG_SHOP_SKILL_BUY_REQ * req0 = (sUG_SHOP_SKILL_BUY_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet0(sizeof(sGU_SHOP_SKILL_BUY_RES));
	sGU_SHOP_SKILL_BUY_RES * res0 = (sGU_SHOP_SKILL_BUY_RES *)packet0.GetPacketData();

	res0->wOpCode = GU_SHOP_SKILL_BUY_RES;
	res0->hNpchandle = req0->hNpchandle;
	res0->wResultCode = GAME_SUCCESS;

	packet0.SetPacketLen(sizeof(sGU_SHOP_SKILL_BUY_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet0);

	// LEARN SKILL
	sUG_SKILL_LEARN_REQ * req = (sUG_SKILL_LEARN_REQ*)pPacket->GetPacketData();

	CSkillTable* pSkillTable = app->g_pTableContainer->GetSkillTable();
	CMerchantTable* pSkillMasterItemTable = app->g_pTableContainer->GetMerchantTable();
	CNPCTable* pNpcTable = app->g_pTableContainer->GetNpcTable();
	size_t iSkillCount = this->gsf->GetTotalSlotSkill(plr->GetCharID());
	for (CTable::TABLEIT itNPCSpawn = pNpcTable->Begin(); itNPCSpawn != pNpcTable->End(); ++itNPCSpawn)
	{
		sNPC_TBLDAT* pNPCtData = (sNPC_TBLDAT*)itNPCSpawn->second;
		if (pNPCtData->tblidx == g_pMobManager->FindNpc(req0->hNpchandle))
		{
			sMERCHANT_TBLDAT* pMerchantData = (sMERCHANT_TBLDAT*)pSkillMasterItemTable->FindData(pNPCtData->amerchant_Tblidx[req0->byMerchantTab]);
			if (pMerchantData->bySell_Type == MERCHANT_SELL_TYPE_SKILL)
			{
				for (RwInt32 j = 0; j < LK_MAX_MERCHANT_COUNT; ++j)
				{
					if (pMerchantData->aitem_Tblidx[j] == INVALID_TBLIDX)
					{
						skill_learn_result = 1004;
						break;
					}

					if (req0->byPos == j)
					{
						sSKILL_TBLDAT* pSkillData = (sSKILL_TBLDAT*)pSkillTable->FindData(pMerchantData->aitem_Tblidx[j]);
						if (app->qry->CheckIfSkillAlreadyLearned(pSkillData->tblidx, plr->GetCharID()) == false)
						{
							if (plr->GetPcProfile()->dwZenny >= pSkillData->dwRequire_Zenny)
							{
								if (plr->GetPcProfile()->byLevel >= pSkillData->byRequire_Train_Level)
								{
									if (plr->GetPcProfile()->dwSpPoint >= pSkillData->wRequireSP)
									{
										skill_learn_result = 500;
										// Skill learned notification
										CLkPacket packet2(sizeof(sGU_SKILL_LEARNED_NFY));
										sGU_SKILL_LEARNED_NFY * res2 = (sGU_SKILL_LEARNED_NFY *)packet2.GetPacketData();
										iSkillCount++;//don't send this directly because if you send directly he don't add in count...
										res2->wOpCode = GU_SKILL_LEARNED_NFY;
										res2->skillId = pSkillData->tblidx;
										res2->bySlot = iSkillCount;

										app->qry->InsertNewSkill(pSkillData->tblidx, plr->GetCharID(), iSkillCount, pSkillData->wKeep_Time, pSkillData->wNext_Skill_Train_Exp);
										plr->GetPcProfile()->dwZenny -= pSkillData->dwRequire_Zenny;
										plr->GetPcProfile()->dwSpPoint -= pSkillData->wRequireSP;

										packet2.SetPacketLen(sizeof(sGU_SKILL_LEARNED_NFY));
										g_pApp->Send(this->GetHandle(), &packet2);
										app->qry->UpdateSPPoint(plr->GetCharID(), plr->GetPcProfile()->dwSpPoint);

										CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_SP));
										sGU_UPDATE_CHAR_SP * res3 = (sGU_UPDATE_CHAR_SP *)packet3.GetPacketData();
										res3->wOpCode = GU_UPDATE_CHAR_SP;
										res3->dwSpPoint = plr->GetPcProfile()->dwSpPoint;
										packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SP));
										g_pApp->Send(this->GetHandle(), &packet3);

										app->qry->SetMinusMoney(plr->GetCharID(), pSkillData->dwRequire_Zenny);
										CLkPacket packet4(sizeof(sGU_UPDATE_CHAR_ZENNY));
										sGU_UPDATE_CHAR_ZENNY * res4 = (sGU_UPDATE_CHAR_ZENNY *)packet4.GetPacketData();
										res4->bIsNew = true;
										res4->byChangeType = 0;
										res4->dwZenny = plr->GetPcProfile()->dwZenny;
										res4->handle = this->GetavatarHandle();
										res4->wOpCode = GU_UPDATE_CHAR_ZENNY;
										packet4.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ZENNY));
										g_pApp->Send(this->GetHandle(), &packet4);
										break;
									}
									else
										skill_learn_result = 645;
									break;

								}
								else
									skill_learn_result = 501;
								break;
							}
							else
								skill_learn_result = 617;
							break;
						}
						else
							skill_learn_result = 620;
						break;
					}
				}
			}
		}
	}

	//LEARN SKILL RESULT
	CLkPacket packet(sizeof(sGU_SKILL_LEARN_RES));
	sGU_SKILL_LEARN_RES * res = (sGU_SKILL_LEARN_RES *)packet.GetPacketData();

	res->wOpCode = GU_SKILL_LEARN_RES;
	res->wResultCode = skill_learn_result;

	packet.SetPacketLen(sizeof(sGU_SKILL_LEARN_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Char learn HTB skill
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharSkillHTBLearn(CLkPacket * pPacket, CGameServer * app)
{
	sUG_HTB_LEARN_REQ * req = (sUG_HTB_LEARN_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packetHTB(sizeof(sGU_HTB_LEARN_RES));
	sGU_HTB_LEARN_RES * res = (sGU_HTB_LEARN_RES*)packetHTB.GetPacketData();

	CHTBSetTable * pHTBTable = app->g_pTableContainer->GetHTBSetTable();
	sHTB_SET_TBLDAT *pHTBSetTblData = reinterpret_cast<sHTB_SET_TBLDAT*>(pHTBTable->FindData(req->skillId));

	res->skillId = pHTBSetTblData->tblidx;
	res->bySkillSlot = pHTBSetTblData->bySlot_Index;
	res->wOpCode = GU_HTB_LEARN_RES;
	res->wResultCode = GAME_SUCCESS;
	packetHTB.SetPacketLen(sizeof(sGU_HTB_LEARN_RES));
	g_pApp->Send(this->GetHandle(), &packetHTB);
	app->qry->InsertNewSkill(pHTBSetTblData->tblidx, plr->GetCharID(), pHTBSetTblData->bySlot_Index, 0, 0);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		MOVE ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendItemMoveReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_ITEM_MOVE_REQ * req = (sUG_ITEM_MOVE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_ITEM_MOVE_RES));
	sGU_ITEM_MOVE_RES * res = (sGU_ITEM_MOVE_RES *)packet.GetPacketData();

	//For equipment
	CLkPacket eqPak(sizeof(sGU_UPDATE_ITEM_EQUIP));
	sGU_UPDATE_ITEM_EQUIP * eq = (sGU_UPDATE_ITEM_EQUIP *)eqPak.GetPacketData();

	app->db->prepare("SELECT * FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySrcPlace);
	app->db->setInt(3, req->bySrcPos);
	app->db->execute();
	app->db->fetch();
	RwUInt32 uniqueID = app->db->getInt("id");
	RwUInt32 ID = app->db->getInt("tblidx");
	BYTE byRank = app->db->getInt("rank");
	BYTE byGrade = app->db->getInt("grade");

	//Using in other if condition
	CItemTable *itemTbl = app->g_pTableContainer->GetItemTable();
	CItemOptionTable* itemOptions = app->g_pTableContainer->GetItemOptionTable();
	sITEM_TBLDAT* pItemData = (sITEM_TBLDAT*)itemTbl->FindData(ID);
	sITEM_OPTION_TBLDAT* optItemData = reinterpret_cast<sITEM_OPTION_TBLDAT*>(itemOptions->FindData(pItemData->Item_Option_Tblidx));
	if ((app->qry->CheckIfCanMoveItemThere(plr->GetCharID(), req->byDestPlace, req->byDestPos) == false))
	{
		res->wResultCode = GAME_MOVE_CANT_GO_THERE;
		this->gsf->printError("An error is occured in SendItemMoveReq: GAME_MOVE_CANT_GO_THERE");
	}
	else
	{
		//DestPlace&SrcPlace = 8 Is for Scouter Items
		if (req->byDestPlace == 7 || req->bySrcPlace == 7 ||
			req->byDestPlace == 8 || req->bySrcPlace == 8)
		{
			if (plr->GetPcProfile()->byLevel >= pItemData->byNeed_Level)
			{
				res->wResultCode = GAME_SUCCESS;
				app->qry->UpdateItemPlaceAndPos(uniqueID, req->byDestPlace, req->byDestPos);

				//Remove equip and Calculate Stats
				if (req->bySrcPlace == CONTAINER_TYPE_EQUIP)
				{
					eq->handle = plr->GetAvatarHandle();
					eq->sItemBrief.tblidx = INVALID_TBLIDX;
					eq->byPos = req->bySrcPos;
					eq->wOpCode = GU_UPDATE_ITEM_EQUIP;
					plr->UpdateBaseAttributeWithEquip(pItemData->tblidx, byRank, byGrade, true);
					plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
				}
				//Equip Item and Calculate Stats
				else if (req->byDestPlace == CONTAINER_TYPE_EQUIP)
				{
					eq->byPos = req->byDestPos;
					eq->handle = plr->GetAvatarHandle();
					eq->sItemBrief.tblidx = ID;
					eq->wOpCode = GU_UPDATE_ITEM_EQUIP;
					plr->UpdateBaseAttributeWithEquip(pItemData->tblidx, byRank, byGrade);
					plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
				}
				//Equip Scouter Chips on Scout and Calculate the Stats
				else if (req->byDestPlace == CONTAINER_TYPE_SCOUT)
				{
					//Update Attributes with values from chip
					if (plr->HaveAnySpaceInScouter())
					{
						eq->handle = plr->GetAvatarHandle();
						eq->sItemBrief.tblidx = ID;
						eq->byPos = req->byDestPlace;
						eq->wOpCode = GU_UPDATE_ITEM_EQUIP;
						for (int i = 0; i < 4; i++)
						{
							if (plr->GetEquipedChips()[i] == INVALID_TBLIDX)
							{
								plr->GetEquipedChips()[i] = pItemData->Item_Option_Tblidx;//We just need the OptionTblidx to decrease the stat
								break;
							}
							else
								continue;
						}
						plr->cPlayerAttribute->UpdateStatsUsingScouterChips(plr->GetAvatarHandle(), pItemData->Item_Option_Tblidx);
					}
					else
					{
						//If all scouter slots are full then we can't let pass
						res->wResultCode = GAME_MOVE_CANT_GO_THERE;
					}

				}
				//Remove Scouter Chips from Scouter and Calculate Stats
				else if (req->bySrcPlace == CONTAINER_TYPE_SCOUT)
				{
					res->wResultCode = GAME_SUCCESS;
					app->qry->UpdateItemPlaceAndPos(uniqueID, req->byDestPlace, req->byDestPos);
					eq->byPos = req->byDestPos;
					eq->handle = plr->GetAvatarHandle();
					eq->sItemBrief.tblidx = ID;
					eq->wOpCode = GU_UPDATE_ITEM_EQUIP;
					//Updating the array with Option tblidx and Updating attributes
					for (int i = 0; i < 4; i++)
					{
						if (plr->GetEquipedChips()[i] == pItemData->Item_Option_Tblidx)
						{
							plr->GetEquipedChips()[i] = INVALID_TBLIDX;//We just need the OptionTblidx to decrease the stat
							break;
						}
						else
							continue;
					}
					plr->cPlayerAttribute->UpdateStatsUsingScouterChips(plr->GetAvatarHandle(), pItemData->Item_Option_Tblidx, true);
				}
				//Just Move the items around the inventory
				else if ((req->byDestPlace >= CONTAINER_TYPE_BAG1) && (req->byDestPlace <= CONTAINER_TYPE_BAG5))
				{
					eq->byPos = req->byDestPos;
					eq->handle = plr->GetAvatarHandle();
					eq->sItemBrief.tblidx = ID;
					eq->wOpCode = GU_UPDATE_ITEM_EQUIP;
				}
			}
			else
			{
				res->wResultCode = GAME_MOVE_CANT_GO_THERE;
				this->gsf->printError("An error is occured in SendItemMoveReq: Level < LevelRequiert");
			}
		}
		else
		{
			app->qry->UpdateItemPlaceAndPos(uniqueID, req->byDestPlace, req->byDestPos);
			res->wResultCode = GAME_SUCCESS;
		}
	}
	res->wOpCode = GU_ITEM_MOVE_RES;
	res->hSrcItem = uniqueID;
	res->bySrcPlace = req->bySrcPlace;
	res->bySrcPos = req->bySrcPos;
	res->hDestItem = -1;
	res->byDestPlace = req->byDestPlace;
	res->byDestPos = req->byDestPos;
	eqPak.SetPacketLen(sizeof(sGU_UPDATE_ITEM_EQUIP));
	g_pApp->Send(this->GetHandle(), &eqPak);
	app->UserBroadcastothers(&eqPak, this);
	packet.SetPacketLen(sizeof(sGU_ITEM_MOVE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr->SavePlayerData(app);
	plr->cPlayerInventory->LoadCharInventory();
	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		DELETE ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendItemDeleteReq(CLkPacket * pPacket, CGameServer * app)
{
	// GET DELETE ITEM
	sUG_ITEM_DELETE_REQ * req = (sUG_ITEM_DELETE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_ITEM_DELETE_RES));
	sGU_ITEM_DELETE_RES * res = (sGU_ITEM_DELETE_RES *)packet.GetPacketData();

	app->db->prepare("SELECT id,place,pos FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySrcPlace);
	app->db->setInt(3, req->bySrcPos);
	app->db->execute();
	app->db->fetch();

	RwUInt32 u_itemid = app->db->getInt("id");
	RwUInt32 item_place = app->db->getInt("place");
	RwUInt32 item_pos = app->db->getInt("pos");

	res->wOpCode = GU_ITEM_DELETE_RES;
	res->wResultCode = GAME_SUCCESS;
	res->byPlace = req->bySrcPlace;
	res->byPos = req->bySrcPos;

	packet.SetPacketLen(sizeof(sGU_ITEM_DELETE_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	// DELETE ITEM
	app->qry->DeleteItemById(u_itemid);

	CLkPacket packet2(sizeof(sGU_ITEM_DELETE));
	sGU_ITEM_DELETE * res2 = (sGU_ITEM_DELETE *)packet2.GetPacketData();

	res2->bySrcPlace = item_place;
	res2->bySrcPos = item_pos;
	res2->hSrcItem = u_itemid;
	res2->wOpCode = GU_ITEM_DELETE;
	plr->cPlayerInventory->RemoveItemFromInventory(u_itemid);
	plr->SavePlayerData(app);
	plr->cPlayerInventory->LoadCharInventory();
	packet2.SetPacketLen(sizeof(sGU_ITEM_DELETE));
	g_pApp->Send(this->GetHandle(), &packet2);

	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		STACK ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendItemStackReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_ITEM_MOVE_STACK_REQ * req = (sUG_ITEM_MOVE_STACK_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	// GET DATA FROM MYSQL
	app->db->prepare("SELECT id,tblidx FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySrcPlace);
	app->db->setInt(3, req->bySrcPos);
	app->db->execute();
	app->db->fetch();
	unsigned int uniqueID = app->db->getInt("id");
	unsigned int item1ID = app->db->getInt("tblidx");

	app->db->prepare("SELECT id,count,tblidx FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->byDestPlace);
	app->db->setInt(3, req->byDestPos);
	app->db->execute();
	app->db->fetch();
	unsigned int uniqueID2 = app->db->getInt("id");
	unsigned int item2ID = app->db->getInt("tblidx");

	if (item1ID == item2ID)
	{
		// UPDATE ITEMS
		CLkPacket packet(sizeof(sGU_ITEM_MOVE_STACK_RES));
		sGU_ITEM_MOVE_STACK_RES * res = (sGU_ITEM_MOVE_STACK_RES *)packet.GetPacketData();

		CLkPacket packet1(sizeof(sGU_ITEM_DELETE));
		sGU_ITEM_DELETE * res1 = (sGU_ITEM_DELETE *)packet1.GetPacketData();

		res->wOpCode = GU_ITEM_MOVE_STACK_RES;
		res->bySrcPlace = req->bySrcPlace;
		res->bySrcPos = req->bySrcPos;
		res->byDestPlace = req->byDestPlace;
		res->byDestPos = req->byDestPos;
		res->hSrcItem = uniqueID;
		res->hDestItem = uniqueID2;
		res->byStackCount1 = req->byStackCount;
		res->byStackCount2 = req->byStackCount + app->db->getInt("count");
		res->wResultCode = GAME_SUCCESS;

		res1->bySrcPlace = req->bySrcPlace;
		res1->bySrcPos = req->bySrcPos;
		res1->hSrcItem = uniqueID;
		res1->wOpCode = GU_ITEM_DELETE;

		// UPDATE AND DELETE
		app->db->prepare("UPDATE items SET count=? WHERE id=?");
		app->db->setInt(1, res->byStackCount2);
		app->db->setInt(2, uniqueID2);
		app->db->execute();

		app->db->prepare("DELETE FROM items WHERE id=?");
		app->db->setInt(1, uniqueID);
		app->db->execute();
		plr->cPlayerInventory->RemoveItemFromInventory(uniqueID);

		// Send packet to client
		packet.SetPacketLen(sizeof(sGU_ITEM_MOVE_STACK_RES));
		g_pApp->Send(this->GetHandle(), &packet);
		packet1.SetPacketLen(sizeof(sGU_ITEM_DELETE));
		g_pApp->Send(this->GetHandle(), &packet1);
	}
	else
	{
		CLkPacket packet(sizeof(sGU_ITEM_MOVE_STACK_RES));
		sGU_ITEM_MOVE_STACK_RES * res = (sGU_ITEM_MOVE_STACK_RES *)packet.GetPacketData();
		res->wOpCode = GU_ITEM_MOVE_STACK_RES;
		res->wResultCode = GAME_FAIL;
		packet.SetPacketLen(sizeof(sGU_ITEM_MOVE_STACK_RES));
		g_pApp->Send(this->GetHandle(), &packet);
		this->gsf->printError("An error is occured in SendItemStackReq: GAME_FAIL item1ID != item2ID");
	}
	plr = NULL;
	delete plr;
}

void CClientSession::SendShopStartReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_SHOP_START_REQ * req = (sUG_SHOP_START_REQ *)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_SHOP_START_RES));
	sGU_SHOP_START_RES * res = (sGU_SHOP_START_RES *)packet.GetPacketData();

	res->wOpCode = GU_SHOP_START_RES;
	res->wResultCode = GAME_SUCCESS;
	res->handle = req->handle;
	res->byType = 1;

	packet.SetPacketLen(sizeof(sGU_SHOP_START_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
void CClientSession::SendShopBuyReq(CLkPacket * pPacket, CGameServer * app)
{
	WORD		buy_item_result;
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_SHOP_BUY_REQ * req = (sUG_SHOP_BUY_REQ *)pPacket->GetPacketData();
	CMerchantTable* pMerchantItemTable = app->g_pTableContainer->GetMerchantTable();
	CItemTable *itemTbl = app->g_pTableContainer->GetItemTable();
	CNPCTable* pNpcTable = app->g_pTableContainer->GetNpcTable();
	for (CTable::TABLEIT itNPCSpawn = pNpcTable->Begin(); itNPCSpawn != pNpcTable->End(); ++itNPCSpawn)
	{
		sNPC_TBLDAT* pNPCtData = (sNPC_TBLDAT*)itNPCSpawn->second;
		if (pNPCtData->tblidx == g_pMobManager->FindNpc(req->handle))
		{
			sMERCHANT_TBLDAT* pMerchantData = (sMERCHANT_TBLDAT*)pMerchantItemTable->FindData(pNPCtData->amerchant_Tblidx[req->sBuyData[0].byMerchantTab]);
			if (pMerchantData->bySell_Type == MERCHANT_SELL_TYPE_ITEM)
			{
				for (RwInt32 j = 0; j < LK_MAX_MERCHANT_COUNT; j++)
				{
					if (pMerchantData->aitem_Tblidx[j] == INVALID_TBLIDX)
					{
						buy_item_result = 501;
						break;
					}
					if (j == 0)
						j = req->sBuyData[j].byItemPos;
					for (int l = 12; l >= 0; l--)
					{
						if (req->sBuyData[l].byItemPos == j)
						{
							/*sITEM_TBLDAT* pItemData = (sITEM_TBLDAT*) itemTbl->FindData( pMerchantData->aitem_Tblidx[j] );
							int ItemPos = 0;

							app->db->prepare("SELECT * FROM items WHERE owner_ID = ? AND place=1 ORDER BY pos ASC");
							app->db->setInt(1, this->plr->pcProfile->charId);
							app->db->execute();
							int k = 0;
							while(app->db->fetch())
							{
							if(app->db->getInt("pos") < LK_MAX_ITEM_SLOT)
							ItemPos = app->db->getInt("pos") + 1;
							else
							ItemPos = app->db->getInt("pos");
							k++;
							}
							app->db->prepare("CALL BuyItemFromShop (?,?,?,?,?, @unique_iID)");
							app->db->setInt(1, pMerchantData->aitem_Tblidx[j]);
							app->db->setInt(2, this->plr->pcProfile->charId);
							app->db->setInt(3, ItemPos);
							app->db->setInt(4, pItemData->byRank);
							app->db->setInt(5, pItemData->byDurability);
							app->db->execute();
							app->db->execute("SELECT @unique_iID");
							app->db->fetch();*/

							/*CLkPacket packet2(sizeof(sGU_ITEM_CREATE));
							sGU_ITEM_CREATE * res2 = (sGU_ITEM_CREATE *)packet2.GetPacketData();

							res2->bIsNew = true;
							res2->wOpCode = GU_ITEM_CREATE;
							res2->handle = app->db->getInt("@unique_iID");
							res2->sItemData.charId = this->GetavatarHandle();
							res2->sItemData.itemNo = pItemData->tblidx;
							res2->sItemData.byStackcount = req->sBuyData[0].byStack;
							res2->sItemData.itemId = app->db->getInt("@unique_iID");
							res2->sItemData.byPlace = 1;
							res2->sItemData.byPosition = ItemPos;
							res2->sItemData.byCurrentDurability = pItemData->byDurability;
							res2->sItemData.byRank = pItemData->byRank;

							packet2.SetPacketLen( sizeof(sGU_ITEM_CREATE) );
							g_pApp->Send( this->GetHandle(), &packet2 );*/
							this->gsf->CreateUpdateItem(plr, req->sBuyData[0].byStack, pMerchantData->aitem_Tblidx[j], false, this->GetHandle());
							break;
						}

					}
				}
			}
		}
	}

	CLkPacket packet(sizeof(sGU_SHOP_BUY_RES));
	sGU_SHOP_BUY_RES * res = (sGU_SHOP_BUY_RES *)packet.GetPacketData();

	res->wOpCode = GU_SHOP_BUY_RES;
	res->wResultCode = plr->cPlayerInventory->GetBagStatus();
	res->handle = req->handle;

	packet.SetPacketLen(sizeof(sGU_SHOP_BUY_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
void CClientSession::SendShopEndReq(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_SHOP_END_RES));
	sGU_SHOP_END_RES * res = (sGU_SHOP_END_RES *)packet.GetPacketData();

	res->wOpCode = GU_SHOP_END_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_SHOP_END_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
void	CClientSession::SendShopSellReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_SHOP_SELL_REQ * req = (sUG_SHOP_SELL_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_SHOP_SELL_RES));
	sGU_SHOP_SELL_RES * res = (sGU_SHOP_SELL_RES *)packet.GetPacketData();
	CLkPacket packet1(sizeof(sGU_ITEM_DELETE));
	sGU_ITEM_DELETE * res1 = (sGU_ITEM_DELETE *)packet1.GetPacketData();
	CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_ZENNY));
	sGU_UPDATE_CHAR_ZENNY * res2 = (sGU_UPDATE_CHAR_ZENNY *)packet2.GetPacketData();

	CItemTable *itemTbl = app->g_pTableContainer->GetItemTable();
	int zenit_amount = 0;
	for (int i = 0; (req->sSellData[i].byStack != 0); i++)
	{
		app->db->prepare("SELECT * FROM items WHERE owner_ID = ? AND place = ? AND pos = ?");
		app->db->setInt(1, plr->GetCharID());
		app->db->setInt(2, req->sSellData[i].byPlace);
		app->db->setInt(3, req->sSellData[i].byPos);
		app->db->execute();
		app->db->fetch();
		int item_id = app->db->getInt("tblidx");
		int id = app->db->getInt("id");

		res1->bySrcPlace = req->sSellData[i].byPlace;
		res1->bySrcPos = req->sSellData[i].byPos;
		res1->hSrcItem = id;
		res1->wOpCode = GU_ITEM_DELETE;
		packet1.SetPacketLen(sizeof(sGU_ITEM_DELETE));
		sITEM_TBLDAT* pItemData = (sITEM_TBLDAT*)itemTbl->FindData(item_id);
		zenit_amount += pItemData->bySell_Price * req->sSellData[i].byStack;
		int count_less = req->sSellData[i].byStack - app->db->getInt("count");
		if (count_less <= 0)
		{
			g_pApp->Send(this->GetHandle(), &packet1);
			app->db->prepare("DELETE FROM items WHERE id = ?");
			app->db->setInt(1, id);
			app->db->execute();
			plr->cPlayerInventory->RemoveItemFromInventory(id);
		}
		else if (count_less >= 1)
		{
			app->db->prepare("UPDATE items SET count=? WHERE id=?");
			app->db->setInt(1, count_less);
			app->db->setInt(2, id);
			app->db->execute();
			this->gsf->CreateUpdateItem(plr, count_less, item_id, true, plr->GetSession());
		}
	}
	plr->GetPcProfile()->dwZenny += zenit_amount;
	res->handle = req->handle;
	res->wOpCode = GU_SHOP_SELL_RES;
	res->wResultCode = GAME_SUCCESS;
	res2->bIsNew = true;
	res2->byChangeType = 1;
	res2->dwZenny = plr->GetPcProfile()->dwZenny;
	res2->handle = this->GetavatarHandle();
	res2->wOpCode = GU_UPDATE_CHAR_ZENNY;
	app->qry->SetPlusMoney(plr->GetCharID(), zenit_amount);
	packet.SetPacketLen(sizeof(sGU_SHOP_SELL_RES));
	packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ZENNY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet1);
	g_pApp->Send(this->GetHandle(), &packet2);
	plr = NULL;
	delete plr;
}
//ROLL DICE
void	CClientSession::SendRollDice(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_DICE_ROLL_RES));
	sGU_DICE_ROLL_RES * res = (sGU_DICE_ROLL_RES *)packet.GetPacketData();

	res->wOpCode = GU_DICE_ROLL_RES;
	res->wDiceResult = (WORD)rand() % 100;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_DICE_ROLL_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	CLkPacket packet2(sizeof(sGU_DICE_ROLLED_NFY));
	sGU_DICE_ROLLED_NFY * res2 = (sGU_DICE_ROLLED_NFY *)packet2.GetPacketData();

	res2->wDiceResult = res->wDiceResult;
	res2->wOpCode = GU_DICE_ROLLED_NFY;
	res2->hSubject = this->GetavatarHandle();

	packet2.SetPacketLen(sizeof(sGU_DICE_ROLLED_NFY));
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet2, this);
}

void	CClientSession::SendScouterIndicatorReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_SCOUTER_INDICATOR_REQ * req = (sUG_SCOUTER_INDICATOR_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_SCOUTER_INDICATOR_RES));
	sGU_SCOUTER_INDICATOR_RES * res = (sGU_SCOUTER_INDICATOR_RES *)packet.GetPacketData();

	res->hTarget = req->hTarget;
	res->dwRetValue = 0;
	res->wOpCode = GU_SCOUTER_INDICATOR_RES;

	int mobid = 0;
	if (g_pMobManager->GetMobByHandle(req->hTarget) != NULL)
	{
		CMonster::MonsterData* mob = g_pMobManager->GetMobByHandle(req->hTarget);
		if ((mobid = mob->MonsterID) != 0)
		{
			CMobTable *Mob = app->g_pTableContainer->GetMobTable();
			for (CTable::TABLEIT itmob = Mob->Begin(); itmob != Mob->End(); ++itmob)
			{
				sMOB_TBLDAT* pMOBtData = (sMOB_TBLDAT*)itmob->second;
				if (pMOBtData->tblidx == mobid)
				{
					res->dwRetValue = this->gsf->CalculePowerLevel(pMOBtData);
					res->wResultCode = GAME_SUCCESS;
					break;
				}
				else
					res->wResultCode = GAME_SCOUTER_TARGET_FAIL;
			}
		}
	}
	else
	{
		PlayersMain *targetPlr = g_pPlayerManager->GetPlayer(req->hTarget);
		if (targetPlr != NULL)
		{
			res->dwRetValue = this->gsf->CalculePowerLevelPlayer(targetPlr);
			res->wResultCode = GAME_SUCCESS;
		}
		else
			res->wResultCode = GAME_SCOUTER_TARGET_FAIL;
	}
	packet.SetPacketLen(sizeof(sGU_SCOUTER_INDICATOR_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
void	CClientSession::SendDragonBallCheckReq(CLkPacket * pPacket, CGameServer * app) // THIS IS THE FIRST VERSION
{
	printf("--- UG_DRAGONBALL_CHECK_REQ --- \n");
	sUG_DRAGONBALL_CHECK_REQ * req = (sUG_DRAGONBALL_CHECK_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_DRAGONBALL_CHECK_RES));
	sGU_DRAGONBALL_CHECK_RES * res = (sGU_DRAGONBALL_CHECK_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * obj = (sGU_OBJECT_CREATE *)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_AVATAR_ZONE_INFO));
	sGU_AVATAR_ZONE_INFO * zone = (sGU_AVATAR_ZONE_INFO *)packet3.GetPacketData();
	
	sOBJECT_TBLDAT* pAltar1 = reinterpret_cast<sOBJECT_TBLDAT*>(app->g_pTableContainer->GetObjectTable(1)->FindData(68));//human start
	sOBJECT_TBLDAT* pAltar2 = reinterpret_cast<sOBJECT_TBLDAT*>(app->g_pTableContainer->GetObjectTable(1)->FindData(69)); //namek start
	sOBJECT_TBLDAT* pAltar3 = reinterpret_cast<sOBJECT_TBLDAT*>(app->g_pTableContainer->GetObjectTable(1)->FindData(583));//majin start

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	int i = 0;
	while (i <= 6)
	{
		if (plr->cPlayerInventory->ExistItemByHandle(req->sData[i].hItem));
		else
		{
			res->hObject = req->hObject;
			res->wResultCode = GAME_DRAGONBALL_NOT_FOUND;
			res->wOpCode = GU_DRAGONBALL_CHECK_RES;
			packet.SetPacketLen(sizeof(sGU_DRAGONBALL_CHECK_RES));
			g_pApp->Send(this->GetHandle(), &packet);
			this->gsf->printError("An error is occured in SendDragonBallReq: GAME_DRAGONBALL_NOT_FOUND");
			i = 0;
			break;
		}
		i++;
	}
	if (i == 7)
	{

		float a1Distance = LkGetDistance(plr->GetPlayerPosition().x, plr->GetPlayerPosition().z, pAltar1->vLoc.x, pAltar1->vLoc.z); //Checking distance for which altar to spawn at
		float a2Distance = LkGetDistance(plr->GetPlayerPosition().x, plr->GetPlayerPosition().z, pAltar2->vLoc.x, pAltar2->vLoc.z); //Checking distance for which altar to spawn at
		float a3Distance = LkGetDistance(plr->GetPlayerPosition().x, plr->GetPlayerPosition().z, pAltar3->vLoc.x, pAltar3->vLoc.z); //Checking distance for which altar to spawn at
		sOBJECT_TBLDAT* pObjectDat = g_pObjectManager->GetObjectTbldatByHandle(req->hObject);
		CDragonBallTable* pDragonBallTable = app->g_pTableContainer->GetDragonBallTable();
		sDRAGONBALL_TBLDAT* pDBDat = (sDRAGONBALL_TBLDAT*)pDragonBallTable->FindData(1);
		//pDBDat->dwAltarGroup

		zone->wOpCode = GU_AVATAR_ZONE_INFO;
		zone->zoneInfo.bIsDark = true;
		//zone->zoneInfo.zoneId = 0;

		packet3.SetPacketLen(sizeof(sGU_AVATAR_ZONE_INFO));
		g_pApp->Send(this->GetHandle(), &packet3);
		app->UserBroadcastothers(&packet3, this);

	/*	if (a1Distance < 20)
		{
*/
			obj->handle = g_pMobManager->CreateUniqueId();
			obj->wOpCode = GU_OBJECT_CREATE;
			obj->sObjectInfo.objType = OBJTYPE_NPC; // this is wrong
			obj->sObjectInfo.npcBrief.tblidx = 6361101; // this is wrong
			obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = pObjectDat->vLoc.x;
			obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = pObjectDat->vLoc.y;
			obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = pObjectDat->vLoc.z;
			obj->sObjectInfo.npcState.sCharStateBase.vCurDir.x = (0.96313f);
			obj->sObjectInfo.npcState.sCharStateBase.vCurDir.z = (0.28984f);
			obj->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
			obj->sObjectInfo.npcBrief.wCurEP = 100;
			obj->sObjectInfo.npcBrief.wCurLP = 100;
			obj->sObjectInfo.npcBrief.wMaxEP = 100;
			obj->sObjectInfo.npcBrief.wMaxLP = 100;
			this->myShenronHandle = obj->handle;
			res->hObject = req->hObject;
			res->wOpCode = GU_DRAGONBALL_CHECK_RES;
			res->wResultCode = GAME_SUCCESS;
			packet.SetPacketLen(sizeof(sGU_DRAGONBALL_CHECK_RES));
			g_pApp->Send(this->GetHandle(), &packet);

			packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
			g_pApp->Send(this->GetHandle(), &packet2);
			app->UserBroadcastothers(&packet2, this);
		//}
		//else if (a2Distance < 20)
		//{
		//	obj->handle = g_pMobManager->CreateUniqueId();
		//	obj->wOpCode = GU_OBJECT_CREATE;
		//	obj->sObjectInfo.objType = OBJTYPE_NPC; // this is wrong
		//	obj->sObjectInfo.npcBrief.tblidx = 6361101; // this is wrong
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = pAltar2->vLoc.x;
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = pAltar2->vLoc.y;
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = pAltar2->vLoc.z;
		//	obj->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
		//	obj->sObjectInfo.npcBrief.wCurEP = 100;
		//	obj->sObjectInfo.npcBrief.wCurLP = 100;
		//	obj->sObjectInfo.npcBrief.wMaxEP = 100;
		//	obj->sObjectInfo.npcBrief.wMaxLP = 100;
		//	this->myShenronHandle = obj->handle;
		//	res->hObject = req->hObject;
		//	res->wOpCode = GU_DRAGONBALL_CHECK_RES;
		//	res->wResultCode = GAME_SUCCESS;
		//	packet.SetPacketLen(sizeof(sGU_DRAGONBALL_CHECK_RES));
		//	g_pApp->Send(this->GetHandle(), &packet);

		//	packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
		//	g_pApp->Send(this->GetHandle(), &packet2);
		//	app->UserBroadcastothers(&packet2, this);
		//}
		//else if (a3Distance < 20)
		//{
		//	obj->handle = g_pMobManager->CreateUniqueId();
		//	obj->wOpCode = GU_OBJECT_CREATE;
		//	obj->sObjectInfo.objType = OBJTYPE_NPC; // this is wrong
		//	obj->sObjectInfo.npcBrief.tblidx = 6361101; // this is wrong
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = pAltar3->vLoc.x;
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = pAltar3->vLoc.y;
		//	obj->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = pAltar3->vLoc.z;
		//	obj->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
		//	obj->sObjectInfo.npcBrief.wCurEP = 100;
		//	obj->sObjectInfo.npcBrief.wCurLP = 100;
		//	obj->sObjectInfo.npcBrief.wMaxEP = 100;
		//	obj->sObjectInfo.npcBrief.wMaxLP = 100;
		//	this->myShenronHandle = obj->handle;
		//	res->hObject = req->hObject;
		//	res->wOpCode = GU_DRAGONBALL_CHECK_RES;
		//	res->wResultCode = GAME_SUCCESS;
		//	packet.SetPacketLen(sizeof(sGU_DRAGONBALL_CHECK_RES));
		//	g_pApp->Send(this->GetHandle(), &packet);

		//	packet2.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
		//	g_pApp->Send(this->GetHandle(), &packet2);
		//	app->UserBroadcastothers(&packet2, this);
		//}
	}
}

void	CClientSession::SendDragonBallRewardReq(CLkPacket * pPacket, CGameServer * app) // THIS IS THE FIRST VERSION
{
	printf("--- UG_DRAGONBALL_REWARD_REQ --- \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_DRAGONBALL_REWARD_REQ * req = (sUG_DRAGONBALL_REWARD_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_DRAGONBALL_REWARD_RES));
	sGU_DRAGONBALL_REWARD_RES * res = (sGU_DRAGONBALL_REWARD_RES *)packet.GetPacketData();

	sDRAGONBALL_REWARD_TBLDAT* pDBtData = (sDRAGONBALL_REWARD_TBLDAT*)app->g_pTableContainer->GetDragonBallRewardTable()->FindData(req->rewardTblidx);

	//printf("Datacontainer = byBallType = %d, byRewardCategoryDepth = %d, rewardType = %d\ndwRewardZenny = %d, catergoryDialogue = %d\nCategoryName = %d, RewardDialog1 = %d, RewardDialog2 = %d\nrewardlinktblidx = %d, rewardname = %d, tdblidx = %d\n",
	//pDBtData->byBallType, pDBtData->byRewardCategoryDepth, pDBtData->byRewardType, pDBtData->dwRewardZenny, pDBtData->rewardCategoryDialog,pDBtData->rewardCategoryName,
	//pDBtData->rewardDialog1, pDBtData->rewardDialog2,pDBtData->rewardLinkTblidx, pDBtData->rewardName, pDBtData->tblidx);
	//printf("Reward have been found.\nReward id = %d.\n", req->rewardTblidx);
	//------------ Wishlist --------//

	CSkillTable* pSkillTable = app->g_pTableContainer->GetSkillTable();
	sSKILL_TBLDAT* pSkillData = (sSKILL_TBLDAT*)pSkillTable->FindData(pDBtData->rewardLinkTblidx);

	CItemTable* pItemTable = app->g_pTableContainer->GetItemTable();
	sITEM_TBLDAT * pItemData = (sITEM_TBLDAT*)pItemTable->FindData(pDBtData->rewardLinkTblidx);

	CLkPacket packet3(sizeof(sGU_AVATAR_ZONE_INFO));
	sGU_AVATAR_ZONE_INFO * zone = (sGU_AVATAR_ZONE_INFO *)packet3.GetPacketData();

	switch (pDBtData->byRewardType)
	{
	case DRAGONBALL_REWARD_TYPE_SKILL:{
		CLkPacket packet3(sizeof(sGU_SKILL_LEARNED_NFY));
		sGU_SKILL_LEARNED_NFY * res3 = (sGU_SKILL_LEARNED_NFY *)packet3.GetPacketData();
		//Fixed Slot Index for Shenron's Buff Skill
		res3->wOpCode = GU_SKILL_LEARNED_NFY;
		res3->skillId = pSkillData->tblidx;
		res3->bySlot = (this->gsf->GetTotalSlotSkill(plr->GetCharID()) + 1);
		app->qry->InsertNewSkill(pSkillData->tblidx, plr->GetCharID(), res3->bySlot, pSkillData->wKeep_Time, 0);
		packet3.SetPacketLen(sizeof(sGU_SKILL_LEARNED_NFY));
		g_pApp->Send(this->GetHandle(), &packet3);
	}
		break;
	case DRAGONBALL_REWARD_TYPE_ITEM:{
		CLkPacket packet4(sizeof(sGU_ITEM_PICK_RES));
		sGU_ITEM_PICK_RES * res4 = (sGU_ITEM_PICK_RES*)packet4.GetPacketData();
		res4->itemTblidx = pItemData->tblidx;
		res4->wOpCode = GU_ITEM_PICK_RES;
		res4->wResultCode = GAME_SUCCESS;
		this->gsf->CreateUpdateItem(plr, pItemData->byMax_Stack, pItemData->tblidx, false, this->GetHandle());

		packet4.SetPacketLen(sizeof(sGU_ITEM_PICK_RES));

		g_pApp->Send(this->GetHandle(), &packet4);
	}
		break;
	case DRAGONBALL_REWARD_TYPE_ZENNY:{
		CLkPacket packet5(sizeof(sGU_UPDATE_CHAR_ZENNY));
		sGU_UPDATE_CHAR_ZENNY * res5 = (sGU_UPDATE_CHAR_ZENNY *)packet5.GetPacketData();
		res5->dwZenny = plr->GetPcProfile()->dwZenny + req->rewardTblidx;//by analazying this is the ammount....				
		res5->bIsNew = true;
		res5->handle = this->GetavatarHandle();
		res5->byChangeType = 0;//never mind
		res5->wOpCode = GU_UPDATE_CHAR_ZENNY;
		packet5.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ZENNY));
		g_pApp->Send(this->GetHandle(), &packet5);
		app->qry->SetPlusMoney(plr->GetCharID(), (res5->dwZenny -= plr->GetPcProfile()->dwZenny));
		plr->GetPcProfile()->dwZenny += res5->dwZenny;

	}
		break;
	}

	//---------------End Wish Table---------------------------------------------------------------------------//
	res->hObject = req->hObject;
	res->wOpCode = GU_DRAGONBALL_REWARD_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_DRAGONBALL_REWARD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	Sleep(4500); //TIME_NARRATION
	zone->wOpCode = GU_AVATAR_ZONE_INFO;
	zone->zoneInfo.bIsDark = false;
	//zone->zoneInfo.zoneId = 0;
	Sleep(1500);
	packet3.SetPacketLen(sizeof(sGU_AVATAR_ZONE_INFO));
	g_pApp->Send(this->GetHandle(), &packet3);
	app->UserBroadcastothers(&packet3, this);

	CLkPacket packet2(sizeof(sGU_DRAGONBALL_SCHEDULE_INFO));
	sGU_DRAGONBALL_SCHEDULE_INFO * res2 = (sGU_DRAGONBALL_SCHEDULE_INFO *)packet2.GetPacketData();
	res2->bIsAlive = true;
	res2->byEventType = SCHEDULE_EVENT_TYPE_NORMAL_DRAGONBALL;
	res2->byTermType = 0;
	res2->nStartTime = timeGetTime();
	res2->nEndTime = timeGetTime() * 2;
	res2->wOpCode = GU_DRAGONBALL_SCHEDULE_INFO;

	packet2.SetPacketLen(sizeof(sGU_DRAGONBALL_SCHEDULE_INFO));
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet2, this);

	CLkPacket packet5(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet5.GetPacketData();
	
	
	res3->handle = this->myShenronHandle;
	res3->sCharState.sCharStateBase.byStateID = CHARSTATE_DESPAWNING;
	res3->wOpCode = GU_UPDATE_CHAR_STATE;
	packet5.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	g_pApp->Send(this->GetHandle(), &packet5);
	app->UserBroadcastothers(&packet5, this);


	Sleep(2500);
	CLkPacket packet4(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY * res4 = (sGU_OBJECT_DESTROY *)packet4.GetPacketData();
	res4->handle = this->myShenronHandle;
	res4->wOpCode = GU_OBJECT_DESTROY;
	packet4.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	g_pApp->Send(this->GetHandle(), &packet4);
	app->UserBroadcastothers(&packet4, this);

	this->myShenronHandle = INVALID_TBLIDX;
	plr = NULL;
	delete plr;

}
void CClientSession::SendGambleBuyReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- UG_SHOP_GAMBLE_BUY_REQ --- \n");
	sUG_SHOP_GAMBLE_BUY_REQ *req = (sUG_SHOP_GAMBLE_BUY_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_SHOP_GAMBLE_BUY_RES));
	sGU_SHOP_GAMBLE_BUY_RES * res = (sGU_SHOP_GAMBLE_BUY_RES *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	res->handle = req->handle;
	res->wOpCode = GU_SHOP_GAMBLE_BUY_RES;
	
	CNPCTable* pNpcMerchantTbl = app->g_pTableContainer->GetNpcTable();
	sNPC_TBLDAT* pNpcData = reinterpret_cast<sNPC_TBLDAT*>(pNpcMerchantTbl->FindData(g_pMobManager->FindNpc(req->handle)));
	sMERCHANT_TBLDAT* pMerchantTblDat = (sMERCHANT_TBLDAT*)app->g_pTableContainer->GetMerchantTable()->FindData(pNpcData->amerchant_Tblidx[0]);
	int count = 0;
	bool bSelectedItem = false;
	TBLIDX tblSelectedItem = INVALID_TBLIDX;
	std::random_device rd;
	std::mt19937_64 mt(rd());
	std::uniform_int_distribution<int> distribution(0, 23);
	
	if (pMerchantTblDat->bySell_Type == MERCHANT_SELL_TYPE_GAMBLE)
	{
		//We gonna try 24 times, if doesn't exist then we gonna set to fail!
		while (!bSelectedItem)
		{
			if (count == 24)
				break;
			int randomized = distribution(mt);
			tblSelectedItem = pMerchantTblDat->aitem_Tblidx[randomized];
			if (tblSelectedItem != INVALID_TBLIDX)
				bSelectedItem = true;
			else
				count++;
		}			
	}
	if (bSelectedItem)
	{		
		this->gsf->CreateUpdateItem(plr, 1, tblSelectedItem, false, this->GetHandle());
		if (plr->cPlayerInventory->GetBagStatus() != 500)
			res->wResultCode = GAME_FAIL;
		else
			res->wResultCode = plr->cPlayerInventory->GetBagStatus();

		res->hItem = plr->cPlayerInventory->GetLastItemCreated();
	}
	else
	{
		res->wResultCode = GAME_FAIL;
		res->hItem = 0;
	}
	
	packet.SetPacketLen(sizeof(sGU_SHOP_GAMBLE_BUY_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);	
}
//------------------------------------------------
// Character Skill Upgrade By luiz45
//------------------------------------------------
void CClientSession::SendCharSkillUpgrade(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	//Upgrading Process
	sUG_SKILL_UPGRADE_REQ * req = (sUG_SKILL_UPGRADE_REQ*)pPacket->GetPacketData();
	CSkillTable* pSkillTable = app->g_pTableContainer->GetSkillTable();

	app->db->prepare("SELECT * FROM skills WHERE owner_id=? AND SlotID = ? ");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySlotIndex);
	app->db->execute();
	app->db->fetch();

	int skillID = app->db->getInt("skill_id");
	sSKILL_TBLDAT* pSkillData = reinterpret_cast<sSKILL_TBLDAT*>(pSkillTable->FindData(skillID));

	if (pSkillData->dwNextSkillTblidx)
	{
		CLkPacket packet(sizeof(sGU_SKILL_UPGRADE_RES));
		sGU_SKILL_UPGRADE_RES * res = (sGU_SKILL_UPGRADE_RES *)packet.GetPacketData();

		CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_SP));
		sGU_UPDATE_CHAR_SP * res2 = (sGU_UPDATE_CHAR_SP *)packet2.GetPacketData();

		res->wOpCode = GU_SKILL_UPGRADE_RES;
		res->wResultCode = GAME_SUCCESS;
		res->skillId = pSkillData->dwNextSkillTblidx;
		res->bySlot = req->bySlotIndex;
		packet.SetPacketLen(sizeof(sGU_SKILL_UPGRADE_RES));
		g_pApp->Send(this->GetHandle(), &packet);

		//Skill Level(ID)
		app->db->prepare("UPDATE skills SET skill_id=? WHERE owner_id=? AND skill_id=?");
		app->db->setInt(1, pSkillData->dwNextSkillTblidx);
		app->db->setInt(2, plr->GetCharID());
		app->db->setInt(3, skillID);
		app->db->execute();

		//Update player's SP
		plr->GetPcProfile()->dwSpPoint -= 1;
		app->qry->UpdateSPPoint(plr->GetCharID(), plr->GetPcProfile()->dwSpPoint);

		//Send a response to client to get Update SP OK
		res2->wOpCode = GU_UPDATE_CHAR_SP;
		res2->dwSpPoint = plr->GetPcProfile()->dwSpPoint;

		packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SP));
		g_pApp->Send(this->GetHandle(), &packet2);
	}
	plr = NULL;
	delete plr;
}
//-------------------------------------------------//
//--		BANK FUNCTION BY DANEOS START		-- //
//-------------------------------------------------//
//--------------------------------------------------------------------------------------//
//		BANK START
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankStartReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("START BANK \n");
	sUG_BANK_START_REQ * req = (sUG_BANK_START_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_BANK_START_RES));
	sGU_BANK_START_RES * res = (sGU_BANK_START_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_START_RES;
	res->wResultCode = GAME_SUCCESS;
	res->handle = req->handle;

	packet.SetPacketLen(sizeof(sGU_BANK_START_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		BANK END
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankEndReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("END BANK \n");
	CLkPacket packet(sizeof(sGU_BANK_END_RES));
	sGU_BANK_END_RES * res = (sGU_BANK_END_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_END_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_BANK_END_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		BANK LOAD
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankLoadReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("LOAD BANK \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_BANK_LOAD_REQ * req = (sUG_BANK_LOAD_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_BANK_LOAD_RES));
	sGU_BANK_LOAD_RES * res = (sGU_BANK_LOAD_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_LOAD_RES;
	res->wResultCode = GAME_SUCCESS;
	res->handle = req->handle;

	CLkPacket packet2(sizeof(sGU_BANK_ITEM_INFO));
	sGU_BANK_ITEM_INFO * res2 = (sGU_BANK_ITEM_INFO *)packet2.GetPacketData();


	app->db->prepare("SELECT * FROM items WHERE owner_id = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();

	res2->wOpCode = GU_BANK_ITEM_INFO;
	res2->byBeginCount = 0;
	res2->byItemCount = app->db->rowsCount();

	while (app->db->fetch())
	{
		res2->aBankProfile->handle = app->db->getInt("id");
		res2->aBankProfile->tblidx = app->db->getInt("tblidx");
		res2->aBankProfile->byPlace = app->db->getInt("place");
		res2->aBankProfile->byPos = app->db->getInt("pos");
		res2->aBankProfile->byStackcount = app->db->getInt("count");
		res2->aBankProfile->byRank = app->db->getInt("rank");
		res2->aBankProfile->byGrade = app->db->getInt("grade");
		res2->aBankProfile->byCurDur = app->db->getInt("durability");

		packet2.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res2->byItemCount * sizeof(sITEM_PROFILE)));
		g_pApp->Send(this->GetHandle(), &packet2);
	}

	CLkPacket packet3(sizeof(sGU_BANK_ZENNY_INFO));
	sGU_BANK_ZENNY_INFO * res3 = (sGU_BANK_ZENNY_INFO *)packet3.GetPacketData();
	//res3->dwZenny = this->plr->GetBankMoney();
	res3->wOpCode = GU_BANK_ZENNY_INFO;
	packet3.SetPacketLen(sizeof(sGU_BANK_ZENNY_INFO));
	g_pApp->Send(this->GetHandle(), &packet3);


	packet.SetPacketLen(sizeof(sGU_BANK_LOAD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		BANK BUY
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankBuyReq(CLkPacket * pPacket, CGameServer * app)
{
	int result = 501;
	sUG_BANK_BUY_REQ * req = (sUG_BANK_BUY_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_BANK_BUY_RES));
	sGU_BANK_BUY_RES * res = (sGU_BANK_BUY_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_BUY_RES;
	res->hNpchandle = req->hNpchandle;
	printf("%i %i \n", req->byMerchantTab, req->byPos);

	/*if (this->plr->GetMoney() >= 3000){
		if (req->byMerchantTab == 0 && req->byPos == 1){
		app->db->prepare("CALL CreateItem (?,?,?,?, @unique_iID)");
		app->db->setInt(1, 19992);
		app->db->setInt(2, plr->GetCharID());
		app->db->setInt(3, 9);
		app->db->setInt(4, 1);
		app->db->execute();
		app->db->execute("SELECT @unique_iID");
		app->db->fetch();
		res->sData.charId = this->GetavatarHandle();
		res->sData.itemNo = 19992;
		res->sData.itemId = app->db->getInt("@unique_iID");
		res->sData.byPlace = 9;
		res->sData.byPosition = 1;
		}
		else if (req->byMerchantTab == 0 && req->byPos == 2){
		app->db->prepare("CALL CreateItem (?,?,?,?, @unique_iID)");
		app->db->setInt(1, 19993);
		app->db->setInt(2, plr->GetCharID());
		app->db->setInt(3, 9);
		app->db->setInt(4, 2);
		app->db->execute();
		app->db->execute("SELECT @unique_iID");
		app->db->fetch();
		res->sData.charId = this->GetavatarHandle();
		res->sData.itemNo = 19993;
		res->sData.itemId = app->db->getInt("@unique_iID");
		res->sData.byPlace = 9;
		res->sData.byPosition = 2;
		}
		result = 500;
		res->hItemhandle = app->db->getInt("@unique_iID");
		this->plr->SetMoney(this->plr->GetMoney() - 3000);
		app->qry->SetMinusMoney(plr->GetCharID(), 3000);
		this->gsf->UpdateCharMoney(pPacket, this, 11, this->plr->GetMoney(), this->GetavatarHandle());
		}
		res->wResultCode = result;

		packet.SetPacketLen(sizeof(sGU_BANK_BUY_RES));
		g_pApp->Send(this->GetHandle(), &packet);*/
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		BANK MONEY
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankMoneyReq(CLkPacket * pPacket, CGameServer * app)
{
	int result = 501;
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_BANK_ZENNY_REQ * req = (sUG_BANK_ZENNY_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_BANK_ZENNY_RES));
	sGU_BANK_ZENNY_RES * res = (sGU_BANK_ZENNY_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_ZENNY_RES;
	res->bIsSave = req->bIsSave;
	res->dwZenny = req->dwZenny;
	res->handle = req->handle;

	/*if (req->bIsSave == TRUE) {
		if (this->plr->GetMoney() >= req->dwZenny){
		this->plr->SetBankMoney(this->plr->GetBankMoney() + req->dwZenny);
		app->qry->SetBankMoney(plr->GetCharID(), this->plr->GetBankMoney());
		//REMOVE MONEY FROM INVENTORY
		this->plr->SetMoney(this->plr->GetMoney() - req->dwZenny);
		app->qry->SetMinusMoney(this->plr->pcProfile->charId, req->dwZenny);
		this->gsf->UpdateCharMoney(pPacket, this, 11, this->plr->GetMoney(), this->GetavatarHandle());

		result = 500;
		}
		}
		else if (req->bIsSave == FALSE) {
		if (this->plr->GetBankMoney() >= req->dwZenny){
		this->plr->SetBankMoney(this->plr->GetBankMoney() - req->dwZenny);
		app->qry->SetBankMoney(plr->GetCharID(), this->plr->GetBankMoney());
		//ADD MONEY TO INVENTORY
		this->plr->SetMoney(this->plr->GetMoney() + req->dwZenny);
		app->qry->SetPlusMoney(this->plr->pcProfile->charId, req->dwZenny);
		this->gsf->UpdateCharMoney(pPacket, this, 11, this->plr->GetMoney(), this->GetavatarHandle());

		result = 500;
		}
		}

		res->wResultCode = result;

		packet.SetPacketLen(sizeof(sGU_BANK_ZENNY_RES));
		g_pApp->Send(this->GetHandle(), &packet);*/
	plr = NULL;
	delete plr;
}
//--------------------------------------------------------------------------------------//
//		BANK MOVE ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankMoveReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("bank move item \n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_BANK_MOVE_REQ * req = (sUG_BANK_MOVE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_BANK_MOVE_RES));
	sGU_BANK_MOVE_RES * res = (sGU_BANK_MOVE_RES *)packet.GetPacketData();

	app->db->prepare("SELECT * FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySrcPlace);
	app->db->setInt(3, req->bySrcPos);
	app->db->execute();
	app->db->fetch();
	RwUInt32 uniqueID = app->db->getInt("id");

	if (app->qry->CheckIfCanMoveItemThere(plr->GetCharID(), req->byDestPlace, req->byDestPos) == false){
		res->wResultCode = GAME_MOVE_CANT_GO_THERE;
	}
	else {
		app->qry->UpdateItemPlaceAndPos(uniqueID, req->byDestPlace, req->byDestPos);
		res->wResultCode = GAME_SUCCESS;
	}

	res->wOpCode = GU_BANK_MOVE_RES;
	res->hSrcItem = uniqueID;
	res->bySrcPlace = req->bySrcPlace;
	res->bySrcPos = req->bySrcPos;
	res->hDstItem = -1;
	res->byDestPlace = req->byDestPlace;
	res->byDestPos = req->byDestPos;
	res->handle = req->handle;

	packet.SetPacketLen(sizeof(sGU_BANK_MOVE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;

}
//--------------------------------------------------------------------------------------//
//		BANK STACK ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankStackReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_BANK_MOVE_STACK_REQ * req = (sUG_BANK_MOVE_STACK_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	// GET DATA FROM MYSQL
	app->db->prepare("SELECT id,tblidx FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySrcPlace);
	app->db->setInt(3, req->bySrcPos);
	app->db->execute();
	app->db->fetch();
	RwUInt32 uniqueID = app->db->getInt("id");
	RwUInt32 Src_Item_ID = app->db->getInt("tblidx");

	app->db->prepare("SELECT id,tblidx,count FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->byDestPlace);
	app->db->setInt(3, req->byDestPos);
	app->db->execute();
	app->db->fetch();
	RwUInt32 uniqueID2 = app->db->getInt("id");

	RwUInt32 result_code;

	if (Src_Item_ID == app->db->getInt("tblidx")) {
		result_code = 500;
	}
	else {
		result_code = 805;
	}

	// UPDATE ITEMS
	CLkPacket packet(sizeof(sGU_BANK_MOVE_STACK_RES));
	sGU_BANK_MOVE_STACK_RES * res = (sGU_BANK_MOVE_STACK_RES *)packet.GetPacketData();

	res->wOpCode = GU_BANK_MOVE_STACK_RES;
	res->bySrcPlace = req->bySrcPlace;
	res->bySrcPos = req->bySrcPos;
	res->byDestPlace = req->byDestPlace;
	res->byDestPos = req->byDestPos;
	res->hSrcItem = uniqueID;
	res->hDestItem = uniqueID2;
	res->byStackCount1 = req->byStackCount;
	res->byStackCount2 = req->byStackCount + app->db->getInt("count");
	res->handle = req->handle;
	res->wResultCode = result_code;

	// Send packet to client
	packet.SetPacketLen(sizeof(sGU_BANK_MOVE_STACK_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	// UPDATE AND DELETE
	if (result_code == 500){
		app->qry->UpdateItemsCount(uniqueID2, res->byStackCount2);
		app->qry->DeleteItemById(uniqueID);
		this->gsf->DeleteItemByUIdPlacePos(pPacket, this, uniqueID, req->bySrcPlace, req->bySrcPos);
	}
	plr = NULL;
	delete plr;

}
//--------------------------------------------------------------------------------------//
//		BANK DELETE ITEM
//--------------------------------------------------------------------------------------//
void CClientSession::SendBankDeleteReq(CLkPacket * pPacket, CGameServer * app)
{
	// GET DELETE ITEM
	sUG_BANK_ITEM_DELETE_REQ * req = (sUG_BANK_ITEM_DELETE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_BANK_ITEM_DELETE_RES));
	sGU_BANK_ITEM_DELETE_RES * res = (sGU_BANK_ITEM_DELETE_RES *)packet.GetPacketData();

	app->db->prepare("SELECT id FROM items WHERE owner_id=? AND place=? AND pos=?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->byPlace);
	app->db->setInt(3, req->byPos);
	app->db->execute();
	app->db->fetch();
	RwUInt32 u_itemid = app->db->getInt("id");

	res->wOpCode = GU_BANK_ITEM_DELETE_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_BANK_ITEM_DELETE_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	// DELETE ITEM
	app->qry->DeleteItemById(u_itemid);
	this->gsf->DeleteItemByUIdPlacePos(pPacket, this, u_itemid, req->byPlace, req->byPos);
	plr = NULL;
	delete plr;
}

//-------------------------------------------------
//      Quick Slot Update insert luiz45
//-------------------------------------------------
void CClientSession::SendCharUpdQuickSlot(CLkPacket * pPacket, CGameServer * app)
{
	sUG_QUICK_SLOT_UPDATE_REQ * req = (sUG_QUICK_SLOT_UPDATE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_QUICK_SLOT_UPDATE_RES));
	sGU_QUICK_SLOT_UPDATE_RES * res = (sGU_QUICK_SLOT_UPDATE_RES*)packet.GetPacketData();

	printf("QUICK SLOT ID: %i", req->bySlotID);
	app->qry->InsertRemoveQuickSlot(req->tblidx, req->bySlotID, plr->GetCharID());

	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_QUICK_SLOT_UPDATE_RES;

	packet.SetPacketLen(sizeof(sGU_QUICK_SLOT_UPDATE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//-------------------------------------------------
//      Quick Slot Update Delete luiz45
//-------------------------------------------------
void CClientSession::SendCharDelQuickSlot(CLkPacket * pPacket, CGameServer * app)
{
	sUG_QUICK_SLOT_DEL_REQ * req = (sUG_QUICK_SLOT_DEL_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_QUICK_SLOT_DEL_NFY));
	sGU_QUICK_SLOT_DEL_NFY * response = (sGU_QUICK_SLOT_DEL_NFY*)packet.GetPacketData();
	if (req->bySlotID <= 47)
		app->qry->InsertRemoveQuickSlot(0, req->bySlotID, plr->GetCharID());

	response->bySlotID = req->bySlotID;
	response->wOpCode = GU_QUICK_SLOT_DEL_NFY;

	packet.SetPacketLen(sizeof(sGU_QUICK_SLOT_DEL_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//void CClientSession::SendPlayerLevelUpCheck(CGameServer * app, int exp)
//{
//	/*CExpTable *expT = app->g_pTableContainer->GetExpTable();
//	for ( CTable::TABLEIT itNPCSpawn = expT->Begin(); itNPCSpawn != expT->End(); ++itNPCSpawn )
//	{
//	sEXP_TBLDAT *expTbl = (sEXP_TBLDAT *)itNPCSpawn->second;
//	printf("%d, %d\n",expTbl->dwExp, expTbl->dwNeed_Exp);
//	}*/
//	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
//	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_EXP));
//	sGU_UPDATE_CHAR_EXP * response = (sGU_UPDATE_CHAR_EXP*)packet.GetPacketData();
//
//	response->dwIncreasedExp = exp + rand() % plr->GetPcProfile()->byLevel * 2;
//	plr->GetPcProfile()->dwCurExp += response->dwIncreasedExp;
//	response->dwAcquisitionExp = exp;
//	response->dwCurExp = plr->GetPcProfile()->dwCurExp;
//	response->dwBonusExp = (exp - response->dwIncreasedExp);
//	response->handle = plr->GetAvatarHandle();
//	response->wOpCode = GU_UPDATE_CHAR_EXP;
//	if (plr->GetPcProfile()->dwCurExp >= plr->GetPcProfile()->dwMaxExpInThisLevel)
//	{
//		CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_SP));
//		sGU_UPDATE_CHAR_SP * res2 = (sGU_UPDATE_CHAR_SP *)packet2.GetPacketData();
//		plr->GetPcProfile()->dwCurExp -= plr->GetPcProfile()->dwMaxExpInThisLevel;
//		plr->GetPcProfile()->dwMaxExpInThisLevel += (plr->GetPcProfile()->dwMaxExpInThisLevel / 2);
//		CLkPacket packet1(sizeof(sGU_UPDATE_CHAR_LEVEL));
//		sGU_UPDATE_CHAR_LEVEL * response1 = (sGU_UPDATE_CHAR_LEVEL*)packet1.GetPacketData();
//		plr->GetPcProfile()->byLevel++;
//		response1->byCurLevel = plr->GetPcProfile()->byLevel;
//		response1->byPrevLevel = plr->GetPcProfile()->byLevel - 1;
//		response1->dwMaxExpInThisLevel = plr->GetPcProfile()->dwMaxExpInThisLevel;
//		response1->handle = plr->GetAvatarHandle();
//		response1->wOpCode = GU_UPDATE_CHAR_LEVEL;
//		packet1.SetPacketLen(sizeof(sGU_UPDATE_CHAR_LEVEL));
//		g_pApp->Send(this->GetHandle(), &packet1);
//		plr->SetLevelUP();
//		plr->cPlayerAttribute->UpdateAvatarAttributes(plr->GetAvatarHandle());
//		plr->GetPcProfile()->dwSpPoint += 1;
//		app->qry->UpdateSPPoint(plr->GetCharID(), plr->GetPcProfile()->dwSpPoint);
//		app->qry->UpdatePlayerLevel(plr->GetPcProfile()->byLevel, plr->GetCharID(), plr->GetPcProfile()->dwCurExp, plr->GetPcProfile()->dwMaxExpInThisLevel);
//		response->dwCurExp = plr->GetPcProfile()->dwCurExp;
//		plr->SetRPBall();
//		plr->SendRpBallInformation();
//		res2->wOpCode = GU_UPDATE_CHAR_SP;
//		res2->dwSpPoint = plr->GetPcProfile()->dwSpPoint;
//		packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SP));
//		g_pApp->Send(this->GetHandle(), &packet2);
//	}
//	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EXP));
//	g_pApp->Send(this->GetHandle(), &packet);
//	plr = NULL;
//	delete plr;
//}

void CClientSession::SendPlayerQuestReq(CLkPacket * pPacket, CGameServer * app)
{
	printf("--- UG_TS_CONFIRM_STEP_REQ --- \n");
	sUG_TS_CONFIRM_STEP_REQ* req = (sUG_TS_CONFIRM_STEP_REQ *)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TS_CONFIRM_STEP_RES));
	sGU_TS_CONFIRM_STEP_RES * res = (sGU_TS_CONFIRM_STEP_RES *)packet.GetPacketData();

	res->byTsType = req->byTsType;
	res->dwParam = req->dwParam;
	res->tcCurId = req->tcCurId;
	res->tcNextId = req->tcNextId;
	res->tId = req->tId;
	res->wOpCode = GU_TS_CONFIRM_STEP_RES;
	res->wResultCode = RESULT_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_TS_CONFIRM_STEP_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	//Save every step
	this->gsf->QuestStarted(plr->GetCharID(), req->tId, req->tcCurId, req->tcNextId, req->byTsType, req->dwEventData);
	//Initialize the tlqManager
	if (this->tlqManager == NULL)
		this->tlqManager = new TLQHandler();
	CObjectTable* pObjTable = app->g_pTableContainer->GetObjectTable(plr->GetWorldTblx());
	sOBJECT_TBLDAT* pObjDat = reinterpret_cast<sOBJECT_TBLDAT*>(pObjTable->FindData(req->dwEventData));
	switch (req->byTsType)
	{
		//Explanation...Case 0 For QuestTrigger and Case 1 For PC Trigger
	case 0:
	{
		printf("Quest Case\n");
		//Adding Special Quests like in the client to spawn some monster
		//Special quest have the range about 11000~14000
		if (((res->tId >= eQUEST_ID_RANGE_SPECIAL_QUEST_MIN) || (res->tId <= eQUEST_ID_RANGE_SPECIAL_QUEST_MAX)) && (res->tcNextId == 254))
		{
			//Your tutorial quest will fall here you need make a "If" condition for every CONFIRM_STEP REQ....you can set a break point above or below
			//PS: Comment your if conditions ;)
			if ((res->tId == 11000) && (res->tcCurId == 1))
			{
				g_pMobManager->CreateMobByTblidx(1, this->GetavatarHandle());
			}
			else if ((res->tId == 11002) && (res->tcCurId == 1))
			{
				g_pMobManager->CreateMobByTblidx(2, this->GetavatarHandle());
			}
			else if ((res->tId == 11003) && (res->tcCurId == 1))
			{
				g_pMobManager->CreateMobByTblidx(3, this->GetavatarHandle());
			}
			else if ((res->tId == 11006) && (res->tcCurId == 1))
			{
				g_pMobManager->CreateNPCByTblidx(1, this->GetavatarHandle());
			}
			else if ((res->tId == 11014) && (res->tcCurId == 1))
			{
				//There shouldnt be anything needed here. After the dialog(Which the lua plays)
				//it is supposed to give you quest 11008 =/
			}
			//TLQ 1 Mob Loader
			else if (res->tId == 11600)
			{
				if (res->tcCurId == 1)
				{
					this->tlqManager->LoadMobForTLQ(res->tId, this->GetavatarHandle());
				}
			}
		}
		else if ((res->tcCurId == 100) && (!pObjDat))
		{
			this->gsf->printError("We need to add the reward");

			// should be the reward because when we rewarsd a quest res->tcNextId is all the time 255
			// WE NEED THE CORRECT TBLIDX FOR THE REWARD
			CQuestRewardTable* pQstRewTable = app->g_pTableContainer->GetQuestRewardTable();

			//Poor way to get Reward TBLIDX
			sQUEST_REWARD_TBLDAT *rew;
			if (res->tId < 1000)
				rew = reinterpret_cast<sQUEST_REWARD_TBLDAT*>(pQstRewTable->FindData(((res->tId * 100) + 1)));
			else
				rew = reinterpret_cast<sQUEST_REWARD_TBLDAT*>(pQstRewTable->FindData(res->tId + 1));

			printf("%d %d %d %d\n%d %d %d %d\n%d\n", rew->arsDefRwd[0], rew->arsDefRwd[1], rew->arsDefRwd[2], rew->arsDefRwd[3], rew->arsSelRwd[0], rew->arsSelRwd[1], rew->arsSelRwd[2], rew->arsSelRwd[3], rew->tblidx);
			//PLEASE GOD CHANGE THIS FUCKING CODE SMELL WHEN I'm NOT LAZY
			if (rew->arsSelRwd[req->dwParam].dwRewardVal != 0)
			{
				switch (rew->arsSelRwd[req->dwParam].byRewardType)
				{
				case eREWARD_TYPE_NORMAL_ITEM:
				{
					sITEM_TBLDAT * pItemData = reinterpret_cast<sITEM_TBLDAT*>(app->g_pTableContainer->GetItemTable()->FindData(rew->arsSelRwd[req->dwParam].dwRewardIdx));

					CLkPacket packet0(sizeof(sGU_ITEM_PICK_RES));
					sGU_ITEM_PICK_RES * res0 = (sGU_ITEM_PICK_RES*)packet0.GetPacketData();
					res0->itemTblidx = pItemData->tblidx;
					res0->wOpCode = GU_ITEM_PICK_RES;
					res0->wResultCode = GAME_SUCCESS;
					this->gsf->CreateUpdateItem(plr, rew->arsSelRwd[req->dwParam].dwRewardVal, pItemData->tblidx, false, this->GetHandle());
					packet0.SetPacketLen(sizeof(sGU_ITEM_PICK_RES));
					g_pApp->Send(this->GetHandle(), &packet0);
				}
					break;
				case eREWARD_TYPE_QUEST_ITEM:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Quest Item");
				}
					break;
				case eREWARD_TYPE_CHANGE_CLASS:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Type Change Class");
				}
					break;
				case eREWARD_TYPE_PROBABILITY:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Probability");
				}
					break;
				case eREWARD_TYPE_REPUTATION:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Reputation");
				}
					break;
				case eREWARD_TYPE_CHANGE_ADULT:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Change Adult");
				}
					break;
				case eREWARD_TYPE_GET_CONVERT_CLASS_RIGHT:
				{
					rew->arsSelRwd[req->dwParam].dwRewardIdx;
					rew->arsSelRwd[req->dwParam].dwRewardVal;
					gsf->printOk("Reward Convert Class");
				}
					break;
				}
			}
			for (int i = 0; i < QUEST_REWARD_DEF_MAX_CNT; i++)
			{
				switch (rew->arsDefRwd[i].byRewardType)
				{
				case eREWARD_TYPE_NORMAL_ITEM:
				{
					sITEM_TBLDAT * pItemData = reinterpret_cast<sITEM_TBLDAT*>(app->g_pTableContainer->GetItemTable()->FindData(rew->arsDefRwd[i].dwRewardIdx));

					CLkPacket packet0(sizeof(sGU_ITEM_PICK_RES));
					sGU_ITEM_PICK_RES * res0 = (sGU_ITEM_PICK_RES*)packet0.GetPacketData();
					res0->itemTblidx = pItemData->tblidx;
					res0->wOpCode = GU_ITEM_PICK_RES;
					res0->wResultCode = GAME_SUCCESS;
					this->gsf->CreateUpdateItem(plr, rew->arsDefRwd[i].dwRewardVal, pItemData->tblidx, false, this->GetHandle());
					packet0.SetPacketLen(sizeof(sGU_ITEM_PICK_RES));
					g_pApp->Send(this->GetHandle(), &packet0);
				}
					break;
				case eREWARD_TYPE_QUEST_ITEM:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Quest Item");
				}
					break;
				case eREWARD_TYPE_EXP:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;

					if (plr->GetPcProfile()->byLevel < 50)
						CClientSession::SendPlayerLevelUpCheck(app, rew->arsDefRwd[i].dwRewardVal);

					gsf->printOk("Reward Experience");
				}
					break;
				case eREWARD_TYPE_SKILL:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Skill");
				}
					break;
				case eREWARD_TYPE_ZENY:
				{
					CLkPacket packet4(sizeof(sGU_ZENNY_PICK_RES));
					sGU_ZENNY_PICK_RES* res4 = (sGU_ZENNY_PICK_RES*)packet4.GetPacketData();

					res4->bSharedInParty = false; //this->plr->isInParty();
					res4->dwBonusZenny = 0;
					plr->GetPcProfile()->dwZenny += (rew->arsDefRwd[i].dwRewardVal + res4->dwBonusZenny);
					res4->dwZenny = plr->GetPcProfile()->dwZenny;
					res4->dwAcquisitionZenny = rew->arsDefRwd[i].dwRewardVal + res4->dwBonusZenny;
					res4->wResultCode = ZENNY_CHANGE_TYPE_REWARD;
					app->qry->SetPlusMoney(plr->GetCharID(), res4->dwAcquisitionZenny);
					res4->wOpCode = GU_ZENNY_PICK_RES;

					CLkPacket packetUpd(sizeof(sGU_UPDATE_CHAR_ZENNY));
					sGU_UPDATE_CHAR_ZENNY * res5 = (sGU_UPDATE_CHAR_ZENNY *)packetUpd.GetPacketData();
					res5->dwZenny = plr->GetPcProfile()->dwZenny;//by analazying this is the ammount...				
					res5->bIsNew = true;
					res5->handle = this->GetavatarHandle();
					res5->byChangeType = ZENNY_CHANGE_TYPE_REWARD;//never mind
					res5->wOpCode = GU_UPDATE_CHAR_ZENNY;
					packet4.SetPacketLen(sizeof(sGU_ZENNY_PICK_RES));
					packetUpd.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ZENNY));
					g_pApp->Send(this->GetHandle(), &packet4);
					g_pApp->Send(this->GetHandle(), &packetUpd);

					gsf->printOk("Reward Zenny");
				}
					break;
				case eREWARD_TYPE_CHANGE_CLASS:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Type Change Class");
				}
					break;
				case eREWARD_TYPE_PROBABILITY:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Probability");
				}
					break;
				case eREWARD_TYPE_REPUTATION:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Reputation");
				}
					break;
				case eREWARD_TYPE_CHANGE_ADULT:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Change Adult");
				}
					break;
				case eREWARD_TYPE_GET_CONVERT_CLASS_RIGHT:
				{
					rew->arsDefRwd[i].dwRewardIdx;
					rew->arsDefRwd[i].dwRewardVal;
					gsf->printOk("Reward Convert Class");
				}
					break;
				}
			}
		}
	}
		break;
	case 1:
	{
		printf("Trigger Case\n");
		sOBJECT_TBLDAT* pObjDat = reinterpret_cast<sOBJECT_TBLDAT*>(pObjTable->FindData(req->dwEventData));
		CWorldTable* pWorldTable = app->g_pTableContainer->GetWorldTable();
		CDungeonTable* pDungeonTable = app->g_pTableContainer->GetDungeonTable();
		if (res->tId >= 6000)
		{
			//Nothing to do here...
			//Time to find data for something we know is a correct tblidx for portal collision
			//printf("PortalId info: \n\rContents Tblidx:%d \n\rwFunction:%d", pObjDat->contentsTblidx, pObjDat->wFunction);
			CScriptLinkTable* pSlink = app->g_pTableContainer->GetScriptLinkTable();
			CWorldZoneTable* pWZTable = app->g_pTableContainer->GetWorldZoneTable();
			CWorldPathTable* pWpathT = app->g_pTableContainer->GetWorldPathTable();
			CWorldPlayTable* pWPlay = app->g_pTableContainer->GetWorldPlayTable();
			sWORLD_TBLDAT* pWorldTbldat = reinterpret_cast<sWORLD_TBLDAT*>(pWorldTable->FindData(120000));
			//printf("res->byTsType = %d, res->dwParam = %d, res->tcCurId = %d, res->tcNextId = %d, res->tId = %d\n", res->byTsType, res->dwParam, res->tcCurId, res->tcNextId, res->tId);
			if (res->tcCurId == 0)
			{
				CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
				sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet3.GetPacketData();
				res3->handle = this->GetavatarHandle();
				res3->sCharState.sCharStateBase.vCurLoc.x = pWorldTbldat->vStart1Loc.x;
				res3->sCharState.sCharStateBase.vCurLoc.y = pWorldTbldat->vStart1Loc.y;
				res3->sCharState.sCharStateBase.vCurLoc.z = pWorldTbldat->vStart1Loc.z;
				res3->sCharState.sCharStateBase.vCurDir.x = pWorldTbldat->vStart1Dir.x;
				res3->sCharState.sCharStateBase.vCurDir.z = pWorldTbldat->vStart1Dir.z;
				res3->sCharState.sCharStateDetail.sCharStateTeleporting.byTeleportType = TELEPORT_TYPE_TMQ_PORTAL;
				res3->sCharState.sCharStateBase.byStateID = CHARSTATE_TELEPORTING;
				res3->wOpCode = GU_UPDATE_CHAR_STATE;


				packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
				g_pApp->Send(this->GetHandle(), &packet3);
			}
			else if (res->tcCurId == 3)
			{

				this->tlqManager->SetTLQAccessForPlayer(res->tId, plr);
			}
			else if (res->tcCurId == 2)
			{
				CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
				sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet3.GetPacketData();
				res3->handle = this->GetavatarHandle();
				res3->sCharState.sCharStateBase.vCurLoc.x = pWorldTbldat->vStart1Loc.x;
				res3->sCharState.sCharStateBase.vCurLoc.y = pWorldTbldat->vStart1Loc.y;
				res3->sCharState.sCharStateBase.vCurLoc.z = pWorldTbldat->vStart1Loc.z;
				res3->sCharState.sCharStateBase.vCurDir.x = pWorldTbldat->vStart1Dir.x;
				res3->sCharState.sCharStateBase.vCurDir.z = pWorldTbldat->vStart1Dir.z;
				res3->sCharState.sCharStateDetail.sCharStateTeleporting.byTeleportType = TELEPORT_TYPE_TMQ_PORTAL;
				res3->sCharState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				res3->wOpCode = GU_UPDATE_CHAR_STATE;

				packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
				g_pApp->Send(this->GetHandle(), &packet3);
				printf("teleport sent");
			}

		}
		else if (req->byEventType == 2)//Is colision
		{
			CLkPacket packetQ(sizeof(sGU_CHAR_TELEPORT_RES));
			sGU_CHAR_TELEPORT_RES*as = (sGU_CHAR_TELEPORT_RES*)packetQ.GetPacketData();
			sWORLD_TBLDAT* pWorldTbldat = reinterpret_cast<sWORLD_TBLDAT*>(pWorldTable->FindData(2));
			if (res->tcCurId == 0)
			{
				CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
				sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet3.GetPacketData();
				res3->handle = this->GetavatarHandle();
				res3->sCharState.sCharStateBase.vCurLoc.x = pWorldTbldat->vStart1Loc.x;
				res3->sCharState.sCharStateBase.vCurLoc.y = pWorldTbldat->vStart1Loc.y;
				res3->sCharState.sCharStateBase.vCurLoc.z = pWorldTbldat->vStart1Loc.z;
				res3->sCharState.sCharStateBase.vCurDir.x = pWorldTbldat->vStart1Dir.x;
				res3->sCharState.sCharStateBase.vCurDir.z = pWorldTbldat->vStart1Dir.z;
				res3->sCharState.sCharStateDetail.sCharStateTeleporting.byTeleportType = TELEPORT_TYPE_DUNGEON;
				res3->sCharState.sCharStateBase.byStateID = CHARSTATE_TELEPORTING;
				res3->wOpCode = GU_UPDATE_CHAR_STATE;

				packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
				g_pApp->Send(this->GetHandle(), &packet3);
			}
			else if (res->tcCurId == 3)
			{

				sWORLD_TBLDAT* pWorldTbldat = reinterpret_cast<sWORLD_TBLDAT*>(pWorldTable->FindData(2));
				as->sWorldInfo.hTriggerObjectOffset = 100000;
				as->sWorldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
				as->sWorldInfo.tblidx = 2;
				as->sWorldInfo.worldID = 2;
				//printf("WorldID:%d\n\r", pWorldTbldat->dwWorldResourceID);
				as->vNewDir.x = pWorldTbldat->vStart1Dir.x;
				as->vNewDir.y = 0;
				as->vNewDir.z = pWorldTbldat->vStart1Dir.z;
				as->vNewLoc.x = pWorldTbldat->vStart1Loc.x;
				as->vNewLoc.y = pWorldTbldat->vStart1Loc.y;
				as->vNewLoc.z = pWorldTbldat->vStart1Loc.z;
				as->wOpCode = GU_CHAR_TELEPORT_RES;
				packetQ.SetPacketLen(sizeof(sGU_CHAR_TELEPORT_RES));
				g_pApp->Send(this->GetHandle(), &packetQ);
				plr->SetWorldID(2);
				plr->SetWorldTblidx(2);
			}
			else if (res->tcCurId == 2)
			{
				CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_STATE));
				sGU_UPDATE_CHAR_STATE * res3 = (sGU_UPDATE_CHAR_STATE *)packet3.GetPacketData();
				res3->handle = this->GetavatarHandle();
				res3->sCharState.sCharStateBase.vCurLoc.x = pWorldTbldat->vStart1Loc.x;
				res3->sCharState.sCharStateBase.vCurLoc.y = pWorldTbldat->vStart1Loc.y;
				res3->sCharState.sCharStateBase.vCurLoc.z = pWorldTbldat->vStart1Loc.z;
				res3->sCharState.sCharStateBase.vCurDir.x = pWorldTbldat->vStart1Dir.x;
				res3->sCharState.sCharStateBase.vCurDir.z = pWorldTbldat->vStart1Dir.z;
				res3->sCharState.sCharStateDetail.sCharStateTeleporting.byTeleportType = TELEPORT_TYPE_DUNGEON;
				res3->sCharState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				res3->wOpCode = GU_UPDATE_CHAR_STATE;

				packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
				g_pApp->Send(this->GetHandle(), &packet3);
				printf("teleport sent");
			}
		}
	}
		break;
	default:
	{
		printf("Unhandled Type, Server and Client Didn't Recognize this Trigger Type");
	}
		break;
	}
	//printf("res->byTsType = %d, res->dwParam = %d, res->tcCurId = %d, res->tcNextId = %d, res->tId = %d\n",res->byTsType, res->dwParam, res->tcCurId, res->tcNextId, res->tId); 
	plr = NULL;
	delete plr;
}
void	CClientSession::SendZennyPickUpReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_ZENNY_PICK_REQ* req = (sUG_ZENNY_PICK_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_ZENNY_PICK_RES));
	sGU_ZENNY_PICK_RES * res = (sGU_ZENNY_PICK_RES *)packet.GetPacketData();

	int amnt = app->FindZenny(req->handle);

	if (amnt > 0)
	{
		app->RemoveZenny(req->handle);
		res->bSharedInParty = false; //this->plr->isInParty();
		res->dwBonusZenny = rand() % 100;
		//res->dwOriginalZenny = amnt;
		plr->GetPcProfile()->dwZenny += (amnt + res->dwBonusZenny);
		res->dwZenny = plr->GetPcProfile()->dwZenny;
		res->dwAcquisitionZenny = amnt + res->dwBonusZenny;
		res->wResultCode = ZENNY_CHANGE_TYPE_PICK;

		app->qry->SetPlusMoney(plr->GetCharID(), res->dwAcquisitionZenny);

		CLkPacket packet3(sizeof(sGU_OBJECT_DESTROY));
		sGU_OBJECT_DESTROY * res3 = (sGU_OBJECT_DESTROY *)packet3.GetPacketData();
		res3->handle = req->handle;
		res3->wOpCode = GU_OBJECT_DESTROY;
		packet3.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
		g_pApp->Send(this->GetHandle(), &packet3);

		CLkPacket packet5(sizeof(sGU_UPDATE_CHAR_ZENNY));
		sGU_UPDATE_CHAR_ZENNY * res5 = (sGU_UPDATE_CHAR_ZENNY *)packet5.GetPacketData();
		res5->dwZenny = plr->GetPcProfile()->dwZenny;//by analazying this is the ammount...				
		res5->bIsNew = true;
		res5->handle = this->GetavatarHandle();
		res5->byChangeType = 0;//never mind
		res5->wOpCode = GU_UPDATE_CHAR_ZENNY;
		packet5.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ZENNY));
		g_pApp->Send(this->GetHandle(), &packet5);
	}
	else
	{
		res->wResultCode = GAME_FAIL;
	}
	res->wOpCode = GU_ZENNY_PICK_RES;
	packet.SetPacketLen(sizeof(sGU_ZENNY_PICK_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}

//Pick up yo shit yo
void	CClientSession::SendItemPickUpReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_ITEM_PICK_REQ* req = (sUG_ITEM_PICK_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_ITEM_PICK_RES));
	sGU_ITEM_PICK_RES * res = (sGU_ITEM_PICK_RES*)packet.GetPacketData();
	CGameServer::ITEMDROPEDFROMMOB* itemDropped = app->FindItemPickup(req->handle);

	if (itemDropped != NULL)
	{
		CItemTable* itemTbl = app->g_pTableContainer->GetItemTable();
		sITEM_DATA* itemTblDat = (sITEM_DATA*)itemTbl->FindData(itemDropped->itemTblidx);
		res->itemTblidx = req->handle;
		res->wOpCode = GU_ITEM_PICK_RES;
		this->gsf->CreateUpdateItem(plr, 1, itemDropped->itemTblidx, false, this->GetHandle(), itemDropped->byGrade, itemDropped->byRank, itemDropped->byBattleAttribute);//Luiz, This needs to be changed i think. Not exactly sure why its doing what its doing.
		res->wResultCode = plr->cPlayerInventory->GetBagStatus();

		CLkPacket packet3(sizeof(sGU_OBJECT_DESTROY));
		sGU_OBJECT_DESTROY * res3 = (sGU_OBJECT_DESTROY *)packet3.GetPacketData();
		res3->handle = req->handle;
		res3->wOpCode = GU_OBJECT_DESTROY;
		packet3.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
		g_pApp->Send(this->GetHandle(), &packet3);
		app->RemoveItemPickup(req->handle);
	}
	else
	{
		res->wResultCode = GAME_FAIL;
	}
	res->wOpCode = GU_ITEM_PICK_RES;
	packet.SetPacketLen(sizeof(sGU_ITEM_PICK_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}


void	CClientSession::SendFreeBattleReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_FREEBATTLE_CHALLENGE_REQ* req = (sUG_FREEBATTLE_CHALLENGE_REQ *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_FREEBATTLE_CHALLENGE_RES));
	sGU_FREEBATTLE_CHALLENGE_RES * res = (sGU_FREEBATTLE_CHALLENGE_RES *)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_FREEBATTLE_ACCEPT_REQ));
	sGU_FREEBATTLE_ACCEPT_REQ * res2 = (sGU_FREEBATTLE_ACCEPT_REQ *)packet2.GetPacketData();

	PlayersMain* targeted = app->GetUserSession(req->hTarget);
	plr->SetCharIDForDuel(targeted->GetCharID());

	res->hTarget = req->hTarget;
	res->wOpCode = GU_FREEBATTLE_CHALLENGE_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->hChallenger = plr->GetAvatarHandle();
	res2->wOpCode = GU_FREEBATTLE_ACCEPT_REQ;

	packet.SetPacketLen(sizeof(sGU_FREEBATTLE_CHALLENGE_RES));
	g_pApp->Send(this->GetHandle(), &packet);

	packet2.SetPacketLen(sizeof(sGU_FREEBATTLE_ACCEPT_REQ));
	g_pApp->Send(targeted->GetSession(), &packet2);
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
}
void	CClientSession::SendFreeBattleAccpetReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_FREEBATTLE_ACCEPT_RES* req = (sUG_FREEBATTLE_ACCEPT_RES *)pPacket->GetPacketData();

	if (req->byAccept == 1)
	{
		CLkPacket packet2(sizeof(sGU_FREEBATTLE_START_NFY));
		sGU_FREEBATTLE_START_NFY * res2 = (sGU_FREEBATTLE_START_NFY *)packet2.GetPacketData();
		res2->hTarget = plr->GetAvatarHandle();
		res2->vRefreeLoc = plr->GetPlayerPosition();
		res2->vRefreeLoc.x += rand() % 10 + 5;
		res2->vRefreeLoc.z -= 2;
		res2->wOpCode = GU_FREEBATTLE_START_NFY;
		packet2.SetPacketLen(sizeof(sGU_FREEBATTLE_START_NFY));
		g_pApp->Send(this->GetHandle(), &packet2);
		app->UserBroadcastothers(&packet2, this);
	}
	else
	{
		CLkPacket packet2(sizeof(sGU_FREEBATTLE_CANCEL_NFY));
		sGU_FREEBATTLE_CANCEL_NFY * res2 = (sGU_FREEBATTLE_CANCEL_NFY *)packet2.GetPacketData();
		res2->wOpCode = GU_FREEBATTLE_CANCEL_NFY;
		res2->wResultCode = GAME_FREEBATTLE_CHALLENGE_ACCEPT_DENIED;
		packet2.SetPacketLen(sizeof(sGU_FREEBATTLE_CANCEL_NFY));
		g_pApp->Send(this->GetHandle(), &packet2);
		app->UserBroadcastothers(&packet2, this);
	}
	plr = NULL;
	delete plr;
}
/////////////////////////////////////////////////////////////////////////////
//// item usage by Luiz45
/////////////////////////////////////////////////////////////////////////////
void	CClientSession::SendItemUseReq(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_ITEM_USE_REQ * req = (sUG_ITEM_USE_REQ*)pPacket->GetPacketData();

	//Defining Packets
	CLkPacket packet(sizeof(sGU_ITEM_USE_RES));
	sGU_ITEM_USE_RES * res = (sGU_ITEM_USE_RES*)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_CHAR_ACTION_ITEM));
	sGU_CHAR_ACTION_ITEM * pItemAct = (sGU_CHAR_ACTION_ITEM*)packet2.GetPacketData();

	CLkPacket packet4(sizeof(sGU_BUFF_REGISTERED));
	sGU_BUFF_REGISTERED * pItemBuff = (sGU_BUFF_REGISTERED*)packet4.GetPacketData();
	//VehiclePackets
	CLkPacket packet5(sizeof(sGU_VEHICLE_START_NFY));
	sGU_VEHICLE_START_NFY* pVehicleStart = (sGU_VEHICLE_START_NFY*)packet5.GetPacketData();

	CLkPacket packet6(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* pVehicleAspcStateUpd = (sGU_UPDATE_CHAR_STATE*)packet6.GetPacketData();

	CLkPacket packet7(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));
	sGU_UPDATE_CHAR_ASPECT_STATE* pVehicleAspect = (sGU_UPDATE_CHAR_ASPECT_STATE*)packet7.GetPacketData();
	//------------------------------------------------------------------------------------------------//
	app->db->prepare("SELECT * FROM items WHERE owner_id = ? AND place = ? AND pos = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->byPlace);
	app->db->setInt(3, req->byPos);
	app->db->execute();
	app->db->fetch();

	//Defining table_dats
	CItemTable * pItemTable = app->g_pTableContainer->GetItemTable();
	CUseItemTable * pItemUseTable = app->g_pTableContainer->GetUseItemTable();
	CSystemEffectTable * pEffectTable = app->g_pTableContainer->GetSystemEffectTable();
	CVehicleTable* pVehicleTable = app->g_pTableContainer->GetVehicleTable();
	sITEM_TBLDAT * itemTBL = reinterpret_cast<sITEM_TBLDAT*>(pItemTable->FindData(app->db->getInt("tblidx")));
	sVEHICLE_TBLDAT* pVehicleTbl = reinterpret_cast<sVEHICLE_TBLDAT*>(pVehicleTable->FindData(app->db->getInt("tblidx")));
	sUSE_ITEM_TBLDAT * itemUseTbl = reinterpret_cast<sUSE_ITEM_TBLDAT*>(pItemUseTable->FindData(itemTBL->Use_Item_Tblidx));
	sSYSTEM_EFFECT_TBLDAT * pEffectTbl = reinterpret_cast<sSYSTEM_EFFECT_TBLDAT*>(pEffectTable->FindData(itemUseTbl->aSystem_Effect[0]));

	//Prepared Item Response	
	res->byPlace = req->byPlace;
	res->byPos = req->byPos;
	res->tblidxItem = itemTBL->tblidx;
	res->wOpCode = GU_ITEM_USE_RES;
	res->wResultCode = GAME_SUCCESS;

	if (!pVehicleTbl)
	{
		pItemAct->handle = this->GetavatarHandle();
		pItemAct->itemTblidx = itemTBL->tblidx;
		pItemAct->aSkillResult[0].hTarget = this->GetavatarHandle();
		pItemAct->aSkillResult[0].effectResult1.fResultValue = itemUseTbl->afSystem_Effect_Value[0];
		pItemAct->aSkillResult[0].effectResult1.eResultType = DBO_SYSTEM_EFFECT_RESULT_TYPE_GENERAL;
		pItemAct->aSkillResult[0].effectResult2.fResultValue = itemUseTbl->afSystem_Effect_Value[1];
		pItemAct->aSkillResult[0].effectResult2.eResultType = DBO_SYSTEM_EFFECT_RESULT_TYPE_GENERAL;
		pItemAct->wOpCode = GU_CHAR_ACTION_ITEM;
		pItemAct->wResultCode = GAME_SUCCESS;
		pItemAct->bySkillResultCount = 1;

		//Item buff is only to display on the Top
		pItemBuff->tblidx = itemTBL->tblidx;
		pItemBuff->hHandle = this->GetavatarHandle();
		pItemBuff->bySourceType = DBO_OBJECT_SOURCE_ITEM;
		pItemBuff->dwInitialDuration = itemUseTbl->dwCoolTimeInMilliSecs;
		pItemBuff->dwTimeRemaining = itemUseTbl->dwKeepTimeInMilliSecs;
		pItemBuff->wOpCode = GU_BUFF_REGISTERED;

		packet.SetPacketLen(sizeof(sGU_ITEM_USE_RES));
		packet2.SetPacketLen(sizeof(sGU_CHAR_ACTION_ITEM));

		g_pApp->Send(this->GetHandle(), &packet);
		g_pApp->Send(this->GetHandle(), &packet2);
		app->UserBroadcastothers(&packet, this);
		app->UserBroadcastothers(&packet2, this);
		if ((pEffectTbl->effectCode == ACTIVE_HEAL_OVER_TIME) || (pEffectTbl->effectCode == ACTIVE_EP_OVER_TIME))
		{
			packet4.SetPacketLen(sizeof(sGU_BUFF_REGISTERED));
			g_pApp->Send(this->GetHandle(), &packet4);
			app->UserBroadcastothers(&packet4, this);
		}

		this->gsf->CreateUpdateItem(plr, 0, itemTBL->tblidx, true, this->GetHandle(), 0, 0, itemTBL->byRank, req->byPlace, req->byPos);
		//Validation by Effect Code for better read
		this->gsf->SendItemEffect(this, itemUseTbl->aSystem_Effect[0], itemUseTbl->tblidx);
	}
	else
	{		
		pVehicleStart->hDriverHandle = this->GetavatarHandle();
		pVehicleStart->hVehicleItem = app->db->getInt("id");
		pVehicleStart->idVehicleItemTblidx = pVehicleTbl->tblidx;
		pVehicleStart->vStartPosition.x = plr->GetPlayerPosition().x;
		pVehicleStart->vStartPosition.y = plr->GetPlayerPosition().y;
		pVehicleStart->vStartPosition.z = plr->GetPlayerPosition().z;
		pVehicleStart->wOpCode = GU_VEHICLE_START_NFY;
		
		pVehicleAspcStateUpd->handle = this->GetavatarHandle();
		pVehicleAspcStateUpd->sCharState.sCharStateDetail.sCharStateRideOn.hTarget = app->db->getInt("id");
		pVehicleAspcStateUpd->sCharState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = ASPECTSTATE_VEHICLE;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sVehicle.bIsEngineOn = false;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sVehicle.idVehicleTblidx = pVehicleTbl->tblidx;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurDir.x = plr->GetPlayerDirection().x;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurDir.y = plr->GetPlayerDirection().y;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurDir.z = plr->GetPlayerDirection().z;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurLoc.x = plr->GetPlayerPosition().x;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurLoc.y = plr->GetPlayerPosition().y;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.vCurLoc.z = plr->GetPlayerPosition().z;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.byStateID = CHARSTATE_RIDEON;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.bFightMode = false;
		pVehicleAspcStateUpd->sCharState.sCharStateBase.dwConditionFlag = CHARCOND_FLAG_ATTACK_DISALLOW;
		pVehicleAspcStateUpd->wOpCode = GU_UPDATE_CHAR_STATE;
		
		pVehicleAspect->aspectState.sAspectStateBase.byAspectStateId = ASPECTSTATE_VEHICLE;
		pVehicleAspect->aspectState.sAspectStateDetail.sVehicle.bIsEngineOn = false;
		pVehicleAspect->aspectState.sAspectStateDetail.sVehicle.idVehicleTblidx = pVehicleTbl->tblidx;
		pVehicleAspect->handle = this->GetavatarHandle();
		pVehicleAspect->wOpCode = GU_UPDATE_CHAR_ASPECT_STATE;

		packet.SetPacketLen(sizeof(sGU_ITEM_USE_RES));
		packet5.SetPacketLen(sizeof(sGU_VEHICLE_START_NFY));
		packet6.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
		packet7.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));

		g_pApp->Send(this->GetHandle(), &packet);
		//app->UserBroadcastothers(&packet5, this);
		g_pApp->Send(this->GetHandle(), &packet5);
		g_pApp->Send(this->GetHandle(), &packet6);
		g_pApp->Send(this->GetHandle(), &packet7);
	}
	plr = NULL;
	delete plr;
}

//---------------------------------------------------------------------//
//------------------Skill Transform Cancel - Luiz45--------------------//
//---------------------------------------------------------------------//
void CClientSession::SendCharSkillTransformCancel(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	//Response Skill
	sUG_TRANSFORM_CANCEL_REQ * req = (sUG_TRANSFORM_CANCEL_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_TRANSFORM_CANCEL_RES));
	sGU_TRANSFORM_CANCEL_RES * res = (sGU_TRANSFORM_CANCEL_RES*)packet.GetPacketData();
	res->wOpCode = GU_TRANSFORM_CANCEL_RES;
	res->wResultCode = GAME_SUCCESS;

	//Update Char State
	CLkPacket packet2(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));
	sGU_UPDATE_CHAR_ASPECT_STATE * myPlayerState = (sGU_UPDATE_CHAR_ASPECT_STATE*)packet2.GetPacketData();
	myPlayerState->handle = plr->GetAvatarHandle();
	myPlayerState->wOpCode = GU_UPDATE_CHAR_ASPECT_STATE;
	myPlayerState->aspectState.sAspectStateBase.byAspectStateId = 255;//Don't see any Const then i send 0 because i'm not going transform ^^

	//Packets Sending
	packet.SetPacketLen(sizeof(sGU_TRANSFORM_CANCEL_RES));
	packet2.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);

	//Sending to others
	app->UserBroadcastothers(&packet, this);
	app->UserBroadcastothers(&packet2, this);
	plr = NULL;
	delete plr;
}


void CClientSession::SendSocialSkillRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_SOCIAL_ACTION * req = (sUG_SOCIAL_ACTION*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_SOCIAL_ACTION));
	sGU_SOCIAL_ACTION* res = (sGU_SOCIAL_ACTION*)packet.GetPacketData();

	res->hSubject = this->GetavatarHandle();
	res->socialActionId = req->socialActionId;
	res->wOpCode = GU_SOCIAL_ACTION;

	packet.SetPacketLen(sizeof(sGU_SOCIAL_ACTION));
	//g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
	//this->gsf->printOk("Sended");
}
//DWORD WINAPI	SendRpChargethread(LPVOID arg)
//{
//	CClientSession* session = (CClientSession*)arg;
//	bool isRpBall = false;
//	while (42)
//	{
//		CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_RP));
//		sGU_UPDATE_CHAR_RP * res3 = (sGU_UPDATE_CHAR_RP *)packet3.GetPacketData();
//
//		session->cPlayersMain->GetPcProfile()->wCurRP += 1;
//		if (session->cPlayersMain->GetRpBallCounter() >= 1)
//		{
//			res3->wMaxRP = (session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP / session->cPlayersMain->GetRpBallCounter());
//			isRpBall = true;
//			if (session->cPlayersMain->GetPcProfile()->wCurRP > (session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP / session->cPlayersMain->GetRpBallCounter()))
//				session->cPlayersMain->GetPcProfile()->wCurRP = session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP / session->cPlayersMain->GetRpBallCounter();
//		}
//		else
//		{
//			res3->wMaxRP = session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP;
//			isRpBall = false;
//			if (session->cPlayersMain->GetPcProfile()->wCurRP > session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP)
//				session->cPlayersMain->GetPcProfile()->wCurRP = session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP;
//			res3->bHitDelay = false;
//		}
//		if (isRpBall == true)
//		{
//			if (session->cPlayersMain->GetPcProfile()->wCurRP >= (session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP / session->cPlayersMain->GetRpBallCounter()))
//			{
//				if (session->cPlayersMain->GetRpBallFull() < 8 && session->cPlayersMain->GetRpBallFull() < session->cPlayersMain->GetRpBallCounter())
//				{
//					res3->bHitDelay = true;
//					session->cPlayersMain->GetPcProfile()->wCurRP = 0;
//					session->cPlayersMain->SendRpBallUpdate(0); // 0 for increase ! 1 for decrease !
//				}
//				else
//				{
//					res3->bHitDelay = false;
//					session->cPlayersMain->GetPcProfile()->wCurRP = (session->cPlayersMain->GetPcProfile()->avatarAttribute.wBaseMaxRP / session->cPlayersMain->GetRpBallCounter());
//				}
//			}
//
//		}
//		res3->handle = session->GetavatarHandle();
//		res3->wCurRP = session->cPlayersMain->GetPcProfile()->wCurRP;
//
//		res3->wOpCode = GU_UPDATE_CHAR_RP;
//		packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_RP));
//		g_pApp->Send(session->cPlayersMain->GetSession(), &packet3);
//		Sleep(5);
//	}
//}
void CClientSession::SendRpCharge(CLkPacket *pPacket, CGameServer * app)
{
	sUG_CHAR_CHARGE * req = (sUG_CHAR_CHARGE*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	if (req->bCharge)
	{
		CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
		sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
		CLkPacket packet2(sizeof(sGU_AVATAR_RP_INCREASE_START_NFY));
		sGU_AVATAR_RP_INCREASE_START_NFY * res2 = (sGU_AVATAR_RP_INCREASE_START_NFY *)packet2.GetPacketData();
		CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_MAX_RP));
		sGU_UPDATE_CHAR_MAX_RP * res3 = (sGU_UPDATE_CHAR_MAX_RP *)packet3.GetPacketData();

		res->handle = this->GetavatarHandle();
		memcpy(&res->sCharState, plr->GetCharState(), sizeof(sCHARSTATE));
		res->sCharState.sCharStateBase.byStateID = CHARSTATE_CHARGING;
		res->wOpCode = GU_UPDATE_CHAR_STATE;
		res2->wOpCode = GU_AVATAR_RP_INCREASE_START_NFY;
		res3->hSubject = this->GetavatarHandle();
		res3->wMaxRp = plr->GetPcProfile()->avatarAttribute.wBaseMaxRP;
		res3->wOpCode = GU_UPDATE_CHAR_MAX_RP;
		packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
		g_pApp->Send(this->GetHandle(), &packet);
		packet2.SetPacketLen(sizeof(sGU_AVATAR_RP_INCREASE_START_NFY));
		g_pApp->Send(this->GetHandle(), &packet2);
		packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_MAX_RP));
		g_pApp->Send(this->GetHandle(), &packet3);

		plr->SendThreadUpdateRP();
}
	if (req->bCharge == false)
	{
		CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
		sGU_UPDATE_CHAR_STATE * res = (sGU_UPDATE_CHAR_STATE *)packet.GetPacketData();
		CLkPacket packet2(sizeof(sGU_AVATAR_RP_INCREASE_STOP_NFY));
		sGU_AVATAR_RP_INCREASE_STOP_NFY * res2 = (sGU_AVATAR_RP_INCREASE_STOP_NFY *)packet2.GetPacketData();

		res->handle = this->GetavatarHandle();
		memcpy(&res->sCharState, plr->GetCharState(), sizeof(sCHARSTATE));
		res->sCharState.sCharStateBase.byStateID = CHARSTATE_STANDING;
		res->wOpCode = GU_UPDATE_CHAR_STATE;
		res2->wOpCode = GU_AVATAR_RP_INCREASE_STOP_NFY;
		packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
		g_pApp->Send(this->GetHandle(), &packet);
		packet2.SetPacketLen(sizeof(sGU_AVATAR_RP_INCREASE_STOP_NFY));
		g_pApp->Send(this->GetHandle(), &packet2);
		app->UserBroadcastothers(&packet, this);
		CLkPacket packet5(sizeof(sGU_AVATAR_RP_DECREASE_START_NFY));
		sGU_AVATAR_RP_DECREASE_START_NFY * res5 = (sGU_AVATAR_RP_DECREASE_START_NFY *)packet5.GetPacketData();
		res5->wOpCode = GU_AVATAR_RP_DECREASE_START_NFY;
		packet5.SetPacketLen(sizeof(sGU_AVATAR_RP_DECREASE_START_NFY));
		g_pApp->Send(this->GetHandle(), &packet5);
	}
	plr = NULL;
	delete plr;
}
//-----------------------------------------------------------------//
//-------------------Skill/Item BUFF Drop--------------------------//
//-----------------------------------------------------------------//
void CClientSession::SendCharSkillBuffDrop(CLkPacket * pPacket, CGameServer * app)
{
	sUG_BUFF_DROP_REQ * req = (sUG_BUFF_DROP_REQ*)pPacket->GetPacketData();
	//Get Skill to Remove
	CSkillTable * pSkillTable = app->g_pTableContainer->GetSkillTable();
	sSKILL_RESULT * pSkillData = (sSKILL_RESULT*)pPacket->GetPacketData();

	//Response Prepare
	CLkPacket packet(sizeof(sGU_BUFF_DROP_RES));
	sGU_BUFF_DROP_RES * res = (sGU_BUFF_DROP_RES*)packet.GetPacketData();
	res->wOpCode = GU_BUFF_DROP_RES;
	res->wResultCode = GAME_SUCCESS;

	//Dropp Event Prepare
	CLkPacket packet2(sizeof(sGU_BUFF_DROPPED));
	sGU_BUFF_DROPPED * pBuffDrop = (sGU_BUFF_DROPPED*)packet2.GetPacketData();
	pBuffDrop->hHandle = this->GetavatarHandle();
	pBuffDrop->bySourceType = DBO_OBJECT_SOURCE_SKILL;//Need be rechecked because this can be a type DBO_OBJECT_SOURCE_ITEM
	pBuffDrop->wOpCode = GU_BUFF_DROPPED;
	pBuffDrop->tblidx = req->tblidx;

	//First Drop,Second Resp to client
	packet2.SetPacketLen(sizeof(sGU_BUFF_DROPPED));
	packet.SetPacketLen(sizeof(sGU_BUFF_DROP_RES));
	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet2, this);
	app->UserBroadcastothers(&packet, this);
}
//-----------------------------------------------------------//
//-----------HTB SKILL REQ/RES  Luiz45  ---------------------//
//-----------------------------------------------------------//
void CClientSession::SendHTBStartReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_HTB_START_REQ * req = (sUG_HTB_START_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	app->db->prepare("SELECT * FROM skills WHERE owner_id = ? AND SlotID= ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->setInt(2, req->bySkillSlot);
	app->db->execute();
	app->db->fetch();

	CLkPacket packet(sizeof(sGU_HTB_START_RES));
	sGU_HTB_START_RES * resp = (sGU_HTB_START_RES*)packet.GetPacketData();

	int skillTbl = app->db->getInt("skill_id");
	CSkillTable * pSkillTable = app->g_pTableContainer->GetSkillTable();
	sHTB_SET_TBLDAT *pHTBSetTblData = reinterpret_cast<sHTB_SET_TBLDAT*>(pSkillTable->FindData(skillTbl));

	resp->wOpCode = GU_HTB_START_RES;
	resp->wResultCode = GAME_SUCCESS;
	resp->bySkillSlot = req->bySkillSlot;

	packet.SetPacketLen(sizeof(sGU_HTB_START_RES));

	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
	//Initiate our HTB Skill
	SendCharUpdateHTBState(skillTbl, app);
	plr = NULL;
	delete plr;
}
//-----------------------------------------------------------//
//-----------HTB States/Steps   Luiz45  ---------------------//
//-----------------------------------------------------------//
void CClientSession::SendCharUpdateHTBState(int SkillID, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * respA = (sGU_UPDATE_CHAR_STATE*)packet.GetPacketData();

	//Setting Our Location
	respA->wOpCode = GU_UPDATE_CHAR_STATE;
	respA->handle = this->GetavatarHandle();
	respA->sCharState.sCharStateBase.byStateID = CHARSTATE_HTB;
	respA->sCharState.sCharStateBase.vCurLoc.x = plr->GetCharState()->sCharStateBase.vCurLoc.x;
	respA->sCharState.sCharStateBase.vCurLoc.y = plr->GetCharState()->sCharStateBase.vCurLoc.y;
	respA->sCharState.sCharStateBase.vCurLoc.z = plr->GetCharState()->sCharStateBase.vCurLoc.z;
	respA->sCharState.sCharStateBase.vCurDir.x = plr->GetCharState()->sCharStateBase.vCurDir.x;
	respA->sCharState.sCharStateBase.vCurDir.y = plr->GetCharState()->sCharStateBase.vCurDir.y;
	respA->sCharState.sCharStateBase.vCurDir.z = plr->GetCharState()->sCharStateBase.vCurDir.z;

	CHTBSetTable *pHTBSetTbl = app->g_pTableContainer->GetHTBSetTable();
	sHTB_SET_TBLDAT *pHTBSetTblData = reinterpret_cast<sHTB_SET_TBLDAT*>(pHTBSetTbl->FindData(SkillID));

	respA->sCharState.sCharStateDetail.sCharStateHTB.hTarget = m_uiTargetSerialId;
	respA->sCharState.sCharStateDetail.sCharStateHTB.byStepCount = pHTBSetTblData->bySetCount;
	respA->sCharState.sCharStateDetail.sCharStateHTB.byCurStep = 0;
	respA->sCharState.sCharStateDetail.sCharStateHTB.byResultCount = 0;
	respA->sCharState.sCharStateDetail.sCharStateHTB.HTBId = pHTBSetTblData->tblidx;
	CSkillTable *pSkillTbl = app->g_pTableContainer->GetSkillTable();
	sSKILL_TBLDAT *pSkillTblData = reinterpret_cast<sSKILL_TBLDAT*>(pSkillTbl->FindData(SkillID));

	//Extract from Client Code
	RwInt8 byResultCount = 0;
	for (RwInt32 i = 0; i < pHTBSetTblData->bySetCount; ++i)
	{
		if (pHTBSetTblData->aHTBAction[i].skillTblidx != INVALID_TBLIDX)
		{
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].byStep = i;
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.byAttackResult = BATTLE_ATTACK_RESULT_HIT;
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.effectResult1.fResultValue = pSkillTblData->fSkill_Effect_Value[0];
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.effectResult2.fResultValue = pSkillTblData->fSkill_Effect_Value[1];
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.vShift.x = 0.0f;
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.vShift.y = 0.0f;
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.vShift.z = 0.0f;
			respA->sCharState.sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.byBlockedAction = 255;

			plr->GetCharState()->sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.effectResult1.fResultValue = pSkillTblData->fSkill_Effect_Value[0];
			plr->GetCharState()->sCharStateDetail.sCharStateHTB.aHTBSkillResult[byResultCount].sSkillResult.effectResult2.fResultValue = pSkillTblData->fSkill_Effect_Value[1];
			respA->sCharState.sCharStateDetail.sCharStateHTB.byResultCount++;

			byResultCount++;
		}
	}
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
}
//-----------------------------------------------------------//
//SendBagState Only Display RP Choice for PVP Luiz45  -------//
//-----------------------------------------------------------//
void CClientSession::SendHTBSendbagState(CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	//Extract from client codes
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE * respA = (sGU_UPDATE_CHAR_STATE*)packet.GetPacketData();
	respA->wOpCode = GU_UPDATE_CHAR_STATE;
	respA->handle = this->GetavatarHandle();
	respA->sCharState.sCharStateBase.byStateID = CHARSTATE_SANDBAG;
	respA->sCharState.sCharStateBase.vCurLoc.x = plr->GetCharState()->sCharStateBase.vCurLoc.x;
	respA->sCharState.sCharStateBase.vCurLoc.y = plr->GetCharState()->sCharStateBase.vCurLoc.y;
	respA->sCharState.sCharStateBase.vCurLoc.z = plr->GetCharState()->sCharStateBase.vCurLoc.z;
	respA->sCharState.sCharStateBase.vCurDir.x = plr->GetCharState()->sCharStateBase.vCurDir.x;
	respA->sCharState.sCharStateBase.vCurDir.y = plr->GetCharState()->sCharStateBase.vCurDir.y;
	respA->sCharState.sCharStateBase.vCurDir.z = plr->GetCharState()->sCharStateBase.vCurDir.z;

	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
}
//-----------------------------------------------------------//
//SendHTBRpBall Player vs Player HTB Choice   Luiz45  -------//
//-----------------------------------------------------------//
void CClientSession::SendHTBRpBall(CLkPacket * pPacket, CGameServer * app)
{
	sUG_HTB_RP_BALL_USE_REQ * req = (sUG_HTB_RP_BALL_USE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_HTB_RP_BALL_USE_RES));
	sGU_HTB_RP_BALL_USE_RES * res1 = (sGU_HTB_RP_BALL_USE_RES*)packet.GetPacketData();

	res1->byRpBallCount = req->byRpBallCount;
	res1->wOpCode = GU_HTB_RP_BALL_USE_RES;
	res1->wResultCode = GAME_SUCCESS;

	CLkPacket packet2(sizeof(sGU_HTB_RP_BALL_USED_NFY));
	sGU_HTB_RP_BALL_USED_NFY * res2 = (sGU_HTB_RP_BALL_USED_NFY*)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_HTB_RP_BALL_RESULT_DECIDED_NFY));
	sGU_HTB_RP_BALL_RESULT_DECIDED_NFY * res3 = (sGU_HTB_RP_BALL_RESULT_DECIDED_NFY*)packet3.GetPacketData();

	res2->byRpBallCount = req->byRpBallCount;
	res2->hSubject = this->GetavatarHandle();
	res2->wOpCode = GU_HTB_RP_BALL_USED_NFY;

	res3->hAttacker = this->GetavatarHandle();
	res3->hWinner = this->GetavatarHandle();
	res3->byTargetRpBallUsed = req->byRpBallCount;
	res3->wOpCode = GU_HTB_RP_BALL_RESULT_DECIDED_NFY;

	packet2.SetPacketLen(sizeof(sGU_HTB_RP_BALL_USED_NFY));
	packet3.SetPacketLen(sizeof(sGU_HTB_RP_BALL_RESULT_DECIDED_NFY));
	packet.SetPacketLen(sizeof(sGU_HTB_RP_BALL_USE_RES));
	g_pApp->Send(this->GetHandle(), &packet2);
	g_pApp->Send(this->GetHandle(), &packet3);
	g_pApp->Send(this->GetHandle(), &packet);

	app->UserBroadcastothers(&packet2, this);
	app->UserBroadcastothers(&packet3, this);
	app->UserBroadcastothers(&packet, this);

}
//-----------------------------------------------------------//
//-----------Advance Steps HTB? Luiz45  ---------------------//
//-----------------------------------------------------------//
void CClientSession::SendHTBFoward(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_HTB_FORWARD_RES));
	sGU_HTB_FORWARD_RES * res = (sGU_HTB_FORWARD_RES*)packet.GetPacketData();

	float newLP = 0;
	int stepNow = plr->GetCharState()->sCharStateDetail.sCharStateHTB.byStepCount;

	if (IsMonsterInsideList(m_uiTargetSerialId) == true)
	{
		/*MobActivity::CreatureData *lol = app->mob->GetMobByHandle(m_uiTargetSerialId);
		if (lol != NULL)
		{
		lol->FightMode = true;
		newLP = (float)lol->CurLP;
		newLP -= plr->GetCharState()->sCharStateDetail.sCharStateHTB.aHTBSkillResult[stepNow].sSkillResult.effectResult1.DD_DOT_fDamage + 100;
		printf("LP: %f, damage: %f\n", newLP, plr->GetCharState()->sCharStateDetail.sCharStateHTB.aHTBSkillResult[stepNow].sSkillResult.effectResult1.DD_DOT_fDamage + 100);
		if (newLP <= 0 || (newLP > lol->MaxLP))
		{
		lol->IsDead = true;
		CClientSession::SendMobLoot(&packet, app, m_uiTargetSerialId);
		this->gsf->printOk("DIE MOTHER FUCKER");
		SendCharUpdateFaintingState(&packet, app, this->GetavatarHandle(), m_uiTargetSerialId);
		}
		else if (newLP > 0 && lol->IsDead == false)
		{
		SendCharUpdateLp(&packet, app, newLP, m_uiTargetSerialId);
		}
		}*/
	}

	plr->GetCharState()->sCharStateDetail.sCharStateHTB.byStepCount++;
	res->wOpCode = GU_HTB_FORWARD_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_HTB_FORWARD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
	plr = NULL;
	delete plr;
}
//-----------------------------------------------------------//
//-----------GMT UPDATES        Luiz45  ---------------------//
//-----------------------------------------------------------//
void CClientSession::SendGmtUpdateReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_GMT_UPDATE_REQ * req = (sUG_GMT_UPDATE_REQ*)pPacket->GetPacketData();
	sGAME_MANIA_TIME * gmMania = (sGAME_MANIA_TIME*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_GMT_UPDATE_RES));
	sGU_GMT_UPDATE_RES * res = (sGU_GMT_UPDATE_RES*)packet.GetPacketData();
	res->wOpCode = GU_GMT_UPDATE_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_GMT_UPDATE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
}

void CClientSession::SendFogOfWarRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_WAR_FOG_UPDATE_REQ * req = (sUG_WAR_FOG_UPDATE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_WAR_FOG_UPDATE_RES));
	sGU_WAR_FOG_UPDATE_RES * res = (sGU_WAR_FOG_UPDATE_RES*)packet.GetPacketData();
	res->handle = req->hObject;
	res->wOpCode = GU_WAR_FOG_UPDATE_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_WAR_FOG_UPDATE_RES));

	g_pApp->Send(this->GetHandle(), &packet);

	sOBJECT_TBLDAT* pObjectDat = g_pObjectManager->GetObjectTbldatByHandle(req->hObject);
	app->db->prepare("INSERT INTO warfoginfo(hObject,owner_id) VALUES(?,?)");
	app->db->setInt(1, req->hObject);
	app->db->setInt(2, plr->GetCharID());
	app->db->execute();
	plr = NULL;
	delete plr;
}

void CClientSession::SendRideOnBusRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_RIDE_ON_BUS_REQ * req = (sUG_RIDE_ON_BUS_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sUG_RIDE_ON_BUS_REQ));
	sGU_RIDE_ON_BUS_RES * res = (sGU_RIDE_ON_BUS_RES *)packet.GetPacketData();

	res->hTarget = req->hTarget;
	res->wOpCode = GU_RIDE_ON_BUS_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_RIDE_ON_BUS_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}

void CClientSession::SendRideOffBusRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_RIDE_OFF_BUS_REQ * req = (sUG_RIDE_OFF_BUS_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sUG_RIDE_OFF_BUS_REQ));
	sGU_RIDE_OFF_BUS_RES * res = (sGU_RIDE_OFF_BUS_RES *)packet.GetPacketData();

	res->wOpCode = GU_RIDE_OFF_BUS_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_RIDE_OFF_BUS_RES));;
	g_pApp->Send(this->GetHandle(), &packet);
}

void CClientSession::SendCharDashKeyBoard(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_CHAR_DASH_KEYBOARD * req = (sUG_CHAR_DASH_KEYBOARD*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_EP));
	sGU_UPDATE_CHAR_EP * res = (sGU_UPDATE_CHAR_EP*)packet.GetPacketData();

	//Response
	plr->GetPcProfile()->wCurEP = (plr->GetPcProfile()->wCurEP - 50);
	plr->GetCharState()->sCharStateBase.vCurDir.x = req->vCurDir.x;
	plr->GetCharState()->sCharStateBase.vCurDir.z = req->vCurDir.z;
	plr->GetCharState()->sCharStateBase.vCurDir.y = req->vCurLoc.y;
	plr->GetCharState()->sCharStateBase.vCurLoc.x = req->vCurLoc.x;
	plr->GetCharState()->sCharStateBase.vCurLoc.z = req->vCurLoc.z;
	plr->GetCharState()->sCharStateBase.vCurLoc.y = req->vCurLoc.y;
	res->handle = this->GetavatarHandle();
	res->wOpCode = GU_UPDATE_CHAR_EP;
	res->wCurEP = plr->GetPcProfile()->wCurEP;//Or some number to get right xD
	res->wMaxEP = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP;

	//this->plr->SavePlayerData();//Always save after update ? or no?

	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EP));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;

}
void CClientSession::SendCharDashMouse(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_CHAR_DASH_MOUSE * req = (sUG_CHAR_DASH_MOUSE*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_UPDATE_CHAR_EP));
	sGU_UPDATE_CHAR_EP * res = (sGU_UPDATE_CHAR_EP*)packet.GetPacketData();

	//Response
	plr->GetPcProfile()->wCurEP = (plr->GetPcProfile()->wCurEP - 50);
	plr->GetCharState()->sCharStateBase.vCurLoc.x = req->vDestLoc.x;
	plr->GetCharState()->sCharStateBase.vCurLoc.z = req->vDestLoc.z;
	plr->GetCharState()->sCharStateBase.vCurLoc.y = req->vDestLoc.y;
	res->handle = this->GetavatarHandle();
	res->wOpCode = GU_UPDATE_CHAR_EP;
	res->wCurEP = plr->GetPcProfile()->wCurEP;//Or some number to get right xD
	res->wMaxEP = plr->GetPcProfile()->avatarAttribute.wBaseMaxEP;

	//this->plr->SavePlayerData();//Always save after update ? or no?

	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EP));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
void CClientSession::SendBusLocation(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_BUS_LOCATION_NFY));
	sGU_BUS_LOCATION_NFY * res = (sGU_BUS_LOCATION_NFY *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CNPCTable* busNpc = app->g_pTableContainer->GetNpcTable();
	CSpawnTable* pSpawnTbl = app->g_pTableContainer->GetNpcSpawnTable(plr->GetWorldID());
	CObjectTable* objTbl = app->g_pTableContainer->GetObjectTable(plr->GetWorldID());
	/*for (CTable::TABLEIT npcTbl = busNpc->Begin(); npcTbl != busNpc->End(); ++npcTbl)
	{
	sNPC_TBLDAT* pBusTblDat = (sNPC_TBLDAT*)npcTbl->second;
	if (pBusTblDat)
	{
	if ((pBusTblDat->byJob == NPC_JOB_BUS) && ((strcmp(pBusTblDat->szModel, "N_BUS_A1") == 0) ||
	(strcmp(pBusTblDat->szModel, "N_BUS_C1") == 0) || (strcmp(pBusTblDat->szModel, "N_BUS_B1") == 0)))
	{

	res->busTblidx = pBusTblDat->tblidx;
	res->vCurDir.x = this->plr->GetDirection().x;
	res->vCurDir.z = this->plr->GetDirection().z;
	res->vCurLoc.x = this->plr->GetPosition().x;
	res->vCurLoc.z = this->plr->GetPosition().z;
	res->hSubject = AcquireSerialId();
	res->wOpCode = GU_BUS_LOCATION_NFY;
	packet.SetPacketLen(sizeof(sGU_BUS_LOCATION_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	}
	}
	}*/
	res->busTblidx;
	res->hSubject;
	res->vCurDir;
	res->vCurLoc;
	res->wOpCode;
}
//----------------------------------//
//--ObjectVisit Luiz45
//----------------------------------//
void CClientSession::SendObjectVisitQuest(CLkPacket * pPacket, CGameServer * app)
{
	sUG_QUEST_OBJECT_VISIT_REQ * req = (sUG_QUEST_OBJECT_VISIT_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_QUEST_OBJECT_VISIT_RES));
	sGU_QUEST_OBJECT_VISIT_RES* res = (sGU_QUEST_OBJECT_VISIT_RES*)packet.GetPacketData();

	res->byObjType = req->byObjType;
	res->objectTblidx = req->objectTblidx;
	res->qId = req->qId;
	res->worldId = req->worldId;
	res->wOpCode = GU_QUEST_OBJECT_VISIT_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_QUEST_OBJECT_VISIT_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--TSUpdateState Luiz45
//----------------------------------//
void CClientSession::SendTSUpdateState(CLkPacket * pPacket, CGameServer * app)
{
	printf("Sending TS_UPDATE_STATE");
	sUG_TS_UPDATE_STATE* req = (sUG_TS_UPDATE_STATE*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_TS_UPDATE_STATE));
	sGU_TS_UPDATE_STATE* res = (sGU_TS_UPDATE_STATE*)packet.GetPacketData();
	res->byTsType = req->byTsType;
	res->byType = req->byType;
	res->wTSState = req->wTSState;
	res->tId = req->tId;
	res->dwParam = req->dwParam;
	res->wOpCode = GU_TS_UPDATE_STATE;
	packet.SetPacketLen(sizeof(sGU_TS_UPDATE_STATE));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--Sending TimeQuestList Luiz45
//----------------------------------//
void CClientSession::SendTimeQuestList(CLkPacket * pPacket, CGameServer * app)
{
	printf("Sending Time Quest List\n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TIMEQUEST_ROOM_LIST_RES));
	sGU_TIMEQUEST_ROOM_LIST_RES* res = (sGU_TIMEQUEST_ROOM_LIST_RES*)packet.GetPacketData();
	CTimeQuestTable* pTmqTable = app->g_pTableContainer->GetTimeQuestTable();
	res->sTMQInfo.byDifficult = this->gsf->GetTmqLevel(plr);
	res->sTMQInfo.tmqTblidx = this->gsf->GetTmq(plr);
	res->sTMQInfo.wWaitIndividualCount = 1;
	res->sTMQInfo.wWaitPartyCount = 1;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TIMEQUEST_ROOM_LIST_RES;
	packet.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_LIST_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//----------------------------------//
//--Leaving Out TMQ Luiz45
//----------------------------------//
void CClientSession::LeaveTimeQuestRoom(CLkPacket * pPacket, CGameServer * app)
{
	printf("Leaving Out TMQ Room\n");
	CLkPacket packet2(sizeof(sGU_TIMEQUEST_ROOM_LEAVE_NFY));
	sGU_TIMEQUEST_ROOM_LEAVE_NFY* res2 = (sGU_TIMEQUEST_ROOM_LEAVE_NFY*)packet2.GetPacketData();

	CLkPacket packet(sizeof(sGU_TIMEQUEST_ROOM_LEAVE_RES));
	sGU_TIMEQUEST_ROOM_LEAVE_RES* res = (sGU_TIMEQUEST_ROOM_LEAVE_RES*)packet.GetPacketData();

	res->wOpCode = GU_TIMEQUEST_ROOM_LEAVE_RES;
	res2->wOpCode = GU_TIMEQUEST_ROOM_LEAVE_NFY;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_LEAVE_RES));
	packet2.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_LEAVE_NFY));

	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet2, this);
}
//----------------------------------//
//--Join TMQ Luiz45
//----------------------------------//
void CClientSession::JoinTimeQuestRoom(CLkPacket * pPacket, CGameServer * app)
{
	printf("Joining...\n");
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_TIMEQUEST_ROOM_JOIN_REQ* req = (sUG_TIMEQUEST_ROOM_JOIN_REQ*)pPacket->GetPacketData();

	//Packets definition
	CLkPacket packet(sizeof(sGU_TIMEQUEST_ROOM_JOIN_RES));
	sGU_TIMEQUEST_ROOM_JOIN_RES* res = (sGU_TIMEQUEST_ROOM_JOIN_RES*)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_TIMEQUEST_ROOM_JOIN_NFY));
	sGU_TIMEQUEST_ROOM_JOIN_NFY* res2 = (sGU_TIMEQUEST_ROOM_JOIN_NFY*)packet2.GetPacketData();

	CLkPacket packet3(sizeof(sGU_TIMEQUEST_ROOM_SELECTION_NFY));
	sGU_TIMEQUEST_ROOM_SELECTION_NFY* res3 = (sGU_TIMEQUEST_ROOM_SELECTION_NFY*)packet3.GetPacketData();

	//Preparing Data
	res->hTroubleMember = this->GetavatarHandle();
	res->sJoinInfo.tmqTblidx = this->gsf->GetTmq(plr);
	res->sJoinInfo.byDifficult = this->gsf->GetTmqLevel(plr);
	res->sJoinInfo.byRoomState = TIMEQUEST_ROOM_STATE_REGISTRAITION;
	res->sJoinInfo.dwRemainTime = 60000;
	res->sJoinInfo.byTimeQuestMode = req->byTimeQuestMode;
	res->wOpCode = GU_TIMEQUEST_ROOM_JOIN_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->sJoinInfo.tmqTblidx = res->sJoinInfo.tmqTblidx;
	res2->sJoinInfo.byDifficult = this->gsf->GetTmqLevel(plr);
	res2->sJoinInfo.byRoomState = TIMEQUEST_ROOM_STATE_WAITENTRY;
	res2->sJoinInfo.byTimeQuestMode = req->byTimeQuestMode;
	res2->sJoinInfo.dwRemainTime = 60000;
	res2->wOpCode = GU_TIMEQUEST_ROOM_JOIN_NFY;

	res3->bIsSecondWinner = true;
	res3->uSelectionInfo.sEntryInfo.bHaveItem = true;
	res3->uSelectionInfo.sEntryInfo.dwReaminEntryTime = 60000;
	res3->uSelectionInfo.sNextTmqInfo.tmqTblidx = this->gsf->GetTmq(plr);
	res3->wOpCode = GU_TIMEQUEST_ROOM_SELECTION_NFY;

	//Finishing
	packet.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_JOIN_RES));
	packet2.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_JOIN_NFY));
	packet3.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_SELECTION_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet3);
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet3, this);
	app->UserBroadcastothers(&packet2, this);
	plr = NULL;
	delete plr;
}
//----------------------------------//
//--Teleport to TMQ Luiz45
//----------------------------------//
void CClientSession::SendTimeQuestTeleport(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TIMEQUEST_ROOM_TELEPORT_REQ* req = (sUG_TIMEQUEST_ROOM_TELEPORT_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_TIMEQUEST_ROOM_TELEPORT_RES));
	sGU_TIMEQUEST_ROOM_TELEPORT_RES* res = (sGU_TIMEQUEST_ROOM_TELEPORT_RES*)packet.GetPacketData();

	res->wOpCode = GU_TIMEQUEST_ROOM_TELEPORT_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_TIMEQUEST_ROOM_TELEPORT_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--Party Dungeon Init/Cancel Luiz45
//----------------------------------//
void CClientSession::SendInitPartyDungeon(CLkPacket * pPacket, CGameServer * app)
{
	sUG_PARTY_DUNGEON_INIT_REQ* req = (sUG_PARTY_DUNGEON_INIT_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PARTY_DUNGEON_INIT_RES));
	sGU_PARTY_DUNGEON_INIT_RES* res = (sGU_PARTY_DUNGEON_INIT_RES*)packet.GetPacketData();

	res->wOpCode = GU_PARTY_DUNGEON_INIT_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_PARTY_DUNGEON_INIT_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--Party Dungeon Difficult Luiz45
//----------------------------------//
void CClientSession::SendPartyChangeDiff(CLkPacket * pPacket, CGameServer * app)
{
	sUG_PARTY_DIFF_CHANGE_REQ* req = (sUG_PARTY_DIFF_CHANGE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PARTY_DUNGEON_DIFF_RES));
	sGU_PARTY_DUNGEON_DIFF_RES* res = (sGU_PARTY_DUNGEON_DIFF_RES*)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_PARTY_DUNGEON_DIFF_NFY));
	sGU_PARTY_DUNGEON_DIFF_NFY* res2 = (sGU_PARTY_DUNGEON_DIFF_NFY*)packet2.GetPacketData();

	res->eDiff = req->eDiff;
	res->wOpCode = GU_PARTY_DUNGEON_DIFF_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->eDiff = req->eDiff;
	res2->wOpCode = GU_PARTY_DUNGEON_DIFF_NFY;

	packet.SetPacketLen(sizeof(sGU_PARTY_DUNGEON_DIFF_RES));
	packet2.SetPacketLen(sizeof(sGU_PARTY_DUNGEON_DIFF_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
}
//----------------------------------//
//--Party Changing Zenny Method Luiz45
//----------------------------------//
void CClientSession::SendPartyChangeZenny(CLkPacket * pPacket, CGameServer * app)
{
	sUG_PARTY_CHANGE_ZENNY_LOOTING_METHOD_REQ* req = (sUG_PARTY_CHANGE_ZENNY_LOOTING_METHOD_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PARTY_CHANGE_ZENNY_LOOTING_METHOD_RES));
	sGU_PARTY_CHANGE_ZENNY_LOOTING_METHOD_RES* res = (sGU_PARTY_CHANGE_ZENNY_LOOTING_METHOD_RES*)packet.GetPacketData();

	res->byNewLootingMethod = req->byLootingMethod;
	res->wOpCode = GU_PARTY_CHANGE_ZENNY_LOOTING_METHOD_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_PARTY_CHANGE_ZENNY_LOOTING_METHOD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--Party Changing Items Method Luiz45
//----------------------------------//
void CClientSession::SendPartyChangeItem(CLkPacket * pPacket, CGameServer * app)
{
	sUG_PARTY_CHANGE_ITEM_LOOTING_METHOD_REQ* req = (sUG_PARTY_CHANGE_ITEM_LOOTING_METHOD_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PARTY_CHANGE_ITEM_LOOTING_METHOD_RES));
	sGU_PARTY_CHANGE_ITEM_LOOTING_METHOD_RES* res = (sGU_PARTY_CHANGE_ITEM_LOOTING_METHOD_RES*)packet.GetPacketData();

	res->byNewLootingMethod = req->byLootingMethod;
	res->wOpCode = GU_PARTY_CHANGE_ITEM_LOOTING_METHOD_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_PARTY_CHANGE_ITEM_LOOTING_METHOD_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--NetPY ShopItem Method Luiz45
//----------------------------------//
void CClientSession::SendNetPyStart(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_SHOP_NETPYITEM_START_RES));
	sGU_SHOP_NETPYITEM_START_RES* res = (sGU_SHOP_NETPYITEM_START_RES*)packet.GetPacketData();

	res->byType = 0;
	res->wOpCode = GU_SHOP_NETPYITEM_START_RES;
	res->wResultCode = GAME_SUCCESS;
	//packet.SetPacketLen(sizeof(sGU_SHOP_NETPYITEM_START_RES));
	//g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--SendWarFogInfo Method Luiz45
//----------------------------------//
void CClientSession::SendAvatarWarFogInfo(CLkPacket * pPacket, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_WAR_FOG_INFO));
	sGU_WAR_FOG_INFO* res = (sGU_WAR_FOG_INFO*)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	app->db->prepare("SELECT * FROM warfoginfo WHERE owner_id = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();
	while (app->db->fetch())
	{

		sOBJECT_TBLDAT* pOBJECT_TBLDAT = g_pObjectManager->GetObjectTbldatByHandle(app->db->getInt("hObject"));
		if (pOBJECT_TBLDAT)
		{
			plr->SetPlayerWarFog(pOBJECT_TBLDAT->contentsTblidx);
		}
	}
	if (app->db->rowsCount()!=0)
		memcpy(res->abyWarFogInfo, plr->GetWarFog(), LK_MAX_SIZE_WAR_FOG);
	res->wOpCode = GU_WAR_FOG_INFO;

	packet.SetPacketLen(sizeof(sGU_WAR_FOG_INFO));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//----------------------------------//
//--HoiPoi Job Method Luiz45
//----------------------------------//
void CClientSession::SendHoiPoiJob(CLkPacket * pPacket, CGameServer * app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_HOIPOIMIX_JOB_SET_REQ* req = (sUG_HOIPOIMIX_JOB_SET_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_HOIPOIMIX_JOB_SET_RES));
	sGU_HOIPOIMIX_JOB_SET_RES* res = (sGU_HOIPOIMIX_JOB_SET_RES*)packet.GetPacketData();
	app->db->prepare("SELECT * FROM warfoginfo WHERE owner_id = ?");
	app->db->setInt(1, plr->GetCharID());
	app->db->execute();

	res->byRecipeType = req->byRecipeType;
	res->hNpchandle = req->hNpchandle;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_HOIPOIMIX_JOB_SET_RES;

	packet.SetPacketLen(sizeof(sGU_HOIPOIMIX_JOB_SET_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	plr = NULL;
	delete plr;
}
//----------------------------------//
//--HoiPoi Job Reset Method Luiz45
//----------------------------------//
void CClientSession::SendHoiPoiJobReset(CLkPacket * pPacket, CGameServer * app)
{
	sUG_HOIPOIMIX_JOB_RESET_REQ* req = (sUG_HOIPOIMIX_JOB_RESET_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_HOIPOIMIX_JOB_SET_RES));
	sGU_HOIPOIMIX_JOB_RESET_RES* res = (sGU_HOIPOIMIX_JOB_RESET_RES*)packet.GetPacketData();

	res->byRecipeType = req->byRecipeType;
	res->hNpchandle = req->hNpchandle;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_HOIPOIMIX_JOB_RESET_RES;

	packet.SetPacketLen(sizeof(sGU_HOIPOIMIX_JOB_RESET_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--Tutorial play Method Luiz45
//----------------------------------//
void CClientSession::SendDirectPlay(CLkPacket *pPacket, CGameServer * app)
{
	sUG_CHAR_DIRECT_PLAY_ACK* req = (sUG_CHAR_DIRECT_PLAY_ACK*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_DIRECT_PLAY));
	sGU_CHAR_DIRECT_PLAY* res = (sGU_CHAR_DIRECT_PLAY*)packet.GetPacketData();

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CDirectionLinkTable* pLinkTbl = app->g_pTableContainer->GetDirectionLinkTable();
	sDIRECTION_LINK_TBLDAT *pLinkTblData = reinterpret_cast<sDIRECTION_LINK_TBLDAT*>(pLinkTbl->FindData(1000));

	if (plr->IsInTutorial())
	{
		res->bSynchronize = true;
		res->byPlayMode = 1;
		res->directTblidx = 1000;
		res->hSubject = this->GetavatarHandle();
		res->wOpCode = GU_CHAR_DIRECT_PLAY;

		packet.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY));
		g_pApp->Send(this->GetHandle(), &packet);

		Sleep(7000);
		CLkPacket packetEnd(sizeof(sGU_CHAR_DIRECT_PLAY_END));
		sGU_CHAR_DIRECT_PLAY_END* res4 = (sGU_CHAR_DIRECT_PLAY_END*)packetEnd.GetPacketData();
		res4->wOpCode = GU_CHAR_DIRECT_PLAY_END;
		packetEnd.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY_END));
		g_pApp->Send(this->GetHandle(), &packetEnd);
	}
	else
	{
		this->tlqManager->SendDirectPlayAckForTLQ();
	}
	plr = NULL;
	delete plr;
}
//----------------------------------//
//--Tutorial play Quit Method Luiz45
//----------------------------------//
void CClientSession::SendTutorialPlayQuit(CLkPacket *pPacket, CGameServer * app)
{
	sUG_TUTORIAL_PLAY_QUIT_REQ* req = (sUG_TUTORIAL_PLAY_QUIT_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_TUTORIAL_PLAY_QUIT_RES));
	sGU_TUTORIAL_PLAY_QUIT_RES* res = (sGU_TUTORIAL_PLAY_QUIT_RES*)packet.GetPacketData();

	res->wOpCode = GU_TUTORIAL_PLAY_QUIT_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_TUTORIAL_PLAY_QUIT_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//----------------------------------//
//--PrivateShop Create Method Luiz45
//----------------------------------//
void CClientSession::SendPrivateShopCreate(CLkPacket *pPacket, CGameServer * app)
{
	sUG_PRIVATESHOP_CREATE_REQ* req = (sUG_PRIVATESHOP_CREATE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PRIVATESHOP_CREATE_RES));
	sGU_PRIVATESHOP_CREATE_RES* res1 = (sGU_PRIVATESHOP_CREATE_RES*)packet.GetPacketData();

	CLkPacket packet2(sizeof(sGU_PRIVATESHOP_CREATE_NFY));
	sGU_PRIVATESHOP_CREATE_NFY* res2 = (sGU_PRIVATESHOP_CREATE_NFY*)packet2.GetPacketData();

	res1->sPrivateShopData.hOwner = this->GetavatarHandle();
	res1->wOpCode = GU_PRIVATESHOP_CREATE_RES;
	res1->wResultCode = GAME_SUCCESS;

	res2->hOwner = this->GetavatarHandle();
	res2->wOpCode = GU_PRIVATESHOP_CREATE_NFY;

	packet.SetPacketLen(sizeof(sGU_PRIVATESHOP_CREATE_RES));
	packet2.SetPacketLen(sizeof(sGU_PRIVATESHOP_CREATE_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
}
//----------------------------------//
//--PrivateShop Exit Method Luiz45
//----------------------------------//
void CClientSession::SendPrivateShopExit(CLkPacket *pPacket, CGameServer * app)
{
	sUG_PRIVATESHOP_EXIT_REQ* req = (sUG_PRIVATESHOP_EXIT_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_PRIVATESHOP_EXIT_RES));
	sGU_PRIVATESHOP_EXIT_RES* res1 = (sGU_PRIVATESHOP_EXIT_RES*)packet.GetPacketData();

	res1->wOpCode = GU_PRIVATESHOP_EXIT_RES;
	res1->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_PRIVATESHOP_EXIT_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//---------------------------------------------
//---Send Notice Luiz45 -- Improved by Kalisto
//---------------------------------------------
void CClientSession::SendServerAnnouncement(wstring wsMsg, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	sGU_SYSTEM_DISPLAY_TEXT* sNotice = (sGU_SYSTEM_DISPLAY_TEXT*)packet.GetPacketData();

	WCHAR wcsMsg[BUDOKAI_MAX_NOTICE_LENGTH + 1];
	char ch[256];
	::ZeroMemory(ch, sizeof(ch));
	char DefChar = '\0';
	WideCharToMultiByte(CP_ACP, 0, wsMsg.c_str(), -1, ch, 256, &DefChar, NULL);
	mbstowcs(wcsMsg, ch, 255);

	sNotice->wOpCode = GU_SYSTEM_DISPLAY_TEXT;
	sNotice->byDisplayType = 1;
	wcscpy_s(sNotice->awchMessage, BUDOKAI_MAX_NOTICE_LENGTH + 1, wcsMsg);
	wcscpy_s(sNotice->awGMChar, LK_MAX_SIZE_CHAR_NAME_UNICODE, L"AKCore Admin");
	sNotice->wMessageLengthInUnicode = (WORD)wcslen(wcsMsg);
	packet.SetPacketLen(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	app->UserBroadcast(&packet);
}
//---------------------------------------------
//---Send Notice by Kalisto
//---------------------------------------------
void CClientSession::SendServerBroadcast(wstring wsMsg, CGameServer * app)
{
	CLkPacket packet(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	sGU_SYSTEM_DISPLAY_TEXT* sNotice = (sGU_SYSTEM_DISPLAY_TEXT*)packet.GetPacketData();

	WCHAR wcsMsg[BUDOKAI_MAX_NOTICE_LENGTH + 1];
	char ch[256];
	::ZeroMemory(ch, sizeof(ch));
	char DefChar = '\0';
	WideCharToMultiByte(CP_ACP, 0, wsMsg.c_str(), -1, ch, 256, &DefChar, NULL);
	mbstowcs(wcsMsg, ch, 255);

	sNotice->wOpCode = GU_SYSTEM_DISPLAY_TEXT;
	sNotice->byDisplayType = 3;
	wcscpy_s(sNotice->awchMessage, BUDOKAI_MAX_NOTICE_LENGTH + 1, wcsMsg);
	wcscpy_s(sNotice->awGMChar, LK_MAX_SIZE_CHAR_NAME_UNICODE, L"AKCore Admin");
	sNotice->wMessageLengthInUnicode = (WORD)wcslen(wcsMsg);
	packet.SetPacketLen(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
	app->UserBroadcast(&packet);
}
void CClientSession::CreateItemById(uint32_t tblidx, int playerId)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* pSession = g_pPlayerManager->GetPlayerByID(playerId);

	CItemTable* pMyItemTbl = app->g_pTableContainer->GetItemTable();
	sITEM_TBLDAT* item = reinterpret_cast<sITEM_TBLDAT*>(pMyItemTbl->FindData(tblidx));
	if (item != NULL)
	{
		GsFunctionsClass* gsFunction = new GsFunctionsClass();//I dont believe that leave this way because the other way is wrong
		gsFunction->CreateUpdateItem(pSession, 1, tblidx, false, pSession->GetSession());
		gsFunction = NULL;
		delete gsFunction;
	}
	else
		cout << "No Such ItemID" << endl;
}

void  CClientSession::CreateMonsterById(unsigned int uiMobId, int playerId)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* pSession = g_pPlayerManager->GetPlayerByID(playerId);
	sVECTOR3 curpos = pSession->GetPlayerPosition();
	CMobTable* pMyMobTable = app->g_pTableContainer->GetMobTable();
	sMOB_TBLDAT* mob = reinterpret_cast<sMOB_TBLDAT*>(pMyMobTable->FindData(uiMobId));
	if (mob != NULL)
	{
		CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
		sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();

		res->wOpCode = GU_OBJECT_CREATE;
		res->sObjectInfo.objType = OBJTYPE_MOB;
		res->handle = AcquireSerialId();//app->mob->AcquireMOBSerialId() this will get your Player Handle,need change "AcquireSerialId" because here is used to generate a Handler for the players! #Issue 6 Luiz45
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = curpos.x;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = curpos.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = curpos.z;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = curpos.x + rand() % 360;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = curpos.y;
		res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = curpos.z;
		res->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
		res->sObjectInfo.mobState.sCharStateBase.bFightMode = false;
		res->sObjectInfo.mobBrief.tblidx = uiMobId;
		res->sObjectInfo.mobBrief.wCurEP = mob->wBasic_EP;
		res->sObjectInfo.mobBrief.wMaxEP = mob->wBasic_EP;
		res->sObjectInfo.mobBrief.wCurLP = mob->wBasic_LP;
		res->sObjectInfo.mobBrief.wMaxLP = mob->wBasic_LP;
		res->sObjectInfo.mobBrief.fLastRunningSpeed = mob->fRun_Speed;
		res->sObjectInfo.mobBrief.fLastWalkingSpeed = mob->fWalk_Speed;
		pSession->myCCSession->InsertIntoMyMonsterList(res->handle, curpos, uiMobId);

		packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
		g_pApp->Send(pSession->GetSession(), &packet);
	}
	else
		cout << "No Mob Exists with that tblidx" << endl;
}


void CClientSession::AddSkillById(uint32_t tblidx, int playerId)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* pSession = g_pPlayerManager->GetPlayerByID(playerId);
	CSkillTable* pSkillTable = app->g_pTableContainer->GetSkillTable();
	int iSkillCount = pSession->myCCSession->gsf->GetTotalSlotSkill(playerId);//you are still using "this" but "this" is null this is why you get the error #Issue 6

	sSKILL_TBLDAT* pSkillData = (sSKILL_TBLDAT*)pSkillTable->FindData(tblidx);
	if (pSkillData != NULL)
	{
		if (app->qry->CheckIfSkillAlreadyLearned(pSkillData->tblidx, playerId) == false)
		{
			CLkPacket packet2(sizeof(sGU_SKILL_LEARNED_NFY));
			sGU_SKILL_LEARNED_NFY * res2 = (sGU_SKILL_LEARNED_NFY *)packet2.GetPacketData();
			iSkillCount++;//don't send this directly if you learned SSJ first...because we always are sending 1...nevermind :P
			res2->wOpCode = GU_SKILL_LEARNED_NFY;
			res2->skillId = pSkillData->tblidx;
			res2->bySlot = iSkillCount;

			app->qry->InsertNewSkill(pSkillData->tblidx, playerId, iSkillCount, pSkillData->wKeep_Time, pSkillData->wNext_Skill_Train_Exp);
			packet2.SetPacketLen(sizeof(sGU_SKILL_LEARNED_NFY));
			g_pApp->Send(pSession->GetSession(), &packet2);

			CLkPacket packet(sizeof(sGU_SKILL_LEARN_RES));
			sGU_SKILL_LEARN_RES * res = (sGU_SKILL_LEARN_RES *)packet.GetPacketData();

			res->wOpCode = GU_SKILL_LEARN_RES;
			res->wResultCode = 500;

			packet.SetPacketLen(sizeof(sGU_SKILL_LEARN_RES));
			g_pApp->Send(pSession->GetSession(), &packet);

		}
		else
			cout << "Player already knows skill" << endl;
	}
	else
		cout << "No skill with that ID exists" << endl;
}


//Trade Start Ress - Luiz45
void CClientSession::SendTradeStartRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_START_REQ* req = (sUG_TRADE_START_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_OK_REQ));
	CLkPacket packet2(sizeof(sGU_TRADE_START_RES));

	sGU_TRADE_OK_REQ* res = (sGU_TRADE_OK_REQ*)packet.GetPacketData();
	sGU_TRADE_START_RES* res1 = (sGU_TRADE_START_RES*)packet2.GetPacketData();

	res->handle = plr->GetAvatarHandle();
	res->wOpCode = GU_TRADE_OK_REQ;

	res1->byEmptyInven = 12;//Here you can put how many free slots are available to trade i've placed the MAX
	res1->handle = plr->GetAvatarHandle();
	res1->hTarget = req->hTarget;
	res1->wResultCode = GAME_TRADE_REPLY_WAIT_OVER;
	res1->wOpCode = GU_TRADE_START_RES;

	packet.SetPacketLen(sizeof(sGU_TRADE_OK_REQ));
	packet2.SetPacketLen(sizeof(sGU_TRADE_START_RES));

	//Getting User Session to trade
	PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);

	if (PlayerTrade->GetPlayerIsInTrade())
	{
		res1->wResultCode = GAME_TRADE_ALREADY_OPEN;
		g_pApp->Send(this->GetHandle(), &packet2);
	}
	else{
		g_pApp->Send(this->GetHandle(), &packet2);
		g_pApp->Send(PlayerTrade->GetSession(), &packet);
	}
	plr = NULL;
	delete plr;
}
//Trade Finish Res - Luiz45
void CClientSession::SendTradeFinish(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_END_REQ* req = (sUG_TRADE_END_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_END_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_END_NFY));
	sGU_TRADE_END_RES* res = (sGU_TRADE_END_RES*)packet.GetPacketData();
	sGU_TRADE_END_NFY* res2 = (sGU_TRADE_END_NFY*)packet2.GetPacketData();

	if (req->bIsSet = true)
	{
		plr->SetTradeOK(true);
		PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);
		res->bIsSet = true;
		res->hTarget = plr->GetAvatarHandle();
		res->dwPacketCount = req->dwPacketCount;
		res->wResultCode = GAME_SUCCESS;
		res->wOpCode = GU_TRADE_END_RES;

		res2->bIsSet = true;
		res2->hTarget = PlayerTrade->GetAvatarHandle();
		res2->wOpCode = GU_TRADE_END_NFY;
		res2->wResultCode = GAME_SUCCESS;

		packet2.SetPacketLen(sizeof(sGU_TRADE_END_NFY));
		packet.SetPacketLen(sizeof(sGU_TRADE_END_RES));
		g_pApp->Send(this->GetHandle(), &packet);
		g_pApp->Send(PlayerTrade->GetSession(), &packet2);

		if (PlayerTrade->GetTradeOK() == true)
		{
			plr->SetTradeOK(false);
			plr->SetPlayerIsInTrade(false);
			PlayerTrade->SetTradeOK(false);
			PlayerTrade->SetPlayerIsInTrade(false);
			//Trading items guys !! =D need fix delete when you wanna trade the max Amount for some item
			if (plr->cPlayerInventory->GetTotalItemsTrade() != 0)
			{
				for (int i = 0; i < plr->cPlayerInventory->GetTotalItemsTrade(); i++)
				{
					this->gsf->CreateUpdateItem(PlayerTrade, plr->cPlayerInventory->GetItemsToTrade()[i].byStackcount, plr->cPlayerInventory->GetItemsToTrade()[i].itemNo, false, PlayerTrade->GetSession());
					this->gsf->CreateUpdateItem(plr, plr->cPlayerInventory->GetItemsToTrade()[i].byStackcount, plr->cPlayerInventory->GetItemsToTrade()[i].itemNo, true, this->GetHandle(), plr->cPlayerInventory->GetItemsToTrade()[i].itemId);
					PlayerTrade->cPlayerInventory->AddItemToInventory(plr->cPlayerInventory->GetItemsToTrade()[i]);//Add in Player Inventory
					plr->cPlayerInventory->RemoveItemFromInventory(plr->cPlayerInventory->GetItemsToTrade()[i].itemId);//Remove for User Inventory
					plr->cPlayerInventory->RemoveUpdateTrade(plr->cPlayerInventory->GetItemsToTrade()[i].itemId, true);//Remove from the list
				}
			}
			if (PlayerTrade->cPlayerInventory->GetTotalItemsTrade() != 0)
			{
				for (int i = 0; i < PlayerTrade->cPlayerInventory->GetTotalItemsTrade(); i++)
				{
					this->gsf->CreateUpdateItem(plr, PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].byStackcount, PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].itemNo, false, this->GetHandle());
					this->gsf->CreateUpdateItem(PlayerTrade, PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].byStackcount, PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].itemNo, true, PlayerTrade->GetSession(), PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].itemId);
					plr->cPlayerInventory->AddItemToInventory(PlayerTrade->cPlayerInventory->GetItemsToTrade()[i]);//Add in Player Inventory
					PlayerTrade->cPlayerInventory->RemoveItemFromInventory(PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].itemId);//Remove for User Inventory
					PlayerTrade->cPlayerInventory->RemoveUpdateTrade(PlayerTrade->cPlayerInventory->GetItemsToTrade()[i].itemId, true);//Remove from the list
				}
			}
			if (plr->cPlayerInventory->GetZennyToTrade() != 0)
			{
				this->gsf->UpdateCharMoney(NULL, PlayerTrade->myCCSession, ZENNY_CHANGE_TYPE_TRADE, (PlayerTrade->GetPcProfile()->dwZenny += plr->cPlayerInventory->GetZennyToTrade()), PlayerTrade->GetAvatarHandle());
				this->gsf->UpdateCharMoney(NULL, this, ZENNY_CHANGE_TYPE_TRADE, (plr->GetPcProfile()->dwZenny -= plr->cPlayerInventory->GetZennyToTrade()), plr->GetAvatarHandle());
				plr->cPlayerInventory->SetAmountZennyToTrade(0);
				app->qry->SetBankMoney(PlayerTrade->GetCharID(), PlayerTrade->GetPcProfile()->dwZenny);
				app->qry->SetBankMoney(plr->GetCharID(), plr->GetPcProfile()->dwZenny);
			}
			if (PlayerTrade->cPlayerInventory->GetZennyToTrade() != 0)
			{
				this->gsf->UpdateCharMoney(NULL, this, ZENNY_CHANGE_TYPE_TRADE, (plr->GetPcProfile()->dwZenny += PlayerTrade->cPlayerInventory->GetZennyToTrade()), plr->GetAvatarHandle());
				this->gsf->UpdateCharMoney(NULL, PlayerTrade->myCCSession, ZENNY_CHANGE_TYPE_TRADE, (PlayerTrade->GetPcProfile()->dwZenny -= PlayerTrade->cPlayerInventory->GetZennyToTrade()), PlayerTrade->GetAvatarHandle());
				PlayerTrade->cPlayerInventory->SetAmountZennyToTrade(0);
				app->qry->SetBankMoney(PlayerTrade->GetCharID(), PlayerTrade->GetPcProfile()->dwZenny);
				app->qry->SetBankMoney(plr->GetCharID(), plr->GetPcProfile()->dwZenny);
			}
		}
	}
	else
	{
		res->bIsSet = false;
		res->hTarget = plr->GetAvatarHandle();
		res->dwPacketCount = req->dwPacketCount;
		res->wResultCode = GAME_SUCCESS;
		res->wOpCode = GU_TRADE_END_RES;

		packet.SetPacketLen(sizeof(sGU_TRADE_END_RES));
		g_pApp->Send(this->GetHandle(), &packet);
	}
	plr = NULL;
	delete plr;
}
//Trade Add Res - Luiz45
void CClientSession::SendTradeAddRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_ADD_REQ* req = (sUG_TRADE_ADD_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_ADD_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_ADD_NFY));
	sGU_TRADE_ADD_RES* res = (sGU_TRADE_ADD_RES*)packet.GetPacketData();
	sGU_TRADE_ADD_NFY* res2 = (sGU_TRADE_ADD_NFY*)packet2.GetPacketData();

	PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);

	res->byCount = req->byCount;
	res->hItem = req->hItem;
	res->wOpCode = GU_TRADE_ADD_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->byCount = req->byCount;
	res2->hItem = req->hItem;
	res2->wOpCode = GU_TRADE_ADD_NFY;
	for (int i = 0; i < plr->cPlayerInventory->GetTotalItemsCount(); i++)
	{
		if (plr->cPlayerInventory->GetInventory()[i].handle == req->hItem)
		{
			res2->sItem.bNeedToIdentify = false;
			res2->sItem.itemId = plr->cPlayerInventory->GetInventory()[i].handle;
			res2->sItem.itemNo = plr->cPlayerInventory->GetInventory()[i].tblidx;
			res2->sItem.byStackcount = req->byCount;
			res2->sItem.byRank = plr->cPlayerInventory->GetInventory()[i].byRank;
			res2->sItem.byCurrentDurability = plr->cPlayerInventory->GetInventory()[i].byCurDur;
			res2->sItem.byGrade = plr->cPlayerInventory->GetInventory()[i].byGrade;
			res2->sItem.byPlace = plr->cPlayerInventory->GetInventory()[i].byPlace;
			res2->sItem.byPosition = plr->cPlayerInventory->GetInventory()[i].byPos;
			plr->cPlayerInventory->AddItemToTrade(res2->sItem);
		}
	}
	packet.SetPacketLen(sizeof(sGU_TRADE_ADD_RES));
	packet2.SetPacketLen(sizeof(sGU_TRADE_ADD_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(PlayerTrade->GetSession(), &packet2);
	plr = NULL;
	delete plr;
}
//Trade Dell - Luiz45
void CClientSession::SendTradeDelRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_DEL_REQ* req = (sUG_TRADE_DEL_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_DEL_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_DEL_NFY));
	sGU_TRADE_DEL_RES* res = (sGU_TRADE_DEL_RES*)packet.GetPacketData();
	sGU_TRADE_DEL_NFY* res2 = (sGU_TRADE_DEL_NFY*)packet2.GetPacketData();

	res->hItem = req->hItem;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TRADE_DEL_RES;

	res2->hItem = req->hItem;
	res2->wOpCode = GU_TRADE_DEL_NFY;

	plr->cPlayerInventory->RemoveUpdateTrade(req->hItem, true, NULL);

	PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);

	packet.SetPacketLen(sizeof(sGU_TRADE_DEL_RES));
	packet2.SetPacketLen(sizeof(sGU_TRADE_DEL_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(PlayerTrade->GetSession(), &packet2);
	plr = NULL;
}
//Trade Cancel - Luiz45
void CClientSession::SendTradeCancelRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_CANCEL_REQ* req = (sUG_TRADE_CANCEL_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_CANCEL_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_CANCEL_NFY));
	sGU_TRADE_CANCEL_RES* res = (sGU_TRADE_CANCEL_RES*)packet.GetPacketData();
	sGU_TRADE_CANCEL_NFY* res2 = (sGU_TRADE_CANCEL_NFY*)packet2.GetPacketData();

	res->hTarget = plr->GetAvatarHandle();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TRADE_CANCEL_RES;

	res2->hTarget = req->hTarget;
	res2->wResultCode = GAME_SUCCESS;
	res2->wOpCode = GU_TRADE_CANCEL_NFY;

	packet.SetPacketLen(sizeof(sGU_TRADE_CANCEL_RES));
	packet2.SetPacketLen(sizeof(sGU_TRADE_CANCEL_NFY));

	PlayersMain* PlayerTarget = app->GetUserSession(req->hTarget);

	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(PlayerTarget->GetSession(), &packet2);
	plr = NULL;
	delete plr;
}
//Trade Deny - Luiz45
void CClientSession::SendTradeDenyRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_DENY_REQ* req = (sUG_TRADE_DENY_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_TRADE_DENY_RES));
	sGU_TRADE_DENY_RES* res = (sGU_TRADE_DENY_RES*)packet.GetPacketData();
	res->bIsDeny = req->bIsDeny;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TRADE_DENY_RES;
	packet.SetPacketLen(sizeof(sGU_TRADE_DENY_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}
//Trade OK - Luiz45
void CClientSession::SendTradeOkRes(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_OK_RES* req = (sUG_TRADE_OK_RES*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_START_NFY));
	CLkPacket packet2(sizeof(sGU_TRADE_START_RES));
	sGU_TRADE_START_RES* res1 = (sGU_TRADE_START_RES*)packet2.GetPacketData();
	sGU_TRADE_START_NFY* res = (sGU_TRADE_START_NFY*)packet.GetPacketData();

	if (req->byOK == true)
	{
		PlayersMain* PlayerTrade = app->GetUserSession(req->handle);
		//Lets tell to the user that guy is in trade
		plr->SetPlayerIsInTrade(true);
		PlayerTrade->SetPlayerIsInTrade(true);
		res->byEmptyInven = 12;//Here you can put how many free slots are available to trade i've placed the MAX
		res->hTarget = plr->GetAvatarHandle();
		res->handle = req->handle;
		res->wOpCode = GU_TRADE_START_NFY;
		res->wResultCode = GAME_SUCCESS;

		res1->byEmptyInven = 12;//Here you can put how many free slots are available to trade i've placed the MAX
		res1->handle = plr->GetAvatarHandle();
		res1->hTarget = req->handle;
		res1->wResultCode = GAME_SUCCESS;
		res1->wOpCode = GU_TRADE_START_RES;

		packet.SetPacketLen(sizeof(sGU_TRADE_START_NFY));
		packet2.SetPacketLen(sizeof(sGU_TRADE_START_RES));
		g_pApp->Send(this->GetHandle(), &packet2);
		app->UserBroadcastothers(&packet, this);
	}
	else
	{
		res1->wResultCode = GAME_TRADE_DENY;
		res1->wOpCode = GU_TRADE_START_RES;
		packet2.SetPacketLen(sizeof(sGU_TRADE_START_RES));
		//Getting User Session to Cancel the trade
		PlayersMain* PlayerTrade = app->GetUserSession(req->handle);

		g_pApp->Send(this->GetHandle(), &packet2);
		g_pApp->Send(PlayerTrade->GetSession(), &packet2);
	}
	plr = NULL;
	delete plr;
}
//Trade Change Item Amount - Luiz45
void CClientSession::SendTradeItemModify(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_MODIFY_REQ* req = (sUG_TRADE_MODIFY_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_MODIFY_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_MODIFY_NFY));
	sGU_TRADE_MODIFY_RES* res = (sGU_TRADE_MODIFY_RES*)packet.GetPacketData();
	sGU_TRADE_MODIFY_NFY* res2 = (sGU_TRADE_MODIFY_NFY*)packet2.GetPacketData();

	PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);
	res->byCount = req->byCount;
	res->hItem = req->hItem;
	res->hTarget = plr->GetAvatarHandle();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TRADE_MODIFY_RES;

	res2->byCount = req->byCount;
	res2->hItem = req->hItem;
	res2->hTarget = PlayerTrade->GetAvatarHandle();
	res2->wOpCode = GU_TRADE_MODIFY_NFY;

	plr->cPlayerInventory->RemoveUpdateTrade(req->hItem, false, req->byCount);//Add in our List

	packet.SetPacketLen(sizeof(sGU_TRADE_MODIFY_RES));
	packet2.SetPacketLen(sizeof(sGU_TRADE_MODIFY_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(PlayerTrade->GetSession(), &packet2);
	plr = NULL;
	delete plr;
}
//Trade Change Zenny Amount - Luiz45
void CClientSession::SendTradeZennyModify(CLkPacket * pPacket, CGameServer * app)
{
	sUG_TRADE_ZENNY_UPDATE_REQ* req = (sUG_TRADE_ZENNY_UPDATE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	CLkPacket packet(sizeof(sGU_TRADE_ZENNY_UPDATE_RES));
	CLkPacket packet2(sizeof(sGU_TRADE_ZENNY_UPDATE_NFY));
	sGU_TRADE_ZENNY_UPDATE_RES* res = (sGU_TRADE_ZENNY_UPDATE_RES*)packet.GetPacketData();
	sGU_TRADE_ZENNY_UPDATE_NFY* res2 = (sGU_TRADE_ZENNY_UPDATE_NFY*)packet2.GetPacketData();
	PlayersMain* PlayerTrade = app->GetUserSession(req->hTarget);

	res->dwZenny = req->dwZenny;
	res->hTarget = plr->GetAvatarHandle();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TRADE_ZENNY_UPDATE_RES;

	res2->dwZenny = req->dwZenny;
	res2->hTarget = req->hTarget;
	res2->wOpCode = GU_TRADE_ZENNY_UPDATE_NFY;

	plr->cPlayerInventory->SetAmountZennyToTrade(req->dwZenny);

	packet.SetPacketLen(sizeof(sGU_TRADE_ZENNY_UPDATE_RES));
	packet2.SetPacketLen(sizeof(sGU_TRADE_ZENNY_UPDATE_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(PlayerTrade->GetSession(), &packet2);
	plr = NULL;
	delete plr;
}

//--------------------------------------------------------------------------------------//
//		Server Change Request
//--------------------------------------------------------------------------------------//
void CClientSession::SendServerChangeReq(CLkPacket * pPacket, CGameServer * app)
{
	sUG_CHAR_SERVER_CHANGE_REQ* req = (sUG_CHAR_SERVER_CHANGE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_CHAR_SERVER_CHANGE_RES));
	sGU_CHAR_SERVER_CHANGE_RES * res = (sGU_CHAR_SERVER_CHANGE_RES*)packet.GetPacketData();

	res->wOpCode = GU_CHAR_SERVER_CHANGE_RES;
	strcpy_s(res->serverInfo.szCharacterServerIP, LK_MAX_LENGTH_OF_IP, app->GetConfigFileExternalIP());
	res->serverInfo.wCharacterServerPortForClient = 20300;
	res->serverInfo.dwLoad = 0;
	strcpy_s((char*)res->achAuthKey, LK_MAX_SIZE_AUTH_KEY, "Dbo");
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen(sizeof(sGU_CHAR_SERVER_CHANGE_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
}
//--------------------------------------------------------------------------------------//
//		Test DirectPlay Scripts -- Admin Function
//--------------------------------------------------------------------------------------//

void	CClientSession::SendTestDirectPlay(uint32_t tblidx, int playerId, bool sync)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	PlayersMain* plr = g_pPlayerManager->GetPlayerByID(playerId);
	CLkPacket packetAck(sizeof(sGU_CHAR_DIRECT_PLAY));
	sGU_CHAR_DIRECT_PLAY* res = (sGU_CHAR_DIRECT_PLAY*)packetAck.GetPacketData();

	CDirectionLinkTable* pLinkTbl = app->g_pTableContainer->GetDirectionLinkTable();
	sDIRECTION_LINK_TBLDAT *pLinkTblData = reinterpret_cast<sDIRECTION_LINK_TBLDAT*>(pLinkTbl->FindData(tblidx));
	if (pLinkTblData->tblidx != INVALID_TBLIDX)
	{
		res->bSynchronize = sync;
		res->byPlayMode = 1;
		res->directTblidx = pLinkTblData->tblidx;
		res->hSubject = plr->GetAvatarHandle();
		res->wOpCode = GU_CHAR_DIRECT_PLAY;

		packetAck.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY));
		g_pApp->SendTo(plr->myCCSession, &packetAck);
	}

}
//Item Upgrade - luiz45
void CClientSession::SendItemUpgradeReq(CLkPacket* pPacket, CGameServer* app)
{
	sUG_ITEM_UPGRADE_REQ* req = (sUG_ITEM_UPGRADE_REQ*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_ITEM_UPGRADE_RES));
	sGU_ITEM_UPGRADE_RES* res = (sGU_ITEM_UPGRADE_RES*)packet.GetPacketData();

	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	plr->cPlayerInventory->GetItemByPosPlace(req->byItemPlace, req->byItemPos)->byGrade+=1;
	plr->cPlayerInventory->GetItemByPosPlace(req->byItemPlace, req->byItemPos)->byBattleAttribute = req->byStonPlace;

	res->byGrade = plr->cPlayerInventory->GetItemByPosPlace(req->byItemPlace, req->byItemPos)->byGrade;
	res->byBattleAttribute = req->byStonPlace;
	res->byItemPlace = req->byItemPlace;
	res->byItemPos   = req->byItemPos;
	res->byStack	 = 1;
	res->byStonPlace = req->byStonPlace;
	res->byStonPos	 = req->byStonPos;
	
	res->wResultCode = GAME_SUCCESS;//By now every item is a success
	res->wOpCode = GU_ITEM_UPGRADE_RES;

	packet.SetPacketLen(sizeof(sGU_ITEM_UPGRADE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
	
}
//Budokai Social Act - Luiz45 -- Didn't work...maybe just receive and not send a answer?
void CClientSession::SendBudokaiSocialAction(CLkPacket * pPacket, CGameServer *app)
{
	sUG_BUDOKAI_SOCIAL_ACTION* req = (sUG_BUDOKAI_SOCIAL_ACTION*)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sGU_SOCIAL_ACTION));
	sGU_SOCIAL_ACTION* res = (sGU_SOCIAL_ACTION*)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	res->hSubject = this->GetavatarHandle();
	res->socialActionId= req->socialAction; //Req->socialAction = 1503 too high for social action i believe.
	res->wOpCode = GU_SOCIAL_ACTION;

	packet.SetPacketLen(sizeof(sGU_SOCIAL_ACTION));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
}
//Vehicle Stunt NFY - Luiz45
void CClientSession::SendVehicleStuntNFY(CLkPacket* pPacket, CGameServer* app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());

	sUG_VEHICLE_STUNT_NFY * req = (sUG_VEHICLE_STUNT_NFY *)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_VEHICLE_STUNT_NFY));
	sGU_VEHICLE_STUNT_NFY* res = (sGU_VEHICLE_STUNT_NFY*)packet.GetPacketData();

	res->hDriverHandle = plr->GetAvatarHandle();
	res->wOpCode = GU_VEHICLE_STUNT_NFY;

	packet.SetPacketLen(sizeof(sGU_VEHICLE_STUNT_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet, this);
}
//Vehicle Direct Play Cancel - Luiz45
void CClientSession::SendVehicleDPCancelRes(CLkPacket* pPacket, CGameServer* app)
{
	sUG_VEHICLE_DIRECT_PLAY_CANCEL_NFY* req = (sUG_VEHICLE_DIRECT_PLAY_CANCEL_NFY*)pPacket->GetPacketData();	
	//CLkPacket packet(sizeof(sGU_VEHICLE_Di))
}
//Vehicle Engine Start - Luiz45
void CClientSession::SendEngineStartRes(CLkPacket* pPacket, CGameServer* app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_VEHICLE_ENGINE_START_REQ* req = (sUG_VEHICLE_ENGINE_START_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_VEHICLE_ENGINE_START_RES));
	CLkPacket packet2(sizeof(sGU_VEHICLE_ENGINE_START_NFY));

	sGU_VEHICLE_ENGINE_START_RES* res  = (sGU_VEHICLE_ENGINE_START_RES*)packet.GetPacketData();
	sGU_VEHICLE_ENGINE_START_NFY* res2 = (sGU_VEHICLE_ENGINE_START_NFY*)packet2.GetPacketData();

	res->wOpCode = GU_VEHICLE_ENGINE_START_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->hDriverHandle = plr->GetAvatarHandle();
	res2->wOpCode = GU_VEHICLE_ENGINE_START_NFY;

	packet.SetPacketLen(sizeof(sGU_VEHICLE_ENGINE_START_RES));
	packet2.SetPacketLen(sizeof(sGU_VEHICLE_ENGINE_START_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet2, this);
}
//Vehicle Engine Stop - Luiz45
void CClientSession::SendEngineStopRes(CLkPacket* pPacket, CGameServer* app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_VEHICLE_ENGINE_STOP_REQ* req = (sUG_VEHICLE_ENGINE_STOP_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_VEHICLE_ENGINE_STOP_RES));
	CLkPacket packet2(sizeof(sGU_VEHICLE_ENGINE_STOP_NFY));
	
	sGU_VEHICLE_ENGINE_STOP_RES* res = (sGU_VEHICLE_ENGINE_STOP_RES*)packet.GetPacketData();
	sGU_VEHICLE_ENGINE_STOP_NFY* res2 = (sGU_VEHICLE_ENGINE_STOP_NFY*)packet2.GetPacketData();

	res->wOpCode = GU_VEHICLE_ENGINE_STOP_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->hDriverHandle = plr->GetAvatarHandle();
	res2->wOpCode = GU_VEHICLE_ENGINE_STOP_NFY;

	packet.SetPacketLen(sizeof(sGU_VEHICLE_ENGINE_STOP_RES));
	packet2.SetPacketLen(sizeof(sGU_VEHICLE_ENGINE_STOP_NFY));
	g_pApp->Send(this->GetHandle(), &packet);
	app->UserBroadcastothers(&packet2, this);
}
//Vehicle End - Luiz45
void CClientSession::SendVehicleEndRes(CLkPacket* pPacket, CGameServer* app)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	sUG_VEHICLE_END_REQ* req = (sUG_VEHICLE_END_REQ*)pPacket->GetPacketData();
	CLkPacket packet(sizeof(sGU_VEHICLE_END_RES));
	CLkPacket packet2(sizeof(sGU_VEHICLE_END_NFY));
	CLkPacket packet3(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));
	CLkPacket packet4(sizeof(sGU_UPDATE_CHAR_STATE));
	
	sGU_VEHICLE_END_RES* res  = (sGU_VEHICLE_END_RES*)packet.GetPacketData();
	sGU_VEHICLE_END_NFY* res2 = (sGU_VEHICLE_END_NFY*)packet2.GetPacketData();
	sGU_UPDATE_CHAR_ASPECT_STATE* res3 = (sGU_UPDATE_CHAR_ASPECT_STATE*)packet3.GetPacketData();
	sGU_UPDATE_CHAR_STATE* res4 = (sGU_UPDATE_CHAR_STATE*)packet4.GetPacketData();

	res->wOpCode = GU_VEHICLE_END_RES;
	res->wResultCode = GAME_SUCCESS;

	res2->hDriverHandle = plr->GetAvatarHandle();
	res2->wOpCode = GU_VEHICLE_END_NFY;
	res2->wResultCode = GAME_SUCCESS;
	
	res3->handle = plr->GetAvatarHandle();
	res3->aspectState.sAspectStateBase.byAspectStateId = ASPECTSTATE_INVALID;
	res3->wOpCode = GU_UPDATE_CHAR_ASPECT_STATE;

	res4->handle = plr->GetAvatarHandle();
	res4->sCharState.sCharStateBase.byStateID = CHARSTATE_STANDING;
	res4->sCharState.sCharStateBase.vCurLoc.x = plr->GetPlayerPosition().x;
	res4->sCharState.sCharStateBase.vCurLoc.y = plr->GetPlayerPosition().y;
	res4->sCharState.sCharStateBase.vCurLoc.z = plr->GetPlayerPosition().z;
	res4->sCharState.sCharStateBase.vCurDir.x = plr->GetPlayerDirection().x;
	res4->sCharState.sCharStateBase.vCurDir.y = plr->GetPlayerDirection().y;
	res4->sCharState.sCharStateBase.vCurDir.z = plr->GetPlayerDirection().z;
	res4->wOpCode = GU_UPDATE_CHAR_STATE;

	packet.SetPacketLen(sizeof(sGU_VEHICLE_END_RES));
	packet2.SetPacketLen(sizeof(sGU_VEHICLE_END_NFY));
	packet3.SetPacketLen(sizeof(sGU_UPDATE_CHAR_ASPECT_STATE));
	packet4.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	g_pApp->Send(this->GetHandle(), &packet);
	g_pApp->Send(this->GetHandle(), &packet2);
	app->UserBroadcastothers(&packet2, this);
	g_pApp->Send(this->GetHandle(), &packet3);
	g_pApp->Send(this->GetHandle(), &packet4);
}
//Create Dojo - Luiz45
void CClientSession::SendCreateDojo(CLkPacket* pPacket, CGameServer* app)
{
	sUG_DOJO_CREATE_REQ* req = (sUG_DOJO_CREATE_REQ*)pPacket->GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(this->GetavatarHandle());
	CLkPacket packet(sizeof(sGU_DOJO_CREATE_RES));
	sGU_DOJO_CREATE_RES* res = (sGU_DOJO_CREATE_RES*)packet.GetPacketData();
		
	res->wOpCode = GU_DOJO_CREATE_RES;
	res->wResultCode = GAME_SUCCESS;
	
	packet.SetPacketLen(sizeof(sGU_DOJO_CREATE_RES));
	g_pApp->Send(this->GetHandle(), &packet);
}