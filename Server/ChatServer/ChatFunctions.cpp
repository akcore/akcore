#include "stdafx.h"
#include "ChatServer.h"
void ChatFunctionsClass::AddRemoveFriend(int CharID, int TargetID, bool bRemove,bool toBlackList)
{
	CChatServer * app = (CChatServer*)LkSfxGetApp();
	if (bRemove)
	{
		app->db->prepare("DELETE FROM buddylist WHERE owner_id = ? AND friend_id = ?");
		app->db->setInt(1, CharID);
		app->db->setInt(2, TargetID);
		app->db->execute();
	}
	else
	{
		if (toBlackList)
		{
			app->db->prepare("UPDATE buddylist SET moveBlackList = 1 WHERE owner_id = ? AND friend_id = ?");
			app->db->setInt(1, CharID);
			app->db->setInt(2, TargetID);
			app->db->execute();
		}
		else
		{
			app->db->prepare("INSERT INTO buddylist (owner_id,friend_id) VALUES (?,?)");
			app->db->setInt(1, CharID);
			app->db->setInt(2, TargetID);
			app->db->execute();
		}		
	}
}

void ChatFunctionsClass::AddRemoveBlackList(int CharID, int TargetID, bool bRemove)
{
	CChatServer * app = (CChatServer*)LkSfxGetApp();
	if (bRemove)
	{
		app->db->prepare("DELETE FROM blacklist WHERE owner_id = ? AND target_id = ?");
		app->db->setInt(1, CharID);
		app->db->setInt(2, TargetID);
		app->db->execute();

		app->db->prepare("DELETE FROM buddylist WHERE owner_id = ? AND friend_id = ?");
		app->db->setInt(1, CharID);
		app->db->setInt(2, TargetID);
		app->db->execute();
	}
	else
	{
		app->db->prepare("INSERT INTO blacklist (owner_id,target_id) VALUES (?,?)");
		app->db->setInt(1, CharID);
		app->db->setInt(2, TargetID);
		app->db->execute();
	}
}

void ChatFunctionsClass::ErrorHandler(CClientSession * session, eSERVER_TEXT_TYPE errorType, const wchar_t * message)
{
	CLkPacket packet(sizeof(sTU_SYSTEM_DISPLAY_TEXT));
	sTU_SYSTEM_DISPLAY_TEXT* sNotice = (sTU_SYSTEM_DISPLAY_TEXT*)packet.GetPacketData();
	CChatServer * app = (CChatServer*)LkSfxGetApp();

	WCHAR wcsMsg[BUDOKAI_MAX_NOTICE_LENGTH + 1];
	char ch[256];
	char DefChar = '\0';
	WideCharToMultiByte(CP_ACP, 0, message, -1, ch, 256, &DefChar, NULL);
	mbstowcs(wcsMsg, ch, 255);
	
	sNotice->wOpCode = TU_SYSTEM_DISPLAY_TEXT;
	sNotice->byDisplayType = errorType;
	wcscpy_s(sNotice->wszMessage, BUDOKAI_MAX_NOTICE_LENGTH + 1, wcsMsg);
	if (errorType == SERVER_TEXT_EMERGENCY)
	wcscpy_s(sNotice->wszGmCharName, LK_MAX_SIZE_CHAR_NAME_UNICODE, L"Akira Tori");

	sNotice->wMessageLengthInUnicode = (WORD)wcslen(message);
	packet.SetPacketLen(sizeof(sTU_SYSTEM_DISPLAY_TEXT));
	app->Send(session->GetHandle(), &packet);
}


sDBO_GUILD_MEMBER_INFO* ChatFunctionsClass::GetGuildMemberInfo(int GuildID, int CharID)
{
	CChatServer * app = (CChatServer*)LkSfxGetApp();
	app->db->prepare("SELECT CharName,Level,Race,Class FROM characters WHERE GuildID = ? AND CharID = ?");
	app->db->setInt(1, GuildID);
	app->db->setInt(2, CharID);
	app->db->execute();

	sDBO_GUILD_MEMBER_INFO * guildMemberInfo = new sDBO_GUILD_MEMBER_INFO;
	
		//guildMemberInfo->bIsOnline = true;
		guildMemberInfo->byClass = app->db->getInt("Class");
		guildMemberInfo->byLevel = app->db->getInt("Level");
		guildMemberInfo->byRace = app->db->getInt("Race");
		guildMemberInfo->charId = CharID;
		guildMemberInfo->dwReputation = 1000;
		wcscpy_s(guildMemberInfo->wszMemberName, LK_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str());

		
		return guildMemberInfo;
}