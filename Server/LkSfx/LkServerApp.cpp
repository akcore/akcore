//***********************************************************************************
//
//	File		:	ServerApp.cpp
//
//	Begin		:	2007-01-04
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkServerApp.h"

#include "LkSessionFactory.h"
#include "LkServerLog.h"

#include "LkString.h"
#include "LkError.h"
#include "LkMiniDump.h"


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
CLkMiniDump					s_miniDump;	// 프로그램 예외발생시의 메모리 덤프 오브젝트
//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerApp::CLkServerApp()
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerApp::~CLkServerApp()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::Init()
{
	m_pSessionFactory = NULL;

	m_pServerLog = NULL;

	m_nMaxSessionCount = 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::Destroy()
{
	CLkThreadFactory::CloseAll();

	SAFE_DELETE( m_pSessionFactory );
	SAFE_DELETE( m_pServerLog );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkServerApp::Create(int argc, _TCHAR* argv[], const char * lpszConfigFile /* = NULL */)
{
	int rc = 0;

	SetConfigFileName(argv[0], lpszConfigFile);
	rc = CLkServerApp::OnConfiguration( m_strConfigFile.c_str() );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}
	rc = OnConfiguration( m_strConfigFile.c_str() );
	if( LK_SUCCESS !=  rc )
	{
		return rc;
	}


	rc = CLkServerApp::OnCommandArgument( argc, argv );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}
	rc = OnCommandArgument( argc, argv );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}


	rc = CLkServerApp::OnInitApp();
	if( LK_SUCCESS != rc )
	{
		return rc;
	}
	rc = OnInitApp();
	if( LK_SUCCESS != rc )
	{
		return rc;
	}


	rc = CLkServerApp::OnCreate();
	if( LK_SUCCESS != rc )
	{
		return rc;
	}
	rc = OnCreate();
	if( LK_SUCCESS != rc )
	{
		return rc;
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int	CLkServerApp::OnInitApp()
{
	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int	CLkServerApp::OnCreate()
{
	int rc = m_performance.Create();
	if( LK_SUCCESS != rc )
	{
		return rc;
	}


	rc = m_network.Create( m_pSessionFactory, m_nMaxSessionCount ); 
	if( LK_SUCCESS != rc )
	{
		return rc;
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkServerApp::OnCommandArgument(int argc, _TCHAR* argv[])
{
	UNREFERENCED_PARAMETER( argc );
	UNREFERENCED_PARAMETER( argv );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkServerApp::OnConfiguration(const char * lpszConfigFile)
{
	UNREFERENCED_PARAMETER( lpszConfigFile );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkServerApp::OnAppStart()
{
	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::OnRun()
{
	while( IsRunnable() )
	{
		LK_PRINT(PRINT_SYSTEM, "Run");
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::Start(bool bAutoDelete /* = false */)
{
	if (NULL == m_pSessionFactory)
	{
		LK_LOG_ASSERT("NULL == m_pSessionFactory");
	}

	m_pAppThread = CLkThreadFactory::CreateThread( this, "LkSfxAppThread", bAutoDelete ); 
	m_pAppThread->Start();

	OnAppStart();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::Stop()
{
	CLkThreadFactory::CloseAll();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::WaitForTerminate()
{
	CLkThreadFactory::JoinAll();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerApp::SetConfigFileName(const char * lpszAppName, const char * lpszFileName)
{
	if( lpszFileName )
	{
		m_strConfigFile = lpszFileName;
	}
	else
	{
		std::string::size_type pos;

		m_strConfigFile = lpszAppName;

		pos = m_strConfigFile.rfind(".");
		m_strConfigFile.erase(pos);
		//pos = strConfigFile.rfind("\\");
		//strConfigFile.erase(0, pos + 1);

		m_strConfigFile += ".ini";
	}
}
