//***********************************************************************************
//
//	File		:	LkServerLog.cpp
//
//	Begin		:	2007-03-07
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************


#include "StdAfx.h"
#include "LkServerLog.h"

#include <tchar.h>


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
const unsigned int SERVER_LOGDATA_RESERVED = 100;
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
DEFINE_DYNAMIC_MEMORYPOOL_THREADSAFE( CLkServerLogData, SERVER_LOGDATA_RESERVED );
//-----------------------------------------------------------------------------------
#ifdef _WIN64
	DWORD64 CLkServerLogData::curSequence = 0;
#else
	DWORD CLkServerLogData::curSequence = 0;
#endif
//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerLog::CLkServerLog(void)
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerLog::~CLkServerLog(void)
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerLog::Init()
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerLog::Destroy()
{
	CLkServerLogData * pLogData = PopLog();
	while( pLogData )
	{
		SAFE_DELETE( pLogData );

		pLogData = PopLog();
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkServerLog::Log(BYTE byLogChannel, bool bDate, LPCTSTR lpszFile, int nLine, LPCTSTR lpszFunc, LPCTSTR lpszText, ...)
{
	CLkServerLogData * pLogData = new CLkServerLogData;
	if( NULL == pLogData )
	{
		// ����ó��
		return;
	}


	int nBuffSize = pLogData->GetBufSize();
	int nWriteSize = 0;

	pLogData->dwSource = GetDefaultLogSource();
	pLogData->byChannel = byLogChannel;


	if( bDate )
	{
		SYSTEMTIME	systemTime;
		GetLocalTime( &systemTime );
		nWriteSize += _stprintf_s( pLogData->achLogText + nWriteSize, nBuffSize - nWriteSize, TEXT("%d-%02d-%02d %02d:%02d:%02d,"), systemTime.wYear, systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond );
	}


	va_list args;
	va_start( args, lpszText );
	nWriteSize += _vstprintf_s( pLogData->achLogText + nWriteSize, nBuffSize - nWriteSize, lpszText, args );
	va_end( args );


	if( lpszFile )
	{
		nWriteSize += _stprintf_s( pLogData->achLogText + nWriteSize, nBuffSize - nWriteSize, TEXT(" file:%s\t(line:%d)"), lpszFile, nLine );
	}


	if( lpszFunc )
	{
		nWriteSize += _stprintf_s( pLogData->achLogText + nWriteSize, nBuffSize - nWriteSize, TEXT(" function[%s]\t"), lpszFunc );
	}


	pLogData->wStrLen = (WORD) nWriteSize;


	if( false == PushLog( pLogData ) )
	{
		SAFE_DELETE( pLogData );
	}

}



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkServerLog::PushLog(CLkServerLogData* pLogData)
{
	m_mutex.Lock();

	pLogData->sequence = ++(pLogData->curSequence);
	m_logQueue.push_back( pLogData );

	m_mutex.Unlock();

	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerLogData* CLkServerLog::PopLog()
{
	CLkServerLogData * pLogData = NULL;

	m_mutex.Lock();

	if( false == m_logQueue.empty() )
	{
		pLogData = m_logQueue.front();
		m_logQueue.pop_front();
	}

	m_mutex.Unlock();


	return pLogData;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkServerLogData* CLkServerLog::PeekLog()
{
	CLkServerLogData * pLogData = NULL;

	m_mutex.Lock();

	if( false == m_logQueue.empty() )
	{
		pLogData = m_logQueue.front();
	}

	m_mutex.Unlock();


	return pLogData;
}
