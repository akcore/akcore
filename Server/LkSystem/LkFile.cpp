//***********************************************************************************
//
//	File		:	LkFile.cpp
//
//	Begin		:	2007-03-19
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************


#include "StdAfx.h"
#include "LkFile.h"

#include "LkError.h"

#include <io.h>
#include <tchar.h>



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkFile::CLkFile(void)
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkFile::~CLkFile(void)
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFile::Init()
{
	m_bAutoClose = false;

	m_hFile = HFILE_ERROR;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFile::Destroy()
{
	Close();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkFile::Create(LPCTSTR lpszFileName, int nOperationFlag /* = _O_CREAT | _O_APPEND | _O_RDWR */, int nSharingFlag /* = _SH_DENYNO */, int nPermissionMode /* = _S_IREAD | _S_IWRITE */, bool bAutoClose /* = false */)
{
	int rc = _tsopen_s( &m_hFile, lpszFileName, nOperationFlag, nSharingFlag, nPermissionMode );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}

	m_strFileName = lpszFileName;

	m_bAutoClose = bAutoClose;


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFile::Close()
{
	if( IsOpened() && m_bAutoClose )
	{
		_close( m_hFile );
		m_hFile = HFILE_ERROR;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkFileStream::CLkFileStream(void)
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkFileStream::~CLkFileStream(void)
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFileStream::Init()
{
	m_pFilePtr = NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFileStream::Destroy()
{
	Close();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkFileStream::Create(LPCTSTR lpszFileName, int nOperationFlag, int nSharingFlag, int nPermissionMode)
{
	CLkFile file;

	int rc = file.Create( lpszFileName, nOperationFlag, nSharingFlag, nPermissionMode, false );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}

	rc = Attach( file );
	if(  LK_SUCCESS != rc )
	{
		return rc;
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkFileStream::Attach(CLkFile & rFile, LPCTSTR lpszMode /* = TEXT */)
{
	if( m_pFilePtr )
	{
		return LK_ERR_SYS_OBJECT_ALREADY_CREATED;
	}


	m_pFilePtr = _fdopen( rFile.GetFileHandle(), lpszMode );
	if( NULL == m_pFilePtr )
	{
		return GetLastError();
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkFileStream::Close()
{
	if( IsOpened() )
	{
		fclose( m_pFilePtr );
		m_pFilePtr = NULL;
	}	
}
