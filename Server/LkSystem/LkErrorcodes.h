
LK_DEFINE_ERROR( LK_FAIL )
LK_DEFINE_ERROR( LK_APP_EXIT )

LK_DEFINE_ERROR( LK_ERR_SYS_MEMORY_ALLOC_FAIL )
LK_DEFINE_ERROR( LK_ERR_SYS_CONFIG_FILE_READ_FAIL )
LK_DEFINE_ERROR( LK_ERR_SYS_LOG_SYSTEM_INITIALIZE_FAIL )
LK_DEFINE_ERROR( LK_ERR_SYS_PROFILE_INITIALIZE_FAIL )
LK_DEFINE_ERROR( LK_ERR_SYS_INPUT_PARAMETER_WRONG )
LK_DEFINE_ERROR( LK_ERR_SYS_OBJECT_ALREADY_CREATED )
LK_DEFINE_ERROR( LK_ERR_SYS_TYPE_CASTING_FAIL )
LK_DEFINE_ERROR( LK_ERR_SYS_OPTION_FILE_READ_FAIL )

LK_DEFINE_ERROR( LK_ERR_DBC_HANDLE_ALREADY_ALLOCATED )
LK_DEFINE_ERROR( LK_ERR_DBC_HANDLE_IS_INVALID )
LK_DEFINE_ERROR( LK_ERR_DBC_ENVIRONMENT_ALLOC_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_ENVIRONMENT_SETATT_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_CONNECTION_ALLOC_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_CONNECTION_CONNECT_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_STATEMENT_ALLOC_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_STATEMENT_PREPARE_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_BIND_PAREMTER_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_BIND_COLUMN_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_CALL_SQLEXECUTE_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_CALL_SQLFETCH_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_CALL_SQLMORERESULT_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_DATABASE_FIND_FAIL )
LK_DEFINE_ERROR( LK_ERR_DBC_DATABASE_SQL_PRECREATE_FAIL )

LK_DEFINE_ERROR( LK_ERR_NET_NETWORK_ALREADY_CREATED )
LK_DEFINE_ERROR( LK_ERR_NET_NETWORK_NOT_CREATED )
LK_DEFINE_ERROR( LK_ERR_NET_ACCEPTOR_ALREADY_CREATED )
LK_DEFINE_ERROR( LK_ERR_NET_ACCEPTOR_ASSOCIATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTOR_ALREADY_CREATED )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTOR_ASSOICATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_INVALID_COMPLETE_IO_MODE )
LK_DEFINE_ERROR( LK_ERR_NET_INVALID_COMPLETE_IO_HANDLE )

LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_CREATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_POOL_ALLOC_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_ADD_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_REMOVE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_NOT_EXIST )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_IO_NOT_COMPLETED )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_STATUS_WRONG )
LK_DEFINE_ERROR( LK_ERR_NET_CONNECTION_SEND_FAIL )

LK_DEFINE_ERROR( LK_ERR_NET_SESSION_RECV_BUFFER_OVERFLOW )
LK_DEFINE_ERROR( LK_ERR_NET_SESSION_SEND_BUFFER_OVERFLOW )
LK_DEFINE_ERROR( LK_ERR_NET_SESSION_CREATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_SESSION_CLOSED )
LK_DEFINE_ERROR( LK_ERR_NET_SESSION_ADD_FAIL )

LK_DEFINE_ERROR( LK_ERR_NET_PACKET_INVALID )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_OPCODE_WRONG )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_ENCRYPT_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_DECRYPT_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_CHECKSUM_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_SEQUENCE_FAIL )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_PENDING_HEADER )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_PENDING_DATA )
LK_DEFINE_ERROR( LK_ERR_NET_PACKET_EXCEED_ALLOWED_SIZE )

LK_DEFINE_ERROR( LK_ERR_NET_THREAD_CREATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_THREAD_CREATE_FAIL )
LK_DEFINE_ERROR( LK_ERR_THREAD_POOL_CREATE_FAIL )
