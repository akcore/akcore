//***********************************************************************************
//
//	File		:	LkProfiler.h
//
//	Begin		:	2006-07-11
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once


// profile ������
#define __LK_FUNCTION_PROFILE__


#ifdef __LK_FUNCTION_PROFILE__


//////////////////////////////////////////////////////////////////////////
//	ProflieNode
//////////////////////////////////////////////////////////////////////////
class CLkProfileNode
{
public:

	CLkProfileNode(const char * lpszNodeName, CLkProfileNode * pParent);

	~CLkProfileNode();


public:

	void						Destroy();

	void						Reset();

	void						Call();

	bool						Return();


public:

	CLkProfileNode *			GetParent() { return m_pParent; }

	CLkProfileNode *			GetChild() { return m_pChild; }

	CLkProfileNode *			GetSibling() { return m_pSibling; }

	const char *				GetName() { return m_lpszNodeName; }

	DWORD						GetTotalCall() { return m_dwTotalCalls; }

	float						GetTotalTime() { return m_fTotalTime; }

	CLkProfileNode *			GetSubNode(const char * lpszNodeName);


protected:

	void						Init();


protected:

	const char *				m_lpszNodeName;

	DWORD						m_dwTotalCalls;

	float						m_fTotalTime;

	DWORD						m_dwRecursionCounter;

	__int64						m_startTime;


	CLkProfileNode *			m_pParent;

	CLkProfileNode *			m_pChild;

	CLkProfileNode *			m_pSibling;
};


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
class CLkProfileIterator
{
friend class CLkProfiler;

protected:

	CLkProfileIterator( CLkProfileNode * start );

public:

	void						First();

	void						Next();

	bool						IsDone();

	void						EnterChild( int index );

	void						EnterParent( void );


public:

	const char *				GetName() { return m_pCurChild->GetName(); }

	DWORD						GetTotalCall() { return m_pCurChild->GetTotalCall(); }

	float						GetTotalTime() { return m_pCurChild->GetTotalTime(); }


	const char *				GetParentName() { return m_pCurParent->GetName(); }

	DWORD						GetParentTotalCall() { return m_pCurParent->GetTotalCall(); }

	float						GetParentTotalTime() { return m_pCurParent->GetTotalTime(); }


private:

	CLkProfileNode	*			m_pCurParent;

	CLkProfileNode *			m_pCurChild;
};


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
class CLkProfiler
{
public:

	CLkProfiler();

	~CLkProfiler();


public:

	int							OpenProfiler(const char * lpszFileName);

	void						CloseProfiler();

	void						Reset();

	void						StartProfile(const char * lpszNodeName);

	void						StopProfile();

	float						GetTimeSinceReset();

	CLkProfileIterator *		GetIterator() { return new CLkProfileIterator( &m_root ); }

	void						ReleaseIterator( CLkProfileIterator * iterator ) { delete iterator; }


public:

	static	CLkProfiler *		GetInstance();


protected:

	void						Init();


private:

	CLkProfileNode				m_root;

	CLkProfileNode *			m_pCurNode;

	__int64						m_resetTime;

	FILE *						m_file;
};


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
class CLkProfileHelper
{
public:

	CLkProfileHelper(const char * lpszNodeName)
	{
		CLkProfiler::GetInstance()->StartProfile( lpszNodeName );
	}

	~CLkProfileHelper()
	{
		CLkProfiler::GetInstance()->StopProfile();
	}
};


#endif // __LK_FUNCTION_PROFILE__



#ifdef __LK_FUNCTION_PROFILE__

	#define LK_PROFILE				CLkProfileHelper __profile_helper__(__FUNCTION__)

#else

	#define LK_PROFILE
					
#endif





