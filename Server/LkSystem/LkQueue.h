//***********************************************************************************
//
//	File		:	LkQueue.h
//
//	Begin		:	2006-01-20
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	큐
//
//***********************************************************************************

#ifndef __NTLQUEUE_H__
#define __NTLQUEUE_H__

#include "LkMutex.h"
#include <deque>

template <class TYPE>
class CLkQueue
{
public:

	CLkQueue();

	virtual ~CLkQueue()
	{
		Clear();
	}


public:

	TYPE					Pop();

	bool					Push(TYPE typeObject, bool bFront = false);

	TYPE					Peek();

	void					Clear();

	DWORD					GetMaxUsedSize() { 	return m_dwMaxUseSize; }

	DWORD					GetSize() { return (DWORD) m_queue.size(); }

	bool					IsEmpty() { return m_queue.empty(); }


private:

	std::deque<TYPE>		m_queue;

	CLkMutex				m_mutex;

	DWORD					m_dwMaxUseSize;

};


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template <class TYPE>
inline CLkQueue<TYPE>::CLkQueue()
{
	m_dwMaxUseSize = 0;
} 


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template <class TYPE>
inline TYPE CLkQueue<TYPE>::Pop()
{
	TYPE typeObject;

	m_mutex.Lock();
	if(false == m_queue.empty())
	{
		typeObject = m_queue.front();
		m_queue.pop_front();
	}
	else
	{
		ZeroMemory( &typeObject, sizeof(typeObject) );
	}
	m_mutex.Unlock();

	return typeObject;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template <class TYPE>
inline bool CLkQueue<TYPE>::Push(TYPE typeObject, bool bFront)
{
	m_mutex.Lock();

	// 정렬 없이 호출 순서대로 집어 넣을때 
	(bFront)?(m_queue.push_front(typeObject)):(m_queue.push_back(typeObject));

	if( GetSize() > m_dwMaxUseSize )
	{
		m_dwMaxUseSize = GetSize();
	}

	m_mutex.Unlock();

	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template <class TYPE>
inline TYPE CLkQueue<TYPE>::Peek()
{
	TYPE typeObject;

	m_mutex.Lock();
	if( false == m_queue.empty() )
	{
		typeObject = m_queue.front();
	}
	else
	{
		ZeroMemory( &typeObject, sizeof(typeObject) );
	}
	m_mutex.Unlock();

	return typeObject;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template <class TYPE>
inline void CLkQueue<TYPE>::Clear()
{
	m_mutex.Lock();
	if( false == m_queue.empty() )
	{
		m_queue.clear();
	}
	m_mutex.Unlock();
}


#endif // __NTLQUEUE_H__
