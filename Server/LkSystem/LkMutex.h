//***********************************************************************************
//
//	File		:	LkMutex.h
//
//	Begin		:	2005-11-30
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Mutex 동기화 오브젝트 클래스
//
//***********************************************************************************

#ifndef __NTLMUTEX_H__
#define __NTLMUTEX_H__

// Mutex Class
class CLkMutex
{
public:
	CLkMutex(DWORD dwSpinCount = 4000);

	virtual ~CLkMutex();

	void				Lock();

	void				Unlock();

	CRITICAL_SECTION *	GetRealMutex();

private:

	CRITICAL_SECTION	m_mutex;
};

// Mutex helper class ( 종료시 자동 해제 )
class CLkAutoMutex
{
public:
	CLkAutoMutex(CLkMutex * pMutex);

	virtual ~CLkAutoMutex(void);


	void				Lock();

	void				Unlock();

private:

	CLkMutex *			m_pMutex;

	bool				m_bLocked;
};


inline CLkMutex::CLkMutex(DWORD dwSpinCount)
{
	::InitializeCriticalSectionAndSpinCount( &m_mutex, dwSpinCount );
}

inline CLkMutex::~CLkMutex()
{
	::DeleteCriticalSection( &m_mutex );
}

inline void CLkMutex::Lock()
{
	::EnterCriticalSection( &m_mutex );
}

inline void CLkMutex::Unlock()
{
	::LeaveCriticalSection( &m_mutex );
}

inline CRITICAL_SECTION * CLkMutex::GetRealMutex()
{
	return &m_mutex;
}


inline CLkAutoMutex::CLkAutoMutex(CLkMutex *pMutex)
:m_pMutex( pMutex ), m_bLocked( false )
{
}

inline CLkAutoMutex::~CLkAutoMutex()
{
	Unlock();
}

inline void CLkAutoMutex::Lock()
{
	if( !m_bLocked )
	{
		m_pMutex->Lock();
		m_bLocked = true;
	}
}

inline void CLkAutoMutex::Unlock()
{
	if( m_bLocked )
	{
		m_bLocked = false;
		m_pMutex->Unlock();
	}
}

#endif // __NTLMUTEX_H__
