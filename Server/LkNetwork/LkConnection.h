//***********************************************************************************
//
//	File		:	LkConnection.h
//
//	Begin		:	2005-12-19
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Network IO Connection Class
//
//***********************************************************************************

#pragma once


#include "LkBitFlag.h"
#include "LkIOCP.h"
#include "LkSocket.h"
#include "LkNetBuffer.h"
#include "LkPacketEncoder.h"

#include "LkMutex.h"
#include "NetworkCompileOption.h"// [5/26/2008 SGpro]

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
const DWORD CONNECTION_KEEP_ALIVE_CHECK_TIME	= 60 * 1000; // 이 시간 이내에 Alive Ping이 들어오지 않으면 클라이언트를 강제 종료 시킴
//-----------------------------------------------------------------------------------


class CLkNetwork;
class CLkSession;
class CLkAcceptor;
class CLkConnector;

#if __SGPRO_BOTSYSTEM_SENDANDRECVTESTCODE__
#include "BotSystemLog_Server.h"
#endif

class CLkConnection
{
	friend class CIocpWorkerThread;

#if __SGPRO_BOTSYSTEM_SENDANDRECVTESTCODE__
	public:
		CBotSystemLog_Server m_cConsolLog;
		void SetCharacterPCID( __int64 nCharacterPCID ) { m_nCharacterPCID = nCharacterPCID; }

		void CreateLogFile( void )
		{
			char szCharacterPCID[1024] = { 0, };

			sprintf_s( szCharacterPCID, sizeof( szCharacterPCID ), "%d.log", m_nCharacterPCID );
			m_cConsolLog.Create( szCharacterPCID );
			return;
		}

	private:
		__int64 m_nCharacterPCID;
#endif

public:

	enum eCONTROLFLAG
	{
		CONTROL_FLAG_CHECK_ALIVE = 0x01 << 0,
		CONTROL_FLAG_CHECK_OPCODE = 0x01 << 1,
		CONTROL_FLAG_USE_SEND_QUEUE = 0x01 << 2,
		CONTROL_FLAG_USE_RECV_QUEUE = 0x01 << 3,

		MAX_CONTROL_FLAG
	};


	enum eSTATUS					
	{
		STATUS_INIT = 0,			// 초기상태
		STATUS_CREATE,				// 생성상태
		STATUS_ACCEPT,				// Accept 대기 상태
		STATUS_CONNECT,				// Connect 대기 상태 ( AcceptEx, ConnectEx 에서 사용 )
		STATUS_ACTIVE,				// 활동상태
		STATUS_CLOSE,				// 닫힌 상태
		STATUS_SHUTDOWN,			// 종료 상태
		STATUS_DESTROY,				// 소멸 상태

		MAX_STATUS
	};


public:

	CLkConnection();

	virtual ~CLkConnection();

public:

	int									Create(CLkNetwork * pNetwork);

public:


	int									CompleteIO(sIOCONTEXT * pIOContext, DWORD dwParam);

	void								Close(bool bForce = false);




public:

	virtual DWORD						GetMaxRecvPacketSize() { return PACKET_MAX_SIZE; }

	virtual DWORD						GetMaxSendPacketSize() { return PACKET_MAX_SIZE; }

	virtual DWORD						GetMaxRecvPacketCount() { return DEF_PACKET_MAX_COUNT; }

	virtual DWORD						GetMaxSendPacketCount() { return DEF_PACKET_MAX_COUNT; }

	virtual DWORD						GetAliveCheckTime() { return CONNECTION_KEEP_ALIVE_CHECK_TIME; }

	void								ResetAliveTime() { m_dwAliveTime = 0; }


public:

	int									Disconnect(bool bGraceful);

	bool								ValidCheck(DWORD dwTickDiff);

	bool								IsShutdownable();

	int									Shutdown();

public:


	int									PostRecv();

	int									PostSend();	

	int									PostAccept(CLkAcceptor* pAcceptor);

	int									PostConnect(CLkConnector* pConnector);


	int									CompleteRecv(DWORD dwTransferedBytes);

	int									CompleteSend(DWORD dwTransferedBytes);

	int									CompleteAccept(DWORD dwTransferedBytes);

	int									CompleteConnect(DWORD dwTransferedBytes);


	int									PushPacket(CLkPacket * pPacket);

	bool								PopPacket(CLkPacket * pPacket);

	virtual bool						IsValidPacket(void * pvPacketHeader, WORD wPacketLength);

	virtual BYTE						GetSequence(void * pvPacketHeader);


	void								Lock() { m_mutex.Lock(); }

	void								Unlock() { m_mutex.Unlock(); }

	CLkMutex *							GetMutex() { return &m_mutex; }


public:


	sIOCONTEXT *						GetRecvContext() { return &m_recvContext; }

	sIOCONTEXT *						GetSendContext() { return &m_sendContext; }

	CLkNetBuffer *						GetRecvBuffer() { return &m_recvBuffer; }

	CLkNetBuffer *						GetSendBuffer() { return &m_sendBuffer; }

	CLkSocket &						GetSocket() { return m_socket; }

	CLkSockAddr &						GetRemoteAddr() { return m_remoteAddr; }

	CLkSockAddr &						GetLocalAddr() { return m_localAddr; }

	void								SetAddress(SOCKADDR_IN * pLocalAddr, SOCKADDR_IN * pRemoteAddr);

	const char *						GetRemoteIP() { return m_remoteAddr.GetDottedAddr(); }

	WORD								GetRemotePort() { return m_remoteAddr.GetPort(); }

	const char *						GetLocalIP() { return m_localAddr.GetDottedAddr(); }

	WORD								GetLocalPort() { return m_localAddr.GetPort(); }

	bool								GetAddressInfo(CLkString * pString, WORD * pPort, bool bRemote /* = true */);

	CLkConnector *						GetConnector() { return m_pConnectorRef; }

	CLkAcceptor *						GetAcceptor() { return m_pAcceptorRef; }

	int									MakeSureCompletedPacket(CLkNetBuffer* pBuffer, WORD* pwPacketLength, CLkPacketEncoder * pPacketEncoder);
										
public:

	void								SetControlFlag(DWORD dwControlFlag) { BIT_FLAG_SET( m_dwControlFlag, dwControlFlag); }

	void								UnsetControlFlag(DWORD dwControlFlag) { BIT_FLAG_UNSET( m_dwControlFlag, dwControlFlag); }

	bool								IsSetControlFlag(DWORD dwControlFlag) { return BIT_FLAG_TEST( m_dwControlFlag, dwControlFlag); }

	void								SetPacketEncoder(CLkPacketEncoder * pPacketEncoder) { m_pPacketEncoder = pPacketEncoder; }

	CLkPacketEncoder *					GetPacketEncoder() { return m_pPacketEncoder; }


public:

	eSTATUS								GetStatus();

	void								SetStatus(eSTATUS status);

	bool								IsStatus(eSTATUS status);

	bool								ExchangeStatus(const eSTATUS prevStatus, bool bResult, const eSTATUS newStatus);

	const char *						GetStatusString();

	bool								IsConnected() { return m_bConnected; }


public:

	DWORD								GetConnectTime() { return m_dwConnectTime; }

	DWORD								GetBytesTotalSize() { return GetBytesRecvSize() + GetBytesSendSize(); }
	DWORD								GetBytesRecvSize() { return m_dwBytesRecvSize; }
	DWORD								GetBytesRecvSizeMax() { return m_dwBytesRecvSizeMax; }
	DWORD								GetBytesSendSize() { return m_dwBytesSendSize; }
	DWORD								GetBytesSendSizeMax() { return m_dwBytesSendSizeMax; }
	void								IncreaseBytesRecv(DWORD dwSize);
	void								IncreaseBytesSend(DWORD dwSize);

	DWORD								GetPacktTotalCount() { return GetPacketRecvCount() + GetPacketSendCount(); }
	DWORD								GetPacketRecvCount() { return m_dwPacketRecvCount; }
	DWORD								GetPacketSendCount() { return m_dwPacketSendCount; }
	void								IncreasePacketRecv();
	void								IncreasePacketSend();

	DWORD								GetRecvQueueMaxUseSize() { return m_recvQueue.GetMaxUsedSize(); }
	DWORD								GetSendQueueMaxUseSize() { return m_sendQueue.GetMaxUsedSize(); }

	virtual int							GetHeaderSize() {return PACKET_HEADSIZE;}
	virtual int							GetPacketLen(BYTE* pHeaderPointer);

	virtual void						SetSequence(void* pvPacketHeader, BYTE bySequence);

	virtual bool						HasValidSequence(void* pvPacketHeader, BYTE bySequence);


private:

	void								Init();

	void								Destroy();


private:

	void								IncreasePostIoCount() { InterlockedIncrement( (LONG*)&m_dwIoPostCount); }

	void								DecreasePostIoCount() { InterlockedDecrement( (LONG*)&m_dwIoPostCount); }


private:

	eSTATUS								m_status;

	CLkNetwork *						m_pNetworkRef;

	CLkAcceptor *						m_pAcceptorRef;

	CLkConnector *						m_pConnectorRef;

	DWORD								m_dwIoPostCount;


	CLkSocket							m_socket;

	CLkSockAddr						m_remoteAddr;

	CLkSockAddr						m_localAddr;

	sIOCONTEXT							m_recvContext;

	sIOCONTEXT							m_sendContext;

	DWORD								m_dwAliveTime;	


private:

	CLkNetBuffer 						m_recvBuffer;

	CLkNetBuffer 						m_sendBuffer;

	CLkNetQueue						m_recvQueue;

	CLkNetQueue						m_sendQueue;

	CLkPacketEncoder *					m_pPacketEncoder;


private:

	DWORD								m_dwControlFlag;

	bool								m_bSending;

	bool								m_bDisconnect;

	bool								m_bConnected;

	CLkMutex							m_mutex;

	CLkMutex							m_mutexSend;


private:

	DWORD								m_dwConnectTime;

	DWORD								m_dwBytesRecvSize;
	DWORD								m_dwBytesSendSize;
	DWORD								m_dwBytesRecvSizeMax;
	DWORD								m_dwBytesSendSizeMax;

	DWORD								m_dwPacketRecvCount;
	DWORD								m_dwPacketSendCount;

};



//-----------------------------------------------------------------------------------
// inline function
//-----------------------------------------------------------------------------------
inline void CLkConnection::IncreaseBytesRecv(DWORD dwSize)
{
	InterlockedExchangeAdd( (LONG*)&m_dwBytesRecvSize, dwSize );

	if( dwSize > m_dwBytesRecvSizeMax )
	{
		m_dwBytesRecvSizeMax = dwSize;
	}
}

inline void CLkConnection::IncreaseBytesSend(DWORD dwSize)
{
	InterlockedExchangeAdd( (LONG*)&m_dwBytesSendSize, dwSize );

	if( dwSize > m_dwBytesSendSizeMax )
	{
		m_dwBytesSendSizeMax = dwSize;
	}
}

inline void CLkConnection::IncreasePacketRecv()
{
	InterlockedIncrement( (LONG*)&m_dwPacketRecvCount );
}

inline void CLkConnection::IncreasePacketSend()
{
	InterlockedIncrement( (LONG*)&m_dwPacketSendCount );
}

inline CLkConnection::eSTATUS CLkConnection::GetStatus()
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	eSTATUS status = m_status;

	return status;
}

inline void	CLkConnection::SetStatus(eSTATUS status)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	m_status = status;
}

inline bool CLkConnection::IsStatus(eSTATUS status)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	return m_status == status;

}

inline bool CLkConnection::ExchangeStatus(const eSTATUS prevStatus, const bool bResult, const eSTATUS newStatus)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	if( bResult )
	{
		if( prevStatus != m_status )
			return false;
	}
	else
	{
		if( prevStatus == m_status )
			return false;
	}

	m_status = newStatus;

	return true;
}

//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
inline int CLkConnection::MakeSureCompletedPacket(CLkNetBuffer* pBuffer, WORD* pwPacketLength, CLkPacketEncoder * pPacketEncoder)
{
	if (NULL == pwPacketLength)
	{
		_ASSERT( 0 );
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	// 헤더가 완성되지 않은 경우
	if ( pBuffer->GetWorkRemainSize() < GetHeaderSize() )
	{
		return LK_ERR_NET_PACKET_PENDING_HEADER;
	}


	int nLinearSize = (int) ( pBuffer->InGetQueueExtraPtr() - pBuffer->GetQueueWorkPtr() );


	// 헤더가 버퍼에서 잘린 경우
	if ( nLinearSize < GetHeaderSize() )
	{
		::CopyMemory( pBuffer->InGetQueueExtraPtr(), pBuffer->InGetQueueBufferPtr(), GetHeaderSize() - nLinearSize );
	}

	if( pPacketEncoder )
	{
		int rc = pPacketEncoder->RxDecrypt( pBuffer->GetQueueWorkPtr() );
		if ( LK_SUCCESS != rc )
		{
			return rc;
		}
	}

	int wPacketLen = GetPacketLen( pBuffer->GetQueueWorkPtr() );

	int nPacketSize = GetHeaderSize() + wPacketLen;
	if( nPacketSize >= pBuffer->GetMaxPacketSize() )
	{
		_ASSERT( 0 );
		return LK_ERR_NET_PACKET_EXCEED_ALLOWED_SIZE;
	}


	// 패킷이 완성되지 않은 경우
	if ( pBuffer->GetWorkRemainSize() < nPacketSize )
	{
		return LK_ERR_NET_PACKET_PENDING_DATA;
	}


	// 패킷이 버퍼에서 잘린 경우
	if ( nLinearSize < nPacketSize )
	{
		if ( nLinearSize < GetHeaderSize() )
		{
			::CopyMemory(	pBuffer->GetQueueWorkPtr() + GetHeaderSize(),
				pBuffer->InGetQueueBufferPtr() + GetHeaderSize() - nLinearSize,
				wPacketLen );
		}
		else
		{
			::CopyMemory(	pBuffer->InGetQueueExtraPtr(),
				pBuffer->InGetQueueBufferPtr(),
				nPacketSize - nLinearSize );
		}
	}

	*pwPacketLength = (WORD) nPacketSize;


	return LK_SUCCESS;
}