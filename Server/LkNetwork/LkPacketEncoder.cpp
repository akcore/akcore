//***********************************************************************************
//
//	File		:	LkPacketEncoder.cpp
//
//	Begin		:	2009-06-22
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkPacketEncoder.h"
#include "LkPacket.h"

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkPacketEncoder::RxDecrypt(CLkPacket& rPacket)
{
	UNREFERENCED_PARAMETER( rPacket );
	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkPacketEncoder::TxEncrypt(CLkPacket& rPacket)
{
	UNREFERENCED_PARAMETER( rPacket );
	return LK_SUCCESS;
}