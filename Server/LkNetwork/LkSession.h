#pragma once


#include "LkConnection.h"


// Session delete macro
#define RELEASE_SESSION(p)			if(p) { p->Release(); p = NULL; }


class CLkSession : public CLkConnection
{
	friend class CLkSessionList;

public:

	CLkSession(SESSIONTYPE sessionType);
	virtual ~CLkSession();


public:

	virtual int						OnConnect() { return LK_SUCCESS; }

	virtual int						OnAccept() { return LK_SUCCESS; }

	virtual void					OnClose() {}

	virtual int						OnDispatch(CLkPacket * pPacket);

	//int								Send(CLkPacket * pPacket) { return CLkConnection::PushPacket( pPacket ); }


public:

	HSESSION						GetHandle() { return m_hSession; }

	SESSIONTYPE						GetSessionType() { return m_sessionType; }


public:

	void							Acquire();

	void							Release();


public:

	static bool						IsInternalConnection(char* pIp);


protected:

	void							Init();

	void							Destroy();


private:

	CLkSession&					operator=(CLkSession&);


private:

	DWORD							m_dwReferenceCount;

	HSESSION						m_hSession;

	const SESSIONTYPE				m_sessionType;


};

class CLkSessionAutoPtr
{

public:
	explicit CLkSessionAutoPtr( CLkSession * pSession ) 
		: m_pSession ( pSession ) 
	{
	}

	~CLkSessionAutoPtr( ) 
	{ 
		RELEASE_SESSION( m_pSession );
	}

private:
	CLkSession * m_pSession;

};//end of class ( CLkSessionAutoPtr )