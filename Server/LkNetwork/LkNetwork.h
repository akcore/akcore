//***********************************************************************************
//
//	File		:	LkNetwork.h
//
//	Begin		:	2005-12-13
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	네트워크 메인 클래스
//
//***********************************************************************************

#pragma once


#include "LkIOCP.h"
#include "LkSessionList.h"

#include <map>


//---------------------------------------------------------------------------------------
//	NETWORK EVENT 
//---------------------------------------------------------------------------------------
enum eNETEVENT
{
	NETEVENT_ACCEPT = 1,
	NETEVENT_CONNECT,
	NETEVENT_CLOSE,
	NETEVENT_RECV,

	INVALID_NETEVENT,
	NETEVENT_COUNT,
};
//---------------------------------------------------------------------------------------



class CLkConnection;
class CLkSession;
class CLkAcceptor;
class CLkConnector;
class CLkSessionFactory;
class CLkNetworkMonitor;
class CLkNetworkProcessor;
class CLkPacket;


class CLkNetwork
{
	friend class CLkNetworkMonitor;
	friend class CLkNetworkProcessor;

	typedef std::multimap<SESSIONTYPE, CLkAcceptor*> CLkAcceptorList;
	typedef std::multimap<SESSIONTYPE, CLkConnector*> CLkConnectorList;


public:

	CLkNetwork();

	virtual ~CLkNetwork();


public:

	int								Create(	CLkSessionFactory * pFactory,											
											int nSessionSize,
											int nCreateThreads = 0,
											int nConcurrentThreads = 0 );

	void							Destroy();


public:

	int								Associate(CLkConnection * pConnection, bool bAssociate);

	int								Associate(CLkAcceptor * pAcceptor, bool bAssociate);

	int								Associate(CLkConnector * pConnector, bool bAssociate);

	int								Send(HSESSION hSession, CLkPacket * pPacket);

	int								Send(CLkSession * pSession, CLkPacket * pPacket);

	int								PostNetEventMessage(WPARAM wParam, LPARAM lParam);



public:

	CLkSessionList *				GetSessionList() { return m_pSessionList; }

	CLkSessionFactory *			GetSessionFactory() { return m_pSessionFactoryRef; }

	CLkAcceptor *					GetAcceptor(SESSIONTYPE sessionType, const char * lpszAddr, WORD wPort);

	CLkConnector *					GetConnector(SESSIONTYPE sessionType, const char * lpszAddr, WORD wPort);


protected:

	void							Init();

	int								StartUp();

	int								Shutdown();

	int								CreateMonitorThread();

	int								CreateDispatcherThread();


private:

	CLkIocp						m_iocp;

	CLkSessionList *				m_pSessionList;

	CLkAcceptorList *				m_pAcceptorList;

	CLkConnectorList *				m_pConnectorList;

	CLkSessionFactory *			m_pSessionFactoryRef;

	CLkNetworkMonitor *			m_pNetworkMonitor;

	CLkNetworkProcessor *			m_pNetworkProcessor;

};

