//***********************************************************************************
//
//	File		:	LkPacket.cpp
//
//	Begin		:	2005-12-13
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Network Packet Class
//
//***********************************************************************************

#include "stdafx.h"
#include "LkPacket.h"

#include "LkLog.h"


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket()
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket(CLkPacket & rhs)
{
	Init();

	(*this) = rhs;
}

/*
///-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket(BYTE * pBuffer, int nSize)
{
InitUseInternalBuffer(pBuffer, nSize);
}
*/


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket(BYTE * pPacketData, WORD wPacketBodySize)
{
	Init();

	InitUseInternalBuffer( NULL, (WORD)GetHeaderSize() + wPacketBodySize );
	SetPacket( pPacketData, wPacketBodySize );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket(WORD wPacketBodySize)
{
	Init();

	InitUseInternalBuffer( NULL, (WORD)GetHeaderSize() + wPacketBodySize );
	SetPacketLen( wPacketBodySize );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::CLkPacket(BYTE * pAttachBuffer)
{
	Init();

	Attach( pAttachBuffer );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket::~CLkPacket()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::Init()
{
	m_pPacketBufferPtr		= NULL;
	m_pAllocateBuffer		= NULL;
	m_pPacketHeader			= NULL;
	m_pPacketData			= NULL;
	m_pEndPos				= NULL;
	m_pReadDataPos			= NULL;
	m_pWriteDataPos			= NULL;
	m_wBufferUsedSize		= 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::InitUseInternalBuffer(BYTE * pPacketBuffer, WORD wBufferUsedSize)
{
	Realloc( wBufferUsedSize );

	m_pPacketBufferPtr = m_pAllocateBuffer;
	if( m_pPacketBufferPtr )
	{
		if( pPacketBuffer )
		{
			memcpy( m_pPacketBufferPtr, pPacketBuffer, wBufferUsedSize );
		}

		m_pPacketHeader			= (LPPACKETHEADER) &m_pPacketBufferPtr[0];
		m_pPacketData			= (BYTE*) &m_pPacketBufferPtr[GetHeaderSize()];
		m_pEndPos				= ( wBufferUsedSize >= 1 ) ? &m_pPacketBufferPtr[wBufferUsedSize - 1] : m_pPacketBufferPtr;
		m_pReadDataPos			= m_pWriteDataPos = m_pPacketData;
		m_wBufferUsedSize		= wBufferUsedSize;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::Destroy()
{
	m_pReadDataPos			= NULL;
	m_pWriteDataPos			= NULL;
	m_pEndPos				= NULL;
	m_pPacketData			= NULL;
	m_pPacketHeader			= NULL;
	m_pPacketBufferPtr		= NULL;
	m_wBufferUsedSize		= 0;

	SAFE_FREE( m_pAllocateBuffer );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::InitUseExternalBuffer(BYTE * pPacketBuffer, WORD wBufferUsedSize)
{
	m_pPacketBufferPtr		= pPacketBuffer;

	m_pPacketHeader			= (LPPACKETHEADER) &m_pPacketBufferPtr[0];
	m_pPacketData			= (BYTE*) &m_pPacketBufferPtr[GetHeaderSize()];
	m_pEndPos				= ( wBufferUsedSize >= 1 ) ? &m_pPacketBufferPtr[wBufferUsedSize - 1] : m_pPacketBufferPtr;

	m_pReadDataPos			= m_pWriteDataPos = m_pPacketData;	
	m_wBufferUsedSize		= wBufferUsedSize;	
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::AttachData(BYTE * pPacketBuffer, WORD wBufferUsedSize)
{
	InitUseExternalBuffer( pPacketBuffer, wBufferUsedSize );
}


//-----------------------------------------------------------------------------------
//		Purpose	: 완전한 패킷 버퍼를 인자로 호출할 것
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::Attach(BYTE * pPacketBuffer)
{
	LPPACKETHEADER pPacketHeader = (LPPACKETHEADER) &pPacketBuffer[0];

	WORD wPacketTotalSize = pPacketHeader->wPacketLen + (WORD)GetHeaderSize();

	InitUseExternalBuffer( pPacketBuffer, wPacketTotalSize );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::Realloc(int nSize)
{
	SAFE_FREE( m_pAllocateBuffer );

	m_pAllocateBuffer = (BYTE*) calloc( nSize, sizeof(BYTE) );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkPacket::IsValidHeader()
{
	if( NULL == m_pPacketBufferPtr )
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return false;
	}


	if( GetUsedSize() < GetHeaderSize() )
	{
		_ASSERT( GetUsedSize() >= GetHeaderSize() );
		return false;
	}
	

	return true;	
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkPacket::IsValidPacket()
{
	if( NULL == m_pPacketBufferPtr )
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return false;
	}

	if( false == IsValidHeader() )
	{
		_ASSERT( true == IsValidHeader() );
		return false;
	}

	if( GetUsedSize() < GetPacketLen() )
	{
		_ASSERT( GetUsedSize() >= GetPacketLen() );
		return false;
	}


	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::CopyToBuffer(BYTE * pDestBuffer, int nSize)
{
	if( NULL == m_pPacketBufferPtr )
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return;
	}

	memcpy( pDestBuffer, m_pPacketBufferPtr, nSize) ;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::CopyFromBuffer(BYTE * pSrcBuffer, int nSize)
{
	InitUseInternalBuffer( pSrcBuffer, (BYTE)nSize );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::ReadData(void * pDest, int nSize)
{
	if( NULL == m_pPacketBufferPtr )
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return;
	}

	if( m_pReadDataPos + nSize > m_pEndPos || 
		m_pReadDataPos + nSize > m_pPacketData + GetPacketDataSize() )
	{
		return;
	}

	memcpy( pDest, m_pReadDataPos, nSize );
	m_pReadDataPos += nSize;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::WriteData(void * pSrc, int nSize)
{
	if( NULL == m_pPacketBufferPtr )
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return;
	}

	if( m_pWriteDataPos + nSize > m_pEndPos )
	{
		return;
	}

	memcpy( m_pWriteDataPos, pSrc, nSize );

	m_pWriteDataPos += nSize;
	m_wBufferUsedSize = m_wBufferUsedSize + (WORD)nSize;
	SetPacketLen( GetPacketLen() + (WORD)nSize );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::SetPacket(BYTE * pPacketData, WORD wPacketBodySize)
{
	if( NULL == m_pPacketBufferPtr)
	{
		_ASSERT( NULL != m_pPacketBufferPtr );
		return;
	}

	if( wPacketBodySize > GetUsedSize() - GetHeaderSize() )
	{
		return;
	}

	SetPacketLen( wPacketBodySize );
	memcpy( m_pPacketData, pPacketData, wPacketBodySize );

	m_pWriteDataPos		= m_pPacketData + wPacketBodySize;
	m_wBufferUsedSize	= wPacketBodySize + (WORD)GetHeaderSize();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkPacket::SetPacketLen(WORD wPacketLen)
{
	if( wPacketLen > PACKET_LIMIT_SIZE )
	{
		_ASSERT( wPacketLen <= PACKET_LIMIT_SIZE );
		m_pPacketHeader->wPacketLen = PACKET_LIMIT_SIZE;
		return;
	}

	m_pPacketHeader->wPacketLen = wPacketLen;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator =( CLkPacket & rhs)
{
	InitUseInternalBuffer( rhs.m_pPacketBufferPtr, rhs.GetUsedSize() );

	size_t wReadPos = rhs.m_pReadDataPos - rhs.m_pPacketData;
	size_t wWritePos = rhs.m_pWriteDataPos - rhs.m_pPacketData;

	m_pReadDataPos = m_pPacketData + wReadPos;
	m_pWriteDataPos = m_pPacketData + wWritePos;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator <<( CLkPacket& rhs )
{
	*this = rhs;
	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator >>( CLkPacket& rhs )
{
	rhs = *this;
	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator <<( LPTSTR arg )
{
	WriteData( (void *) arg, (int) strlen( arg ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator >>( LPTSTR arg )
{
	ReadData( (void*) arg, (int) strlen( ( LPTSTR ) m_pReadDataPos ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator <<( bool arg )
{
	WriteData( &arg, sizeof( bool ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator >>( bool& arg )
{
	ReadData( &arg, sizeof( bool ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator <<( int arg )
{
	WriteData( &arg, sizeof( int ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator >>( int& arg )
{
	ReadData( &arg, sizeof( int ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator <<( long arg )
{
	WriteData( &arg, sizeof( long ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket& CLkPacket::operator >>( long& arg )
{
	ReadData( &arg, sizeof( long ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket& CLkPacket::operator <<( DWORD arg )
{
	WriteData( &arg, sizeof( DWORD ) );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkPacket & CLkPacket::operator >>( DWORD& arg )
{
	ReadData( &arg, sizeof( DWORD ) );

	return *this;
}
