//***********************************************************************************
//
//	File		:	Connector.h
//
//	Begin		:	2005-12-19
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once


#include "LkIOCP.h"
#include "LkSockAddr.h"
#include "LkSocket.h"

#include "LkMutex.h"


//#define __USE_CONNECTEX__


class CLkSession;
class CLkConnection;

class CLkConnector
{
	friend class CConnectorThread;

public:

	CLkConnector();

	virtual ~CLkConnector();


public:

	int						Create(	LPCTSTR lpszConnectAddr,
									WORD wConnectPort,
									SESSIONTYPE sessionType,
									DWORD dwRetryTime = 1000,
									DWORD dwProcessTime = 1000);

	void					Destroy();


public:


	SESSIONTYPE				GetSessionType() { return m_sessionType; }

	CLkSockAddr &			GetConnectAddr() { return m_connectAddr; }

	const char *			GetConnectIP() { return m_connectAddr.GetDottedAddr(); }

	WORD					GetConnectPort() { return m_connectAddr.GetPort(); }

	DWORD					GetTotalConnectCount() { return m_dwTotalConnectCount; }



public:

	int						OnAssociated(CLkNetwork * pNetwork);

	void					OnConnected();

	void					OnDisconnected(bool bConnected);


protected:

	int						CreateThread();

	int						DoConnect();

	void					Init();


private:

	CLkNetwork *			m_pNetwork;

	CLkSockAddr			m_connectAddr;

	SESSIONTYPE				m_sessionType;

private:

	DWORD					m_dwRetryTime;

	DWORD					m_dwProcessTime;

	DWORD					m_dwTotalConnectCount;

	BOOL					m_bConnected;

private:

	CConnectorThread *		m_pThread;

	CLkMutex				m_mutex;

};


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
inline void	CLkConnector::OnConnected()
{
	InterlockedExchange( (LONG*)&m_bConnected, TRUE );
	++m_dwTotalConnectCount;
}

inline void	CLkConnector::OnDisconnected(bool bConnected)
{
	if( bConnected )
	{
		InterlockedExchange( (LONG*)&m_bConnected, FALSE );
	}
}
//-----------------------------------------------------------------------------------