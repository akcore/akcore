//***********************************************************************************
//
//	File		:	NetworkDispatcher.cpp
//
//	Begin		:	2007-01-04
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkNetworkProcessor.h"

#include "LkNetwork.h"
#include "LkSession.h"
#include "LkPacket.h"

#include "LkLog.h"
#include "LkError.h"


//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
const ULONG_PTR THREAD_CLOSE = (ULONG_PTR)(-1);	// thread terminate value
//---------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkNetworkProcessor::CLkNetworkProcessor(CLkNetwork *  pNetwork):
m_hEventIOCP( 0 )
{
	SetArg( pNetwork);
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkNetworkProcessor::~CLkNetworkProcessor()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetworkProcessor::Create()
{
	if (NULL != m_hEventIOCP)
	{
		LK_LOG_ASSERT("(NULL != m_hEventIOCP) m_hEventIOCP = %016x", m_hEventIOCP);
	}

	m_hEventIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);
	if( NULL == m_hEventIOCP )
	{
		return GetLastError();
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkNetworkProcessor::Destroy()
{
	if( NULL != m_hEventIOCP )
	{
		CloseHandle( m_hEventIOCP );
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkNetworkProcessor::Run()
{
	CLkNetwork * pNetwork = (CLkNetwork*) GetArg();
	if( NULL == pNetwork )
	{
		LK_LOG_ASSERT("(NULL == pNetwork)");
		return;	
	}


	BOOL bResult = FALSE;
	DWORD dwBytesTransferred = 0;
	ULONG_PTR netEvent = INVALID_NETEVENT;
	CLkSession * pSession = NULL;

	while( IsRunnable() )
	{	
		bResult = GetQueuedCompletionStatus(	m_hEventIOCP,
												&dwBytesTransferred,
												(ULONG_PTR*) &netEvent,
												(LPOVERLAPPED*) &pSession,
												INFINITE );


		if( THREAD_CLOSE == (ULONG_PTR) netEvent )
		{
			//LK_PRINT( PRINT_SYSTEM,"Thread Close" );
			break;
		}	

		if( FALSE == bResult )
		{
			//int rc = GetLastError();
			//LK_PRINT( PRINT_SYSTEM, "Dispatcher\tGQCS Failed : Err:%d(%s)", rc, LkGetErrorMessage(rc) );
			continue;
		}

		if( NULL == pSession )
		{
			continue;
		}

		switch( netEvent )
		{
		case NETEVENT_ACCEPT: 
			{
				int rc = pSession->OnAccept();
				if( LK_SUCCESS != rc )
				{
					pSession->Disconnect( false );
				}
				
			}
			break;

		case NETEVENT_CONNECT:
			{
				int rc = pSession->OnConnect();
				if( LK_SUCCESS != rc )
				{
					pSession->Disconnect( false );
				}
			}
			break;

		case NETEVENT_CLOSE:
			{
				static int nCount = 0;
				//LK_PRINT( 2, "Session[%X] NETEVENT_CLOSE Close Count[%u]", pSession, ++nCount );
				pSession->OnClose();
				RELEASE_SESSION( pSession );
			}
			break;

		case NETEVENT_RECV:
			{
				int rc = LK_SUCCESS;
				CLkPacket packet;
				if( pSession->PopPacket( &packet ) )
				{
					rc = pSession->OnDispatch( &packet );

					int nPacketLen = pSession->GetPacketLen( (BYTE*) (packet.GetPacketHeader()) );
					pSession->GetRecvBuffer()->IncreasePopPos( pSession->GetHeaderSize() + nPacketLen );

					if( pSession->IsSetControlFlag( CLkSession::CONTROL_FLAG_CHECK_OPCODE ) )
					{
						if( LK_SUCCESS != rc )
						{
							pSession->Disconnect( false );
							break;
						}
					}
				}
			}
			break;

		default:
			LK_LOG_ASSERT("netEvent is not valid. netEvent = %d", netEvent);
			break;

		}

	} // end of while( m_bRunning )

}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetworkProcessor::PostNetEvent(WPARAM wParam, LPARAM lParam)
{
	if (NULL == m_hEventIOCP)
	{
		LK_LOG_ASSERT("(NULL == m_hEventIOCP)");
	}

	if( 0 == PostQueuedCompletionStatus( m_hEventIOCP, 0, (ULONG_PTR)wParam, (LPOVERLAPPED)lParam ) )
	{
		return GetLastError();
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkNetworkProcessor::Close()
{
	PostQueuedCompletionStatus( m_hEventIOCP, 0, THREAD_CLOSE, NULL );

	CLkRunObject::Close();
}
