//***********************************************************************************
//
//	File		:	LkSession.cpp
//
//	Begin		:	2005-12-13
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Network Session Class
//
//***********************************************************************************

#include "stdafx.h"
#include "LkSession.h"

#include "LkPacketSYS.h"
#include "LkLog.h"
#include "LkError.h"



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSession::CLkSession(SESSIONTYPE sessionType)
:
m_sessionType( sessionType )
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSession::~CLkSession()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSession::Init()
{
	m_hSession = INVALID_HSESSION;

	m_dwReferenceCount = 1;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSession::Destroy()
{
	if( IsConnected() )
	{
		/*LK_LOG( LOG_TRAFFIC, "%u,%s,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%d,%d",
									GetSessionType(), GetRemoteIP(), GetRemotePort(),
									GetTickCount() - GetConnectTime(),
									GetBytesTotalSize(),
									GetBytesRecvSize(),
									GetBytesRecvSizeMax(),
									GetBytesSendSize(),
									GetBytesSendSizeMax(),
									GetPacktTotalCount(),
									GetPacketRecvCount(),
									GetPacketSendCount(),
									GetRecvQueueMaxUseSize(),
									GetSendQueueMaxUseSize(),
									GetRecvBuffer()->GetQueueLoopCount(),
									GetSendBuffer()->GetQueueLoopCount() );*/
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSession::Acquire()
{
	InterlockedIncrement( (LONG*)&m_dwReferenceCount );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSession::Release()
{
	if( 0 == InterlockedDecrement( (LONG*)&m_dwReferenceCount ) )
	{
		delete this;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkSession::IsInternalConnection(char* pIp)
{
#if defined(LK_INTERNAL_ADDRESS_PREFIX)
	#undef LK_INTERNAL_ADDRESS_PREFIX
#endif

#define LK_INTERNAL_ADDRESS_PREFIX		"10.0.0."
	if (NULL == pIp)
	{
		return false;
	}

	if (0 == strncmp(pIp, LK_INTERNAL_ADDRESS_PREFIX, strlen(LK_INTERNAL_ADDRESS_PREFIX)))
	{
		return true;
	}
	else
	{
		return false;
	}
#undef LK_INTERNAL_ADDRESS_PREFIX
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSession::OnDispatch(CLkPacket * pPacket)
{
	PACKETDATA * pPacketData = (PACKETDATA *) pPacket->GetPacketData();

	switch( pPacketData->wOpCode )
	{
	case SYS_ALIVE:
		{
			ResetAliveTime();
		}
		break;

	case SYS_PING:
		break;

	default:
		LK_DBGREPORT( "Session[%s:%X] Type[%u] Send Wrong Packet[%u]", typeid(this), this, m_sessionType, pPacketData->wOpCode );
		return LK_ERR_NET_PACKET_OPCODE_WRONG;
	}

	return LK_SUCCESS;
}
