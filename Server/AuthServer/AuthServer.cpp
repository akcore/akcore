//-----------------------------------------------------------------------------------
//		Auth Server by Daneos @ Ragezone 
//-----------------------------------------------------------------------------------

#include "stdafx.h"
#include "AuthServer.h"

#include "LkSfx.h"
#include "LkFile.h"

#include "LkPacketUA.h"
#include "LkPacketAU.h"
#include "ResultCode.h"

#include <iostream>
#include <map>
#include <list>

using namespace std;


//-----------------------------------------------------------------------------------
CClientSession::~CClientSession()
{
	//LK_PRINT(PRINT_APP, "CClientSession Destructor Called");
}


int CClientSession::OnAccept()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );

	return LK_SUCCESS;
}


void CClientSession::OnClose()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CAuthServer * app = (CAuthServer*) LkSfxGetApp();
}

int CClientSession::OnDispatch(CLkPacket * pPacket)
{
	CAuthServer * app = (CAuthServer*) LkSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch( pHeader->wOpCode )
	{
	case UA_LOGIN_REQ:
	{
		CClientSession::SendCharLogInReq(pPacket, app);
	}
		break;
	case UA_LOGIN_DISCONNECT_REQ:
	{
		CClientSession::SendLoginDcReq(pPacket);
	}
		break;

	default:
		return CLkSession::OnDispatch( pPacket );
	}

	return LK_SUCCESS;
}



//-----------------------------------------------------------------------------------
//		AuthServerMain
//-----------------------------------------------------------------------------------
int AuthServerMain(int argc, _TCHAR* argv[])
{
	CAuthServer app;
	CLkFileStream traceFileStream;

	// LOG FILE
	int rc = traceFileStream.Create("authlog");
	if (LK_SUCCESS != rc)
	{
		printf("log file CreateFile error %d(%s)", rc, LkGetErrorMessage(rc));
		return rc;
	}

	// CHECK INI FILE AND START PROGRAM
	LkSetPrintStream(traceFileStream.GetFilePtr());
	LkSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	rc = app.Create(argc, argv, ".\\Server.ini");
	if (LK_SUCCESS != rc)
	{
		LK_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, LkGetErrorMessage(rc));
		return rc;
	}

	// CONNECT TO MYSQL
	app.db = new MySQLConnWrapper;
	app.db->setConfig(app.GetConfigFileHost(), app.GetConfigFileUser(), app.GetConfigFilePassword(), app.GetConfigFileDatabase());
	try
	{
		app.db->connect();
		printf("Connected to database server.\n\r");
	}
	catch (exception e)
	{
		printf("couldn't connect to database server ErrID:%s\n\r", e.what());
	}
	try
	{
		app.db->switchDb(app.GetConfigFileDatabase());
	}
	catch (exception e)
	{
		printf("Couldn't switch database to %s Error:%s\n\r", app.GetConfigFileDatabase(), e.what());
	}

	app.Start();
	Sleep(500);
	std::cout << "\n\n" << std::endl;
	std::cout << "\t  ____                              ____        _ _ " << std::endl;
	std::cout << "\t |  _ \\ _ __ __ _  __ _  ___  _ __ | __ )  __ _| | |" << std::endl;
	std::cout << "\t | | | | '__/ _` |/ _` |/ _ \\| '_ \\|  _ \\ / _` | | |" << std::endl;
	std::cout << "\t | |_| | | | (_| | (_| | (_) | | | | |_) | (_| | | |" << std::endl;
	std::cout << "\t |____/|_|  \\__,_|\\__, |\\___/|_| |_|____/ \\__,_|_|_|" << std::endl;
	std::cout << "\t                  |___/                             " << std::endl;
	std::cout << "\t______           AKCore :O 2014					______\n\n" << std::endl;
	app.WaitForTerminate();
	

	return 0;
}
