/*****************************************************************************
 *
 * File			: LkFileSerializer.h
 * Author		: 
 * Copyright	: (��)NTL
 * Date			: 2007. 02. 06
 * Abstract		: 
 *****************************************************************************
 * Desc         : 
 *
 *****************************************************************************/

#pragma once

#include "LkSerializer.h"

class CLkFileSerializer : public CLkSerializer
{
public:

	CLkFileSerializer();
	CLkFileSerializer(int iBufferSize, int iGlowSize);

	CLkFileSerializer(char* pszFullPath);
	CLkFileSerializer(WCHAR* pwszFullPath);

	virtual ~CLkFileSerializer();

public:
	bool SaveFile(char* pszFullPathFileName, bool bCrypt = FALSE, char* szCryptPassword = NULL);
	bool SaveFile(WCHAR* pwszFullPathFileName, bool bCrypt = FALSE, WCHAR* szCryptPassword = NULL);

	bool LoadFile(char* pszFullPathFileName, bool bCrypt = FALSE, char* szCryptPassword = NULL);
	bool LoadFile(char* pszBuffer, int nSize, bool bCrypt = FALSE, char* szCryptPassword = NULL);

	bool LoadFile(WCHAR* pwszFullPathFileName, bool bCrypt = FALSE, WCHAR* szCryptPassword = NULL);
};