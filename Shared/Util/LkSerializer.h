/*****************************************************************************
*
* File			: LkSerializer.h
* Author		: HyungSuk, Jang
* Copyright	: (��)NTL
* Date			: 2006. 12. 09	
* Abstract		: system event definition.
*****************************************************************************
* Desc         : 
*
*****************************************************************************/

#ifndef __LK_SERIALIZER__
#define __LK_SERIALIZER__

#include <string>

class CLkSerializer
{
protected:

	char			*m_pBuffer;			/** stream buffer */
	int				m_iCurrSize;		/** current buffer size */
	int				m_iStartPointer;	/** data start pointer */
	int				m_iEndPointer;		/** data end pointer */
	int				m_iGlowSize;		/** data glow size */

protected:

	void Allocate(void);
	void Dellocate(void);	

public:

	CLkSerializer();
	CLkSerializer(int iBufferSize, int iGlowSize);
	~CLkSerializer();

	CLkSerializer& operator = (const CLkSerializer& s);

	//! operation

	void			Refresh(void);    

	CLkSerializer&	In(const void *pData, int iSize);
	CLkSerializer& Format(const char *pData /*= NULL*/, ...);
	CLkSerializer&	Out(void *pData, int uiSize);
	CLkSerializer&	Out(CLkSerializer& s, int uiSize);

	void			IncrementEndPointer(int nSize);

	CLkSerializer& operator << (char chData);
	CLkSerializer& operator << (unsigned char byData);
	CLkSerializer& operator << (short shData);
	CLkSerializer& operator << (unsigned short wData);
	CLkSerializer& operator << (int iData);
	CLkSerializer& operator << (unsigned int uiData);
	CLkSerializer& operator << (long lData);
    CLkSerializer& operator << (unsigned long ulData);
	CLkSerializer& operator << (__int64 i64Data);
	CLkSerializer& operator << (unsigned __int64 ui64Data);
	CLkSerializer& operator << (float fData);
	CLkSerializer& operator << (double dData);
	CLkSerializer& operator << (char *pData);
	CLkSerializer& operator << (const char *pData);
	CLkSerializer& operator << (std::string& str);

	CLkSerializer& operator >> (char& chData);
	CLkSerializer& operator >> (unsigned char& byData);
	CLkSerializer& operator >> (short& shData);
	CLkSerializer& operator >> (unsigned short& wData);
	CLkSerializer& operator >> (int& iData);
	CLkSerializer& operator >> (unsigned int& uiData);
	CLkSerializer& operator >> (long& lData);
    CLkSerializer& operator >> (unsigned long& ulData);
	CLkSerializer& operator >> (__int64& i64Data);
	CLkSerializer& operator >> (unsigned __int64& ui64Data);
	CLkSerializer& operator >> (float& fData);
	CLkSerializer& operator >> (double& dData);
	CLkSerializer& operator >> (std::string& str);	

	//! attribute

	bool			IsEmpty(void);

	int				GetBufferSize(void) const;
	int				GetGlowSize(void) const;
	int				GetDataSize(void);
	int				GetStartPointer(void) const;
	int				GetEndPointer(void) const;
	const char*		GetData(void) const;

    bool CheckInBuffer(int iSize);
    bool CheckOutBuffer(int iSize);
};

inline int CLkSerializer::GetBufferSize(void) const
{
	return m_iCurrSize; 
}

inline int CLkSerializer::GetGlowSize(void) const
{
	return m_iGlowSize;
}

inline int CLkSerializer::GetStartPointer(void) const
{
	return m_iStartPointer;
}

inline int CLkSerializer::GetEndPointer(void) const
{
	return m_iEndPointer;
}

inline const char* CLkSerializer::GetData(void) const 
{
	return m_pBuffer; 
}

#endif
